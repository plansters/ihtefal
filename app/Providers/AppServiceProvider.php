<?php

namespace App\Providers;

use App\models\Hall;
use App\models\Notification;
use App\models\Reservation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Reservation::whereDate('start', '<=', date('Y-m-d'))->where('status', 'approved')->update(['status' => 'completed']);
        $reservation = Reservation::whereDate('start', '<=', date('Y-m-d H:i', time() - 24 * 60 * 60))->where('status', 'pending');
        //dd($reservation->get());
        $reservation->update(['status' => 'cancelled']);
        //dd($res->get()->toArray());
        Blade::if('ifCanDo', function ($permissions) {
            if(Auth::check())
                return Auth::user()->canDo($permissions);
            return false;

        });
        view()->composer('*', function ()
        {
            $notifications = Notification::where('is_new','yes');
            if (\auth()->check() && !\auth()->user()->hasRole('administrator')) {
                $hall_ids = Hall::where('user_id', \auth()->user()->id)->pluck('id')->toArray();
                $notifications = $notifications->whereIn('hall_id', $hall_ids);
            }
            $notifications = $notifications->orderBy('created_at','desc')->orderBy('notified','asc')->take(15)->get();

            $alerts = $notifications->filter(function($alert){
                return $alert->is_new == "yes";
            });

            view()->share(['notifications' => $notifications,'alerts' => $alerts->count()]);
            //...with this variable
            //$view->with('cart', $cart );
        });
    }
}
