<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;
use Config;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */


    public function boot(ResponseFactory $factory)
    {
        $factory->macro('api', function ($data, $errorCode = null) use ($factory) {

            $language = 'ar';
            $errors = $data;
            $data = (isset($errorCode) && $errorCode == "DATA001") ? null : $data;
            // $hasContent= $data != null && !empty($data);
            // $hasContent=isset($hasContent)?$hasContent: (is_array($data)? )
            $data = is_array($data) && sizeof($data) == 0 ? null : $data;
            $data = is_object($data) && sizeof($data) == 0 ? null : $data;

            $customFormat = [
                'code' => isset($errorCode) ? ($errorCode == "DATA001" ? "DATA001" : $errorCode) : "OK",
                'message' => isset($errorCode) ? ($language == 'en' ? Config::get('enums.Errors_En.' . $errorCode) : Config::get('enums.Errors_Ar.' . $errorCode)) : ($language == 'en' ? "Operation completed successfully" : "تمت العملية بنجاح"),
                'detailed_error' => (isset($errorCode) && $errorCode == "DATA001") ? $errors : [],
                'isSuccessful' => (!isset($errorCode) || (isset($errorCode) && $errorCode != "DATA001")) && (substr($errorCode, 0, 1) !== '_'),
                'hasContent' =>  $data != null && !empty($data) && isset($data),
                'data' => $data
            ];
            return $factory->make($customFormat);
        });
    }
}
