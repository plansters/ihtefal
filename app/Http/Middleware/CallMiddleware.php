<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CallMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $language = \App\Helper::getLanguage($request);
        $language = "en";
        if (!isset($_SERVER["HTTP_API_KEY"]) || $_SERVER["HTTP_API_KEY"] != "DED537D5C461BF9F78412C453264520A" || !Auth::check())
            return response()->api(null, "_AUTH000");
        return $next($request);
    }
}
