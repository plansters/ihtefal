<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\HallCollection;
use App\Http\Resources\ReservationCollection;
use App\models\Favorite;
use App\models\Hall;
use App\models\Inquiry;
use App\models\User;
use App\models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\models\Permission;
class UserController extends Controller
{
    private function RegisterValidations()
    {
        return [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'phone' => ['required', 'unique:users,phone', 'min:10', 'max:10', 'regex:/(05)[0-9]{8}/'],
            'email' => 'nullable|email|unique:users,email',
            'address' => 'nullable|string',
            'password' => 'required|min:4|max:24|confirmed',
            'app_token' => 'nullable|string',

        ];
    }
    public function resetPassword(Request $request)
    {

        $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
        // $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];
        $arabic = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];

        $num = range(0, 9);
        if (isset($request["identity"])) {
            $request["identity"] = str_replace($arabic, $num, $request["identity"]);
        }
        $validator = Validator::make($request->all(), ['identity' => 'required|max:10|exists:users,phone']);
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())], 422);
        }

        $identity = $request['identity'];
        $user = User::where('phone', $identity)->first();
        if ($user) {
            $new_pass = randomPassword();
            $user->password = Hash::make($new_pass);
            $user->save();
            if ($user->phone) {
                $request['message'] = " كلمة السر الجديدة الخاصة بك هي (" . $new_pass . ") ";
                $request['numbers'] = "966" . substr($user->phone, 1);
                $res = SendSMS($request);
                return $res;
            }
        } else {
            return response()->json(['status' => 'fail', 'errors' => ['Users Not Found']], 422);
        }
    }
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), $this->RegisterValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())], 422);
        }
        DB::beginTransaction();
        $validated_data = $validator->validated();
        //$validated_data["password"] = Hash::make($validated_data["password"]);
        $phones = User::where('phone', $validated_data['phone'])->count();
        if ($phones != 0) {
            DB::rollBack();
            return response()->json(['status' => 'fail', 'errors' => ["Error!"]], 422);
        }
        $user = User::create($validated_data);
        $user->identity = $user->phone;
        $user->password = Hash::make($request->password);
        $user->update();
        DB::commit();
        return response()->json(['status' => 'success', 'data' => $user->toArray()]);
    }


    private function LoginValidations()
    {
        return [
            'identity' => 'required',
            'password' => 'required'
        ];
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), $this->LoginValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())], 422);
        }
        try {
            $user = User::where(['identity' => $request->identity])->first();

            if ($user && Hash::check($request->password, $user->password)) {
                if ($user->role == "partner")
                    return response()->json(['status' => 'fail', 'errors' => ["Your Account type is 'Partner', please login through the web app"]], 422);

                if ($user->status == "active")
                    return response()->json(['status' => 'success', 'data' => $user->toArray()], 200);
                return response()->json(['status' => 'fail', 'errors' => [trans('admin.account_not_active')]], 422);
            }
            return response()->json(['status' => 'fail', 'errors' => ['اسم المستخدم أو كلمة المرور غير صحيحة']], 422);
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }

    public function getReservations($id)
    {
        $reservationsToReturn = collect();
        $reservations = User::findOrFail($id)->userActiveReservations($id)->paginate(10);
        return new ReservationCollection($reservations);
    }

    public function setToken(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'token' => 'required|string'
        ]);
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())], 422);
        }

        if ($request->has('user_id') && !empty($request->user_id))
            $user = User::findOrFail($request->user_id)->update([
                'app_token' => $request->token
            ]);

        DB::table('app_visitors')->insert([
            [
                "token" => $request->token,
                "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
                "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
            ]
        ]);



        return response()->json(['status' => 'success']);
    }

    private function InqueryValidations()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'question' => 'required|string|min:3|max:100'

        ];
    }
    public function inquiry(Request $request)
    {

        $validator = Validator::make($request->all(), $this->InqueryValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())], 422);
        }
        DB::beginTransaction();
        $validated_data = $validator->validated();

        $inquery = Inquiry::create($validated_data);
        notify_admin($validated_data['user_id'], 'inquery', $inquery->id);
        DB::commit();
        return response()->json(['status' => 'success', 'data' => $inquery->toArray()]);
    }

    public function myInquiries($id)
    {
        $user = User::find($id);
        $inqueries = $user->inquiries()->with(['user', 'admin'])->get()->toArray();
        $user->inquiries()->update(['notified' => 'yes']);

        return response()->json(['status' => 'success', 'data' => $inqueries]);
    }

    public function newAnsweredInquiries($id)
    {
        $user = User::find($id);

        return response()->json(['status' => 'success', 'data' => $user->inquiries()->where('notified', 'no')->count()]);
    }

    private function UpdateProfileValidations()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'phone' => ['required', 'min:10', 'max:10', 'regex:/(05)[0-9]{8}/'],
            'address' => 'nullable|string',
            'password' => 'nullable|min:4|max:24|confirmed',

        ];
    }

    public function updateProfile(Request $request)
    {

        $validator = Validator::make($request->all(), $this->UpdateProfileValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());

            return response()->json(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())], 422);
        }
        DB::beginTransaction();
        $validated_data = $validator->validated();

        $user = User::find($validated_data['user_id']);
        $user->update($validated_data);
        if ($request->has('password') && !empty($request->password)) {
            $user->password = Hash::make($request->password);
            $user->update();
        }

        DB::commit();
        return response()->json(['status' => 'success', 'data' => $user->toArray()]);
    }

    public function getFavorite(Request $request)
    {
        $user_id = $request->user_id;
        $favorite_ids = Favorite::where('user_id', $user_id)->pluck('hall_id');
        $halls = Hall::whereIn('id', $favorite_ids)->paginate(10);

        return new HallCollection($halls);
    }



    public function user_guide()
    {
        $ug = DB::table('app_info')->where('id', 1)->first();

        return response()->json(['status' => 'success', 'data' => $ug]);
    }


}
