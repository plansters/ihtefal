<?php

namespace App\Http\Controllers\api;

use App\models\City;
use App\Http\Resources\CityCollection;
use App\Http\Resources\City as Resource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function index()
    {
        $cities = City::all();
        return new CityCollection($cities);
    }
    public function show($id)
    {
        $cities = City::find($id);
        return new Resource($cities);
    }
}
