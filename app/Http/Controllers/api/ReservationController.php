<?php

namespace App\Http\Controllers\api;

use App\models\Notification;
use App\models\Reservation;
use Illuminate\Http\Request;
use App\models\Partner;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{ 
    private function RegisterValidations()
    {  
        return [
            'user_id'   => 'required|exists:users,id',
            'coupon_id'   => 'nullable|exists:coupons,id',
            'title'     => 'required|string|min:3|max:100',
            'hall_id'   => 'required|string|exists:halls,id',
            'description' => 'nullable|string',
            'start'     => 'required|date|after_or_equal:'. date('Y-m-d'),
            // 'start'     => 'required|date|after:now',
            'end'       => 'nullable|date',
        ];
    }
 
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->RegisterValidations());

        if ($validator->fails()) {
            Log::error($validator->errors());
 
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())],422);
        }
        DB::beginTransaction(); 
        $validated_data = $validator->validated();
        $gregorian_date = $request->has('start') && $request->start ? $request->start : '';
        $validated_data['end'] = $request->has('start') && $request->start ? Carbon::parse($request['start'])->add(1,'day')->format('Y-m-d'):'';
        $chk_status=Reservation::whereDate('start', $gregorian_date)->where('hall_id', $request->hall_id)->where('status','!=','cancelled')->count()>0?true:false;       
        if($chk_status){
    return response()->json(['status' => 'FAILURE','errors' => ["Hall is Reserved in this date, please select another date."]],400);

}
        $reservation = Reservation::create($validated_data);
        $reservation->title=$reservation->user->first_name.' '.$reservation->user->last_name .'-'.$reservation->hall->name;
        $reservation->source='mobile';
        $reservation->save();


        $partners=Partner::where('status','1')->pluck('id');
        $new_pass= randomPassword();
        for ($i = 0; $i < sizeof($partners); $i++){
        DB::table('partner_coupons')->insert(
        ['user_id' => $reservation->user_id, 'name' => $new_pass, 'value' => 0, 'partner_id' => $partners[$i], 'reservation_date' => $request['start'], 'status' => 1,'reservation_id' =>$reservation->id]
            );
        }


        // start sending message to reserver
        if(isset($reservation->start)){
        $request["message"]="لديك حجز غير مؤكد لقاعه (".$reservation->hall->name.") بتاريخ (".$reservation->start.") يرجى دفع العربون خلال 24 ساعه من وقت الحجز لتفادي الإلغاء, رقم صاحب القاعة (".$reservation->hall->user->phone.")";        
        }else{
        $request["message"]="لديك حجز غير مؤكد لقاعه (".$reservation->hall->name.")  يرجى دفع العربون خلال 24 ساعه من وقت الحجز لتفادي الإلغاء, رقم صاحب القاعة (".$reservation->hall->user->phone.")";        
                 }     
        $request['numbers']="966".substr($reservation->user->phone, 1);
        $result= SendSMS($request);


        //start sending sms to hall owner
        if($reservation->user){
        $request["message"]="لقد تم حجز قاعتكم (".$reservation->hall->name.") 
        يوم (".$reservation->start.")
        اسم المحتفل :".$reservation->user->first_name.' '.$reservation->user->last_name. "
        رقم جواله : ".$reservation->user->phone."";        
        $request['numbers']="966".substr($reservation->hall->user->phone, 1);
        $result1= SendSMS($request);
         }


        notify_admin($validated_data['user_id'], 'reservation', $reservation->id, $validated_data['hall_id']);
        DB::commit();


        return response()->json([
            'status' => 'success',
            'data' => $reservation->toArray(),
            "result"=>$result,
            "result1"=>$result1
            ]);
    } 

    public function cancelReservation($id) {
        //echo $id; exit;
        $reservation = Reservation::find($id);
        if ($reservation->status == 'pending') {
            $reservation->update(['status' => 'cancelled']);
            return response()->json(['status' => 'success','message' => 'تم الغاء الحجز']);
        } else {
            return response()->json(['status' => 'error','message' => 'يمكن الغاء الحجز في حال كانت حالته غير مؤكد فقط']);
        }

    }
}
