<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\Hall as HallResource;
use App\models\Ads;
use App\models\Hall;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{
    public function index()
    {
        $ads = Ads::all()->toArray();

        foreach ($ads as $key =>$ad)
        {
            if($ad["type"] == "hall")
            {
                $ads[$key]["hall"] = new HallResource(Hall::findOrFail($ad["item_id"]));
            }
            else
            {
                $ads[$key]["hall"] = [];
            }
        }

        return $ads;
    }
}
