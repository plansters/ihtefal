<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\CategoryCollection;
use App\models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return new CategoryCollection($categories);
    }
}
