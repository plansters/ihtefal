<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\HallCollection;
use App\Http\Resources\Hall as HallResource;

use App\models\Hall;
use App\models\HallExtra;
use App\models\HallRating;
use App\models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use File;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;
use URL;
class HallController extends Controller
{


public function resizeAllImages(){
    $files = File::allFiles(public_path('uploads/hall/'));
foreach($files as $image){
    $img=pathinfo($image);
    $img=$img["extension"];
    if(($img == 'image/jpg')||($img == 'image/PNG')||($img == 'image/GIF')||($img == 'image/WebP')){

    $img = Image::make($image);
    if($img->filesize() >1000000){
        $img->resize(1920, 1080);
        $img->save(public_path('uploads/hall/'.$image->getFilename()));

    }
}
}

$files = File::allFiles(public_path('uploads/hall_album/'));
foreach($files as $image){
    $img=pathinfo($image);
    $img=$img["extension"];
    if(($img == 'image/jpg')||($img == 'image/PNG')||($img == 'image/GIF')||($img == 'image/WebP')){

    $img = Image::make($image);
    if($img->filesize() >1000000){
        $img->resize(1920, 1080);
        $img->save(public_path('uploads/hall/'.$image->getFilename()));

    }
}
}
return response()->json("Yes");

}

    public function resetPendingHalls(){
        $IDs="";
        $date=Carbon::now()->toDateTimeString();
$pending_reservation=Reservation::where('status','pending')->where('created_at', '<', Carbon::now()->subDays(1)->toDateTimeString())->get();
foreach($pending_reservation as $reservation){
    $reservation->status='cancelled';
    $reservation->save();
$IDs=$IDs.", ".$reservation->id;
}
$result=DB::table('pending_reservations_cancelled')->insert(array('IDs'=>$IDs,'date'=>$date));

// start checking copouns
$coups=DB::table('partner_coupons')->where('status',1)->get();
foreach($coups as $coup){
if(Carbon::now()->toDateTimeString() > Carbon::parse($coup->reservation_date)->addDays(1)->toDateTimeString()){
    $cou=DB::table('partner_coupons')->where('id',$coup->id)->update(['status' => 0]);
}

}
 return response()->json("Yes");
    }



    public function index(Request $request)
    {

        $per_page = $request->has('per_page') ? $request->per_page : 10;
        $orders=[];
if($request->has('orders')){
if(isset($request->orders[0]['order'])||isset($request->orders[1]['order'])||isset($request->orders[2]['order'])){
    $orders=$request->orders;
    $orders[0]['order_by']='temp_price';
}
else{
    $orders=[
        [
            'order_by' => 'temp_price',
            'order' => ""
        ],
        [
            'order_by' => 'max_capacity',
            'order' => ""
        ],
        [
            'order_by' => 'avg_rating',
            'order' => ""
        ],

    ];
}

}
        $reservation_date = $request->has('reservation_date') ? $request->reservation_date : '1970-01-01';
        $city_id = $request->has('city_id') ? $request->city_id : '';
        $date_type = $request->has('date_type') ? $request->date_type : 'gregorian';
        $q = $request->has('search') ? $request->search : '';
        if ($date_type == 'hijri') {
            $reservation_date = convertToGregorian($reservation_date);
        }
        $category_id = $request->has('category_id') ? $request->category_id : 1;

        $request->merge(['gregorian_date' => $reservation_date]);
        $excepted_halls_ids = Reservation::whereDate('start', $reservation_date)->where('status', '!=', 'cancelled')->pluck('hall_id')->toArray();
        $request->merge(['excepted_halls_ids' => $excepted_halls_ids]);


        $halls = Hall::where('status', 'published');
            if ($category_id) {
            $halls = $halls->where('category_id', $category_id);
        }
        if ($city_id) {
            $halls = $halls->where('city_id', $city_id);
        }
        if ($q) {
            $halls = $halls->where('name', 'like', "%$q%");
        }
//Set Temp Price Value
        $temp_halls=$halls;
        $temp_halls=$temp_halls->paginate($per_page);
foreach($temp_halls as $tmp){
    $tmp_total_price=$tmp->get_current_price($tmp->total_price,$reservation_date);
    if(isset($tmp_total_price)){
        $tmp->temp_price=$tmp_total_price;
        $tmp->save();
    }
    foreach($tmp->extras as $extra){
        $extra['temp_priority']=$extra->categoryExtra->priority;
        $extra->save();
    }
}
//End Set Temp Price Value
        $ordered = false;
        if ($orders) {
            foreach ($orders as $order) {
                if ($order['order']) {
                    $ordered = true;
                    $order_type = $order['order'] ? $order['order'] : 'asc';
                    $halls = $halls->orderBy($order['order_by'],$order_type);
                }

            }

            if(!$ordered){
                    $order_type = 'asc';
                    $halls = $halls->orderBy('priority',$order_type);
            }
        }
        $halls = $halls->paginate($per_page);


return new HallCollection($halls);
    }

    public function show($id)
    {

        $hall = Hall::findOrFail($id);

        return new HallResource($hall);
    }

    public function rateHall(Request $request) {
        $hall_id = $request->hall_id;
        $user_id =  $request->user_id;
        $value = $request->rate;
        $old_rating = HallRating::where(['hall_id' => $hall_id, 'user_id' => $user_id]);
        if ($old_rating->count()) {
            $old_rating->update(['rate' => $value]);
        } else {
            HallRating::create([
                'hall_id' => $hall_id,
                'user_id' => $user_id,
                'rate' => $value
            ]);
        }

        $all_ratings = HallRating::where('hall_id', $hall_id);
        $ratings_sum = $all_ratings->sum('rate');
        $ratings_count = $all_ratings->count();
        $avg_rating = number_format($ratings_sum / $ratings_count, 1, '.', '');
        Hall::find($hall_id)->update(['avg_rating' => $avg_rating]);
        return response()->json(['status' => 'success']);

    }

    public function rateNote(Request $request) {
        $hall_id = $request->hall_id;
        $user_id = $request->user_id;
        $note = $request->note;
        $old_rating = HallRating::where(['hall_id' => $hall_id, 'user_id' => $user_id]);
        if ($old_rating->count()) {
            $old_rating->update(['note' => $note]);
            return response()->json(['status' => 'success']);
        } else {
            return response()->json(['status' => 'error', 'message' => 'not_rated']);
        }
    }
}
