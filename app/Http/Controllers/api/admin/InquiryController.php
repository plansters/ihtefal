<?php

namespace App\Http\Controllers\api\admin;

use App\models\Inquiry;
use Carbon\Carbon;
use App\models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InquiryController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.inquiries.index';
        $this->create_view = 'admin.inquiries.create';
        $this->show_view = 'admin.inquiries.show';
        $this->edit_view = 'admin.inquiries.edit';
        $this->index_route = 'admin.inquiries.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Inquiry::class;
    }

    private function UpdateValidationRules()
    {
        return [
            'answer' => 'required|string|min:3|max:200',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        has_access('show_inquiries');
        Notification::where('type','inquery')->update(["is_new"=>'no','notified'=>'yes']);

        $inquiries = $this->model_instance::orderBy('id', 'desc')->get();
        return view($this->index_view, compact('inquiries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        has_access('update_inquiries');
        $inquiry = $this->model_instance::find($id);
        return view($this->edit_view, compact(['inquiry']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        has_access('update_inquiries');
        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
            $validated_data['admin_id'] = Auth::id();
            $validated_data['answer_date'] = Carbon::now();
            $validated_data['notified'] = 'no';
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->update($validated_data);

            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }

    public function destroy(Request $request, $id)
    {
        has_access('delete_inquiries');
        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }

        }

        return redirect()->route($this->index_route);
    }
}
