<?php

namespace App\Http\Controllers\api\admin;

use App\Http\Resources\HallCollection;
use App\Http\Resources\ReservationCollection;
use App\models\Favorite;
use App\models\Hall;
use App\models\Role;
use App\models\Partner;
use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\models\Permission;

class UserController extends Controller
{

    //admin login
    public function AdminLogin(Request $request)
    {
        $validator = Validator::make($request->all(), $this->LoginValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            if (
                Auth::guard('web')->attempt(['identity' => request('identity'), 'password' => request('password')])
            ) {
                $user = Auth::guard('web')->user();
                if (isset($user) && Hash::check($request->password, $user->password)) {

                    $user->Otoken = $user->createToken('Ihtefal')->accessToken;
                    if ($user->status == "active") {
                        if ($user->role == "partner") {
                            $user->partner_id=Partner::where('user_id',$user->id)->first()->id;
                        }
                        return response()->api($user->toArray(), null);
                    } else {
                        return response()->api(null, "_AUTH004");
                    }
                }
            } else {
                return response()->api(null, "_USER002");
            }
        } catch (\Throwable $exception) {
            Log::error($exception->getMessage());
            return response()->api($exception->getMessage(), "DATA001");
        }
    }
    //

    private function LoginValidations()
    {
        return [
            'identity' => 'required',
            'password' => 'required'
        ];
    }



    public function roles()
    {
        $roles = Role::all();
        return response()->api($roles, null);
    }
    private function StoreValidationRules()
    {
        return [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'phone' => 'nullable|required_if:email,|required_if:register_type,phone|min:10|max:10|sometimes|regex:/(05)[0-9]{8}/|unique:users,phone',
            'email' => 'nullable|required_if:phone,|required_if:register_type,email|email|sometimes|unique:users,email|unique:users,email',
            'address' => 'nullable|string',
            'role' => 'required|in:client,vendor,admin',
            'register_type' => 'required|in:phone,email',
            'password' => 'required|min:4|max:24|confirmed',
        ];
    }



    private function UpdateValidationRules($id = null)
    {
        return [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'phone' => 'nullable|required_if:email,|required_if:register_type,phone|min:10|max:10|regex:/(05)[0-9]{8}/|unique:users,phone,' . $id,
            'email' => 'nullable|required_if:phone,|required_if:register_type,email|email|unique:users,email,' . $id,
            'address' => 'nullable|string',
            'role' => 'required|in:client,vendor,admin',
            'register_type' => 'required|in:phone,email',
            'password' => 'nullable|min:4|max:24|confirmed',
        ];
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->StoreValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        try {
            DB::beginTransaction();
            $validated_data = $request->validate($this->StoreValidationRules());

            $phones = User::where('phone', $validated_data['phone'])->count();

            if ($phones != 0) {
                DB::rollBack();
                return response()->api(null, "_GENERAL000");
            }
            $user = User::create($validated_data);
            $user->password = bcrypt($request['password']);
            $user->identity = $user->register_type == 'phone' ? $user->phone : $user->email;
            $user->status = "active";
            $user->save();
            if ($validated_data['role'] == 'vendor') {
                $role = Role::where('slug', 'vendor')->first();
                $user->roles()->save($role);
            } elseif ($validated_data['role'] == 'client') {
                $role = Role::where('slug', 'client')->first();
                $user->roles()->save($role);
            }
            elseif ($validated_data['role'] == 'admin') {
                $role = Role::where('slug', 'administrator')->first();
                $user->roles()->save($role);
            }

            $user->update();
            if ($user->phone && $user->status == 'active') {
                $request['message'] = "مرحباً بكم في احتفال! لقد تم تفعيل حسابكم.";
                $request['numbers'] = "966" . substr($user->phone, 1);
                $res = SendSMS($request);
            }
            DB::commit();
            $user->Otoken = $user->createToken('Ihtefal')->accessToken;
            return response()->api($user->toArray(), null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }


    public function update(Request $request, $id)
    {

        if (auth::check() && auth::user()->role = "admin") {
            $user = Auth::user();
        } else {
            return response()->api(null, "_AUTH001");
        }

        $validator = Validator::make($request->all(), $this->UpdateValidationRules($id));

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        $validated_data = $request->validate($this->UpdateValidationRules($id));
        try {
            DB::beginTransaction();
            $user = User::find($id);
            $password = $validated_data['password'];
            unset($validated_data['password']);
            $user->update($validated_data);
            if ($password) {
                $user->password = bcrypt($password);
            }


            $identity = $user->register_type == 'phone' ? $user->phone : $user->email;

            $user->update([
                'identity' => $identity
            ]);


            if (request()->has('roles')) {
                foreach (request()->roles as $role_id) {
                    $role = Role::find($role_id);
                    $user->roles()->attach($role);
                }
            }

            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }


    public function destroy(Request $request, $id)
    {
        try {
            $deleted = User::findOrFail($id)->delete();
            if ($deleted) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api(null, "_GENERAL000");
        }
    }

    public function index(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->per_page : 10;
        $users = User::where('id', '!=', 1);
        $users = $users->paginate($per_page);
        return response()->api($users);
    }

    public function getPermissions(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->per_page : 10;
        $Permissions = Permission::where('id', '!=', 0);
        $Permissions = $Permissions->paginate($per_page);
        return response()->api($Permissions);
    }

    public function getRolesPermissions($id)
    {
        $role = Role::findOrFail($id);
        $role_permissions = $role->permissions()->get();
        return response()->api(compact(['role_permissions']));
    }

    private function ValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'description' => 'required|string|min:3|max:500',
        ];
    }
    public function updateRolePermissions(Request $request, $id)
    {

        $validator = Validator::make($request->all(), $this->ValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        $validated_data = $request->validate($this->ValidationRules());
        $request['permissions'] = isset($request['permissions']) ? json_decode($request['permissions']) : [];
        try {
            DB::beginTransaction();
            $updated_instance = Role::find($id);
            $updated_instance->permissions()->sync(request()->permissions);
            $updated_instance->update($validated_data);

            if ((in_array(46, request()->permissions) || in_array(47, request()->permissions) || in_array(48, request()->permissions)) && (!in_array(17, request()->permissions))) {
                $affected = DB::table('permission_role')->insert([
                    ['permission_id' => '17', 'role_id' => $id]
                ]);
            }

            if ((in_array(43, request()->permissions) || in_array(44, request()->permissions) || in_array(45, request()->permissions)) && (!in_array(16, request()->permissions))) {
                DB::table('permission_role')->insert([
                    ['permission_id' => '16', 'role_id' => $id]
                ]);
            }

            if ((in_array(49, request()->permissions) || in_array(50, request()->permissions) || in_array(51, request()->permissions)) && (!in_array(18, request()->permissions))) {
                DB::table('permission_role')->insert([
                    ['permission_id' => '18', 'role_id' => $id]
                ]);
            }
            DB::commit();
            if ($updated_instance) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex);
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }


    public function getFavoriteHalls(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->per_page : 10;
        $user_id = Auth::user()->id;
        $favorite_ids = Favorite::where('user_id', $user_id)->pluck('hall_id');
        $halls = Hall::whereIn('id', $favorite_ids)->paginate(10);


        return response()->api(new HallCollection($halls));
    }
}
