<?php

namespace App\Http\Controllers\api\admin;

use App\models\City;
use App\Http\Resources\CityCollection;
use App\Http\Resources\City as Resource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class CityController extends Controller
{
    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'location' => 'required|string',
            'zoom' => 'nullable'
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'location' => 'required|string',
            'zoom' => 'nullable'
        ];
    }
    public function index()
    {
        $cities = City::all();
        return response()->api(new CityCollection($cities), null);

    }
    public function show($id)
    {
        $cities = City::find($id);
        return response()->api(new Resource($cities), null);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->StoreValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        try {
            DB::beginTransaction();
            $validated_data = $request->validate($this->StoreValidationRules());
            City::create($validated_data);
            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->UpdateValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            DB::beginTransaction();
            $validated_data = $request->validate($this->UpdateValidationRules());

            $updated_instance = City::find($id);
            $updated_instance->update($validated_data);

            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $deleted = City::findOrFail($id)->delete();
            if ($deleted) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api(null, "_GENERAL000");
        }
}
}
