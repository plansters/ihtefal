<?php

namespace App\Http\Controllers\api\admin;

use Illuminate\Support\Facades\Validator;

use App\models\Alert;
use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class AlertController extends Controller
{


    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:200',
            'message' => 'required|string|min:3|max:200',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'title' => 'sometimes|string|min:3|max:200',
            'message' => 'sometimes|string|min:3|max:200',
        ];
    }

    public function index()
    {

        $app_alerts = Alert::all();
        return response()->api($app_alerts, null);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), $this->StoreValidationRules());
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        try {
            $validated_data = $request->validate($this->StoreValidationRules());

            $object = Alert::create($validated_data);

            return response()->api(null, null);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), $this->UpdateValidationRules());
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            $validated_data = $request->validate($this->UpdateValidationRules());

            $object = Alert::find($id);
            $updated_instance = $object->update($validated_data);


            if ($updated_instance) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }



        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }


    }


    public function destroy(Request $request, $id)
    {
        try {
            $deleted = Alert::findOrFail($id)->delete();
            if ($deleted) {
            return response()->api(null, null);
            } else {
            return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api(null, "_GENERAL000");
        }
    }


    public function send(Request $request)
    {
        $alert = Alert::findOrFail($request->alert_id);
        $send_result = $this->sendNotifications($alert->title,$alert->message);
        if($send_result)
        {
            return response()->api(null, null);
        }
        return response()->api(null, "_GENERAL000");
    }

    private function sendNotifications($title,$message)
    {
        $notification = [
            'title' => $title,
            'body' => $message,
            'priority' => 'high'
        ];
        User::sendPublicNotifications($notification);
        return response()->api(null, null);
    }
}
