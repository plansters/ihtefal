<?php

namespace App\Http\Controllers\api\admin;

use App\models\Coupon;
use App\models\Hall;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class ReservationCouponController extends Controller
{


    public function __construct()
    {
        $this->model_instance = Coupon::class;
    }

    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:100',
            'description' => 'required|string',
            'coupon_id' => 'required|string',
            'hall_id' => Auth::user()->hasRole('administrator') ? 'nullable|exists:halls,id' : 'required|exists:halls,id',
            'coupon_type' => 'required|string|in:percentage,value',
            'coupon_value' => 'required|numeric',
            'start_date' => 'nullable|date|after_or_equal:today',
            'expired_date' => 'nullable|date|after:start_date',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:100',
            'description' => 'required|string',
            'coupon_id' => 'required|string',
            'hall_id' => Auth::user()->hasRole('administrator') ? 'nullable|exists:halls,id' : 'required|exists:halls,id',
            'coupon_type' => 'required|string|in:percentage,value',
            'coupon_value' => 'required|numeric',
            'start_date' => 'nullable|date|after_or_equal:today',
            'expired_date' => 'nullable|date|after:start_date',
        ];
    }


    public function index()
    {
        if (Auth::user()->hasRole('administrator')) {
            $coupons = $this->model_instance::all();
        } else {
            $hall_ids = Hall::where('user_id', Auth::id())->pluck('id')->toArray();
            $coupons = $this->model_instance::whereIn('hall_id', $hall_ids)->get();
        }
        return response()->api($coupons, null);
    }


  
    public function store(Request $request)
    {
        has_access('create_coupons');
        $validator = Validator::make($request->all(), $this->StoreValidationRules());
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        try {
            $validated_data = $request->validate($this->StoreValidationRules());
            DB::beginTransaction();
            $this->model_instance::create($validated_data);
            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }
    }



    public function update(Request $request, $id)
    {
        has_access('update_coupons');

        $validator = Validator::make($request->all(), $this->UpdateValidationRules());
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        try {
            $validated_data = $request->validate($this->UpdateValidationRules());

            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->update($validated_data);
            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }
    }


    public function destroy(Request $request, $id)
    {
        $deleted = $this->model_instance::findOrFail($id)->delete();
        if ($deleted) {
            $log_message = trans('ads.delete_log') . '#' . $id;
            return response()->api(null, null);
        } else {
            return response()->api(null, "_GENERAL000");
        }
    }

}
