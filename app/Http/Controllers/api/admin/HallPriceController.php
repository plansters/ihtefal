<?php

namespace App\Http\Controllers\api\admin;
use Carbon\Carbon;
use App\models\HallPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class HallPriceController extends Controller
{

    private $model_instance;


    public function __construct()
    {
        $this->model_instance = HallPrice::class;
    }

    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:200',
            'price' => 'required|numeric',
            'price_type' => 'required|in:by_days,by_islamic,by_gregorian',
            'from_date' => 'nullable|required_if:price_type,by_gregorian|date|after:now',
            'to_date' => 'nullable|required_if:price_type,by_gregorian|date|after:from_date',
            'hijri_from' => 'nullable|required_if:price_type,by_islamic|date',
            'hijri_to' => 'nullable|required_if:price_type,by_islamic|date|after:hijri_from',
            'days' => 'required_if:price_type,by_days|array',
            'hall_id' => 'required|exists:halls,id'
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
        ];
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->StoreValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        $validated_data = $request->validate($this->StoreValidationRules());

        try {
            DB::beginTransaction();
            $last_priority = $this->model_instance::where('hall_id', $validated_data['hall_id'])->max('priority');
            $validated_data['priority'] = ++$last_priority;
            if ($validated_data['price_type'] == 'by_islamic') {
                $validated_data['from_date'] = convertToGregorian($validated_data['hijri_from']);
                $validated_data['to_date'] = convertToGregorian($validated_data['hijri_to']);
            }
            if ($validated_data['days']) {
                $validated_data['days'] = implode(',', $validated_data['days']);
            }
            $data=$this->model_instance::create($validated_data);

            DB::commit();
            return response()->api($data->toarray(), null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }


    }

    public function HijriToJD($m, $d, $y){
        return (int)((11 * $y + 3) / 30) + 354 * $y + 
          30 * $m - (int)(($m - 1) / 2) + $d + 1948440 - 385;
     }


    public function update(Request $request, $id)
    {
        has_access('update_halls');
        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->update($validated_data);

            DB::commit();
            // return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }

    function sortPrices(Request $request) {
        $priority = 1; 
        foreach ($request->item as $item) {
            $price = $this->model_instance::find($item);
            $price->priority = $priority;
            $price->update();
            $priority++;
        }
        return response()->api(null, null);

    }

    public function destroy(Request $request, $id)
    {
     try{
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        
        }  catch(\Exception $ex){
            return response()->api(null, "_GENERAL000");

        }
    }
  
}
