<?php

namespace App\Http\Controllers\api\admin;

use App\Http\Resources\PartnerCollection;
use App\Http\Resources\Partner as Resource;
use App\models\Partner;
use App\models\PartnerBranch;
use App\models\City;
use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\models\PartnerProduct;

class PartnerController extends Controller
{


    private function addBranchValidation()
    {
        return [
            'name' => 'required|string|min:3|max:200'
        ];
    }
    public function addBranch(Request $request)
    {
        $validator = Validator::make($request->all(), $this->addBranchValidation());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }


        $partner = Partner::where('user_id', Auth::User()->id)->first();
        if ($partner) {
            if (($partner->user_id == Auth::user()->id) || (Auth::user()->role = "admin")) {
                $PartnerBranch = new PartnerBranch;
                $PartnerBranch->name = $request['name'];
                $PartnerBranch->lat = $request['lat'];
                $PartnerBranch->lon = $request['lon'];
                $PartnerBranch->partner_id = $partner->id;
                $PartnerBranch->save();
                return response()->api($PartnerBranch->toArray(), null);
            }
        }
    }


    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'partner_word' => 'nullable|string',
            'image' => 'required',
            'password' => 'required|min:4|max:24',
            'phone' =>  ['required', 'unique:users,phone', 'min:10', 'max:10', 'regex:/(05)[0-9]{8}/'],
            'city_id' => 'required',
            'neighborhood' => 'nullable',
            'description' => 'nullable',
            'map_location' => 'nullable'
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'nullable|string|min:3|max:100',
            'partner_word' => 'nullable|string',
            'image' => 'nullable|mimes:jpg,png,jpeg,gif,svg',
        ];
    }
    private function validatedPassword()
    {
        return [
            'password' => 'nullable|min:4|max:24'
        ];
    }
    public function index()
    {
        $partners = Partner::where('status', '1')->orderBy('priority')->get();
        foreach ($partners as $partner) {
            $productsData = [];
            $city = City::find($partner['city_id']);
            if ($city)
                $partner['city'] = $city->name;
            $partner['discount_title'] = "";
            if ($partner->discount_type == "1") {
                $partner['discount_title'] = "حسم {{$partner->discount}}% على جميع الخدمات لمستخدمي احتفال";
            }

            if ($partner->discount_type == "2") {
                $partner['discount_title'] = "حسم حسب المنتج";
            }

            if ($partner->location) {
                $x1 = explode(',', $partner->location);
                $partner["lat"] = $x1[0];
                $partner["lon"] = $x1[1];
            }

            foreach ($partner->barnches as $barnch) {
                $barnch['region'] = ["lat" => $barnch->lat, "lon" => $barnch->lon];
            }
            foreach ($partner->products as $product) {
                if ($partner->discount_type == "1") { // general discount
                    $product["discount_price"] = $product["price"] - ($product["price"] * ($partner["discount"] / 100));
                } else {
                    if ($partner->discount_type == "2") { //per product discount
                        if ($product["discount"] != "0") {
                            $product["discount_price"] = $product["price"] - ($product["price"] * ($product["discount"] / 100));
                        } else {
                            $product["discount_price"] = $product["price"];
                        }
                    } else {
                        $product["discount_price"] = null;
                    }
                }
                $product["image"] = asset('/uploads/partners/' . $product["image"]);
            }
        }
        return response()->api($partners, null);
    }

    public function deleteBranch(Request $request)
    {
        $PartnerBranch = PartnerBranch::where('id', $request['branch_id'])->get();
        $PartnerBranch[0]->delete();
        return response()->api(null, null);
    }

    public function couponInvoice(Request $request)
    {
        DB::table('partner_coupons')
            ->where('id', $request['id'])
            ->update(
                [
                    'price_before' => $request['price_before'],
                    "price_after" => $request['price_after']
                ]
            );
        return response()->api(null, null);
    }

    public function changeCity(Request $request)
    {
        $partner = Partner::where('user_id', Auth::User()->id)->get();
        $partner[0]->city_id = $request["city_id"];
        $partner[0]->save();
       $city= City::find( $request["city_id"]);
        return response()->api($city->toArray(), null);
    }

    public function changeDiscType(Request $request)
    {
        $partner = Partner::where('user_id', Auth::User()->id)->first();
        $partner->discount_type = $request["new_value"];
        $message = "";
        if ($request["new_value"] == "0") {
            $message = "لا يوجد خصم";
        }

        if ($request["new_value"] == "1") {
            $message = "حسم {{$partner->discount}}% على جميع الخدمات لمستخدمي احتفال";
        }

        if ($request["new_value"] == "2") {
            $message = "حسم حسب المنتج";
        }

        $partner->save();

        return response()->api($message, null);
    }

    public function updateBranch($id,Request $request)
    {

        $validator = Validator::make($request->all(), $this->addBranchValidation());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        $partner = Partner::where('user_id', Auth::user()->id)->first();
        if (($partner->user_id == Auth::user()->id) || (Auth::user()->role = "admin")) {
            $PartnerBranch = PartnerBranch::where('id', $id)->first();
            $PartnerBranch->name = $request['name'];
            $PartnerBranch->lat = $request['lat'];
            $PartnerBranch->lon = $request['lon'];
            $PartnerBranch->save();
            return response()->api($PartnerBranch->toArray(), null);
        }
    }

    public function show($id)
    {
        $partner = Partner::findOrFail($id);
        return response()->api(new Resource($partner), null);
    }
    public function activateCoupone(Request $request)
    {
        $status = $request['status'] == 'true' ? "1" : "0";
        DB::table('partner_coupons')->where('id', $request['id'])->update(['status' => $status]);
        return response()->api(null, null);
    }

    public function activatePartner(Request $request)
    {
        $status = $request['status'] == 'true' ? "1" : "0";
        $user_status = $request['status'] == 'true' ? "active" : "inactive";
        $partner = Partner::where('id', $request['id'])->first();
        $user = User::where('id', $partner->user_id)->first();
        $user->status = $user_status;
        $user->save();
        DB::table('partners')->where('id', $request['id'])->update(['status' => $status]);
        return response()->api(null, null);
    }

    public function changePriority(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'value' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        $p_id = $request["id"];
        $new_priority = $request["value"];
        $partner = Partner::find($p_id);
        $old_partner = Partner::find($p_id)->where('priority', $new_priority)->first();

        if ($old_partner) {
            $old_partner->update(['priority' => $partner->priority]);
            $partner->update(['priority' => $new_priority]);
        } else {
            $partner->update(['priority' => $new_priority]);
        }
        return response()->api(null, null);
    }


    public function store(Request $request)
    {
        $validated_data = $request->validate($this->StoreValidationRules());
        $validator = Validator::make($request->all(), $this->StoreValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            DB::beginTransaction();
            $last_priority = Partner::max('priority');
            $validated_data['priority'] = ++$last_priority;
            $validated_data1['name'] = $validated_data['name'];
            $validated_data1['partner_word'] = $validated_data['partner_word'];
            $validated_data1['priority'] = $validated_data['priority'];
            $validated_data1['city_id'] = $request['city_id'];
            $validated_data1['neighborhood'] = $request['neighborhood'];
            $validated_data1['address'] = $request['description'];
            $validated_data1['discount'] = '0';
            $validated_data1['location'] = $validated_data['map_location'];
            $partner_user = new User;
            $partner_user->first_name = $validated_data['name'];
            $partner_user->last_name = $validated_data['name'];
            $partner_user->role = 'partner';
            $partner_user->identity = $validated_data['phone'];
            $partner_user->phone = $validated_data['phone'];
            $partner_user->register_type = "phone";
            $partner_user->password = bcrypt($validated_data['password']);
            $partner_user->status = 'inactive';
            $partner_user->save();
            $validated_data1['user_id'] = $partner_user->id;
            $partner = Partner::create($validated_data1);
            if ($request->hasFile('image')) {
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $partner->avatar_path = $image_data["media_path"];
                $partner->avatar_url = $image_data["media_url"];
                $partner->update();

            }
            $partner->update();
            DB::commit();
            if (auth::check() && Auth::user()->role == "admin") {
                $partner_user->status = 'active';
                $partner_user->save();
            }
            return response()->api($partner->toArray(), null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }

    public function update(Request $request, $id)
    {

        $validated_data = $request->validate($this->UpdateValidationRules());

        try {
            DB::beginTransaction();
            $partner = Partner::find($id);
            $user = User::where('id', $partner->user_id)->first();
            $password = $request['password'];
            if ($password) {
                $validatedPassword = $request->validate($this->validatedPassword());
                $password = bcrypt($password);
                $user->password = $password;
            }
            if ($request['name']) {
                $user->first_name = $request['name'];
            }
            $user->save();
            $partner->update($validated_data);

            if ($request->hasFile('image')) {
                hl_deleteFile($partner->avatar_path);
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $partner->avatar_path = $image_data["media_path"];
                $partner->avatar_url = $image_data["media_url"];
                $partner->update();
            }

            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }

    public function destroy(Request $request, $id)
    {
        try {
            $deleted = Partner::findOrFail($id)->delete();
            if ($deleted) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api(null, "_GENERAL000");
        }
    }


    public function main()
    {
        if ((auth::check() && Auth::user()->role == "partner") || (Auth::user()->role == "admin")) {
            $partner = Partner::where('user_id', Auth::user()->id)->first();
            if ($partner) {
                foreach ($partner->products as $product) {
                    if ($partner->discount_type == "1") { // general discount
                        $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$partner["discount"] / 100));
                    } else {
                        if ($partner->discount_type == "2") { //per product discount
                            if ($product["discount"] != "0") {
                                $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$product["discount"] / 100));
                            } else {
                                $product["discount_price"] = (int)$product["price"];
                            }
                        } else {
                            $product["discount_price"] = null;
                        }
                    }
                    $product["image"] = asset('/uploads/partners/' . $product["image"]);
                }
            }
        }
        $branches = $partner->barnches;
        return response()->api($branches, null);
    }

    public function services()
    {
        if ((auth::check() && Auth::user()->role == "partner") || (Auth::user()->role == "admin")) {
            $partner = Partner::where('user_id', Auth::user()->id)->first();
            if ($partner) {
                $services = $partner->products;
            }
        }
        return response()->api($services, null);
    }

    public function removeProduct($p)
    {
        $user = Auth::user()->id;
        $partner = Partner::where('user_id', $user)->first();
        if (($partner->user_id == $user) || (Auth::user()->role = "admin")) {
            $PartnerProduct = PartnerProduct::where('id', $p)->first();
            $x = $PartnerProduct->delete();
            return response()->api(null, null);
        }
    }
    private function storeProdValidation()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'price' => 'required|numeric',
            'image' => 'required',
        ];
    }
    public function addProd(Request $request)
    {

        $validator = Validator::make($request->all(), $this->storeProdValidation());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        $partner=Partner::where("user_id",Auth::user()->id)->first();
        $validated_data = $request->validate($this->storeProdValidation($request));
        try {
            if ($request->hasFile('image')) {
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $prodImage = str_replace("partners/", "", $image_data["media_path"]);
                $PartnerProduct = new PartnerProduct;
                $PartnerProduct->name = $validated_data["name"];
                $PartnerProduct->price = $validated_data["price"];
                $PartnerProduct->image = $prodImage;
                $PartnerProduct->discount = "0";
                $PartnerProduct->partner_id = $partner->id;
                $PartnerProduct->save();
            }
            return response()->api($PartnerProduct->toArray(), null);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");

        }
    }

    public function updatediscountPerProduct(Request $request)
    {
        $PartnerProduct = PartnerProduct::where('id', $request['id'])->first();
        $PartnerProduct->discount = $request['newValue'];
        $PartnerProduct->save();
        return response()->api($PartnerProduct->toArray(), null);
    }

    private function editProdValidation()
    {
        return [
            'name' => 'nullable|string|min:3|max:200',
            'price' => 'nullable|numeric',
            'image' => 'nullable'
                ];
    }

    public function edit(Request $request)
    {

        $value = $request["new_value"];
        $user = Auth::user()->id;
        if (($request["partner_id"] == $user) || (Auth::user()->role = "admin")) {
            $partner = Partner::where('user_id', $request["partner_id"])->first();
            $partner->discount_type = "1";
            $partner->discount = $value;
            $partner->save();
            $message = "حسم {{$partner->discount}}% على جميع الخدمات لمستخدمي احتفال";

            return response()->api($message, null);
        }
    }

    public function editExistProduct($id,Request $request)
    {
        $validator = Validator::make($request->all(), $this->editProdValidation());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        $partner=Partner::where("user_id",Auth::user()->id)->first();

        $PartnerProduct = PartnerProduct::where('id', $id)->first();
        $validated_data = $request->validate($this->editProdValidation());
        try {
            if ($request->hasFile('image')) {
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $prodImage = str_replace("partners/", "", $image_data["media_path"]);
                $PartnerProduct->image = $prodImage;
            }
            $PartnerProduct->name = $validated_data["name"];
            $PartnerProduct->price = $validated_data["price"];
            $PartnerProduct->save();

            return response()->api($PartnerProduct->toArray(), null);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(),"DATA001");
        }
    }

    public function getCoupons(Request $request)
    {
        $user=Auth::user();
        $id=$user->id;
        if($user->role == "partner"){
            $partner = Partner::where('user_id', $id)->first();
        }
        else{
            $partner = Partner::where('user_id', $user->partner_id)->first();
        }
            if ($partner) {
                $coupons = DB::table('partner_coupons')->where('partner_id', $partner->id)->orderBy('id','desc');
            }
            if($request["code_name"]){
                $coupons=$coupons->where('name','like','%'.$request["code_name"].'%');
                            }

                            $coupons=$coupons->get();
            foreach ($coupons as $coup) {
                $user_name = User::where('id', $coup->user_id)->first();
                if ($user_name) {
                    $coup->user_name = $user_name->first_name . ' ' . $user_name->last_name;
                } else {
                    DB::table('partner_coupons')->where('id', $coup->id)->delete();
                }
                $coup->reservation_date = convertToHijri($coup->reservation_date);
            }
            return response()->api($coupons,null);

    }

    public function myStore()
    {
        $user=Auth::user();
        $id=$user->id;
        $partner = Partner::where('user_id', $id)->first();
        if ($partner) {
            $branches = $partner->barnches;
            foreach ($partner->products as $product) {
                if ($partner->discount_type == "1") { // general discount
                    $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$partner["discount"] / 100));
                } else {
                    if ($partner->discount_type == "2") { //per product discount
                        if ($product["discount"] != "0") {
                            $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$product["discount"] / 100));
                        } else {
                            $product["discount_price"] = (int)$product["price"];
                        }
                    } else {
                        $product["discount_price"] = null;
                    }
                }
                $product["image"] = asset('/uploads/partners/' . $product["image"]);
            }
        }
        $partner['city'] = City::find($partner->city_id);
        $latlon  = $partner->location;

        $pieces = explode(",", $latlon);
        $partner['lat'] = $latlon?$pieces[0]:""; // lat
        $partner['lon'] = $latlon?$pieces[1]:""; // lon
        return response()->api(compact('partner', 'branches'),null);


    }

    public function getSales()
    {
        $user=Auth::user();
        $id=$user->id;
        $users = User::where('partner_id', Auth::user()->id)->where('role', 'sales')->get();
        return response()->api($users,null);


    }


    private function addSalesManValidation()
    {
        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:4|max:24',
                ];
    }


    public function addSalesMan(Request $request)
    {

        $validator = Validator::make($request->all(), $this->addSalesManValidation());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        try {
            if (Auth::check() && Auth::User()->role == "partner") {
                $user = new User;
                $user->partner_id = Auth::user()->id;
                $user->first_name = "Sales Man";
                $user->last_name = "- " . Auth::user()->id;
                $user->email = $request['email'];
                $user->identity = $request['email'];
                $user->password = Hash::make($request['password']);
                $user->register_type = "email";
                $user->role = "sales";
                $user->status = "active";
                $user->save();

                return response()->api($user->toArray(),null);
            } else {
                return response()->api(null,"_AUTH001");
            }
        } catch (\Exception $ex) {

            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(),"_GENERAL000");
        }
    }
}
