<?php

namespace App\Http\Controllers\api\admin;

use App\Http\Resources\HallCollection;
use App\Http\Resources\Hall as HallResource;
use Illuminate\Support\Facades\Validator;
use App\models\Hall;
use App\models\HallExtra;
use App\models\HallRating;
use App\models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use File;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;
use URL;

class HallController extends Controller
{
    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'city_id' => 'required|string|exists:cities,id',
            'neighborhood' => 'nullable|string|min:3|max:200',
            'description' => 'required|string',
            'address' => 'required|string',
            'max_capacity' => 'required|numeric|gt:min_chair_number',
            'price_type' => 'required|in:per_person,per_hall',
            'total_price' => 'nullable|numeric',
            'price_per_chair' => 'nullable|numeric',
            'min_chair_number' => 'nullable|numeric',
            'map_location' => 'nullable|string',
            'category_id' => 'required|exists:categories,id',
            'image' => 'required',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'id'=>'required|string|exists:halls,id',
            'name' => 'required|string|min:3|max:200',
            'city_id' => 'required|string|exists:cities,id',
            'neighborhood' => 'nullable|string|min:3|max:200',
            'description' => 'required|string',
            'address' => 'required|string',
            'max_capacity' => 'required|numeric|gt:min_chair_number',
            'price_type' => 'required|in:per_person,per_hall',
            'total_price' => 'nullable|numeric',
            'priority' => 'nullable|numeric',
            'price_per_chair' => 'nullable|numeric',
            'min_chair_number' => 'nullable|numeric',
            'map_location' => 'nullable|string',
            'category_id' => 'required|exists:categories,id',
            'image' => 'required',
        ];
    }
    public function index(Request $request)
    {
        $per_page = $request->has('per_page') ? $request->per_page : 10;
        $orders = [];
        if ($request->has('orders')) {
            if (isset($request->orders[0]['order']) || isset($request->orders[1]['order']) || isset($request->orders[2]['order'])) {
                $orders = $request->orders;
                $orders[0]['order_by'] = 'temp_price';
            } else {
                $orders = [
                    [
                        'order_by' => 'temp_price',
                        'order' => ""
                    ],
                    [
                        'order_by' => 'max_capacity',
                        'order' => ""
                    ],
                    [
                        'order_by' => 'avg_rating',
                        'order' => ""
                    ],

                ];
            }
        }
        $reservation_date = $request->has('reservation_date') ? $request->reservation_date : '1970-01-01';
        $city_id = $request->has('city_id') ? $request->city_id : '';
        $date_type = $request->has('date_type') ? $request->date_type : 'gregorian';
        $q = $request->has('search') ? $request->search : '';
        if ($date_type == 'hijri') {
            $reservation_date = convertToGregorian($reservation_date);
        }
        $category_id = $request->has('category_id') ? $request->category_id : 1;

        $request->merge(['gregorian_date' => $reservation_date]);
        $excepted_halls_ids = Reservation::whereDate('start', $reservation_date)->where('status', '!=', 'cancelled')->pluck('hall_id')->toArray();
        $request->merge(['excepted_halls_ids' => $excepted_halls_ids]);

        if ((Auth::user()->role) == "admin") {
            $halls = Hall::where('status', 'published');
        } else {
            $halls = Hall::where('status', 'published')->where('user_id', Auth::user()->id);
        }
        if ($category_id) {
            $halls = $halls->where('category_id', $category_id);
        }
        if ($city_id) {
            $halls = $halls->where('city_id', $city_id);
        }
        if ($q) {
            $halls = $halls->where('name', 'like', "%$q%");
        }

        //Set Temp Price Value
        $temp_halls = $halls;
        $temp_halls = $temp_halls->paginate($per_page);
        foreach ($temp_halls as $tmp) {
            $tmp_total_price = $tmp->get_current_price($tmp->total_price, $reservation_date);
            if (isset($tmp_total_price)) {
                $tmp->temp_price = $tmp_total_price;
                $tmp->save();
            }
            foreach ($tmp->extras as $extra) {
                $extra['temp_priority'] = $extra->categoryExtra->priority;
                $extra->save();
            }
        }

        //End Set Temp Price Value
        $ordered = false;
        if ($orders) {
            foreach ($orders as $order) {
                if ($order['order']) {
                    $ordered = true;
                    $order_type = $order['order'] ? $order['order'] : 'asc';
                    $halls = $halls->orderBy($order['order_by'], $order_type);
                }
            }

            if (!$ordered) {
                $order_type = 'asc';
                $halls = $halls->orderBy('priority', $order_type);
            }
        }

        $halls = $halls->paginate($per_page);
        return response()->api(
            [
                "total_halls" => $halls->total(),
                "lastPage" => $halls->lastPage(),
                "perPage" => $halls->perPage(),
                "currentPage" => $halls->currentPage(),
                "halls" => new HallCollection($halls)
            ],
            null
        );
    }

    public function show($id)
    {

        $hall = Hall::findOrFail($id);
        return response()->api(new HallResource($hall), null);
    }



    public function store(Request $request)
    {
        $request['extra']=is_array($request['extra'])?$request['extra']:json_decode($request['extra']);

        if (auth::check()) {
            $user = Auth::user();
        } else {
            return response()->api(null, "_AUTH001");
        }
        $validator = Validator::make($request->all(), $this->StoreValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            DB::beginTransaction();
            $validated_data = $request->validate($this->StoreValidationRules());
            $validated_data['user_id'] =  $user->id;
            $validated_data['min_capacity'] = $validated_data['min_chair_number'];
            $hall = Hall::create($validated_data);

            if ($request->hasFile('image')) {
                $img = Image::make($validated_data["image"]->getRealPath())->encode('jpg');
                if ($img->filesize() > 1000000) {
                    $img->resize(1920, 1080);
                }
                $hash = time() . md5($img->__toString());
                $target = public_path('uploads/hall/' . $hash . '.jpg');
                $img->save($target);
                $url = 'hall/' . $hash . '.jpg';
                $hall->image_path = $url;

                $hall->update();
            }
            $hall->updateExtra($request);
            DB::commit();
            return response()->api(new HallCollection(HAll::where('id',$hall->id)->get()), null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }




    public function update(Request $request, $id)
    {
        $request['id']=$id;
        if (auth::check()) {
            $user = Auth::user();
        } else {
            return response()->api(null, "_AUTH001");
        }
        $validator = Validator::make($request->all(), $this->UpdateValidationRules());

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            DB::beginTransaction();
            $validated_data = $request->validate($this->UpdateValidationRules());
            $updated_instance = Hall::find($id);
            $validated_data['min_capacity'] = $validated_data['min_chair_number'];
            $updated_instance->update($validated_data);


            if ($request->hasFile('image')) {
                hl_deleteFile($updated_instance->image_path);
                $image_data = hl_uploadFileTo($validated_data["image"], 'hall');
                $updated_instance->image_path = $image_data["media_path"];
                $updated_instance->update();
            }
            $updated_instance->updateExtra($request);
            DB::commit();
            if ($updated_instance) {
                return response()->api(new HallCollection(HAll::where('id',$updated_instance->id)->get()), null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "_GENERAL000");
        }
    }

    public function destroy(Request $request, $id)
    {
        try {

            $hall = Hall::findOrFail($id);
            if ((Auth::user()->role) == "client") {
                return response()->api(null, "_AUTH002");
            }
            if ((Auth::user()->role) != "client" && (Auth::user()->role) != "admin" && $hall->user_id != Auth::user()->id) {
                return response()->api(null, "_AUTH002");
            }
            $deleted = $hall->delete();
            if ($deleted) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api(null, "_GENERAL000");
        }
    }

    public function getPendigHalls (Request $request) {
       
            $halls = Hall::where('status', 'pending')->paginate(4);
            return response()->api($halls);
    
    }
}
