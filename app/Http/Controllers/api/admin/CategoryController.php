<?php

namespace App\Http\Controllers\api\admin;

use App\Http\Resources\CategoryCollection;
use App\models\Category;
use App\models\CategoryExtra;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
        ];
    }

    public function index()
    {
        $categories = Category::all();
        return response()->api(new CategoryCollection($categories), null);
    }

    public function getExtra($id) {
       
            $extra = CategoryExtra::where('category_id', $id)->get();
            return response()->api($extra, null);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->StoreValidationRules($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            DB::beginTransaction();
            $validated_data = $request->validate($this->StoreValidationRules($request));

            Category::create($validated_data);

            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }
    }
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $this->UpdateValidationRules($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            DB::beginTransaction();
            $validated_data = $request->validate($this->UpdateValidationRules($request));
            $updated_instance = Category::find($id);
            if(!$updated_instance)
                return response()->api(null, "_GENERAL000");

            $updated_instance->update($validated_data);
            $updated_instance->updateRelations();
            DB::commit();
            return response()->api(null, null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }
    }

    public function destroy(Request $request, $id)
    {
        try {

            $deleted = Category::findOrFail($id)->delete();
            if ($deleted) {
                return response()->api(null, null);
            } else {
                return response()->api(null, "_GENERAL000");
            }
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api(null, "_GENERAL000");
        }

    }

}
