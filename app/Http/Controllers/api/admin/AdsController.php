<?php

namespace App\Http\Controllers\api\admin;

use App\Http\Resources\Hall as HallResource;
use App\models\Ads;
use App\models\Hall;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class AdsController extends Controller
{
    private function StoreValidationRules($request)
    {
        return [
            'title' => 'required|string|min:3|max:200',
            'type' => 'required|in:ads,hall',
            'item_id' => Rule::requiredIf($request->type == "hall"),
            'image' => 'required|mimes:jpg,png,jpeg,gif,svg',
        ];
    }
    private function UpdateValidationRules($request)
    {
        return [
            'id' => 'required|exists:ads',
            'title' => 'required|string|min:3|max:200',
            'type' => 'required|in:ads,hall',
            'item_id' => Rule::requiredIf($request->type == "hall"),
            'image' => 'sometimes|mimes:jpg,png,jpeg,gif,svg',
        ];
    }
    public function index()
    {
        $ads = Ads::all()->toArray();

        foreach ($ads as $key =>$ad)
        {
            if($ad["type"] == "hall")
            {
                $ads[$key]["hall"] = new HallResource(Hall::findOrFail($ad["item_id"]));
            }
            else
            {
                $ads[$key]["hall"] = [];
            }
        }
        return response()->api($ads, null);
    }

    public function CreateNewAd(Request $request)
    {
        $validator = Validator::make($request->all(), $this->StoreValidationRules($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
        try {
            $validated_data = $request->validate($this->StoreValidationRules($request));
            $object = Ads::create($validated_data);
            if ($request->hasFile('image')) {

                $image_data = hl_uploadFileTo($validated_data["image"], 'ads');
                $object->image_url = $image_data["media_url"];
                $object->image_path = $image_data["media_path"];
                $object->update();
            }
            $log_message = trans('ads.create_log') . '#' . $object->id;
            return response()->api(null, null);
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }
    }

    public function update(Request $request) {
        $validator = Validator::make($request->all(), $this->UpdateValidationRules($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }
    try {
        $validated_data = $request->validate($this->UpdateValidationRules($request));
        $object = Ads::find($request['id']);
        $updated_instance = $object->update($validated_data);

        if ($request->hasFile('image')) {
            $image_data = hl_uploadFileTo($validated_data["image"], 'ads');
            hl_deleteFile($object->image_path);
            $object->image_url = $image_data["media_url"];
            $object->image_path = $image_data["media_path"];
            $object->update();
        }
        $log_message = trans('ads.update_log') . '#' . $object->id;

        if ($updated_instance) {
                return response()->api(null, null);
        } else {
                return response()->api(null, "_GENERAL000");
        }
    } catch (\Exception $ex) {
        Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
    }
}


public function delete($id){
        $deleted = Ads::findOrFail($id)->delete();
        if ($deleted) {
            $log_message = trans('ads.delete_log') . '#' . $id;
            return response()->api(null, null);
        } else {
            return response()->api(null, "_GENERAL000");
        }
}
}
