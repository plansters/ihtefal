<?php

namespace App\Http\Controllers\api\admin;

use App\models\Category;
use App\models\City;
use App\models\Coupon;
use App\models\Reservation;
use App\models\User;
use App\models\Partner;
use App\models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\models\Hall;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{


    public function __construct()
    {
        $this->model_instance = Reservation::class;
    }

    private function StoreValidationRules()
    {
        return [
            'user_id' => 'nullable|exists:users,id',
            'title' => 'nullable|string|min:3|max:100',
            'start' => 'nullable|date|after_or_equal:' . date('Y-m-d'),
            'coupon_id' => 'nullable|string|min:3|max:100',
            'hall_id' => 'required|string|exists:halls,id',
            'description' => 'nullable|string',
            'end'       => 'nullable|date',
        ];
    }

    private function StoreStartValidationRules()
    {
        return [
            'user_id' => 'nullable|exists:users,id',
            'title' => 'nullable|string|min:3|max:100',
            'start' => 'required|date|after_or_equal:' . date('Y-m-d'),
            'coupon_id' => 'nullable|string|min:3|max:100',
            'hall_id' => 'required|string|exists:halls,id',
            'description' => 'nullable|string',
            'end'       => 'nullable|date',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:100',
            'description' => 'nullable|string',
        ];
    }

    public function index(Request $request)
    {
        has_access('reservations');

        $cities = City::all();
        $categories = Category::all();
        Notification::where('type', 'reservation')->update(["is_new" => 'no', 'notified' => 'yes']);

        if (Auth::user()->role == "client") {
            $my_halls = Auth::user()->reservations()->pluck('hall_id')->toArray();
        } elseif (Auth::user()->role == "vendor") {
            $resrvs_ids = Reservation::pluck('hall_id')->toArray();
            $vendor_ids = Auth::user()->halls->pluck('id')->toArray();
            $my_halls = array_intersect($resrvs_ids, $vendor_ids);
        } else {
            $Reservations = Reservation::get();
            $Reservations = $Reservations->keyBy('id');

            foreach ($Reservations as $rsrv) {

                if ($rsrv->hall) {
                    if (!Auth::user()->CanDo('edit_rest_halls')) {
                        if ($rsrv->hall->category_id == 4) {
                            $Reservations->forget($rsrv->id);
                        }
                    }

                    if (!Auth::user()->CanDo('edit_palace_halls')) {
                        if ($rsrv->hall->category_id == 1) {
                            $Reservations->forget($rsrv->id);
                        }
                    }

                    if (!Auth::user()->CanDo('edit_hotels_halls')) {
                        if ($rsrv->hall->category_id == 2) {
                            $Reservations->forget($rsrv->id);
                        }
                    }
                }
            }
            $my_halls = $Reservations->pluck('hall_id')->toArray();
        }

        $gregorian_date = $request->has('from_date') && $request->from_date ? $request->from_date : '';
        $gregorian_to = $request->has('to_date') && $request->to_date ? $request->to_date : '';
        $hijri_date = $request->has('hijri_from') && $request->hijri_from ? $request->hijri_from : '';
        $hijri_to = $request->has('hijri_to') && $request->hijri_to ? $request->hijri_to : '';

        $city_id = $request->has('city_id') ? $request->city_id : '';
        $category_id = $request->has('category_id') ? $request->category_id : '';
        $hall = $request->has('hall') ? $request->hall : '';
        $user = $request->has('user') ? $request->user : '';
        $status = $request->has('status') ? $request->status : '';
        $date_type = $hijri_date ? 'hijri' : 'gregorian';
        if ($date_type == 'hijri') {
            $gregorian_date = convertToGregorian($hijri_date);
            $gregorian_to = convertToGregorian($hijri_to);
        }

        $reservations = $this->model_instance::whereIn('hall_id', $my_halls);

        if ($city_id) {

            $reservations = $reservations->whereHas('hall', function ($query) use ($city_id) {
                $query->where('city_id', $city_id);
            });
        }
        if ($category_id) {
            $reservations = $reservations->whereHas('hall', function ($query) use ($category_id) {
                $query->where('category_id', $category_id);
            });
        }
        if ($hall) {
            $reservations = $reservations->whereHas('hall', function ($query) use ($hall) {
                $query->where('name', 'LIKE', "%$hall%");
            });
        }
        if ($user) {
            $reservations = $reservations->whereHas('user', function ($query) use ($user) {
                $query->where('first_name', 'LIKE', "%$user%");
            });
        }
        if ($status) {
            $reservations = $reservations->where('status', $status);
        }
        if ($gregorian_date) {
            $reservations = $reservations->whereBetween('start', [$gregorian_date, $gregorian_to]);
        }
        $reservations = $reservations/*->orWhere('user_id', Auth::id())*/->orderby('id', 'desc')->get();

        $dataToReturn = [];
        $total_price = 0;
        foreach ($reservations as $reservation) {
            if ($reservation->hall) {
                array_push($dataToReturn, $reservation);
                $total_price += $reservation->hall->get_current_price($reservation->hall->total_price, $reservation->start) ?? 0;
            }
        }
        $total_price= number_format($total_price);
        $message="مجموع اسعار الحجوزات هو ".$total_price." ريال سعودي";
        $reservations = $dataToReturn;
        $count = sizeof($reservations);
        return response()->api(compact(["total_price","message",'reservations', 'count']), NULL);
    }

    public function store(Request $request)
    {

        if (!Auth::id()) {
            return response()->api(null, "_AUTH001");
        }

        $gregorian_date = $request->has('start') && $request->start ? $request->start : '';
        $validated_data['end'] = $request->has('start') && $request->start ? Carbon::parse($request['start'])->add(1,'day')->format('Y-m-d'):'';
        $chk_status=Reservation::whereDate('start', $gregorian_date)->where('hall_id', $request->hall_id)->where('status','!=','cancelled')->count()>0?true:false;       
        if($chk_status){
            return response()->api(null, "_DATA002");
}

        if ($request->has('start') && $request->start) {
            $validator = Validator::make($request->all(), $this->StoreStartValidationRules());
            $validated_data = $request->validate($this->StoreStartValidationRules());
            $validated_data['end'] = $request->has('start') && $request->start ? Carbon::parse($request['start'])->add(1, 'day')->format('Y-m-d') : '';
        } else {  
            $validator = Validator::make($request->all(), $this->StoreValidationRules());
            $validated_data = $request->validate($this->StoreValidationRules());
        }
        if ($validator->fails()) {
            Log::error($validator->errors());
            $data = formatValidationMessages($validator->errors()->getMessages());
            return response()->api($data, "DATA001");
        }

        $category = Hall::find($request['hall_id'])->category->id;

        try {
            DB::beginTransaction();
            $coupon_id = $validated_data['coupon_id'];
            if ($coupon_id) {
                $coupon = Coupon::where('coupon_id', $coupon_id)->whereDate('start_date', '<=', $validated_data['start'])->whereDate('expired_date', '>=', $validated_data['start']);
                $is_valid = $coupon->count();
                if ($is_valid) {
                    if ($coupon->first()->hall_id && $coupon->first()->hall_id == $validated_data['hall_id']) {
                        $validated_data['coupon_id'] = $coupon->first()->id;
                    } else {
                        return response()->api(null, "_DATA004");
                    }
                } else {
                    return response()->api(null, "_DATA004");
                }
            }
            $validated_data['user_id'] = $request->has('admin') ? null : Auth::id();

            $reserv = $this->model_instance::create($validated_data);
            $checkForTitle = $reserv->user ? false : true;
            if (!$checkForTitle) {

                $reserv->title = $reserv->user->first_name . ' ' . $reserv->user->last_name . '-' . $reserv->hall->name;
            } else {
                $reserv->title = $reserv->title . '-' . $reserv->hall->name;
            }
            $reserv->source='mobile';
            $reserv->save();

            $partners = Partner::where('status', '1')->pluck('id');
            $new_pass = randomPassword();
            for ($i = 0; $i < sizeof($partners); $i++) {
                DB::table('partner_coupons')->insert(
                    ['user_id' => Auth::user()->id, 'name' => $new_pass, 'value' => 0, 'partner_id' => $partners[$i], 'reservation_date' => $request['start'], 'status' => 1, 'reservation_id' => $reserv->id]
                );
            }



            if (isset($reserv->start)) {
                $request["message"] = "لديك حجز غير مؤكد لقاعه (" . $reserv->hall->name . ") بتاريخ (" . $reserv->start . ") يرجى دفع العربون خلال 24 ساعه من وقت الحجز لتفادي الإلغاء, رقم صاحب القاعة (" . $reserv->hall->user->phone . ")";
            } else {
                $request["message"] = "لديك حجز غير مؤكد لقاعه (" . $reserv->hall->name . ")  يرجى دفع العربون خلال 24 ساعه من وقت الحجز لتفادي الإلغاء, رقم صاحب القاعة (" . $reserv->hall->user->phone . ")";
            }

            $request['numbers'] = "966" . substr(Auth::user()->phone, 1);
            $result = SendSMS($request);


            if ($reserv->user) {
                $request["message"] = "لقد تم حجز قاعتكم (" . $reserv->hall->name . ")
       يوم (" . $reserv->start . ")
       اسم المحتفل :" . $reserv->user->first_name . ' ' . $reserv->user->last_name . "
       رقم جواله : " . $reserv->user->phone . "";
                $request['numbers'] = "966" . substr($reserv->hall->user->phone, 1);
                $result2 = SendSMS($request);
            }

            DB::commit();
            if ($request['isAdmin'] == "1") {
                $reserv->status = "approved";
                $reserv->save();
            }
            if ($request->has('redirect')) {
                return response()->api(null, "GENERAL002");

                return redirect($request->redirect)->with('success', $this->success_message);
            }

            return response()->api([$reserv,$result,$result2], null);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->api($ex->getMessage(), "DATA001");
        }
    }



    public function destroy(Request $request, $id)
    {

        $deleted = Reservation::findOrFail($id)->delete();
        if ($deleted) {
            $log_message = 'حذف حجز' . '#' . $id;
            return response()->api(null, null);
        } else {
            return response()->api(null, "_GENERAL000");
        }
    }



    public function cancelReservation($id)
    {
        $res = $this->model_instance::find($id);
        if ($res) {
            $res->update(['status' => 'cancelled']);
            return response()->api(null, null);
        } else {
            return response()->api(null, "_DATA005");
        }
    }

    public function reservationStatus(Request $request, $id)
    {
        $result = "Yes";
        $reservation = $this->model_instance::find($id);
        $reservation->update(['status' => $request->status]);

        $code = DB::table('partner_coupons')
            ->select('name')
            ->where('reservation_id', $reservation->id)
            ->first();

        $status = 'مكتمل';

        if ($request->status == 'pending') {
            $status = 'غير مؤكد';
        }
        if ($request->status == 'approved') {
            $status = 'مؤكد';
            if ($code && $reservation->user && $reservation->user->phone) {
                $request["message"] = "لقد تم تأكيد حجزك بنجاح الكود الخاص بك (" . $code->name . ") من  (" . Carbon::now()->format('Y-m-d') . ") إلى (" . $reservation['start'] . ")";
                $request['numbers'] = "966" . substr($reservation->user->phone, 1);
                $result = SendSMS($request);
            }
        }
        if ($request->status == 'cancelled') {
            $status = 'ملغى';
        }

        $user = User::find($reservation->user_id);
        if ($user) {
            $user_tokens = [User::find($reservation->user_id)->app_token];
            SendNotifications($user_tokens, "حالة طلبك رقم #$reservation->id أصبحت $status");
        }
        return response()->api($result, null);
    }
}
