<?php

namespace App\Http\Controllers\api;

use App\Http\Resources\PartnerCollection;
use App\Http\Resources\Partner as Resource;
use App\models\Partner;
use App\models\PartnerBranch;
use App\models\City;
use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{

    public function deleteBranch(Request $request){
$PartnerBranch=PartnerBranch::where('id',$request['id'])->get();
         $PartnerBranch[0]->delete();
         return response()->json(['status' => 'success','message' => 'تم الحذف بنجاح']);

    } 
    public function couponInvoice(Request $request){
        DB::table('partner_coupons')
        ->where('id',$request['id'])
        ->update(
            [
                'price_before'=>$request['price_before'],
                "price_after"=>$request['price_after']
                ]
            );
        return response()->json(['status' => 'success']);
    }

    public function changeCity(Request $request){
        $partner=Partner::where('user_id',$request["partner_id"])->get();
        $partner[0]->city_id=$request["city_id"];
        $partner[0]->save();
        return response()->json(['status' => 'success','message' => 'تم التحديث بنجاح']);
        
        }

public function changeDiscType(Request $request){
$partner=Partner::where('user_id',$request["partner_id"])->first();
$partner->discount_type=$request["new_value"];
$partner->save();
return response()->json(['status' => 'success','message' => 'تم التحديث بنجاح']);

}
 

    public function index () {
        $partners = Partner::where('status','1')->orderBy('priority')->get();
        foreach($partners as $partner){
            $productsData=[];
            $city= City::find($partner['city_id']);
            if($city)
            $partner['city']=$city->name;
            $partner['discount_title']="";
            if($partner->discount_type =="1"){
                $partner['discount_title']="حسم {{$partner->discount}}% على جميع الخدمات لمستخدمي احتفال";
            }

            if($partner->discount_type =="2"){
                $partner['discount_title']="حسم حسب المنتج";
            }

            if($partner->location){
                $x1= explode( ',', $partner->location );
                $partner["lat"]=$x1[0];
                $partner["lon"]=$x1[1];
            }
            
            foreach($partner->barnches as $barnch){
                $barnch['region']=["lat"=>$barnch->lat,"lon"=>$barnch->lon];
            }
            foreach($partner->products as $product){
                if($partner->discount_type =="1"){// general discount
                        $product["discount_price"]=$product["price"]-($product["price"]*($partner["discount"]/100));
                    }
                    else{
                        if($partner->discount_type =="2"){ //per product discount
                            if($product["discount"]!="0"){
                    $product["discount_price"]=$product["price"]-($product["price"]*($product["discount"]/100));
                                                     }
                                                     else{
                        $product["discount_price"]=$product["price"];
                                                     
                                                     }
                                                    
                                                    }
                            else{
                                $product["discount_price"]=null; 
                            }
                    }
                    $product["image"]=asset('/uploads/partners/'.$product["image"]);
                     
                }
            }
        return response()->json(['status' => 'success','data' => $partners]);
    }

    public function show ($id) { 
        $partner = Partner::findOrFail($id);
        return new Resource($partner);
    }
    public function activateCoupone(Request $request){
        $status= $request['status']=='true'?"1":"0";
        DB::table('partner_coupons')->where('id',$request['id'])->update(['status'=>$status]);
        return response()->json(['status' => 'success']);

        return $request;
    }

    public function activatePartner(Request $request){
        $status= $request['status']=='true'?"1":"0";
        $user_status= $request['status']=='true'?"active":"inactive";
        $partner=Partner::where('id',$request['id'])->first();
        $user=User::where('id',$partner->user_id)->first();
        $user->status=$user_status;
        $user->save();
        DB::table('partners')->where('id',$request['id'])->update(['status'=>$status]);
        return response()->json(['status' => 'success']);
    }

    public function changePriority(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'value' => 'required|numeric',
            ]);
            if ($validator->fails()) {
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }
$p_id=$request["id"]; 
$new_priority=$request["value"];
$partner=Partner::find($p_id);
$old_partner=Partner::find($p_id)->where('priority',$new_priority)->first();

if($old_partner){
    $old_partner->update(['priority' => $partner->priority]);
    $partner->update(['priority' => $new_priority]);
}
else{
    $partner->update(['priority' => $new_priority]);
}

return response()->json(['status' => 'success','data' => $old_partner->id ?? '']);
    }


    private function LoginValidations()
    {
        return [
            'username' => 'required',
            'password' => 'required'
        ];
    }
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), $this->LoginValidations());
        if ($validator->fails()) {
            Log::error($validator->errors());
            return response()->json(['status' => 'fail','errors' => formatValidationMessages($validator->errors()->getMessages())],200);
        }
        try
        {
            $partner = Partner::where(['username' => $request->username])->first();
            if($partner && Hash::check($request->password, $partner->password))
            {
              $x=  Auth::login($partner);
                    // return response()->json(['status' => 'success','data' => $partner->toArray()], 200);
                    return view('admin.partners.show')->with('status', 'success');

            }
            return response()->json(['status' => 'fail','errors' => [trans('admin.invalid_username_or_password')]],200);
        }
        catch (\Throwable $exception)
        {
            Log::error($exception->getMessage());
            return response()->json(['error' => $exception->getMessage()], 500);
        }
    }
}
 