<?php

namespace App\Http\Controllers\api;

use App\models\Favorite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FavoriteController extends Controller
{
    public function store (Request $request) {
        $user_id = $request->user_id;
        $hall_id = $request->hall_id;
        $already_favorite = Favorite::where('user_id', $user_id)->where('hall_id', $hall_id);
        if ($already_favorite->count()) {
            $already_favorite->delete();
            return response()->json(['success' => 'تمت ازالة القاعة من المفضلة بنجاح']);
        } else {
            Favorite::create([
                'hall_id' => $hall_id,
                'user_id' => $user_id
            ]);
            return response()->json(['success' => 'تمت اضافة القاعة للمفضلة بنجاح']);
        }
    }

    public function destroy ($id) {
        Favorite::find($id)->delete();
        return response()->json(['success' => 'تمت ازالة القاعة من المفضلة بنجاح']);
    }
}
