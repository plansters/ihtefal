<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
//use App\models\UserActivity;
use App\models\City;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Redirect;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/admin/halls';
    protected function redirectTo()
    {
        if(auth::check()&&auth::user()->role=="partner"){
            return '/admin/partners/main/'.auth::user()->id;
        }

        if(auth::check()&&auth::user()->role=="sales"){
            return '/admin/partners/coupons/'.auth::user()->partner_id;
            
        }

        return '/admin/halls';
    }
    /**
     * Create a new controller instance. 
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'identity';
    }

    public function loginForm()
    {
        if(Auth::check())
        {
            $log_message = trans('admin.login_log') ;
            //UserActivity::logActivity($log_message);
            return redirect()->route('admin.dashboard');
        }
        $cities = City::all();
        return view('auth.login',compact('cities'));
    }
} 
