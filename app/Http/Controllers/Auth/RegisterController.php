<?php

namespace App\Http\Controllers\Auth;

use App\models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/halls';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];
        $num = range(0, 9);
        if(isset($data['phone'])){
            $data['phone'] = str_replace($arabic, $num,$data['phone']);
        }
        // $data['status']='inactive';
        $data['status']='active';

        return Validator::make($data, [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'phone' => ['nullable','required_if:email,','required_if:register_type,phone','min:10','max:10','regex:/(05)[0-9]{8}/','sometimes','unique:users,phone'],
            'email' => 'nullable|required_if:phone,|required_if:register_type,email|email|unique:users,email',
            'address' => 'nullable|string',
            'status' => 'nullable|string',
            'role' => 'required|in:client,vendor,admin',
            'register_type' => 'required|in:phone,email',
            'password' => 'required|min:4|max:24|confirmed',
        ]);

    }
  
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        $user = User::create(request()->all());
        
        //dd(request()->password);
        $user->password = bcrypt(request()->password);
        $user->identity = $user->register_type == 'phone' ? $user->phone : $user->email;
        $user->status='active';
        $user->update();
        notify_admin($user->id, 'user', $user->id);


        if($user->phone && $user->status == 'active'){
            $request['message']="مرحباً بكم في احتفال! لقد تم تفعيل حسابكم.";
            $request['numbers']="966".substr($user->phone, 1);
            $res=SendSMS($request);
                    }
 
                    if ($user->role == 'vendor') {
                        $role = Role::where('slug', 'vendor')->first();
                        if (!$user->roles->contains($role->id))
                            $user->roles()->attach($role->id);
                    } elseif ($user->role == 'client') {
                        $role = Role::where('slug', 'client')->first();
                        if (!$user->roles->contains($role->id))
                            $user->roles()->attach($role->id);
                    }


        DB::commit();
        // if ($user->role == "vendor") {
        //     Session::flash('success', 'شكراً لك سيتم التواصل معك لتفعيل حسابك! ');
        //     logout($user);
        //     return '/login';
        // }
        return $user;
    }


    protected function redirectTo()
    {

        // if (auth()->user()->role == "vendor") {
        //     Session::flash('success', 'شكراً لك سيتم التواصل معك لتفعيل حسابك! ');
        //     Auth::logout();
        //     return '/login';
        // }
        // else
        //     return '/admin/halls';
        return '/admin/halls';
    }
}
 