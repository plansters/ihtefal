<?php

namespace App\Http\Controllers\admin;

use App\models\HallMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HallMediaController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.media.index';
        $this->create_view = 'admin.media.create';
        $this->show_view = 'admin.media.show';
        $this->edit_view = 'admin.media.edit';
        $this->index_route = 'admin.media.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = HallMedia::class;
    }

    public function destroy(Request $request, $id)
    {
        has_access('delete_halls');
        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }

        }

        //return redirect()->route($this->index_route);
    }
}
