<?php

namespace App\Http\Controllers\admin;

use App\models\Alert;
use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class AlertController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.alerts.index';
        $this->create_view = 'admin.alerts.create';
        $this->show_view = 'admin.alerts.show';
        $this->edit_view = 'admin.alerts.edit';
        $this->index_route = 'admin.alerts.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Alert::class;
    }

    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:200',
            'message' => 'required|string|min:3|max:200',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'title' => 'sometimes|string|min:3|max:200',
            'message' => 'sometimes|string|min:3|max:200',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $app_alerts = $this->model_instance::all();
        return view($this->index_view, compact('app_alerts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view($this->create_view);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validated_data = $request->validate($this->StoreValidationRules());

        try {

            $object = $this->model_instance::create($validated_data);

            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $alerts = $this->model_instance::findOrFail($id);

        return view($this->edit_view, compact(['alerts']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validated_data = $request->validate($this->UpdateValidationRules());

        try {
            $object = $this->model_instance::find($id);
            $updated_instance = $object->update($validated_data);


            if ($updated_instance) {
                return redirect()->route($this->index_route)->with('success', $this->update_success_message);
            } else {
                return redirect()->route($this->index_route)->with('error', $this->update_error_message);
            }



        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }

        }

        return redirect()->route($this->index_route);
    }


    public function send(Request $request)
    {

        has_access('alert_send');

        $alert = Alert::findOrFail($request->alert_id);
        $send_result = $this->sendNotifications($alert->title,$alert->message);
        if($send_result)
        {

            return response()->json(['status' => 'success']);
        }


        return response()->json(['status' => 'fail']);


    }

    private function sendNotifications($title,$message)
    {

        $notification = [
            'title' => $title,
            'body' => $message,
            'priority' => 'high'
        ];
 
        User::sendPublicNotifications($notification);

        return true;
    }
}
