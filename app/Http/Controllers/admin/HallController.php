<?php

namespace App\Http\Controllers\admin;

use App\models\Category;
use App\models\City;
use App\models\Favorite;
use App\models\Hall;
use App\models\HallExtra;
use App\models\HallRating;
use App\models\Partner;
use App\models\HallMedia;
use App\models\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use URL;
class HallController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.halls.index';
        $this->create_view = 'admin.halls.create';
        $this->show_view = 'admin.halls.show';
        $this->edit_view = 'admin.halls.edit';
        $this->index_route = 'admin.halls.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Hall::class;
    }

    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            //'color' => 'required|string|min:3|max:20',
            'city_id' => 'required|string|exists:cities,id',
            'neighborhood' => 'nullable|string|min:3|max:200',
            'description' => 'required|string',
            'address' => 'required|string',
            //'min_capacity' => 'required|numeric',
            'max_capacity' => 'required|numeric|gt:min_chair_number',
            'price_type' => 'required|in:per_person,per_hall',
            'total_price' => 'nullable|numeric',
            'min_chair_number' => 'nullable|numeric',
            'map_location' => 'nullable|string',
            'category_id' => 'required|exists:categories,id',
            'image' => 'nullable|mimes:jpg,png,jpeg,gif,svg',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            //'color' => 'required|string|min:3|max:20',
            'city_id' => 'required|string|exists:cities,id',
            'neighborhood' => 'nullable|string|min:3|max:200',
            'description' => 'required|string',
            'address' => 'required|string',
            //'min_capacity' => 'required|numeric',
            'max_capacity' => 'required|numeric|gt:min_chair_number',
            'price_type' => 'required|in:per_person,per_hall',
            'total_price' => 'nullable|numeric',
            'priority' => 'nullable|numeric',
            'price_per_chair' => 'nullable|numeric',
            'min_chair_number' => 'nullable|numeric',
            'map_location' => 'nullable|string',
            'category_id' => 'required|exists:categories,id',
            'image' => 'nullable|mimes:jpg,png,jpeg,gif,svg',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //has_access('show_halls');
        // if (Auth::user()->canDo("show_rest_halls")) {
        $cities = City::all();
        $categories = Category::all();
        $partners = Partner::where('status','1')->orderBy('priority','asc')->get();
        return view($this->index_view, compact(['cities', 'categories', 'partners']));
        // }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function listHalls()
    {

        has_access('Show_Hall_Owner_Info');
        $halls=Hall::orderby('priority')->get();
        $halls = $halls->keyBy('id');
        $dataToReturn=[];

        foreach($halls as $hall){


                        if(!Auth::user()->CanDo('edit_rest_halls'))
                        {
                            if($hall->category_id == 4)
                            {
                                $halls->forget($hall->id);
                            }
                        }

                        elseif(!Auth::user()->CanDo('edit_palace_halls'))
                        {
                            if($hall->category_id == 1)
                            {
                                $halls->forget($hall->id);
                            }
                        }

                        elseif(!Auth::user()->CanDo('edit_hotels_halls'))
                        {
                            if($hall->category_id == 2)
                            {
                                $halls->forget($hall->id);
                            }
                        }
                     else{
                        if($hall->user){
                            array_push($dataToReturn,$hall);
                        }
                     }
                       }
                       $halls = $dataToReturn;
        return view('admin.halls.list',compact('halls'));
    }

    public function pendingHalls()
    {
        //has_access('show_pending_halls');
        return view('admin.halls.pending');
    }

    public function hallsOnMap(Request $request) {

        $gregorian_date = $request->has('gregorian') && $request->gregorian ? $request->gregorian : '';
        $hijri_date = $request->has('hijri') && $request->hijri ? $request->hijri : '';

        $order_by =  $request->has('order_by') ? $request->order_by : 'created_at';
        $order =  $request->has('order') ? $request->order : 'desc';
        $city_id = $request->has('city_id') ? $request->city_id : '';
        $q = $request->has('q') ? $request->q : '';
        $date_type = $gregorian_date ? 'gregorian' : 'hijri';
        if ($date_type == 'hijri') {
            $gregorian_date = convertToGregorian($hijri_date);
        }
        $category_id = $request->has('category_id') ? $request->category_id : 1;
        $request->merge(['gregorian_date' => $gregorian_date]);

        $halls = $this->model_instance::where('status', 'published');
        if ($category_id) {
            $halls = $halls->where('category_id', $category_id);
        }
        if ($city_id) {
            $halls = $halls->where('city_id', $city_id);
        }
        if ($q) {
            $halls = $halls->where('name', 'like', "%$q%");
        }
        $halls = $halls->orderBy($order_by,$order)->get();
        $city = City::find($city_id);
    foreach($halls as $h){
        $chk_status=Reservation::whereDate('start', $gregorian_date)->where('hall_id', $h->id)->where('status','!=','cancelled')->count()>0?true:false;
        $h->tags = ['status', $chk_status];
    }
        return view('admin.halls.maps', compact(['halls', 'city']));
    }

    public function getHallReserved(Request $request) {
        $hall_id = $request->hall_id;
        $date = $request->date;
        return Reservation::whereDate('start', $date)->where('hall_id', $hall_id)->count();
    }

    public function getMyHalls () {
        $cities = City::all();
        $categories = Category::all();
        $partners = Partner::all();
        if(auth()->check()){
        $id= auth()->user()->id;
        $myHalls = $this->model_instance::where('status', 'published')->where('user_id',$id)->orderby('priority')->get();
        }
        return view('admin.halls.myHalls', compact(['myHalls','cities', 'categories', 'partners']));
    }

    public function getHallsAjax (Request $request) {
        $tab="1";

        if ($request->ajax()) {
            $gregorian_date = $request->has('gregorian') && $request->gregorian ? $request->gregorian : '';
            $hijri_date = $request->has('hijri') && $request->hijri ? $request->hijri : '';
            if ((empty(Auth::user()) || !Auth::user()->hasRole('administrator'))) {
                if (!$gregorian_date && !$hijri_date) {
                    return response()->json(['halls' => ['data' => [], 'dd' => $gregorian_date]]);
                }
            }
            $per_page = $request->has('per_page') ? $request->per_page : 70;
            $order_by =  $request->has('order_by') ? $request->order_by : 'priority';
            $order =  $request->has('order') ? $request->order : 'asc';
            // dd($order_by,$order);
            $city_id = $request->has('city_id') ? $request->city_id : '';
            $q = $request->has('q') ? $request->q : '';
            $date_type = $gregorian_date ? 'gregorian' : 'hijri';
            if ($date_type == 'hijri') {
                $gregorian_date = convertToGregorian($hijri_date);
            }
            // $category_id = $request->has('category_id') ? $request->category_id : 1;
            $category_id;
 $category_id = $request->has('category_id') ? $request->category_id : null;
            $request->merge(['gregorian_date' => $gregorian_date]);
            $excepted_halls_ids = Reservation::whereDate('start', $gregorian_date)->where('status', '!=', 'cancelled')->pluck('hall_id')->toArray();
            $halls = $this->model_instance::where('status', 'published');
            if ($city_id) {
                $halls = $halls->where('city_id', $city_id);
            }
            if ($q) {
                $halls = $halls->where('name', 'like', "%$q%");
            }
            if ($category_id && !$q && is_numeric($category_id)) {
                $halls = $halls->where('category_id', $category_id);
                $tab=$category_id;
            }

if (!is_numeric($category_id) && !$q) {
    $countCat1= Hall::where('category_id', 1)->where('city_id', $city_id)->count();
            if($countCat1 < 1){
            $countCat2= Hall::where('category_id', 2)->where('city_id', $city_id)->count();
                        if($countCat2 < 1){

                            $countCat4= Hall::where('category_id', 4)->where('city_id', $city_id)->count();
                                    if(!$countCat4 < 1){
                                        $category_id=4;
                                        $tab="4";
                                    }
                        }
                        else{
                        $category_id=2;
                        $tab="2";
                        }
            }
            else{
                $category_id=1;
            }
    $halls = $halls->where('category_id', $category_id);
}

            $halls = $halls->orderBy($order_by,$order)->paginate($per_page);
            return response()->json(['halls' => $halls, 'reserved_ids' => $excepted_halls_ids,"tab"=>$tab]);
        }
    }

    public function addFavorite ($id) {
        $this->middleware('auth');
        $user_id = Auth::id();
        $already_favorite = Favorite::where('user_id', $user_id)->where('hall_id', $id)->count();
        if ($already_favorite) {
            return redirect(url()->previous())->with('error', trans('halls.already_favorite'));
        } else {
            Favorite::create([
                'hall_id' => $id,
                'user_id' => $user_id
            ]);
            return redirect(url()->previous())->with('success', trans('halls.added_favorite'));
        }
    }

    public function removeFavorite ($id) {
        $this->middleware('auth');
        $user_id = Auth::id();
        $favorite = Favorite::where('user_id', $user_id)->where('hall_id', $id);
        if ($favorite->count()) {
            $favorite->delete();
            return redirect(url()->previous())->with('success', 'تم حذف القاعة من المفضلة');
        } else {
            return redirect(url()->previous())->with('error', 'القاعة غير موجودة في المفضلة');
        }
    }

    public function favorite () {
        $this->middleware('auth');

        return view('admin.halls.favorite');

    }

    public function getFavorite () {
        $this->middleware('auth');
        $user_id = Auth::id();
        $favorite_ids = Favorite::where('user_id', $user_id)->pluck('hall_id');
        $halls = Hall::whereIn('id', $favorite_ids)->paginate(9);

        return response()->json(['halls' => $halls]);

    }

    public function getPendigHallsAjax (Request $request) {
        if ($request->ajax()) {
            $halls = $this->model_instance::where('status', 'pending')->paginate(4);
            return response()->json($halls);
        }
    }

    public function show ($id) {

        $date = \request()->has('date') ? \request()->date : '';
        $type = \request()->has('type') ? \request()->type : '';
        $chk_status=Reservation::whereDate('start', $date)->where('hall_id', $id)->where('status','!=','cancelled')->count()>0?"Reserved":"Available";
        $gregorian_date = "";
        $hijri_date = "";
        if ($type == 'gregorian') {
            $gregorian_date = $date;
            $hijri_date = convertToHijri($gregorian_date);
        } else {
            $hijri_date = date('Y-m-d', strtotime($date));
            $gregorian_date = date('Y-m-d', strtotime(convertToGregorian($hijri_date)));
        }
        $is_favorite = Favorite::where('user_id', Auth::id())->where('hall_id', $id)->count();
        \request()->merge(['gregorian_date' => $gregorian_date]);
        $hall = $this->model_instance::all()->find($id);
        // $hijri_date=date_format(date_sub(date_create($hijri_date), date_interval_create_from_date_string('1 days')), 'Y-m-d');
        $hall->extras=$hall->extras->sortBy('category_extra_id');
        $hall->extras=$hall->extras->keyBy('category_extra_id');
        if(isset($hall->extras[1])&&$hall->extras[1]->value=='1'){
            if(isset($hall->extras[4])){
            $hall->extras->forget('2');
            isset($hall->extras[4])?$hall->extras[4]->categoryExtra->field_name="عدد المقاعد":"";
            }else{
    isset($hall->extras[2])?$hall->extras[2]->categoryExtra->field_name="عدد المقاعد":"";
            }

        }

        if(isset($hall->extras[7])&&$hall->extras[7]->value=='1'){
            if(isset($hall->extras[9])){
                $hall->extras->forget('8');
                isset($hall->extras[9])? $hall->extras[9]->categoryExtra->field_name="عدد المقاعد":"";
            }else{
                isset($hall->extras[8])?$hall->extras[8]->categoryExtra->field_name="عدد المقاعد":"";
                        }
            }



            foreach($hall->extras as $extra){
                $extra['priority']=$extra->categoryExtra->priority;
            }
           $hall->extras=$hall->extras->sortBy('priority');



        return view($this->show_view, compact(['hall','hijri_date','gregorian_date', 'is_favorite','chk_status']));
    }

    public function activeHall ($id) {

        $hall = $this->model_instance::all()->find($id);
        $hall->update(['status' => 'published']);
        if($hall->user->phone){
            $request['message']="تم إضافة قاعتكم في احتفال, نتمنى أن تحوز خدماتنا على رضاكم.";
            $request['numbers']="966".substr($hall->user->phone, 1);
            $res=SendSMS($request);
            return $res;
                    }
        return response()->json($this->success_message);
    }

    public function rateHall(Request $request) {
        has_access('rate_halls');
        if ($request->ajax()) {
            $hall_id = $request->hall_id;
            $user_id = Auth::id();
            $value = $request->value;
            $old_rating = HallRating::where(['hall_id' => $hall_id, 'user_id' => $user_id]);
            if ($old_rating->count()) {
                $old_rating->update(['rate' => $value]);
            } else {
                HallRating::create([
                    'hall_id' => $hall_id,
                    'user_id' => $user_id,
                    'rate' => $value
                ]);
            }

            $all_ratings = HallRating::where('hall_id', $hall_id);
            $ratings_sum = $all_ratings->sum('rate');
            $ratings_count = $all_ratings->count();
            $avg_rating = number_format($ratings_sum / $ratings_count, 1, '.', '');
            $this->model_instance::find($hall_id)->update(['avg_rating' => $avg_rating]);
        }
    }

    public function rateNote(Request $request) {
        has_access('rate_halls');
        if ($request->ajax()) {
            $hall_id = $request->hall_id;
            $user_id = Auth::id();
            $note = $request->note;
            $old_rating = HallRating::where(['hall_id' => $hall_id, 'user_id' => $user_id]);
            if ($old_rating->count()) {
                $old_rating->update(['note' => $note]);
            }
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        has_access('create_halls');
        $categories = Category::all();

        $cities = City::all();
        return view($this->create_view, compact(['categories', 'cities']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slugType="";
        if($request['category_id']==1){
            $slugType="create_hotels_halls";
        }
        elseif($request['category_id']==2){
            $slugType="create_palace_halls";
        }
        elseif($request['category_id']==4){
            $slugType="create_rest_halls";
        }
        else {$slugType="create_halls";}
                if (Auth::user()->canDo($slugType)) {
        has_access('create_halls');
        $validated_data = $request->validate($this->StoreValidationRules());

        $validated_data['user_id'] = Auth::id();
        $validated_data['min_capacity'] = $validated_data['min_chair_number'];

        try {
            DB::beginTransaction();
            $hall = $this->model_instance::create($validated_data);
            $image_path = "";

            if ($request->hasFile('image')) {
  $img = Image::make($validated_data["image"]->getRealPath())->encode('jpg');
//   if($img->width() >350){
//       $img->resize(350, 400);
//   }
if($img->filesize() >1000000){

    $img->resize(1920, 1080);
}
                $hash = time().md5($img->__toString());
                $target = public_path('uploads/hall/' . $hash.'.jpg');
                $img->save($target);
                $url = 'hall/' . $hash.'.jpg';
                $hall->image_path = $url;

                $hall->update();
            }
            $hall->updateExtra($request);
            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }

    }
    else{
        return redirect()->back()->with('error', 'لا يمكنك إنشاء قاعة من هذا التصنيف');
    }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $category_id = Hall::find($id)->category->id;
        $slugType="";
        if($category_id==1){
            $slugType="edit_hotels_halls";
        }
        elseif($category_id==2){
            $slugType="edit_palace_halls";
        }
        elseif($category_id==4){
            $slugType="edit_rest_halls";
        }
        else {$slugType="update_halls";}
                if (Auth::user()->canDo($slugType)) {
        has_access('update_halls');
        $categories = Category::all();
        $cities = City::all();
        if(Auth::user()->role !="admin")
        $hall = $this->model_instance::where('id', $id)->where('user_id', Auth::id());
        else{
        $hall = $this->model_instance::where('id', $id);
        }

        if (!$hall->count()) {
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
        $hall = $hall->first();

        $hall_extras = HallExtra::where('hall_id', $id)->with('categoryExtra')->orderby('category_extra_id')->get();
        // dd($hall_extras);
        foreach($hall->prices as $price){
        $price->from_date=Carbon::parse($price->from_date)
        // ->sub(1,'day')
        ->toDateTimeString();
        $price->to_date=Carbon::parse($price->to_date)
        // ->sub(1,'day')
        ->toDateTimeString();
        }
        // dd($price->from_date,$price->to_date,$price->hijri_from,$price->hijri_to);
// start
$hall->extras=$hall->extras->keyBy('category_extra_id');
if(isset($hall->extras[1])&&$hall->extras[1]->value=='1'){

    if(isset($hall->extras[4])){
    $hall->extras->forget('2');
    isset($hall->extras[4])?$hall->extras[4]->categoryExtra->field_name="عدد المقاعد":"";
    }else{
isset($hall->extras[2])?$hall->extras[2]->categoryExtra->field_name="عدد المقاعد":"";
    }

}

if(isset($hall->extras[7])&&$hall->extras[7]->value=='1'){
    if(isset($hall->extras[9])){
        $hall->extras->forget('8');
        isset($hall->extras[9])? $hall->extras[9]->categoryExtra->field_name="عدد المقاعد":"";
    }else{
        isset($hall->extras[8])?$hall->extras[8]->categoryExtra->field_name="عدد المقاعد":"";
                }
    }
// end
$hall_extras=$hall->extras;
        //  dd($hall_extras);
        return view($this->edit_view, compact(['hall', 'categories', 'cities', 'hall_extras']));
    }
    else{

        return redirect()->back()->with('error', 'لا يمكنك تعديل قاعة من هذا التصنيف');
    }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        has_access('update_halls');
        $validated_data = $request->validate($this->UpdateValidationRules());

        try {
            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $validated_data['min_capacity'] = $validated_data['min_chair_number'];
            $updated_instance->update($validated_data);

            $image_path = "";

            if ($request->hasFile('image')) {
                hl_deleteFile($updated_instance->image_path);
                $image_data = hl_uploadFileTo($validated_data["image"], 'hall');
                $updated_instance->image_path = $image_data["media_path"];
                $updated_instance->update();
            }
            $updated_instance->updateExtra($request);
            DB::commit();
            if ($updated_instance) {

                if($request['tabChecker'] && $request['tabChecker'] =='1')
                return redirect()->back()->with('success', $this->update_success_message)->with('nav-home-tab','1');
               elseif($request['tabChecker'] && $request['tabChecker'] =='2')
               return redirect()->back()->with('success', $this->update_success_message)->with('nav-profile-tab','2');
               elseif($request['tabChecker'] && $request['tabChecker'] =='3')
               return redirect()->back()->with('success', $this->update_success_message)->with('nav-images-tab','3');
               elseif($request['tabChecker'] && $request['tabChecker'] =='4')
               return redirect()->back()->with('success', $this->update_success_message)->with('nav-prices-tab','4');
               else
               return redirect()->back()->with('success', $this->update_success_message)->with('nav-home-tab','1');
            } else {
                return redirect()->back()->with('error', $this->update_error_message);
                // return redirect()->route($this->index_route)->with('error', $this->update_error_message);
            }
        }
         catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // dd("sss");

        $category_id = Hall::find($id)->category->id;
        $slugType="";
        if($category_id==1){
            $slugType="delete_hotels_halls";
        }
        elseif($category_id==2){
            $slugType="delete_palace_halls";
        }
        elseif($category_id==4){
            $slugType="delete_rest_halls";
        }
        else {$slugType="delete_halls";}
                if (Auth::user()->canDo($slugType)) {
        has_access('delete_halls');
        if ($request->ajax()) {
            if(Auth::user()->role=="admin"){
            $hall = $this->model_instance::where('id', $id);
                }
             else if(Auth::user()->role!="admin") {
            $hall = $this->model_instance::where('id', $id)->where('user_id', Auth::id());
             }
            if (!$hall->count()) {
                return response()->json(['type' => 'error', 'status' => trans('admin.fail'), 'message' => trans('admin.fail_while_delete')]);
            }
            $deleted = $hall->first()->delete();
            if ($deleted) {
                return response()->json(['type' => 'success', 'status' => trans('admin.success'), 'message' => trans('deleted_successfully')]);
            } else {
                return response()->json(['type' => 'error', 'status' => trans('admin.fail'), 'message' => trans('admin.fail_while_delete')]);
            }

        }

        return redirect()->route($this->index_route);
    }
    else{
        return response()->json(['type' => 'error', 'status' => trans('admin.fail'), 'message' => "عذراً لا يمكنك حذف قاعة من هذا التصنيف"]);
    }
    }

    public function reservations ($id) {
        $event = new \stdClass();
        $event->title = trans('calendar.new_event');
        $event->hall_id = $id;
        $event->user_id = Auth::id();
        $event->description = '';
        $event->color = $this->model_instance::find($id)->color;
        $event->id = '';



        $calendar = \Calendar::addEvents([]) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'editable' => 1,
            'selectable' => true,
            'plugins' => [ 'bootstrap', 'moment-hijri' ],
            'themeSystem' => 'bootstrap',
            'droppable' => true,
            'aspectRatio' => 1,
            'height' => 650,
            'allDay' => false,
            'display' => 'inline-block',
            'className' => 'col-md-6',
            'defaultTimedEventDuration' => '00:50:00',
            'forceEventDuration' => true,
            'header' => [
                'left' => 'prev,next today',
                'center' => 'title',
                'right' => 'month,agendaWeek,agendaDay'
            ],
            //'classNames' => 0,
            //'classNames' => 0,
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            "viewRender" => "function(view, element) { getEvents(view, element, $id) }",
            "eventDrop" => "function(info) { resizeEvent(info, $id) }",
            "drop" => "function(date, allDay) { dropEnd(date, allDay, $id); }",
            "eventResize" => "function(info) { resizeEvent(info, $id) }",
            //"select" => "function (start, end, allDay) { selectItem(start, end, allDay); }",
            "dayClick" => "function (date, jsEvent, view) { openDay(date, jsEvent, view, $id); }",
            "remove" => "function(calEvent, jsEvent, view) { console.log('removeClick id: ' + calEvent.id); }",
            "eventClick" => "function(info) {  editEvent(info, $id) }",
            'dayRender' => 'function(date, cell) { set_hijri_content(date, cell) }',

        ]);

        return view('admin.halls.reservations', compact(['calendar', 'event', 'id']));
    }

    public function search(Request $request)
    {
        $term = trim($request->q);

        if (empty($term)) {
            return response()->json([]);
        }
        $users = $this->model_instance::where(['status' => 'published'])->where('name', 'LIKE', '%' . $term . '%')->orderby('priority');
        if (!Auth::user()->hasRole('administrator')) {
            $users = $users->where('user_id', Auth::id());
        }
        $users = $users->limit(5)->get(['id', 'name']);
        $map = $users->map(function($results){
            $data["id"] = $results->id;
            $data["text"] = $results->name;
            return $data;
        });

        return response()->json($map->toArray());

    }
}
