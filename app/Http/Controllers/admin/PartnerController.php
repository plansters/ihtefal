<?php

namespace App\Http\Controllers\admin;

use App\models\Partner;
use Carbon\Carbon;
use App\models\PartnerBranch;
use App\models\PartnerProduct;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\models\User;
use App\models\City;
use Illuminate\Support\Facades\Auth;
use Redirect;

class PartnerController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.partners.index';
        $this->create_view = 'admin.partners.create';
        $this->show_view = 'admin.partners.show';
        $this->edit_view = 'admin.partners.edit';
        $this->index_route = 'admin.partners.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Partner::class;
    }

    public function addSalesMan(Request $request)
    {

        try {
            if (Auth::check() && Auth::User()->role == "partner") {
                $validatedData = $request->validate([
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|min:4|max:24',
                ]);
                $user = new User;
                $user->partner_id = Auth::user()->id;
                $user->first_name = "Sales Man";
                $user->last_name = "- " . Auth::user()->id;
                $user->email = $request['email'];
                $user->identity = $request['email'];
                $user->password = Hash::make($request['password']);
                $user->register_type = "email";
                $user->role = "sales";
                $user->status = "active";
                $user->save();

                return redirect()->back()->with('success', "تم الاضافة بنجاح");
            } else {
                return redirect()->back()->with('error', "الرجاء التأكد من المعلومات");
            }
        } catch (\Exception $ex) {
            // dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->back()->with('error', "الرجاء التأكد من المعلومات");
        }
    }
    public function updateBranch(Request $request)
    {
        $validator = Validator::make($request->all(), $this->addBranchValidation($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            return redirect()->back()->with(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }
        $partner = Partner::where('id', $request['partner_id'])->first();
        if (($partner->user_id == Auth::user()->id) || (Auth::user()->role = "admin")) {
            $PartnerBranch = PartnerBranch::where('id', $request['branch_id'])->first();
            $PartnerBranch->name = $request['name'];
            $PartnerBranch->lat = $request['lat'];
            $PartnerBranch->lon = $request['lon'];
            $PartnerBranch->save();
            return redirect()->back()->with('success', "تم التعديل بنجاح");
        }
    }

    private function addBranchValidation($request)
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'partner_id' => 'required'
        ];
    }
    public function addBranch(Request $request)
    {
        $validator = Validator::make($request->all(), $this->addBranchValidation($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            return redirect()->back()->with(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }
        $partner = Partner::where('id', $request['partner_id'])->first();
        if (($partner->user_id == Auth::user()->id) || (Auth::user()->role = "admin")) {
            $PartnerBranch = new PartnerBranch;
            $PartnerBranch->name = $request['name'];
            $PartnerBranch->lat = $request['lat'];
            $PartnerBranch->lon = $request['lon'];
            $PartnerBranch->partner_id = $request['partner_id'];
            $PartnerBranch->save();
            return redirect()->back()->with('success', "تم الاضافة بنجاح");
        }
    }

    private function editProdValidation($request)
    {
        return [
            'name' => 'nullable|string|min:3|max:200',
            'price' => 'nullable|numeric',
            'image' => 'nullable|mimes:jpg,png,jpeg,gif,svg',
            'partner_id' => 'required'
        ];
    }
    public function updatediscountPerProduct(Request $request)
    {
        $PartnerProduct = PartnerProduct::where('id', $request['id'])->first();
        $PartnerProduct->discount = $request['newValue'];
        $PartnerProduct->save();
        return redirect()->back()->with('success', "تم التعديل بنجاح");
    }
    public function partnersCouponeAdd(Request $request)
    {
        $name = $request['name'];
        $value = $request['value'];
        $partner_id = $request['partner_id'];
        $status = '0';
        if ($name && $value) {
            DB::table('partner_coupons')->insert([
                ['name' => $name, 'value' => $value, 'partner_id' => $partner_id, 'status' => $status]
            ]);
            return redirect()->back()->with('success', "تم الاضافة بنجاح");
        }
    }
    public function editExistProduct(Request $request)
    {
        $validator = Validator::make($request->all(), $this->editProdValidation($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            return redirect()->back()->with(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }
        $PartnerProduct = PartnerProduct::where('id', $request['product_id'])->first();
        $validated_data = $request->validate($this->editProdValidation($request));
        try {
            if ($request->hasFile('image')) {
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $prodImage = str_replace("partners/", "", $image_data["media_path"]);
                $PartnerProduct->image = $prodImage;
            }
            $PartnerProduct->name = $validated_data["name"];
            $PartnerProduct->price = $validated_data["price"];
            $PartnerProduct->save();

            return redirect()->back()->with('success', "تم التعديل بنجاح");
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', "الرجاء التأكد من المعلومات");
        }
    }



    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'partner_word' => 'nullable|string',
            'image' => 'required|mimes:jpg,png,jpeg,gif,svg',
            'password' => 'required|min:4|max:24',
            'phone' => 'required|numeric|regex:/(05)[0-9]{8}/|unique:users,phone',
            'city_id' => 'required',
            'neighborhood' => 'nullable',
            'description' => 'nullable',
            'map_location' => 'nullable'
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'nullable|string|min:3|max:100',
            'partner_word' => 'nullable|string',
            'image' => 'nullable|mimes:jpg,png,jpeg,gif,svg',
            // 'password' => 'nullable|min:4|max:24',

        ];
    }


    public function goToEditProductPage($id, $p)
    {
        $user = Auth::user()->id;
        $partner = Partner::where('id', $p)->first();
        if (($partner->user_id == $user) || (Auth::user()->role = "admin")) {
            $PartnerProduct = PartnerProduct::where('id', $id)->first();
            return view("admin.partners.editProduct", compact('PartnerProduct', 'partner'));
        }
    }


    public function removeProduct($id, $p)
    {
        $user = Auth::user()->id;
        $partner = Partner::where('id', $p)->first();
        if (($partner->user_id == $user) || (Auth::user()->role = "admin")) {
            $PartnerProduct = PartnerProduct::where('id', $id)->first();
            $x = $PartnerProduct->delete();
            return redirect()->back()->with('message', 'تم التحديث بنجاح');
        }
        dd($partner->user_id, $user);
    }


    public function edit(Request $request)
    {
        $value = $request["general_discount_value"];
        $user = Auth::user()->id;
        if (($request["partner"] == $user) || (Auth::user()->role = "admin")) {
            $partner = Partner::where('user_id', $request["partner"])->first();
            $partner->discount_type = "1";
            $partner->discount = $value;
            $partner->save();
            return redirect()->back()->with('message', 'تم التحديث بنجاح');
        }
    }
    private function storeProdValidation($request)
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'price' => 'required|numeric',
            'image' => 'required',
            'partner_id' => 'required'
        ];
    }

    public function addProd(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeProdValidation($request));
        if ($validator->fails()) {
            Log::error($validator->errors());
            return redirect()->back()->with(['status' => 'fail', 'errors' => formatValidationMessages($validator->errors()->getMessages())]);
        }

        $validated_data = $request->validate($this->storeProdValidation($request));
        try {
            if ($request->hasFile('image')) {
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $prodImage = str_replace("partners/", "", $image_data["media_path"]);
                $PartnerProduct = new PartnerProduct;
                $PartnerProduct->name = $validated_data["name"];
                $PartnerProduct->price = $validated_data["price"];
                $PartnerProduct->image = $prodImage;
                $PartnerProduct->discount = "0";
                $PartnerProduct->partner_id = $request["partner_id"];
                $PartnerProduct->save();
            }
            return redirect()->back()->with('success', "تم الاضافة بنجاح");
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', "الرجاء التأكد من المعلومات");
        }
    }

    public function branchDetails($id)
    {
        if (auth::check() && Auth::user()->role == "partner") {
            $branch = PartnerBranch::find($id)->first();
            $partner = Partner::where('user_id', Auth::user()->id)->first();

            if ($branch->partner_id == $partner->id) {
                return view('admin.partners.branchInfo', compact('partner', 'branch'));
            }
        }
    }

    public function coupons($id)
    {
        if ((auth::check() && Auth::user()->role == "partner" && Auth::user()->id == $id) || (auth::check() && Auth::user()->role == "admin") || (auth::check() && Auth::user()->role == "sales")) {
            $partner = Partner::where('user_id', $id)->first();
            if ($partner) {
                $coupons = DB::table('partner_coupons')->where('partner_id', $partner->id)->get();
            }
            foreach ($coupons as $coup) {
                $Cuser = User::where('id', $coup->user_id)->first();
                if ($Cuser) {
                    $coup->Cuser = $Cuser->first_name . ' ' . $Cuser->last_name;
                } else {
                    DB::table('partner_coupons')->where('id', $coup->id)->delete();
                }
                $coup->reservation_date = convertToHijri($coup->reservation_date);
            }
            return view('admin.partners.coupons', compact('partner', 'coupons'));
        }
    }

    public function getSalesMen($id)
    {
        $users = User::where('partner_id', Auth::user()->id)->where('role', 'sales')->get();
        return view('admin.partners.salesmen', compact('users'));
    }
    public function main($id)
    {
        if ((auth::check() && Auth::user()->role == "partner" && Auth::user()->id == $id) || (Auth::user()->role == "admin")) {
            $partner = Partner::where('user_id', $id)->first();
            if ($partner) {
                // try{
                foreach ($partner->products as $product) {
                    if ($partner->discount_type == "1") { // general discount
                        $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$partner["discount"] / 100));
                    } else {
                        if ($partner->discount_type == "2") { //per product discount
                            if ($product["discount"] != "0") {
                                $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$product["discount"] / 100));
                            } else {
                                $product["discount_price"] = (int)$product["price"];
                            }
                        } else {
                            $product["discount_price"] = null;
                        }
                    }
                    $product["image"] = asset('/uploads/partners/' . $product["image"]);
                }
            }
        }
        $cities = City::all();
        $branches = $partner->barnches;
        return view('admin.partners.main', compact('partner', 'cities', 'branches'));
    }


    public function branches($id)
    {
        // if(auth::check()&&Auth::user()->role== "partner"&&Auth::user()->id==$id){
        $partner = Partner::where('user_id', $id)->first();
        if ($partner) {
            $branches = $partner->barnches;
            foreach ($partner->products as $product) {
                if ($partner->discount_type == "1") { // general discount
                    $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$partner["discount"] / 100));
                } else {
                    if ($partner->discount_type == "2") { //per product discount
                        if ($product["discount"] != "0") {
                            $product["discount_price"] = (int)$product["price"] - ((int)$product["price"] * ((int)$product["discount"] / 100));
                        } else {
                            $product["discount_price"] = (int)$product["price"];
                        }
                    } else {
                        $product["discount_price"] = null;
                    }
                }
                $product["image"] = asset('/uploads/partners/' . $product["image"]);
            }
        }
        $partner['city'] = City::find($partner->city_id);

        $latlon  = $partner->location;
        $pieces = explode(",", $latlon);
        $partner['lat'] = $pieces[0]; // lat
        $partner['lon'] = $pieces[1]; // lon
        // $branches=$partner->barnches;
        return view('admin.partners.branch', compact('partner', 'branches'));
        // }
    }

    public function index()
    {
        has_access('show_partners');
        $cities = City::all();
        $partners = Partner::orderBy('priority', 'asc')->get();
        // $partners=DB::table('partners')->orderBy('priority','asc')->get();
        foreach ($partners as $partner) {
            $partner['city'] = City::find($partner['city_id']);
            $partner['phone'] = User::find($partner['user_id'])->phone;
        }
        return view($this->index_view, compact('partners', 'cities'));
    }

    public function create()
    {
        // has_access('create_partners');
        return view($this->create_view);
    }

    public function addNew($id)

    {
        $partner = Partner::where('id', $id)->first();

        return view('admin.partners.addNewBranch', compact('partner'));
    }

    public function editBranch($id, $b_id)

    {
        $partner = Partner::where('id', $id)->first();
        $branch = PartnerBranch::where('id', $b_id)->first();
        return view('admin.partners.edit_branch', compact('partner', 'branch'));
    }


    public function store(Request $request)
    {
        $validated_data = $request->validate($this->StoreValidationRules());
        try {
            DB::beginTransaction();
            $last_priority = Partner::max('priority');
            $validated_data['priority'] = ++$last_priority;
            $validated_data1['name'] = $validated_data['name'];
            $validated_data1['partner_word'] = $validated_data['partner_word'];
            $validated_data1['priority'] = $validated_data['priority'];
            $validated_data1['city_id'] = $request['city_id'];
            $validated_data1['neighborhood'] = $request['neighborhood'];
            $validated_data1['address'] = $request['description'];
            $validated_data1['discount'] = '0';
            $validated_data1['location'] = $validated_data['map_location'];
            $partner_user = new User;
            $partner_user->first_name = $validated_data['name'];
            $partner_user->last_name = $validated_data['name'];
            $partner_user->role = 'partner';
            $partner_user->identity = $validated_data['phone'];
            $partner_user->phone = $validated_data['phone'];
            $partner_user->register_type = "phone";
            $partner_user->password = bcrypt($validated_data['password']);
            $partner_user->status = 'inactive';
            $partner_user->save();
            $validated_data1['user_id'] = $partner_user->id;
            $partner = $this->model_instance::create($validated_data1);
            if ($request->hasFile('image')) {
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $partner->avatar_path = $image_data["media_path"];
                $partner->avatar_url = $image_data["media_url"];
                $partner->update();
            }
            $partner->update();
            DB::commit();
            if (auth::check() && Auth::user()->role == "admin") {
                //
            } else {
                auth::logout($partner_user);
            }
            return Redirect::back()->with('success', 'تم إنشاء الحساب بنجاح!');
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return view('admin.partners.show')->with('error', $this->error_message);
        }
    }

    private function validatedPassword()
    {
        return [
            'password' => 'nullable|min:4|max:24'
        ];
    }
    public function update(Request $request, $id)
    {

        $validated_data = $request->validate($this->UpdateValidationRules());

        try {
            DB::beginTransaction();
            $partner = $this->model_instance::find($id);
            $user = User::where('id', $partner->user_id)->first();
            $password = $request['password'];
            if ($password) {
                $validatedPassword = $request->validate($this->validatedPassword());
                $password = bcrypt($password);
                $user->password = $password;
            }
            if ($request['name']) {
                $user->first_name = $request['name'];
            }
            $user->save();
            $partner->update($validated_data);

            if ($request->hasFile('image')) {
                hl_deleteFile($partner->avatar_path);
                $image_data = hl_uploadFileTo($validated_data["image"], 'partners');
                $partner->avatar_path = $image_data["media_path"];
                $partner->avatar_url = $image_data["media_url"];
                $partner->update();
            }

            DB::commit();
            return redirect()->route($this->index_route)->with('success', 'تم التعديل بنجاح !');
        } catch (\Exception $ex) {
            DB::rollBack();
            // dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $ex->getMessage());
        }
    }
}
