<?php

namespace App\Http\Controllers\admin;

use App\models\Hall;
use App\models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function getNew()
    {
        $notifications = Notification::where('notified','no');
        $alerts = Notification::where('is_new','yes');
        if (!\auth()->user()->hasRole('administrator')) {
            $hall_ids = Hall::where('user_id', Auth::id())->pluck('id')->toArray();
            $alerts = $alerts->whereIn('hall_id', $hall_ids);
        }
        $data = $notifications->get()->toArray();
        $notifications->update(['notified' => 'yes']);

        return response()->json(['data' => $data,'count' => $alerts->count() == 0 ? '' : $alerts->count() ], 200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }
}
