<?php

namespace App\Http\Controllers\admin;

use App\models\Ads;
// use App\models\UserActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class AdsController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.ads.index';
        $this->create_view = 'admin.ads.create';
        $this->show_view = 'admin.ads.show';
        $this->edit_view = 'admin.ads.edit';
        $this->index_route = 'admin.ads.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Ads::class;
    }

    private function StoreValidationRules($request)
    {
        return [
            'title' => 'required|string|min:3|max:200',
            'type' => 'required|in:ads,hall',
            // 'item_id' => 'nullable',
            'item_id' => Rule::requiredIf($request->type=="hall"),
            'image' => 'required|mimes:jpg,png,jpeg,gif,svg',
        ];
    }

    private function UpdateValidationRules($request)
    {
        return [
            'title' => 'required|string|min:3|max:200',
            'type' => 'required|in:ads,hall',
            // 'item_id' => 'nullable',
            'item_id' => Rule::requiredIf($request->type=="hall"),
            'image' => 'sometimes|mimes:jpg,png,jpeg,gif,svg',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $ads = $this->model_instance::all();
        return view($this->index_view, compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        has_access('ads_create');
        return view($this->create_view);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        has_access('ads_create');
        $validated_data = $request->validate($this->StoreValidationRules($request));
//dd($validated_data);
        try {

            $object = $this->model_instance::create($validated_data);
            if($request->hasFile('image'))
            {

                $image_data = hl_uploadFileTo($validated_data["image"],'ads');
                $object->image_url = $image_data["media_url"];
                $object->image_path = $image_data["media_path"];
                $object->update();
            }

            $log_message = trans('ads.create_log') . '#' . $object->id;
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $category = $this->model_instance::findOrFail($id);
        return view($this->show_view, compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        has_access('ads_update');
        $ads = $this->model_instance::findOrFail($id);

        return view($this->edit_view, compact(['ads']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        has_access('ads_update');
        $validated_data = $request->validate($this->UpdateValidationRules($request));

        try {
            $object = $this->model_instance::find($id);
            //dd($validated_data);
            $updated_instance = $object->update($validated_data);

            if($request->hasFile('image'))
            {

                $image_data = hl_uploadFileTo($validated_data["image"],'ads');
                hl_deleteFile($object->image_path);
                $object->image_url = $image_data["media_url"];
                $object->image_path = $image_data["media_path"];
                $object->update();
            }


            $log_message = trans('ads.update_log') . '#' . $object->id;
           // UserActivity::logActivity($log_message);

            if ($updated_instance) {
                return redirect()->route($this->index_route)->with('success', $this->update_success_message);
            } else {
                return redirect()->route($this->index_route)->with('error', $this->update_error_message);
            }



        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        has_access('ads_remover');

        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                $log_message = trans('ads.delete_log') . '#' . $id;
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }




        }

        return redirect()->route($this->index_route);
    }
}
