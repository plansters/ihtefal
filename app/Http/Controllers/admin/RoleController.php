<?php

namespace App\Http\Controllers\admin;

use App\models\Permission;
use App\models\Role;
use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.roles.index';
        $this->create_view = 'admin.roles.create';
        $this->show_view = 'admin.roles.show';
        $this->edit_view = 'admin.roles.edit';
        $this->index_route = 'admin.roles.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Role::class;
    }

    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'description' => 'required|string|min:3|max:500',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'description' => 'required|string|min:3|max:500',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        has_access('show_roles');
        $roles = $this->model_instance::all();
        return view($this->index_view, compact('roles'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        has_access('create_roles');
        $permissions = Permission::all();
        return view($this->create_view,compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        has_access('create_roles');
//dd($request->all());
        $validated_data = $request->validate($this->StoreValidationRules());
        /*$validator = Validator::make($request->all(), $this->StoreValidationRules());
                if ($validator->fails()) {
                    dd($validator->errors());
                }
        dd('jj');*/

        try {
            $validated_data['slug'] = Str::slug($validated_data['name'], '_');
            $object = $this->model_instance::create($validated_data);

            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        has_access('update_roles');
        $role = $this->model_instance::findOrFail($id);
        $permissions = Permission::all();
        $role_permissions = $role->permissions()->pluck('permissions.id')->toArray();
        return view($this->edit_view, compact(['role','permissions', 'role_permissions']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        has_access('update_roles');
        $validated_data = $request->validate($this->UpdateValidationRules());

        try {
            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->permissions()->sync(request()->permissions);
            $updated_instance->update($validated_data);

            if((in_array(46,request()->permissions)||in_array(47,request()->permissions)||in_array(48,request()->permissions))&&(!in_array(17,request()->permissions))){
                $affected=DB::table('permission_role')->insert([
                    ['permission_id' => '17', 'role_id' => $id]]);
            }
    
            if((in_array(43,request()->permissions)||in_array(44,request()->permissions)||in_array(45,request()->permissions))&&(!in_array(16,request()->permissions))){
                DB::table('permission_role')->insert([
                    ['permission_id' => '16', 'role_id' => $id]]);
            }

            if((in_array(49,request()->permissions)||in_array(50,request()->permissions)||in_array(51,request()->permissions))&&(!in_array(18,request()->permissions))){
                DB::table('permission_role')->insert([
                    ['permission_id' => '18', 'role_id' => $id]]);
            }
            DB::commit();
            if ($updated_instance) {
                return redirect()->route($this->index_route)->with('success', $this->update_success_message);
            } else {
                return redirect()->route($this->index_route)->with('error', $this->update_error_message);
            }
        } catch (\Exception $ex) {
            DB::rollBack();
            //dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        has_access('delete_roles');
        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }

        }

        return redirect()->route($this->index_route);
    }
}
