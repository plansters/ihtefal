<?php

namespace App\Http\Controllers\admin;

use App\models\Reservation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
 
class CalendarController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->index_view = 'admin.calendar.index';
        $this->create_view = 'admin.calendar.create';
        $this->show_view = 'admin.calendar.show';
        $this->edit_view = 'admin.calendar.edit';
        $this->index_route = 'admin.calendar.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Reservation::class;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 

        has_access('reservations');
        $events = [];

        $freeEvents = [];
        // $eloquentEvents = $this->model_instance::whereIn('hall_id',Auth::user()->halls()->pluck('id')->toArray())->get();
        if(Auth::user()->role=="admin"){
$eloquentEvents = $this->model_instance::where('user_id',null)->get(); 
$eloquentEvents = $eloquentEvents->keyBy('id');
foreach($eloquentEvents as $rsrv){
    if($rsrv->hall){

                if(!Auth::user()->CanDo('edit_rest_halls'))
                {
                    if($rsrv->hall->category_id == 4)
                    {
                        $eloquentEvents->forget($rsrv->id);
                    }
                }
                if(!Auth::user()->CanDo('edit_palace_halls'))
                {
                    if($rsrv->hall->category_id == 1)
                    {
                        $eloquentEvents->forget($rsrv->id);

                    }
                }
                if(!Auth::user()->CanDo('edit_hotels_halls'))
                {
    
                    if($rsrv->hall->category_id == 2)
                    {
                        
                        $eloquentEvents->forget($rsrv->id);
                    }
                }
   } 
}
}
        else{
        

$eloquentEvents = $this->model_instance::whereIn('hall_id',Auth::user()->halls()->pluck('id')->toArray())->get();
$eloquentEvents = $eloquentEvents->keyBy('id');  
// dd($eloquentEvents);
foreach($eloquentEvents as $rsrv){
    if($rsrv->hall){
                if(!Auth::user()->CanDo('edit_rest_halls')&&Auth::user()->role!="vendor")
                {
                    if($rsrv->hall->category_id == 4)
                    {
                        $eloquentEvents->forget($rsrv->id);
                    }
                }
                if(!Auth::user()->CanDo('edit_palace_halls')&&Auth::user()->role!="vendor")
                {
                    if($rsrv->hall->category_id == 1)
                    {
                        $eloquentEvents->forget($rsrv->id);
                    }
                }
                if(!Auth::user()->CanDo('edit_hotels_halls') && Auth::user()->role!="vendor")
                {
                    if($rsrv->hall->category_id == 2)
                    {
                        $eloquentEvents->forget($rsrv->id);
                    }
                }
   } 
}
}
  
        foreach ($eloquentEvents as $event) {
            // dd($event);
            if($event->start) {
                // dd($event);
                    $events[] = \Calendar::event(
                    $event->title, //event title
                    $event->all_day, //full day event?
                    new \DateTime($event->start), //start time (you can also use Carbon instead of DateTime)
                    new \DateTime($event->end), //end time (you can also use Carbon instead of DateTime)
                    $event->id //optionally, you can specify an event ID
                ); 
            } else {
                $freeEvents[] = $event;
            }

        }
        // dd($events);
        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'editable' => 1,
            'selectable' => true,
            'plugins' => [ 'bootstrap', 'momentPlugin'],
            'themeSystem' => 'bootstrap',
            'droppable' => true,
            'aspectRatio' => 1,
            'height' => 650,
            'allDay' => false,
            'display' => 'inline-block',
            'className' => 'col-md-6',
            'defaultTimedEventDuration' => '00:50:00',
            'forceEventDuration' => true,
            //'classNames' => 0,
            //'classNames' => 0,
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'viewRender' => 'function(view, element) { getEvents(view, element) }',
            'eventDrop' => 'function(info) { resizeEvent(info) }',
            'drop' => 'function(date, allDay) { dropEnd(date, allDay); }',
            'eventResize' => 'function(info) { resizeEvent(info) }',
            //'select' => 'function (start, end, allDay) { selectItem(start, end, allDay); }',
            'dayClick' => 'function (date, jsEvent, view) { openDay(date, jsEvent, view); }',
            'remove' => 'function(calEvent, jsEvent, view) { console.log("removeClick id: " + calEvent.id); }',
            'eventClick' => 'function(info) {  editEvent(info) }',
            'dayRender' => 'function(date, cell) { set_hijri_content(date, cell) }',
        ]);
        return view('admin.calendar.index', compact(['calendar', 'freeEvents']));
    }

    public function getEvents (Request $request) {

        has_access('reservations');
        if (!$request->ajax()) {
            return response()->json();
        }
        $events = [];
        //return response()->json($request->all());
        $ids = [];
        if ($request->has('id')) {
            $ids[] = $request->id;
        } else {
            $ids = Auth::user()->halls()->pluck('id')->toArray();
        }
        // $eloquentEvents = $this->model_instance::whereBetween('start', [$request->start, $request->end])->whereIn('hall_id', $ids)->get();
   if(Auth::user()->role=="admin"){
        $eloquentEvents = $this->model_instance::whereBetween('start', [$request->start, $request->end])
        ->where('status','!=','cancelled')
        // ->where('status','!=','completed')
        ->get();
        $eloquentEvents = $eloquentEvents->keyBy('id');
        foreach($eloquentEvents as $rsrv){

            if(!Auth::user()->CanDo('edit_rest_halls'))
            {
                if($rsrv->hall->category_id == 4)
                {
                    $eloquentEvents->forget($rsrv->id);
                }
            }
        
            if(!Auth::user()->CanDo('edit_palace_halls'))
            {
                if($rsrv->hall->category_id == 1)
                {
                    $eloquentEvents->forget($rsrv->id);
                }
            }
        
            if(!Auth::user()->CanDo('edit_hotels_halls'))
            {
                if($rsrv->hall->category_id == 2)
                {
                    $eloquentEvents->forget($rsrv->id);
                }
            }
        
           } 

    }
    elseif(Auth::user()->role=="vendor"){
        $resrvs_ids=Reservation::pluck('hall_id')->toArray();
        $vendor_ids=Auth::user()->halls->pluck('id')->toArray(); 
        $my_halls=array_intersect($resrvs_ids, $vendor_ids); 
$eloquentEvents = $this->model_instance::whereBetween('start', [$request->start, $request->end])
->whereIn('hall_id',$my_halls)
->where('status','!=','cancelled')
// ->where('status','!=','completed')
->get();

    }
        else
    $eloquentEvents = $this->model_instance::whereBetween('start', [$request->start, $request->end])->where('user_id',Auth::user()->id)
    ->where('status','!=','cancelled')
    // ->where('status','!=','completed')
    ->get();

        foreach ($eloquentEvents as $event) {
            $user = $event->user ?? '';
            $event_color = $user ? '#D32F2F' : '#43A047';
            $hall_id = $event->hall->id ?? '';
            // $description = $event->description ?? '';
            $hall_name = $event->hall->name ?? '';
            $events[] = [
                'title' => $event->title, //event title
                'isAllDay' => $event->all_day, //full day event?
                'start' => new \DateTime($event->start), //start time (you can also use Carbon instead of DateTime)
                'end' => new \DateTime($event->end), //end time (you can also use Carbon instead of DateTime)
                'id' => $event->id, //optionally, you can specify an event ID, //optionally, you can specify an event ID
                'description'=>$event->all_day,
               
                'options' => [
                    'color' => $event_color,
                    'user' => $user,
                    'hall_route' => route('admin.halls.show', $hall_id),
                    // 'description'=>$event->description,
                    'hall_name' => $hall_name
                    
                ]
            ];
        }
        // dd($events);
        return response()->json($events);
    }

    public function create(Request $request)
    {
        
        // return $request;
        has_access('reservations');
        if (!$request->ajax()) {
            // dd("?ss");
            return response()->json();
        }
        if ($request->has('hall_id')) {

            $date = strtotime($request->date);
            $createArr = [
                'start' => Carbon::parse($date),
                'end' => Carbon::parse($date),
                'all_day' => 1,
                'hall_id' => $request->hall_id,
                'user_id' => Auth::id(),
                'title' => trans('calendar.new_event')
            ];
            // dd("ddddd");
            // $res=Reservation::whereDate('start', Carbon::parse(Carbon::parse($date))->addDays(1))->where('hall_id', $request->hall_id)->where('status','!=','cancelled')->get();
            // $chk_status=$res->count()>0?true:false;       
            // if($chk_status){
            //     $dat=Carbon::parse($event->start);
            //     $dat=convertToHijri($dat);
            //         return response()->json(['status' => 'FAILURE','errors' =>"يوجد حجز اخر بتاريخ (".$dat.") للقاعة (".$res->first()->hall->name."), يرجى تغيير الموعد."]);
            //     }

            $reservation = $this->model_instance::create($createArr);
            $event = $this->model_instance::find($reservation->id);
            $event_color = $event->user_id ? '#D32F2F' : '#43A047';
            $user = $event->user;
            $hall_id = $event->hall->id ?? '';
            $hall_name = $event->hall->name ?? '';
            $event_data = [
                'title' => $event->title, //event title
                'description' => $event->description, //event description
                'isAllDay' => $event->all_day, //full day event?
                'start' => new \DateTime($event->start), //start time (you can also use Carbon instead of DateTime)
                'end' => new \DateTime($event->end), //end time (you can also use Carbon instead of DateTime)
                'id' => $event->id, //optionally, you can specify an event ID, //optionally, you can specify an event ID
                'description'=>$event->description,
                'options' => [
                    'color' => $event_color,
                    'user' => $user,
                    'hall_route' => route('admin.halls.show', $hall_id),
                    'hall_name' => $hall_name
                ]
            ];
            return response()->json($event_data);
        } else {
             $event = $this->model_instance::findOrFail($request->id);
            //$d    = new DateTime($request->date);
            //return response()->json($d->format('l'));;
            $date = strtotime($request->date);
            $updArr = [
                'start' => Carbon::parse($date),
                'end' => Carbon::parse($date),
                'all_day' => 1
            ];
            // 
            $res=Reservation::whereDate('start', Carbon::parse($date))->where('hall_id', $event->hall_id)->where('status','!=','cancelled')->get();
            $chk_status=$res->count()>0?true:false;      
            if($chk_status){
                $date=convertToHijri($date);
                    return response()->json(['status' => 'FAILURE','errors' =>"يوجد حجز اخر بتاريخ (".$date.") للقاعة (".$res->first()->hall->name."), يرجى تغيير الموعد."]);
                }
            // 
            $event->update($updArr);
            $event = $this->model_instance::findOrFail($request->id);
            $event_color = $event->user_id ? '#D32F2F' : '#43A047';
            $user = $event->user;
            $hall_id = $event->hall->id ?? '';
            $hall_name = $event->hall->name ?? '';
            // dd($event->description);
            $event_data = [
                'title' => $event->title, //event title
                'isAllDay' => $event->all_day, //full day event?
                'start' => new \DateTime($event->start), //start time (you can also use Carbon instead of DateTime)
                'end' => new \DateTime($event->end), //end time (you can also use Carbon instead of DateTime)
                'id' => $event->id, //optionally, you can specify an event ID, //optionally, you can specify an event ID
                'description'=>$event->description,
                    'options' => [
                    'color' => $event_color,
                    'user' => $user,
                    'hall_route' => route('admin.halls.show', $hall_id),
                    'hall_name' => $hall_name
                ]
            ];
            return response()->json($event_data);
        }
    }

    public function resize(Request $request)
    {
        
        has_access('reservations');
        has_access('calendar');
        $updArr = [];
        if (!$request->ajax()) {
            return response()->json();
        }
        $reservation = $this->model_instance::findOrFail($request->id);
        // if (Auth::id() == $reservation->user_id || Auth::id() == $reservation->hall->user_id) {
            $start = strtotime($request->start);
            $end = strtotime($request->end);
            $updArr = [
                'start' => Carbon::parse($start)->addDays(1),
                'end' => Carbon::parse($end)->addDays(1),
                'all_Day' => $request->all_day ? 1 : 0
            ];
            // 
 $res=Reservation::whereDate('start', Carbon::parse($start)->addDays(1))->where('hall_id', $reservation->hall_id)->where('status','!=','cancelled')->get();
$chk_status=$res->count()>0?true:false;       
if($chk_status){
    $dat=Carbon::parse($reservation->start);
    $dat=convertToHijri($dat);
        return response()->json(['status' => 'FAILURE','errors' =>"يوجد حجز اخر بتاريخ (".$dat.") للقاعة (".$res->first()->hall->name."), يرجى تغيير الموعد."]);
    }
            // 
            $reservation->update($updArr);
        // }

        return response()->json($updArr);
        //return Respons::json();
    }


    public function update(Request $request)
    {
    
        has_access('reservations');
        if (!$request->ajax()) {
            return response()->json();
        }
        $reservation = $this->model_instance::findOrFail($request->id);
        if (Auth::id() == $reservation->user_id || Auth::id() == $reservation->hall->user_id) {
            $where = array('id' => $request->id);
            $updateArr = ['title' => $request->title, 'start' => $request->start, 'end' => $request->end];
            $reservation->update($updateArr);
        }

        return response()->json($reservation);
    }


    public function destroy(Request $request)
    {
        has_access('reservations');
        if (!$request->ajax()) {
            return response()->json();
        }
        $reservation = $this->model_instance::findOrFail($request->id);
        if (Auth::id() == $reservation->user_id || Auth::id() == $reservation->hall->user_id) {
            $event = $reservation->delete();
        }
        return response()->json($event);
    }
}
