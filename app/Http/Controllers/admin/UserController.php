<?php

namespace App\Http\Controllers\admin;

use App\models\Role;
use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\models\Notification;
use App\models\Partner;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.users.index';
        $this->create_view = 'admin.users.create';
        $this->show_view = 'admin.users.show';
        $this->edit_view = 'admin.users.edit';
        $this->index_route = 'admin.users.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = User::class;
    }


    public function contact_us()
    {


        return view('/contact-us', compact('users'));
    }
    private function StoreValidationRules()
    {
        return [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'phone' => 'nullable|required_if:email,|required_if:register_type,phone|min:10|max:10|sometimes|regex:/(05)[0-9]{8}/|unique:users,phone',
            'email' => 'nullable|required_if:phone,|required_if:register_type,email|email|sometimes|unique:users,email|unique:users,email',
            'address' => 'nullable|string',
            'role' => 'required|in:client,vendor,admin',
            'register_type' => 'required|in:phone,email',
            'password' => 'required|min:4|max:24|confirmed',
        ];
    }

    private function UpdateValidationRules($id = null)
    {
        return [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'phone' => 'nullable|required_if:email,|required_if:register_type,phone|min:10|max:10|regex:/(05)[0-9]{8}/|unique:users,phone,'. $id.',id',
            'email' => 'nullable|required_if:phone,|required_if:register_type,email|email|unique:users,email,'.$id.',id',
            'address' => 'nullable|string',
            'role' => 'required|in:client,vendor,admin',
            'register_type' => 'required|in:phone,email',
            'password' => 'nullable|min:4|max:24|confirmed',
        ];
    }

    private function UpdateProfileValidationRules($id = null)
    {
        return [
            'first_name' => 'required|string|min:3|max:100',
            'last_name' => 'required|string|min:3|max:100',
            'password' => 'nullable|min:4|max:24|confirmed',
        ];
    }



    public function index()
    {
        has_access('show_users');
        $users = $this->model_instance::all();
        Notification::where('type', 'user')->update(["is_new" => 'no', 'notified' => 'yes']);

        return view($this->index_view, compact('users'));
    }


    public function create()
    {
        has_access('create_users');
        $roles = Role::all();
        return view($this->create_view, compact('roles'));
    }


    public function store(Request $request)
    {
        has_access('create_users');
        $validated_data = $request->validate($this->StoreValidationRules());

        try {
            DB::beginTransaction();
            $phones = User::where('phone', $validated_data['phone'])->count();

            if ($phones != 0) {
                DB::rollBack();
                return redirect()->route($this->index_route)->with('error', $this->error_message);
            }
            $user = $this->model_instance::create($validated_data);
            $user->password = bcrypt($user->password);
            $user->identity = $user->register_type == 'phone' ? $user->phone : $user->email;
            $user->status = "active";
            if ($validated_data['role'] == 'vendor') {
                $role = Role::where('slug', 'vendor')->first();
                $user->roles()->save($role);
            } elseif ($validated_data['role'] == 'client') {
                $role = Role::where('slug', 'client')->first();
                $user->roles()->save($role);
            }
            $user->update();
            if ($user->phone && $user->status == 'active') {
                $request['message'] = "مرحباً بكم في احتفال! لقد تم تفعيل حسابكم.";
                $request['numbers'] = "966" . substr($user->phone, 1);
                $res = SendSMS($request);
                // return $res;
            }
            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }

    public function edit($id)
    {
        has_access('update_users');
        $roles = Role::all();
        $user = $this->model_instance::find($id);
        $user_roles = $user->roles()->pluck('roles.id')->toArray();
        return view($this->edit_view, compact(['user', 'roles', 'user_roles']));
    }

    public function update(Request $request, $id)
    {
        
        has_access('update_users');
      dd(  $this->UpdateValidationRules($id));
        $validated_data = $request->validate($this->UpdateValidationRules($id));
        try {
            DB::beginTransaction();
            $user = $this->model_instance::find($id);
            $password = $validated_data['password'];
            unset($validated_data['password']);
            $user->update($validated_data);
            if ($password) {
                $user->password = bcrypt($password);
            }


            $identity = $user->register_type == 'phone' ? $user->phone : $user->email;

            $user->update([
                'identity' => $identity
            ]);


            if (request()->has('roles')) {
                foreach (request()->roles as $role_id) {
                    $role = Role::find($role_id);
                    $user->roles()->attach($role);
                }
            }

            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }

    public function editProfile()
    {
        $id = Auth::id();
        $user = $this->model_instance::find($id);
        return view('admin.users.profile', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $id = Auth::id();
        $validated_data = $request->validate($this->UpdateProfileValidationRules($id));

        try {
            DB::beginTransaction();
            $user = $this->model_instance::find($id);
            $password = $validated_data['password'];
            unset($validated_data['password']);
            $user->update($validated_data);
            //dd($password);
            if ($password) {
                $user->password = bcrypt($password);
                $user->update();
            }

            DB::commit();
            return redirect()->route('admin.profile.edit')->with('success', $this->update_success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            // dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route('admin.profile.edit')->with('error', $this->error_message);
        }
    }

    public function changeStatus(Request $request)
    {
        has_access('update_users');
        $user = $this->model_instance::find($request->id);
        $status = $user->status;
        $new_status = $status == 'active' ? 'inactive' : 'active';
        $user->status = $new_status;
        $user->update();
        if ($new_status == 'active') {
            if ($user['role'] == 'vendor') {
                $role = Role::where('slug', 'vendor')->first();
                if (!$user->roles->contains($role->id))
                    $user->roles()->attach($role->id);
            } elseif ($user['role'] == 'client') {
                $role = Role::where('slug', 'client')->first();
                if (!$user->roles->contains($role->id))
                    $user->roles()->attach($role->id);
            }
        } else {
            if ($user['role'] == 'vendor') {
                $role = Role::where('slug', 'vendor')->first();
                $user->roles()->detach($role->id);
            } elseif ($user['role'] == 'client') {
                $role = Role::where('slug', 'client')->first();
                $user->roles()->detach($role->id);
            }
        }
        if ($user->phone && $new_status == 'active') {
            $request['message'] = "مرحباً بكم في احتفال! لقد تم تفعيل حسابكم.";
            $request['numbers'] = "966" . substr($user->phone, 1);
            $res = SendSMS($request);
            return $res;
        }
    }
    public function destroy(Request $request, $id)
    {
        has_access('delete_users');
        if ($request->ajax()) {
            $user = $this->model_instance::findOrFail($id);
            $partner = Partner::where('user_id', $user->id)->first();
            if ($partner) {
                $partner->delete();
            }
            $deleted =  $user->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }
        }

        return redirect()->route($this->index_route);
    }

    public function search(Request $request)
    {
        $term = trim($request->q);

        if (empty($term)) {
            return response()->json([]);
        }

        $users = $this->model_instance::where(['status' => 'active'])->where('first_name', 'LIKE', '%' . $term . '%')->orWhere('last_name', 'LIKE', '%' . $term . '%')->limit(5)->get(['id', 'first_name', 'last_name']);
        $map = $users->map(function ($results) {
            $data["id"] = $results->id;
            $data["text"] = $results->first_name . ' ' . $results->last_name;
            return $data;
        });

        return response()->json($map->toArray());
    }

    public function getReservations()
    {
        $reservations = $this->model_instance::findOrFail(Auth::id())->reservations()->orderBy('id', 'desc')->paginate(10);
        return view('admin.users.reservations', compact('reservations'));
    }
}
