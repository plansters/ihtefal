<?php

namespace App\Http\Controllers\admin;

use App\models\Coupon;
use App\models\Hall;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CouponController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.coupons.index';
        $this->create_view = 'admin.coupons.create';
        $this->show_view = 'admin.coupons.show';
        $this->edit_view = 'admin.coupons.edit';
        $this->index_route = 'admin.coupons.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = Coupon::class;
    }

    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:100',
            'description' => 'required|string',
            'coupon_id' => 'required|string',
            'hall_id' => Auth::user()->hasRole('administrator') ? 'nullable|exists:halls,id' : 'required|exists:halls,id',
            'coupon_type' => 'required|string|in:percentage,value',
            'coupon_value' => 'required|numeric',
            'start_date' => 'nullable|date|after_or_equal:today',
            'expired_date' => 'nullable|date|after:start_date',
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:100',
            'description' => 'required|string',
            'coupon_id' => 'required|string',
            'hall_id' => Auth::user()->hasRole('administrator') ? 'nullable|exists:halls,id' : 'required|exists:halls,id',
            'coupon_type' => 'required|string|in:percentage,value',
            'coupon_value' => 'required|numeric',
            'start_date' => 'nullable|date|after_or_equal:today',
            'expired_date' => 'nullable|date|after:start_date',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        has_access('show_coupons');
        if (Auth::user()->hasRole('administrator')) {
            $coupons = $this->model_instance::all();
        } else {
            $hall_ids = Hall::where('user_id', Auth::id())->pluck('id')->toArray();
            $coupons = $this->model_instance::whereIn('hall_id', $hall_ids)->get();
        }
        return view($this->index_view, compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->create_view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        has_access('create_coupons');
        $validated_data = $request->validate($this->StoreValidationRules());


        try {
            DB::beginTransaction();
            $this->model_instance::create($validated_data);

            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        has_access('update_coupons');
        $coupon = $this->model_instance::find($id);
        return view($this->edit_view, compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        has_access('update_coupons');
        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->update($validated_data);
            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            //dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }




    public function destroy(Request $request, $id)
    {
        has_access('delete_cities');
        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }

        }

        return redirect()->route($this->index_route);
    }
}
