<?php

namespace App\Http\Controllers\admin;
use Carbon\Carbon;
use App\models\HallPrice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HallPriceController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.halls.index';
        $this->create_view = 'admin.halls.create';
        $this->show_view = 'admin.halls.show';
        $this->edit_view = 'admin.halls.edit';
        $this->index_route = 'admin.halls.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = HallPrice::class;
    }

    private function StoreValidationRules()
    {
        return [
            'title' => 'required|string|min:3|max:200',
            'price' => 'required|numeric',
            'price_type' => 'required|in:by_days,by_islamic,by_gregorian',
            'from_date' => 'nullable|required_if:price_type,by_gregorian|date|after:now',
            'to_date' => 'nullable|required_if:price_type,by_gregorian|date|after:from_date',
            'hijri_from' => 'nullable|required_if:price_type,by_islamic|date',
            'hijri_to' => 'nullable|required_if:price_type,by_islamic|date|after:hijri_from',
            'days' => 'required_if:price_type,by_days|array',
            'hall_id' => 'required|exists:halls,id'
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //has_access('show_halls');
        $halls = $this->model_instance::all();
        return view($this->index_view, compact('halls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //has_access('create_halls');
        return view($this->create_view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //has_access('create_halls');
        $validated_data = $request->validate($this->StoreValidationRules());
        try {
            DB::beginTransaction();
            $last_priority = $this->model_instance::where('hall_id', $validated_data['hall_id'])->max('priority');
            $validated_data['priority'] = ++$last_priority;
            if ($validated_data['price_type'] == 'by_islamic') {
                $validated_data['from_date'] = convertToGregorian($validated_data['hijri_from']);
                $validated_data['to_date'] = convertToGregorian($validated_data['hijri_to']);
                // $validated_data['from_date']=$validated_data['from_date']->add(1, 'day');
                // $validated_data['to_date'] = convertToGregorian($validated_data['hijri_to'])->add(1, 'day');
            //    $t=$validated_data['hijri_to'];
            //    $f=$validated_data['hijri_from'];
            //    $tp=Carbon::parse($t);
            //    $fp=Carbon::parse($f);

// $from_date=Carbon::parse(jdtogregorian($this->HijriToJD($fp->month,$fp->day,$fp->year)))->toDateTimeString();
// $to_date=Carbon::parse(jdtogregorian($this->HijriToJD($tp->month,$tp->day,$tp->year)))->toDateTimeString();
// dd($from_date);
    // $validated_data['from_date'] = $from_date;
    // $validated_data['to_date'] = $to_date;
            
            }
            // dd($validated_data['from_date']);
            if ($validated_data['days']) {
                $validated_data['days'] = implode(',', $validated_data['days']);
            }
            $this->model_instance::create($validated_data);

            DB::commit();
            return redirect()->route($this->edit_view, $validated_data['hall_id'])->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            // dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->edit_view, $validated_data['hall_id'])->with('error', $this->error_message);
        }


    }

    public function HijriToJD($m, $d, $y){
        return (int)((11 * $y + 3) / 30) + 354 * $y + 
          30 * $m - (int)(($m - 1) / 2) + $d + 1948440 - 385;
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        has_access('update_halls');
        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->update($validated_data);

            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            // dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }

    function sortPrices(Request $request) {

        $priority = 1; 
        foreach ($request->item as $item) {
            $price = $this->model_instance::find($item);
            $price->priority = $priority;
            $price->update();
            $priority++;
        }
        return response()->json($this->success_message);
    }

    public function destroy(Request $request, $id)
    {
        has_access('delete_halls');
        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }

        }

        return redirect()->route($this->index_route);
    }
}
