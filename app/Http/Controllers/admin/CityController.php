<?php

namespace App\Http\Controllers\admin;

use App\models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CityController extends Controller
{
    private $index_view;
    private $create_view;
    private $edit_view;
    private $show_view;
    private $index_route;
    private $model_instance;
    private $success_message;
    private $error_message;
    private $update_success_message;
    private $update_error_message;

    public function __construct()
    {
        $this->index_view = 'admin.cities.index';
        $this->create_view = 'admin.cities.create';
        $this->show_view = 'admin.cities.show';
        $this->edit_view = 'admin.cities.edit';
        $this->index_route = 'admin.cities.index';
        $this->success_message = trans('admin.created_successfully');
        $this->update_success_message = trans('admin.update_created_successfully');
        $this->error_message = trans('admin.fail_while_create');
        $this->update_error_message = trans('admin.fail_while_update');
        $this->model_instance = City::class;
    }

    private function StoreValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'location' => 'required|string',
            'zoom'=>'nullable'
        ];
    }

    private function UpdateValidationRules()
    {
        return [
            'name' => 'required|string|min:3|max:200',
            'location' => 'required|string',
            'zoom'=>'nullable'
        ];
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        has_access('show_cities');
        $cities = $this->model_instance::all();
        return view($this->index_view, compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        has_access('create_cities');
        return view($this->create_view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        has_access('create_cities');
        $validated_data = $request->validate($this->StoreValidationRules());


        try {
            DB::beginTransaction();
            $this->model_instance::create($validated_data);

            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            //dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        has_access('update_cities');
        $city = City::find($id);
        return view($this->edit_view, compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        has_access('update_cities');
        $validated_data = $request->validate($this->UpdateValidationRules());
        try {
            DB::beginTransaction();
            $updated_instance = $this->model_instance::find($id);
            $updated_instance->update($validated_data);

            DB::commit();
            return redirect()->route($this->index_route)->with('success', $this->success_message);
        } catch (\Exception $ex) {
            DB::rollBack();
            dd($ex->getMessage());
            Log::error($ex->getMessage());
            return redirect()->route($this->index_route)->with('error', $this->error_message);
        }
    }

    public function destroy(Request $request, $id)
    {
        has_access('delete_cities');
        if ($request->ajax()) {
            $deleted = $this->model_instance::findOrFail($id)->delete();
            if ($deleted) {
                return response()->json(['status' => 'success', 'message' => 'deleted_successfully']);
            } else {
                return response()->json(['status' => 'fail', 'message' => 'fail_while_delete']);
            }

        }

        return redirect()->route($this->index_route);
    }


}
