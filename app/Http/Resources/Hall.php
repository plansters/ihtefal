<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class Hall extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $objectArray =  parent::toArray($request);

        $objectArray = Arr::except($objectArray,$this->api_hidden);

        $additional_data = [
            'gallery' => $this->media()->get()->toArray(),
            'category' => new Category($this->category),
            'extras' => $this->extras()->with('categoryExtra:id,field_name,priority')->orderby('category_extra_id')->get(),
            'city' => $this->city()->first()->toArray(),
            'prices' => $this->prices()->get()

        ];

        return array_merge($objectArray,$additional_data);
    }
}
