<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Reservation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $objectArray =  parent::toArray($request);
        $additional_data = [
            'hall' => new Hall(\App\models\Hall::find($this->hall_id)) ?? null
        ];

        return array_merge($objectArray,$additional_data);
    }
}
