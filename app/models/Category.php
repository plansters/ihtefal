<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

    public function extras() {
        return $this->hasMany('App\models\CategoryExtra');
    }



public function updateRelations()
{
    //dd(request()->extras);
    if (request()->has('extras') && !empty(request()->extras)) {
        //dd(request()->extras);
        foreach (request()->extras as $extra) {
            if (empty($extra['id'])) {
                $insert = [
                    'category_id' => $this->id,
                    'field_name' => $extra['title'],
                    'field_type' => $extra['type'],
                    'field_values' => isset($extrvaluesa['']) ? implode(',', $extra['values']) : null,
                ];
                CategoryExtra::create($insert);
            } else {
                $category_extra = CategoryExtra::find($extra['id']);
                $update = [
                    'category_id' => $this->id,
                    'field_name' => $extra['title'],
                    'field_type' => $extra['type'],
                    'field_values' => isset($extra['values']) ? implode(',', $extra['values']) : null,
                ];
                $category_extra->update($update);
            }

        }
    }
}

    public static function boot()
    {
        parent::boot();
        static::created(function($category)
        {
            //dd(request()->extras);
            if (request()->has('extras') && !empty(request()->extras)) {
                foreach (request()->extras as $extra) {
                    $insert = [
                        'category_id' => $category->id,
                        'field_name' => $extra['title'],
                        'field_type' => $extra['type'],
                        'field_values' => isset($extra['values']) ? implode(',', $extra['values']) : null,
                    ];
                    CategoryExtra::create($insert);
                }
            }
        });


        static::deleting(function($category)
        {
            CategoryExtra::where('category_id', $category->id)->delete();
        });
    }
}
