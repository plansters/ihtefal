<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = 'ads';
    protected $guarded = ["id","image","created_at","updated_at", "hall_id"];
    protected $hidden = ["image_path"];

    public function get_hall()
    {
        if($this->type == "hall")
            return Hall::findOrFail($this->item_id);
        else
            return false;
    }
}
