<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HallMedia extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function($media)
        {
            hl_deleteFile($media->media_path);
        });
    }
}
