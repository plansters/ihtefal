<?php

namespace App\models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use PHPZen\LaravelRbac\Traits\Rbac;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use HasApiTokens,Notifiable,Rbac;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','password','created_at','updated_at', 'password_confirmation', 'user_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\models\Role');
    }

    public function reservations()
    {
        return $this->hasMany('App\models\Reservation');
    }

    public function userActiveReservations($id)
    {
        return $this->hasMany('App\models\Reservation')->whereIn('hall_id', function($query){
            $query->select('id')
            ->from(with(new Hall)->getTable());
        })->orderBy('id','desc');
    }
    public function halls()
    {
        return $this->hasMany('App\models\Hall');
    }

    public function activities()
    {
        return $this->hasMany('App\models\UserActivity');
    }

    public function inquiries()
    {
        return $this->hasMany('App\models\Inquiry');
    }

    public function checkUserRole($role)
    {
        return $this->role == $role;
    }


    public static function boot()
    {
        parent::boot();

        static::created(function($user)
        {
            if (request()->has('roles')) {
                foreach (request()->roles as $role_id) {
                    $role = Role::find($role_id);
                    $user->roles()->attach($role);
                }
            }

        });

        static::updated(function($user)
        {

        });

        static::deleting(function($user)
        {

        });
    }



    public function sendNotification($title,$message)
    {

        if($this->app_token)
        {
            $notification = [
                'title' => $title,
                'body' => $message,
                'priority' => 'high'
            ];
            SendNotifications($notification,[$this->app_token]);
        }

        return true;
    }

    public static function sendPublicNotifications($notification)
    {
        // $users_tokens = self::whereNotNull('app_token')->pluck('app_token')->toArray();
        $visitors_tokens = DB::table('app_visitors')->distinct('token')->pluck('token')->toArray();
// dd($visitors_tokens);

        SendNotifications($notification,$visitors_tokens);

    }
}
