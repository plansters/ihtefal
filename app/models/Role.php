<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function users () {
        return $this->hasMany('App\models\User');
    }

    public function permissions () {
        return $this->belongsToMany('App\models\Permission');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($role)
        {
            $role->permissions()->sync(request()->permissions);
        });

        static::updating(function($user)
        {

        });

        static::deleting(function($user)
        {

        });
    }
}
