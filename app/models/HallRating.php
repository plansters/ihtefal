<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HallRating extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

    public function user() {
        return $this->belongsTo('App\models\User');
    }
}
