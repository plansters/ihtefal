<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

    public function hall () {
        return $this->belongsTo('App\models\Hall');
    }
}
