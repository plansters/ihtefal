<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class CategoryExtra extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

}
