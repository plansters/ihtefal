<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HallPrice extends Model
{
    public $guarded = ['id', 'created_at', 'updated_at', 'hijri_from', 'hijri_to'];
    protected $appends = ['hijri_from', 'hijri_to'];

    public function getHijriFromAttribute()
    {
        return convertToHijri($this->from_date);
    }

    public function getHijriToAttribute()
    {
        return convertToHijri($this->to_date);
    }
}
