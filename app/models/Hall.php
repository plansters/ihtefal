<?php

namespace App\models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use URL;
class Hall extends Model
{
    protected $guarded = ['id','created_at','updated_at','image'];
    protected $appends = ['image_url', 'show_url', 'edit_url', 'delete_url', 'reservations_url', 'active_url','lat', 'is_reserved','lan'];

    public $api_hidden = ['show_url','edit_url','delete_url','image_path','reservations_url','active_url','map_location'];

    public function getTotalPriceAttribute($attribute)
    { 
        return $this->get_current_price($attribute);
    }

    public function getIsReservedAttribute($attribute)
    {
        return $this->isReserved($attribute);
    }

    public function getLatAttribute()
    {
        return $this->getMapCoordinates() ? current($this->getMapCoordinates()) : null;
    }
    public function getLanAttribute()
    {
        return $this->getMapCoordinates() ? getLastElement($this->getMapCoordinates()) : null;
    }

    public function getImageUrlAttribute()
    {
        return url('/uploads/'.$this->image_path);
    }

    public function getShowUrlAttribute()
    {
        return route('admin.halls.show', $this->id);
    }

    public function getActiveUrlAttribute()
    {
        return route('admin.halls.active', $this->id);
    }

    public function getReservationsUrlAttribute()
    {
        return route('admin.halls.reservations', $this->id);
    }

    public function getEditUrlAttribute()
    {
        return route('admin.halls.edit', $this->id);
    }

    public function getDeleteUrlAttribute()
    {
        return route('admin.halls.destroy', $this->id);
    }

    public function category()
    {
        return $this->belongsTo('App\models\Category');
    }

    public function prices()
    {
        return $this->hasMany('App\models\HallPrice')->orderBy('priority');
    }

    public function user()
    {
        return $this->belongsTo('App\models\User');
    }

    public function city()
    {
        return $this->belongsTo('App\models\City');
    }

    public function media()
    {
        return $this->hasMany('App\models\HallMedia');
    }

    public function extras()
    {
        return $this->hasMany('App\models\HallExtra');
    } 

    public function rating()
    {
        return $this->hasMany('App\models\HallRating');
    }

    public function reservations()
    {
        return $this->hasMany('App\models\Reservation');
    }

    public function isReserved() {
        if (is_array(request('excepted_halls_ids'))) {
            if (in_array($this->id, request('excepted_halls_ids'))) {
                return true;
            }
        }
        return false;
    }

    public function get_current_price($total_price, $date = null)
    {
        // dd($date);
        $prices = $this->prices()->get();
        $now = request()->gregorian_date ? new Carbon(request()->gregorian_date) : "";
        if ($date) {
            $now = new Carbon($date);
        } 
        if ($now) {
            // dd($date);
            $day = get_day($now->dayOfWeek);
            foreach ($prices as $price)
            {
                $price_type = $price->price_type;

                if($price->days)
                {
                    $price_days = explode(',',$price->days) ?? null ;
                    if($price_days)
                        if(in_array($day,$price_days))
                        {
                            
                            if (!$price->from_date) {
                                return $price->price;
                                break;
                            } else {
                                if($now->gte(new Carbon($price->from_date)) && $now->lte(new Carbon($price->to_date)))
                                {
                                    
                                    return $price->price; ////
                                    
                                }
                            }

                        }
                        
                }
                else
                {
                    
                    if($now->gte(new Carbon($price->from_date)) && $now->lte(new Carbon($price->to_date)))
                    {
                        
                        return $price->price;
                    }
                }
            }
        }
        return $total_price;
    }

    public function getMapCoordinates()
    {
        $location = $this->map_location;
        $coordinates = explode(',',$location) ?? false;
        if($coordinates)
            return $coordinates ;

        return [];
    }

    public function updateExtra($request) {
      
        HallExtra::where('hall_id', $this->id)->delete();
        if ($request->has('extra') && !empty($request->extra)) {
            foreach ($request->extra as $key => $value) {   
                $insert = [
                    'hall_id' => $this->id,
                    'category_extra_id' => $key,
                    'value' => $value
                ];
                HallExtra::create($insert);

            }
        }

        if ($request->has('album') && !empty($request->album)) {
            foreach ($request->album as $image) {

                $image_data=Image::make($image->getRealPath())->encode('jpg');
                if($image_data->filesize() >1000000){

                    $image_data->resize(1920, 1080);
                } 
                $hash = time().md5($image_data->__toString());
                $target = public_path('uploads/hall_album/' . $hash.'.jpg');
                $image_data->save($target);
                $url = 'hall_album/' . $hash.'.jpg';
                

                $insert= [
                    'hall_id' => $this->id,
                    'media_path' => $url,
                    'media_url' => URL::to('/').'/uploads/'.$url,
                    'media_type' => 'image',
                ];

              $x=  HallMedia::create($insert);
            }
        }
    }

    public static function boot()
    {
        parent::boot();
        static::created(function($hall)
        {
            //dd(request()->extra);
            if (request()->has('extra') && !empty(request()->extra)) {
                foreach (request()->extra as $key => $value) {
                    $insert = [
                        'hall_id' => $hall->id,
                        'category_extra_id' => $key,
                        'value' => $value
                    ];
                    HallExtra::create($insert);
                }
            }

            // if (request()->has('album') && !empty(request()->album)) {
            //     foreach (request()->album as $image) {
            //         //dd(\File::extension($image));
            //         $image_data = hl_uploadFileTo($image, 'hall_album');
            //         $insert= [
            //             'hall_id' => $hall->id,
            //             'media_path' => $image_data['media_path'],
            //             'media_url' => $image_data['media_url'],
            //             'media_type' => 'image',
            //         ];

            //         HallMedia::create($insert);
            //     }
            // }
        });
        static::deleting(function($hall)
        {
            HallExtra::where('hall_id', $hall->id)->delete();
            foreach ($hall->media as $media) {
                hl_deleteFile($media->media_path);
            }
            HallMedia::where('hall_id', $hall->id)->delete();
        });
    }
}
