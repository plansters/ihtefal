<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use PHPZen\LaravelRbac\Traits\Rbac;
use App\models\PartnerBranch;
use App\models\PartnerProduct;

class Partner extends Model
{
    use Notifiable,Rbac; 
    protected $guarded = ['id','created_at','updated_at', 'image'];
    protected $hidden = [
        'password',
    ];


    public function barnches()
    {
        return $this->hasMany('App\models\PartnerBranch');
    }

    public function products()
    {
        return $this->hasMany('App\models\PartnerProduct');
    }
}
  