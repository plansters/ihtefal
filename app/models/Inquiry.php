<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function user () {
        return $this->belongsTo('App\models\User');
    }

    public function admin () {
        return $this->belongsTo('App\models\User', 'admin_id');
    }
}
