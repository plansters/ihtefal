<?php

namespace App\models;
use App\models\Partner;
use Illuminate\Database\Eloquent\Model;

class PartnerProduct extends Model
{
    protected $fillable = ['name','price'];

    public function partner()
    {
        return $this->belongsTo('App\models\Partner');
    }
}
