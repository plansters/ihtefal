<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $guarded = ['id','created_at','updated_at'];
    protected $appends = ['ar_status'];

    public function getArStatusAttribute() {
        if ($this->status == 'pending') {
            return 'غير مؤكد';
        } elseif($this->status == 'approved') {
            return 'مؤكد';
        } elseif($this->status == 'cancelled') {
            return 'ملغى';
        } else {
            return 'مكتمل';
        }
    }
 
    public function hall () {
        return $this->belongsTo('App\models\Hall');

    }

    public function palaceHalls () {
        return $this->hall()->where('category_id',2);
    } 
    public function restHalls () {
        return $this->hall()->where('category_id',4);
    } 
    public function hotelsHalls() {
        return $this->hall()->where('category_id',1);
    } 

    public function user () {
        return $this->belongsTo('App\models\User');
    }

    public function coupon () {
        return $this->belongsTo('App\models\Coupon');
    }
}
