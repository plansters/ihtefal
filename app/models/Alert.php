<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $guarded = ['id','created_at','updated_at'];
}
