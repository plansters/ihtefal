<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $guarded = ['id', 'created_at', 'updated'];
}
