<?php

namespace App\models;
use App\models\Partner;

use Illuminate\Database\Eloquent\Model;

class PartnerBranch extends Model
{
    public function partner()
    {
        return $this->belongsTo('App\models\Partner');
    }
}
