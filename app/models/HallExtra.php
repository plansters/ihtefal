<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class HallExtra extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function categoryExtra()
    {
        return $this->belongsTo('App\models\CategoryExtra');
    }
}
