<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class City extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

    protected $appends = ['lat', 'lon'];


    public function getLatAttribute()
    {
        return $this->getMapCoordinates() ? current($this->getMapCoordinates()) : null;
    }
    public function getLonAttribute()
    {
        return $this->getMapCoordinates() ? getLastElement($this->getMapCoordinates()) : null;
    }

    public function getMapCoordinates()
    {
        $location = $this->location ?? '24.7104948,46.6797144';
        $coordinates = explode(',',$location) ?? false;
        if($coordinates)
            return $coordinates ;

        return [];
    }

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
        });
    }
}
