<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use GeniusTS\HijriDate\Hijri;

/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 1/13/2020
 * Time: 2:40 PM
 */

function get_file_path($folder_name="", $file_name="") {
    return storage_path("app/public/$folder_name" . ($file_name ? "/$file_name" : ""));
}

function get_file_url($folder_name="", $file_name="") {
    return url("storage/$folder_name" . ($file_name ? "/$file_name" : ""));
}

function hl_uploadFileTo($file,$path)
{
// dd($path);
    $file_path = Storage::disk('public_images')->put($path, $file);

    return [
        'media_path' => $file_path,
        'media_url' => getMediaUrl($file_path)
    ];


}

function hl_deleteFile($file_path)
{
    return Storage::disk('public_images')->delete($file_path);
}


function getMediaUrl($path)
{
    return url('/uploads/'.$path);
}

function checkCurrentItem($parameter) {
    if(request()->is("*/$parameter") || request()->is("*/$parameter/*") || request()->is("$parameter"))
        return true;

    return false;
}

function has_access ($slugs) {
    if(!Auth::check()){
        return abort(404);
    }
    if (!Auth::user()->canDo($slugs)) {
        abort(403);
    }
    return true;
}

function convertToGregorian($date) {
    if (!$date) {
        return "";
    }
    $year = date('Y', strtotime($date));
    $month = date('m', strtotime($date));
    $day = date('d', strtotime($date));
    return Hijri::convertToGregorian($day, $month, $year);
}

function convertToHijri($date) {
    if (!$date) {
        return "";
    }
    $hijri = Hijri::convertToHijri($date);
    $day = $hijri->day;
    $month = $hijri->month;
    $year = $hijri->year;
    return "$year-$month-$day";
}

function formatValidationMessages($errors)
{
    return Arr::flatten($errors);
}

function get_day($day_of_week)
{

   $days =  [
        '1' => 'monday',
        '2' => 'tuesday',
        '3' => 'wednesday',
        '4' => 'thursday',
        '5' => 'friday',
        '6' => 'saturday',
        '7' => 'sunday',
        '0' => 'sunday', 
    ];
   return $days[$day_of_week] ?? '';
}

function getRequestOrderKey($str)
{
    return substr($str, strrpos($str, '_') + 1);
}

function getLastElement($array)
{
    return $array[array_key_last($array)];
}


function formatDateAsName($date)
{
    $d    = new DateTime($date);
    return $d->format('l');
}


function SendNotifications($notification,$tokens)
{
    $API_ACCESS_KEY = "AAAAQjX8Nz4:APA91bFq_4AYhmjv9wDwJGKVcSjbtG6QKZVT8aplo8JVg1b0ZNIlptPu6n_JV1oh9U_skl6icCzioQ3Gl1AI4Q4yUfOWd3-NP_DOEsncByQVyL3mi63b1_94-Bw7dgL_8VDVkyCLDeeu";
    $url = 'https://fcm.googleapis.com/fcm/send';

    // prepare the message
    $fields = array(
        'registration_ids' => $tokens,
        'notification' => $notification

    );
    $headers = array(
        "Authorization: key=$API_ACCESS_KEY",
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL,$url);
    curl_setopt( $ch,CURLOPT_POST,true);
    curl_setopt( $ch,CURLOPT_HTTPHEADER,$headers);
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER,false);
    curl_setopt( $ch,CURLOPT_POSTFIELDS,json_encode($fields,true));
    $result = curl_exec($ch);
    curl_close($ch);
    return  $result; 
}

function notify_admin($user_id, $type, $type_id, $hall_id = null) {
    $user = \App\models\User::find($user_id);
   if($type != "user"){
    return \App\models\Notification::create([
        'title' => $type == 'inquery' ? 'استفسار جديد' : 'حجز جديد',
        'type' => $type,
        'hall_id' => $hall_id,
        'notification_link' => $type == 'inquery' ? route('admin.inquiries.index') : route('admin.reservations.index'),
        'description' => $type == 'inquery' ? "قام المستخدم $user->email باستفسار جديد برقم #$type_id " : "قام المستخدم $user->email بحجز جديد برقم #$type_id",
    ]);
    }
    else{
        return \App\models\Notification::create([
            'title' => 'مستخدم جديد',
            'type' => $type,
            'hall_id' => null,
            'notification_link' => route('admin.users.index').'?user='.$user->identity,
            'description' => "تمت إضافة حساب جديد ",
        ]);        
    }
} 


function SendSMS($request) {
    $user = "ehtfal"; 
    $password = "123456";
    $sendername = "EHTFAL";
    $text = urlencode($request['message']);
    $to = $request['numbers'];
    $url = "https://www.4jawaly.net/api/sendsms.php?username=$user&password=$password&message=$text&sender=$sendername&unicode=E&Rmduplicated=1&return=json&numbers=$to";
    // 
    // $ret = file_get_contents($url);
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$ret = curl_exec($curl);
curl_close($curl);
    return json_decode ($ret,JSON_UNESCAPED_UNICODE);
} 

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 4; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}