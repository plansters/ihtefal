<?php

return [
    'Errors_En' => [
        '_GENERAL000' => 'General error!',
        '_GENERAL001' => 'Multiple errors!',
        'GENERAL002' => 'Updated Successfully',

        '_AUTH000' => 'Invalid key!',
        '_AUTH001' => 'Not authenticated!',
        '_AUTH002' => 'Not authorized!',
        '_AUTH003' => 'products limit exceeded!',
        '_AUTH004' => 'Admin blocked your account!',

        '_USER000' => 'User not found!',
        '_USER001' => 'User products count is larger than this limit!',
        '_USER002' => 'Invalid username and/or password!',
        '_USER003' => 'Phone already exists!',
        '_USER004' => 'Email already exists!',
        '_USER005' => 'Old password field does not exist!',
        '_USER006' => 'New password field does not exist!',
        '_USER007' => 'Old password is not correct!',
        '_USER008' => 'The password\'s first character must be a letter then any letter or number, and the password\'s length must be between 6 and 15!',
        '_USER009' => 'This product is already in your favorites list!',
        '_USER010' => 'This product is not in your favorites list!',
        'USER011' => 'Your Activation Code is',
        '_USER012' => 'Account Not Verified, new verification code has been sent',

        'DATA001' => 'Entered Data Is Invalid',
        '_DATA002' => 'Hall is Reserved in this date, please select another date.',
        '_DATA003' => 'Reservation Status is not pending.',
        '_DATA004' => 'Invalid Coupon Code',
        '_DATA005' => 'Reservation Not Exist!',
    ],
    'Errors_Ar' => [

        '_GENERAL000' => 'خطأ عام!',
        '_GENERAL001' => 'أخطاء عديدة!',
        'GENERAL002' => 'تم التعديل بنجاح',

        '_AUTH000' => 'المفتاح غير صالح!',
        '_AUTH001' => 'فشل التحقق من الهوية!',
        '_AUTH002' => 'لا يوجد صلاحية!',
        '_AUTH003' => 'لا يمكنك إضافة منتجات جديدة!',
        '_AUTH004' => 'قام المدير بحظر حسابك!',

        '_USER000' => 'هذا المستخدم غير موجود!',
        '_USER001' => 'عدد منتجات المستخدم أكبر من هذا الحد!',
        '_USER002' => 'اسم المستخدم و/أو كلمة المرور غير صحيحين!',
        '_USER003' => 'رقم الهاتف موجود حالياً!',
        '_USER004' => 'البريد الالكتروني موجود حالياً!',
        '_USER005' => 'حقل كلمة المرور القديمة غير موجود!',
        '_USER006' => 'حقل كلمة المرور الجديدة غير موجود!',
        '_USER007' => 'كلمة المرور القديمة غير صحيحة!',
        '_USER008' => 'يجب أن تبدأ كلمة المرور بحرف لاتيني و بعدها أي حرف أو رقم, و يجب أن يكون طول كلمة المرور بين 6 و 15!',
        '_USER009' => 'هذه المنتج موجودة حالياً في قائمة المفضلة الخاصة بك!',
        '_USER010' => 'هذه المنتج غير موجودة في قائمة المفضلة الخاصة بك!',
        'USER011' => 'رمز تفعيل حسابك هو ',
        '_USER012' => 'حسابك غير مفعل, تم ارسال رمز تفعيل جديد',

        'DATA001' => 'البيانات المدخلة غير صحيحة',
        '_DATA002' => 'يوجد حجز اخر بهذا التاريخ',
        '_DATA003' => 'يمكن الغاء الحجز في حال كانت حالته غير مؤكد فقط',
        '_DATA004' => 'رقم الكوبون غير صحيح',
        '_DATA005' => 'الحجز غير موجود!',

    ],
];
