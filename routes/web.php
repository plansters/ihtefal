<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', 'Auth\LoginController@loginForm');*/

Auth::routes();
// Route::get('/dd', function () {
//     $img = Image::make(public_path('uploads/ads/s.jpeg')); //1000000

//     return $img->filesize();
// });
Route::get('/', function () {
    return redirect(route('admin.halls.index'));
})->name('home');
Route::get('/home', function () {
    return redirect(route('admin.halls.favorite'));
});

Route::group( [ 'prefix' => 'admin' , 'namespace' => 'admin' , 'as' => 'admin.'], function()
{
    Route::get('/halls/get/pendings/ajax', 'HallController@getPendigHallsAjax')->name('get.pending.halls.ajax');
    Route::group( ['middleware' => ['auth']], function() {
        Route::view('/dashboard', 'admin/dashboard')->name('dashboard');
        Route::post('/halls/rate/hall', 'HallController@rateHall')->name('halls.rate');
        Route::post('/halls/rate/active/{id}', 'HallController@activeHall')->name('halls.active');
        Route::post('/halls/rate/note', 'HallController@rateNote')->name('rate.note');
        Route::get('/halls/reservations/{id}', 'HallController@reservations')->name('halls.reservations');
        Route::get('/halls/get/pendings', 'HallController@pendingHalls')->name('get.pending.halls');
        Route::get('/halls/list', 'HallController@listHalls')->name('halls.list');
        Route::resource('/roles', 'RoleController');
        Route::resource('/users', 'UserController');
        Route::get('/users/search/all', 'UserController@search')->name('search.user');
        Route::post('/users/status', 'UserController@changeStatus')->name('users.status');
        Route::get('/profile/edit', 'UserController@editProfile')->name('profile.edit');
        Route::put('/profile/update', 'UserController@updateProfile')->name('profile.update');
        Route::get('/user/get/reservations', 'UserController@getReservations')->name('user.reservations');
        Route::resource('/categories', 'CategoryController');
        Route::get('/categories/extra/get', 'CategoryController@getExtra')->name('categories.extra');
        Route::resource('/category_extras', 'CategoryExtraController');
        Route::resource('/cities', 'CityController');
        Route::resource('/coupons', 'CouponController');
        Route::resource('/reservations', 'ReservationController');
        Route::post('/reservations/cancel/{id}', 'ReservationController@cancelReservation')->name('reservations.cancel');
        Route::post('/reservations/status/{id}', 'ReservationController@reservationStatus')->name('reservations.status');
        Route::resource('/media', 'HallMediaController');
        Route::resource('/inquiries', 'InquiryController');
        Route::resource('/ads', 'AdsController');
        Route::resource('/prices', 'HallPriceController');
        Route::post('/prices/sort/hall', 'HallPriceController@sortPrices')->name('prices.sort');
        Route::get('/halls/search', 'HallController@search')->name('halls.search');
        Route::get('/calendar', 'CalendarController@index')->name('calendar.index');

        Route::post('calendar/get/events','CalendarController@getEvents')->name('get.events');
        Route::post('calendar/create','CalendarController@create')->name('event.add');
        Route::post('calendar/resize','CalendarController@resize')->name('event.resize');
        Route::post('calendar/update','CalendarController@update')->name('event.update');
        Route::post('calendar/delete','CalendarController@destroy')->name('event.destroy');


        Route::get('/notifications','NotificationController@getNew')->name('notifications.new');


        Route::resource('/alerts','AlertController');
        Route::post('/alert/send','AlertController@send')->name('alerts.send');

        Route::get('/hall/add/favorite/{id}', 'HallController@addFavorite')->name('hall.add.favorite');
        Route::get('/hall/remove/favorite/{id}', 'HallController@removeFavorite')->name('hall.remove.favorite');
        Route::get('/hall/favorite/show', 'HallController@favorite')->name('halls.favorite');
        Route::get('/halls/get/favorite', 'HallController@getFavorite')->name('halls.get.favorite');
        Route::post('/hall/get/reserved', 'HallController@getHallReserved')->name('hall.reserved');
        Route::get('/myHalls', 'HallController@getMyHalls');
    });
    Route::get('/halls/get/maps', 'HallController@hallsOnMap')->name('get.halls.map');

    Route::resource('/halls', 'HallController');
    Route::get('/halls/get/ajax', 'HallController@getHallsAjax')->name('get.halls');
    Route::get('/halls/search/all', 'HallController@search')->name('search.hall');
    Route::resource('/partners', 'PartnerController');
    Route::get('/partners/main/{id}', 'PartnerController@main');
    Route::get('/partners/getSalesMen/{id}', 'PartnerController@getSalesMen');

    Route::get('/partners/branches/{id}', 'PartnerController@branches');
    Route::post('/partners/addBranch', 'PartnerController@addBranch');
    Route::post('/partners/updateBranch', 'PartnerController@updateBranch');
    Route::get('/partners/coupons/{id}', 'PartnerController@coupons');
    // Route::get('/partners/coupons/{id}/details', 'PartnerController@couponDetails');
    Route::get('/partners/branch/{id}/details', 'PartnerController@branchDetails');
    Route::post('/partners/product/edit', 'PartnerController@editProduct');
    Route::post('/partners/product/updatediscountPerProduct', 'PartnerController@updatediscountPerProduct');
    Route::post('/partners/product/add', 'PartnerController@addProd');
    Route::post('/partnersCouponeAdd', 'PartnerController@partnersCouponeAdd');
    Route::post('/partners/product/update/existProduct', 'PartnerController@editExistProduct');
    // Route::get('/partners/discount/edit', 'PartnerController@eDiscountType');
    Route::get('/partners/discount/edit', 'PartnerController@edit');
    Route::get('/partners/product/remove/{id}/{p}', 'PartnerController@removeProduct');
    Route::get('/partners/Branch/addNew/{id}', 'PartnerController@addNew');
    Route::get('/partners/Branch/edit/{id}/{b_id}', 'PartnerController@editBranch');
    Route::get('/partners/product/update/{id}/{p}', 'PartnerController@goToEditProductPage');
    Route::get('/partners/salesman/store/', 'PartnerController@addSalesMan');

});

Route::post('/test', 'admin\HallController@store');
Route::get('/user-guide',function () {
    return view('user-guide');
});
Route::get('/contact-us', 'admin\UserController@contact_us');
