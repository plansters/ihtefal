<?php

use Illuminate\Http\Request;
use App\models\Hall;
use App\models\User;
use App\models\Partner;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['namespace' => 'api'], function () {

    //Route::apiResource('halls','HallController');
    Route::post('halls', 'HallController@index');
    Route::get('halls', 'HallController@index');

    Route::get('halls/{id}', 'HallController@show');
    Route::get('resizeAllImages', 'HallController@resizeAllImages');

    Route::apiResource('cities', 'CityController');
    Route::apiResource('favorites', 'FavoriteController');
    Route::apiResource('partners', 'PartnerController');
    Route::apiResource('categories', 'CategoryController');
    Route::apiResource('reservations', 'ReservationController');
    Route::post('/reservation/cancel/{id}', 'ReservationController@cancelReservation');
    Route::get('partner/priority/update', 'PartnerController@changePriority');
    Route::post('partner/discounType/change', 'PartnerController@changeDiscType');
    Route::post('partner/city/change', 'PartnerController@changeCity');
    Route::post('partner/activateCoupone', 'PartnerController@activateCoupone');
    Route::post('partner/activatePartner', 'PartnerController@activatePartner');
    Route::post('partner/couponInvoice', 'PartnerController@couponInvoice');
    Route::post('partner/branch/delete', 'PartnerController@deleteBranch');
    Route::post('auth/login', 'UserController@login');
    Route::post('auth/partner/login', 'PartnerController@login');
    Route::post('auth/forgetPassword', 'UserController@resetPassword');
    Route::post('auth/register', 'UserController@register');
    Route::post('/sms/send', 'UserController@sendSms');
    Route::post('user/inquiry', 'UserController@inquiry');
    Route::get('user/{id}/inquiries', 'UserController@myInquiries');
    Route::get('user/new/answered/{id}', 'UserController@newAnsweredInquiries');
    Route::post('user/get/favorite', 'UserController@getFavorite');
    Route::post('update/profile', 'UserController@updateProfile');
    Route::get('ads', 'AdsController@index');
    Route::get('user/reservations/{id}', 'UserController@getReservations');
    Route::get('/user_guide', 'UserController@user_guide');
    Route::post('users/auth/token', 'UserController@setToken');
    Route::post('/halls/rate/hall', 'HallController@rateHall')->name('halls.rate');
    Route::post('/halls/rate/note', 'HallController@rateNote')->name('rate.note');
    Route::get('/halls/pending/remove', 'HallController@resetPendingHalls');
    Route::get(
        'convert',
        function (Request $request) {
            if ($request->to == 'hijri') {
                return response()->json(['status' => 'success', 'data' => convertToHijri($request->date)]);
            } else {
                return response()->json(['status' => 'success', 'data' => convertToGregorian($request->date)]);
            }
        }


    );
    Route::get(
        'getHallsWithoutImages',
        function (Request $request) {
            $ids = [];
            $users = User::where('status', 'active')->where('role', 'vendor')->orWhere('role', 'admin')->pluck('id');
            $halls = Hall::where('status', 'published')->whereIn('user_id', $users)->get();
            foreach ($halls as $hall) {
                $path_to_file = public_path('uploads/'. $hall->image_path);
                if (!file_exists($path_to_file)) {
                    array_push($ids, $hall->name);
                }
            }
            dd([$halls->count(),$ids]);
            return ([$halls->count(),$ids]);
        }


    );

    Route::get(
        'getHallsWithImages',
        function (Request $request) {
            $ids = [];
            $users = User::where('status', 'active')->where('role', 'vendor')->orWhere('role', 'admin')->pluck('id');
            $halls = Hall::where('status', 'published')->whereIn('user_id', $users)->get();
            foreach ($halls as $hall) {
                $path_to_file = public_path('uploads/'. $hall->image_path);
                if (file_exists($path_to_file)) {
                    array_push($ids, $hall->name);
                }
            }
            dd([$halls->count(),$ids]);
            return ([$halls->count(),$ids]);
        }

    );

    Route::get(
        'getPartnersWithoutImages',
        function (Request $request) {
            $ids = [];
            $Partners = Partner::get();
            foreach ($Partners as $Partner) {
                $path_to_file = public_path('uploads/'. $Partner->avatar_path);
                if (!file_exists($path_to_file)) {
                    array_push($ids, $Partner->name);
                }
            }
            dd([$Partner->count(),$ids]);
        }


    );



    // start admin apis group
    Route::prefix('admin')->group(function () {

        Route::group(['middleware' => 'authorization'], function () { //CallMiddleware

            Route::group(['middleware' => 'auth:api'], function () {
                //Ads APis
                Route::get('ads', 'admin\AdsController@index');
                Route::post('ads/Create', 'admin\AdsController@CreateNewAd');
                Route::post('ads/update', 'admin\AdsController@update');
                Route::post('ads/delete/{id}', 'admin\AdsController@delete');

                //coupons Apis 
                Route::get('coupons', 'admin\ReservationCouponController@index');
                Route::post('coupons/create', 'admin\ReservationCouponController@store');
                Route::post('coupons/{ids}/update', 'admin\ReservationCouponController@update');
                Route::post('coupons/delete/{id}', 'admin\ReservationCouponController@destroy');

                // Alerts Apis
                Route::get('alerts', 'admin\AlertController@index');
                Route::post('alerts/Create', 'admin\AlertController@store');
                Route::post('alerts/delete/{id}', 'admin\AlertController@destroy');
                Route::post('alerts/send', 'admin\AlertController@send');


                // Alerts Apis
                Route::get('categories', 'admin\CategoryController@index');
                Route::post('categories/Create', 'admin\CategoryController@store');
                Route::post('categories/delete/{id}', 'admin\CategoryController@destroy');
                Route::post('categories/update/{id}', 'admin\CategoryController@update');
                Route::get('/categories/extra/get/{id}', 'admin\CategoryController@getExtra');


                // Halls Apis
                Route::post('halls', 'admin\HallController@index');
                Route::post('halls/getPendigHalls', 'admin\HallController@getPendigHalls');
                Route::post('getFavoriteHalls', 'admin\UserController@getFavoriteHalls');
                Route::post('halls/Create', 'admin\HallController@store');
                Route::post('halls/Price', 'admin\HallPriceController@store');
                Route::post('halls/Price/Delete/{id}', 'admin\HallPriceController@Destroy');
                Route::post('halls/Price/Sort', 'admin\HallPriceController@sortPrices');
                Route::post('halls/delete/{id}', 'admin\HallController@destroy');
                Route::post('halls/update/{id}', 'admin\HallController@update');

                // cities Apis
                Route::get('cities', 'admin\CityController@index');
                Route::post('cities/Create', 'admin\CityController@store');
                Route::post('cities/delete/{id}', 'admin\CityController@destroy');
                Route::post('cities/update/{id}', 'admin\CityController@update');

                // Partner Apis
                Route::get('partners', 'admin\PartnerController@index');
                Route::post('partners/delete/{id}', 'admin\PartnerController@destroy');
                Route::post('partners/update/{id}', 'admin\PartnerController@update');
                Route::post('partners/priority/change', 'admin\PartnerController@changePriority');
                Route::post('partners/activate', 'admin\PartnerController@activatePartner');
                Route::post('partners/coupons/activate', 'admin\PartnerController@activateCoupone');

                //reservation APis
                Route::post('reservation', 'admin\ReservationController@index');
                Route::post('reservation/Create', 'admin\ReservationController@store');
                Route::post('reservation/update', 'admin\ReservationController@update');
                Route::post('reservation/delete/{id}', 'admin\ReservationController@destroy');
                Route::post('reservations/cancel/{id}', 'admin\ReservationController@cancelReservation');
                Route::post('reservations/status/{id}', 'admin\ReservationController@reservationStatus');


                //Partner APis
                Route::get('Partner/branches', 'admin\PartnerController@main');
                Route::post('Partner/branch/store', 'admin\PartnerController@addBranch');
                Route::post('Partner/getCoupons', 'admin\PartnerController@getCoupons');
                Route::get('Partner/myStore', 'admin\PartnerController@myStore');
                Route::get('Partner/getSales', 'admin\PartnerController@getSales');
                Route::post('Partner/branch/delete', 'admin\PartnerController@deleteBranch');
                Route::post('Partner/addSalesMan', 'admin\PartnerController@addSalesMan');

                Route::post('Partner/branch/{id}/update', 'admin\PartnerController@updateBranch');

                Route::get('Partner/services', 'admin\PartnerController@services');
                Route::post('Partner/service/store', 'admin\PartnerController@addProd');
                Route::post('Partner/service/{id}/delete', 'admin\PartnerController@removeProduct');
                Route::post('Partner/service/{id}/update', 'admin\PartnerController@editExistProduct');
                Route::post('Partner/discount/edit', 'admin\PartnerController@edit');


                Route::post('Partner/city/change', 'admin\PartnerController@changeCity');
                Route::post('Partner/DiscType/change', 'admin\PartnerController@changeDiscType');
                Route::post('Partner/coupon_invoice/change', 'admin\PartnerController@couponInvoice');
                Route::post('/partners/product/updatediscountPerProduct', 'admin\PartnerController@updatediscountPerProduct');


                //Users APis
                Route::post('Users', 'admin\UserController@index');
                Route::get('Users/getRoles', 'admin\UserController@roles');
                Route::get('Users/getAllPermissions', 'admin\UserController@getPermissions');
                Route::get('Roles/{id}/permissions', 'admin\UserController@getRolesPermissions');
                Route::post('Roles/{id}/updatePermissions', 'admin\UserController@updateRolePermissions');

                Route::post('User/update/{id}', 'admin\UserController@update');
                Route::post('User/destroy/{id}', 'admin\UserController@destroy');
            });
        });
        Route::post('User/store', 'admin\UserController@store');
        Route::post('partners/Create', 'admin\PartnerController@store');
        Route::post('login', 'admin\UserController@AdminLogin');
    });

    //


});
