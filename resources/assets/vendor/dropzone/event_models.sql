-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2020 at 12:00 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test1`
--

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `title`, `description`, `start`, `end`, `all_day`, `created_at`, `updated_at`) VALUES
(1, 'Valentine\'s Da', NULL, '2020-01-22 07:46:51', '2020-01-22 10:30:00', 0, NULL, '2020-01-22 08:39:55'),
(2, 'Valentine', 'Hello all of you', '2020-01-09 02:00:00', '2020-01-09 02:50:00', NULL, NULL, '2020-01-22 08:50:39'),
(3, 'Something', 'Hello all of you', NULL, NULL, NULL, NULL, '2020-01-22 08:53:47'),
(4, 'Some', 'Hello all of you', '2020-01-16 11:30:00', '2020-01-16 15:30:00', 0, NULL, '2020-01-22 09:35:06'),
(5, 'Somegg', 'Hello all of you', NULL, NULL, NULL, NULL, '2020-01-22 08:56:02'),
(6, 'Helloooo', 'Youuuu are welcome', '2020-01-16 07:00:00', '2020-01-16 08:00:00', 0, NULL, '2020-01-22 09:34:56');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
