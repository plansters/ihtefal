  <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<head>
    {{-- <meta charset="utf-8"> --}}
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title> @yield('title')</title>

    <!-- Scripts -->


    <!-- Fonts --

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Cairo:200,300,400,600,700,900&amp;subset=arabic,latin-ext" rel="stylesheet">
    <!-- Styles -->
{{-- <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="{{ asset('css/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    @if(app()->getLocale() == "ar")
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css" integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If" crossorigin="anonymous">
        <style>
            body {
                font-family: 'Cairo', sans-serif!important;
            }
            #page-top
            {
                direction: rtl !important;
            }

            .page-link {
    display: inline-flex;
}
        </style>
    @endif
    @yield('css-links')

    @yield('styles')
{{-- 
    <link href="{{ asset('css/navs.less') }}" rel="stylesheet"> --}}
</head>
<body id="page-top" >

<div id="wrapper">

    <!-- Sidebar -->
@include('partials.sidebar')
@auth
    @endauth
<!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
                @include('partials.navbar')
                @auth
            @endauth

            @yield('content')


        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; Solutions Time {{ date('Y') }}</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> --}}
    {{-- <script src="{{ asset('js/jquery.plugin.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/jquery.calendars.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/jquery.calendars.plus.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/jquery.calendars.ummalqura.js') }}"></script> --}}
    <script src="{{ asset('js/app2.js') }}"></script>

    @yield('scripts')
    <script>
        function submitForm(formid)
        {
            $('#'+formid).submit();
        }
        $(document).ready(function () {

            var url = '{!! route('admin.notifications.new') !!}';
            @auth
            setInterval(function(){



                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'JSON',

                })
                    .done(function(response) {
                        // console.log("RN");
                        if(response.data.length !== 0)
                        {
                            // alert(response.data);
                            $.each(response.data,function(key,val){
                                var notification_template = `<a class="dropdown-item d-flex align-items-center" style="display: none" href="${val.notification_link}">
                        <div class="mr-3">
                            <div class="icon-circle ${val.type === "order" ? 'bg-warning' : 'bg-primary' }">
                                <i class="fas ${val.type === "order" ? 'fa-shopping-basket' : 'fa-file-alt' } text-white"></i>
                            </div>
                        </div>
                        <div>
                            <div class="small text-gray-500">${val.created_at}</div>
                            <span class="font-weight-bold">${val.title}</span>
                            <span class="badge badge-pill badge-success">جديد</span>
                        </div>
                    </a>`;

                                $('#notification_bar').prepend(notification_template);
                            });

                            const element =  document.querySelector('.alert_bell');
                            element.classList.add('animated', 'swing');

                            element.addEventListener('animationend', function() {
                                element.classList.remove('animated', 'swing')
                            });


                            if(response.has_order)
                            {
                                $('#total_orders').text(response.order_count);

                                const total_div =  document.querySelector('#total_orders_div');
                                if (typeof(total_div) != 'undefined' && total_div != null)
                                {
                                    total_div.classList.add('animated', 'shake');

                                    total_div.addEventListener('animationend', function() {
                                        total_div.classList.remove('animated', 'shake')
                                    })
                                }
                            }




                        }

                        $('#notifications_count').text(response.count);


                    })
                    .fail(function(err) {
                        console.log(err);
                    })
                    .always(function() {
                     //   console.log("complete");
                    });



            }, 4000);

@endauth

        });
    </script>
</body>
</html>
