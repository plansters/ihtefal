<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('halls.create'))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('halls.create')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.halls.store') }}" enctype="multipart/form-data" id="form">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <section id="tabs">
                                        <div class="row">
                                            <div class="col-12 ">
                                                <nav>
                                                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                        <a class="nav-item nav-link active" id="nav-home-tab"
                                                           data-toggle="tab" href="#nav-home" role="tab"
                                                           aria-controls="nav-home"
                                                           aria-selected="true">@lang('halls.data')</a>
                                                        <a class="nav-item nav-link" id="nav-profile-tab"
                                                           data-toggle="tab" href="#nav-profile" role="tab"
                                                           aria-controls="nav-profile"
                                                           aria-selected="false">@lang('halls.category')</a>

                                                    </div>
                                                </nav>
                                                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                                    <div class="tab-pane fade show active " id="nav-home"
                                                         role="tabpanel" aria-labelledby="nav-home-tab">
                                                        <div class="row">
                                                            <div class="col-6">

                                                                <div class="form-group">
                                                                    <label for="hall-name" data-required="yes">@lang('halls.name')</label>
                                                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" id="hall-name" placeholder="">
                                                                </div>

                                                                <!--div class="form-group">
                                                                    <label for="hall-name">@lang('halls.color')</label>
                                                                    <input type="button" class="btn btn-outline-primary btn-sm" style="width: 100%; background-color: #FFF59D" id="color-picker-2" value="Open Picker">
                                                                    <input type="hidden" id="hall-color" name="color" value="">
                                                                </div-->
                                                                <div class="form-row">
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <label for="city-name" data-required="yes">@lang('halls.city')</label>
                                                                            <select id="city-name" name="city_id" class="form-control">
                                                                                @foreach($cities as $city)
                                                                                    <option value="{{ $city->id }}" @if($city->id == old('city_id')) selected @endif>{{ $city->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <label for="neighborhood" data-required="yes">@lang('halls.neighborhood')</label>
                                                                            <input type="text" class="form-control" name="neighborhood" value="{{ old('neighborhood') }}" id="neighborhood" placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="descritipn" data-required="yes">@lang('halls.description')</label>
                                                                    <textarea class="form-control" name="description" id="description" rows="3">{{ old('description') }}</textarea>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="select-preson" data-required="yes">@lang('halls.price')</label>
                                                                    <div class="form-row">
                                                                        <div class="col-6">
                                                                            <select id="select-preson" name="price_type" class="form-control">
                                                                                <option value="per_person"  @if('per_person' == old('price_type')) selected @endif> على الشخص </option>
                                                                                <option value="per_hall" @if('per_hall' == old('price_type')) selected @endif> على كل القاعة </option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            {{-- <label for="total_price" data-required="yes">@lang('halls.price')</label> --}}
                                                                            <input type="text" class="form-control" value="{{ old('total_price') }}" name="total_price" id="price" placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="form-row">
                                                                        <div class="col-6">
                                                                            <label for="capacity-size" data-required="yes">@lang('halls.minimum_count')</label>
                                                                            <input type="text" class="form-control" value="{{ old('min_chair_number') }}" name="min_chair_number" id="capacity-size" placeholder="">
                                                                        </div>
                                                                        <!--div class="col-4">
                                                                            <label for="capacity">@lang('halls.min_capacity')
                                                                               </label>
                                                                            <select id="select-capacity" name="min_capacity" class="form-control">
                                                                                <option value="100" selected>100 </option>
                                                                                <option value="500">500 </option>
                                                                                <option value="1000">1000 </option>
                                                                                <option value="1500">1500 </option>
                                                                            </select>
                                                                        </div-->
                                                                        <div class="col-6">
                                                                            <label for="capacity" data-required="yes">@lang('halls.max_capacity')</label>
                                                                            <input type="number" class="form-control" value="{{ old('max_capacity') }}" name="max_capacity" id="max_capacity">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="hall-title" data-required="yes">@lang('halls.address')</label>
                                                                    <input type="text" class="form-control" value="{{ old('address') }}" name="address" id="hall-title" placeholder="">
                                                                </div>

                                                                <div class="form-group">
                                                                    <input id="myloc" name="map_location" type="text" value="24.7104948,46.6797144" hidden/>
                                                                    <div id="map" style="width: 100%; height: 400px"></div>
                                                                </div>


                                                            </div>
                                                            <div class="col-6">

                                                                <div class="form-group" id="addForm">
                                                                    <label data-required="yes">@lang('halls.main_image')</label>
                                                                    <div class="drop">
                                                                        <div class="cont">
                                                                            <i class="fa fa-upload"></i>
                                                                            <div class="tit">
                                                                                Drag & Drop
                                                                            </div>
                                                                            <div class="desc">
                                                                                your files to Assets, or
                                                                            </div>
                                                                            <div class="browse">
                                                                                click here to browse
                                                                            </div>
                                                                        </div>
                                                                        <input id="file" name="image" type="file" accept="image/*"/>
                                                                        <output id="list"></output>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" id="addForm">
                                                                    <label>@lang('halls.album')</label>
                                                                    <div class="drop">
                                                                        <div class="cont">
                                                                            <i class="fa fa-upload"></i>
                                                                            <div class="tit">
                                                                                Drag & Drop
                                                                            </div>
                                                                            <div class="desc">
                                                                                your files to Assets, or
                                                                            </div>
                                                                            <div class="browse">
                                                                                click here to browse
                                                                            </div>
                                                                        </div>
                                                                        <input id="album" name="album[]" multiple="true" type="file" accept="image/*"/>
                                                                        <output id="album-list"></output>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- ----------------- -->

<a onclick="document.getElementById('nav-profile-tab').click()" class="btn btn-info">التالي</a>
                                                        </div>

                                                    </div>
                                                    <div class="tab-pane fade" id="nav-profile" role="tabpanel"
                                                         aria-labelledby="nav-profile-tab">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="category_id" data-required="yes">@lang('halls.category')</label>
                                                                    <select id="category_id" name="category_id" onchange="getCategoriesExtra()" class="form-control">
                                                                        <option value="" selected>@lang('halls.category')</option>
                                                                        {{-- @foreach($categories as $category)
                                                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                                        @endforeach --}}
                                                        @if (Auth::user()->canDo("create_rest_halls")) 
                                                            <option value="4" >الاستراحات</option>
                                                        @endif
                                                        @if (Auth::user()->canDo("create_palace_halls")) 
                                                            <option value="2">قصور الافراح</option>
                                                        @endif
                                                        @if (Auth::user()->canDo("create_hotels_halls")) 
                                                            <option value="1">قاعات الفنادق</option>
                                                        @endif
                                                                    
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <span id="category-info"></span>
                                                            <br>
                                                            <br>
                                                        </div>
                                                        <a onclick="smbt()" class="btn btn-primary">@lang('admin.create')</a>
     
                                                    </div>

                                                    {{-- <button type="submit" class="btn btn-primary">@lang('admin.create')</button> --}}
                                                </div>

                                            </div>
                                        </div>
<script>
function smbt() {
    if( $("input[name='extra[7]']").val()=='0' ){
    alert("قيمة خصائص التصنيف (عدد الأقسام) لا يمكن أن تكون صفراَ");
}  
else if(!$("#hall-name").val()){
    alert("الرجاء ملئ اسم القاعة");
}
else if(!$("#neighborhood").val()){
    alert("الرجاء ملئ اسم الحي");
}

else if(!$("#description").val()){
    alert("الرجاء ملئ الوصف");
}

else if(!$("#price").val()){
    alert("الرجاء ملئ حقل السعر");
}

else if(!$("#capacity-size").val()){
    alert("الرجاء ملئ حقل الحد الأدنى للحجز ");
}

else if(!$("#max_capacity").val()){
    alert("الرجاء ملئ حقل الحد الأعلى للسعة");
}

else if(!$("#hall-title").val()){
    alert("الرجاء ملئ حقل العنوان");
}

else if(!$("#file").val()){
    alert("الرجاء ملئ حقل الصورة الرئيسية");
}


else if(!$("#file").val()){
    alert("الرجاء اختيار ألبوم الصور");
}

else if($("#category_id").val() === ""){;

    alert("اختر تصنيف");
}
else{
    var $form = $("#form");
console.log($form[0].validationMessage);
    if ($form[0].checkValidity()) {
        $("#form").submit();
      }
    else{
          alert("يرجى التأكد من قيم خصائص التصنيف");
    }
}
} 
    
</script>
                                    </section>

                                </div>


                            </div>
 
                            {{-- <button type="submit" class="btn btn-primary">@lang('admin.create')</button> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <style type="text/css">
        .select2-selection__rendered {
            line-height: 31px !important;
        }

        .select2-container .select2-selection--single {
            height: 35px !important;
        }

        .select2-selection__arrow {
            height: 34px !important;
        }

    </style>
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
            async defer></script>
    <script type="text/javascript">




        $(".select2-single").select2({
            'height': '30px'
        });

        function getCategoriesExtra() {
            var category_id = $('#category_id').val();
            var url = '{{ route('admin.categories.extra') }}';
            $.ajax({
                url: url,
                type: 'GET',
                datatype: 'JSON',
                data: {id: category_id }
            }).done(function(data) {
                var html = `<div id="extra-inputs"><div class="row">`;
                for (var i=0; i<data.length; i++) {
                    if (data[i].field_type == 'text') {
                        html += `

                                <div class="col-md-4" id="div[${data[i].id}]">
                                    <div class="form-group">
                                        <label data-required="yes" id="lab[${data[i].id}]">${data[i].field_name}</label>
                                        <input class="form-control" type="text" name="extra[${data[i].id}]" id="extra[${data[i].id}]" onchange="checkExtras(${data[i].id})"  required>
                                    </div>
                                </div>`;
                    } else if (data[i].field_type == 'number') {
                        if((data[i].id != '6')&&(data[i].id != '5')&&(data[i].id != '22')&&(data[i].id != '10')&&(data[i].id != '11')){ 
                            html += `
                                <div class="col-md-4" id="div[${data[i].id}]"> 
                                    <div class="form-group">
                                        <label data-required="yes" id="lab[${data[i].id}]" >${data[i].field_name}</label>
                                        <input class="form-control" type="number" min="1" name="extra[${data[i].id}]" id="extra[${data[i].id}]" onchange="checkExtras(${data[i].id})" required>
                                    </div>
                                </div>
                            `;
                        }
                        else {
                            html += `
                                <div class="col-md-4" id="div[${data[i].id}]">
                                    <div class="form-group">
                                        <label data-required="yes" id="lab[${data[i].id}]" >${data[i].field_name}</label>
                                        <input class="form-control" type="number" min="0" name="extra[${data[i].id}]" id="extra[${data[i].id}]" onchange="checkExtras(${data[i].id})" required>
                                    </div>
                                </div>
                            `;

                        }


                    } else if (data[i].field_type == 'select') {
                        var options = data[i].field_values.split(',');
                        var options_text = ``;
                        $.each(options, function (key, value) {
                            options_text += `<option value="${value}">${value}</option>`
                        })
                        html += `
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label  data-required="yes">${data[i].field_name}</label>
                                        <select name="extra[${data[i].id}]" class="form-control" required>
                                            ${options_text}
                                        </select>
                                    </div>

                            </div>`;
                    }

                }
                html += `</div></div>`;
                $('#extra-inputs').remove();
                $('#category-info').after(html);
            }).fail(function(e) {
                swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
            })
        }

        function initAutocomplete() {
            var markers = [];
            var input11 = document.getElementById("myloc").value
            var coords = input11.split(",");
            var input23 = new google.maps.LatLng(coords[0], coords[1]);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: input23,
                zoom: 13,
                mapTypeId: 'roadmap'
            });

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(map, event.latLng);
            });

            function placeMarker(map, location) {
                deleteMarkers();
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                $("#myloc").val(location.lat() + ',' + location.lng());
                //infowindow.open(map,marker);
                markers.push(marker);
                //console.log(markers);
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
                setMapOnAll(null);
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('myloc');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }

        var drop = $("input");
        drop.on('dragenter', function (e) {
            $(".drop").css({
                "border": "4px dashed #09f",
                "background": "rgba(0, 153, 255, .05)"
            });

            $(".cont").css({
                "color": "#09f"
            });

        }).on('dragleave dragend mouseout drop', function (e) {
            $(".drop").css({
                "border": "3px dashed #DADFE3",
                "background": "transparent"
            });

            $(".cont").css({
                "color": "#8E99A5"
            });

        });


        function handleFileSelect(evt) {
            $('#list').html('');
            var f = evt.target.files[0]; // FileList object
            // Loop through the FileList and render image files as thumbnails.

            // Only process image files.
            if (!f.type.match('image.*')) {
                //continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            }(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
            window.CP.exitedLoop(0);
        }

        $('#file').change(handleFileSelect);


        function handleAlbumSelect(evt) {
            var files = evt.target.files; // FileList object
            // console.log(files);
            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {
                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }

                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        var span = document.createElement('span');
                        span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
                        document.getElementById('album-list').insertBefore(span, null);
                    };
                }(f);

                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }

            //window.CP.exitedLoop(0);
        }

        $('#album').change(handleAlbumSelect);
    </script>

    <script>
function checkExtras($v){

if($v =="7"){
    var str =document.getElementById("extra[7]").value;
    var women_seats = document.getElementById("div[8]");
    var women_seats_input = document.getElementById("extra[8]");
    var men_seats_input = document.getElementById("extra[9]");
if(str==1){
    women_seats.setAttribute("hidden", "hidden");
    women_seats_input.removeAttribute("required");
    women_seats_input.value=10;
    document.getElementById("lab[9]").innerHTML="عدد المقاعد";
    $("#extra[7]").keyup();
    $("#extra[7]").click();

}
else{
    women_seats.removeAttribute("hidden");
    women_seats_input.setAttribute("required","required");
    document.getElementById("lab[9]").innerHTML="عدد المقاعد الرجال";
    $("#extra[7]").keyup();
    $("#extra[7]").click();
}
}

if($v =="1"){
    var str =document.getElementById("extra[1]").value;
    var women_seats = document.getElementById("div[2]");
    var women_seats_input = document.getElementById("extra[2]");
    var men_seats_input = document.getElementById("extra[4]");


if(str==1){
    women_seats.setAttribute("hidden", "hidden");
    women_seats_input.removeAttribute("required");
    women_seats_input.value=10;
    document.getElementById("lab[4]").innerHTML="عدد المقاعد";
    $("#extra[1]").keyup();
    $("#extra[1]").click();

}
else{
    women_seats.removeAttribute("hidden");
    women_seats_input.setAttribute("required","required");
    document.getElementById("lab[4]").innerHTML="عدد المقاعد الرجال";
    $("#extra[1]").keyup();
    $("#extra[1]").click();

}
}



}
    </script>
@endsection
