@extends('layouts.app')

@section('title',trans('sidebar.halls'))

@section('content')
<!-- Button trigger modal -->
<button type="button" onclick="$('#exampleModal').show();" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="loginMoadlButton" hidden>
    login
  </button>
   
  <!-- start login modal  -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">البريد او الرقم *</label>
                  <input required type="text" class="form-control"  id="exampleInputEmail1"  placeholder="البريد أو الرقم">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">كلمة السر *</label>
                  <input required type="password" class="form-control" id="exampleInputPassword1" placeholder="كلمة السر" >
                </div>
            </form>

            <a href="#" data-toggle="modal" data-target="#exampleModalLong" data-dismiss="modal">
                إنشاءحساب؟
            </a>
        </div>
        <div id="msgs">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">إنهاء</button>
          <button type="submit" class="btn btn-primary" onclick="loginModal()">تسجيل الدخول</button>
        </div>
      </div>
    </div> 
  </div>
  {{-- end login modal --}}



  {{-- start register modal --}}
  <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-body">
            <form >
                @csrf
                <div class="row">
              
            <div class="col-sm-6"> 
                        <div class="form-group">
                            <label for="first_name">الاسم الاول</label>
                            <input type="text" class="form-control"  name="first_name" id="first_name"  >
                          </div>
             </div>
             <div class="col-sm-6">             
                          <div class="form-group">
                            <label for="last_name">الاسم الاخير</label>
                            <input type="text" class="form-control" name="last_name" id="last_name"  >
                          </div>
            </div> 
            
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="phone"> الهاتف </label>
                    <input type="text" name="phone" class="form-control" id="phone"  >
                  </div>
            
                  <div class="form-group">
                    <label for="email"> البريد الالكتروني </label>
                    <input type="email" name="email" class="form-control" id="email"  >
                  </div>
            
                </div> 
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="register_password_input"> كلمة السر </label>
                        <input type="password" class="form-control" name="password" id="register_password_input">
                      </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password_confirmation"> تاكيد كلمة السر  </label>
                        <input type="password" class="form-control " name="password_confirmation" id="password_confirmation">                      </div>
                </div>

                
                    <div class="form-group col-sm-6">
                        <label class="text-md-right">التسجيل عن طريق</label>
                        <select style="padding-top: 0;" class="form-control " name="register_type" id="register_type">
                            <option value="phone">الهاتف</option>
                            <option value="email">البريد الالكتروني</option>
                        </select>
                        
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="text-md-right">نوع الحساب </label>
                        <select style="padding-top: 0;" class="form-control " name="role" id="role">
                            <option value="client">المحتفل</option>
                            <option value="vendor">صاحب قاعة</option>
                        </select>
                        
                    </div>
              
                    {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                </div>
            
        </div>
        <div id="msgs2">
        </div>
        <div class="modal-footer">
          {{-- <button type="submit" class="btn btn-primary">إنشاء حساب</button> --}}
          <a  class="btn btn-primary" onclick="registerModal()">إنشاء حساب</a>

        </form>
        </div>
      </div>
    </div>
  </div>
    {{-- end register modal --}}

{{-- start register ajax --}}
<script>
    function registerModal(){
        $first_name= $("#first_name").val();
        $last_name= $("#last_name").val();
        $phone= $("#phone").val();
        $email= $("#email").val();
        $password= $("#register_password_input").val();
        $confirm_password= $("#password_confirmation").val();
        $role= $("#role").val();
        $register_type= $("#register_type").val();

        if(!$first_name || !$last_name || !$password || !$confirm_password){
       $("#msgs2").append('<div class="alert alert-danger" role="alert"> الرجاء ملئ جميع الحقول</div>');
        }
    else{

    
        $.ajaxSetup({
                             headers: {
                                 'Accept':'application/json',
                                 'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                             } 
                         });
                          jQuery.ajax({
                              url: "{{ route('register') }} ", 
                             method: 'post', 
                             data:{
                                 "first_name": $first_name,
                                 "last_name": $last_name,
                                 "phone": $phone,
                                 "email": $email,
                                 "password": $password,
                                 "password_confirmation": $confirm_password,
                                 "role": $role,
                                 "register_type": $register_type,
                                 "_token": "{{ csrf_token() }}"
                                //  "method_field":"{{ route('register') }} "
                             },
                             statusCode: {
                                200: function (response) {
                                       location.reload();
                                    console.log(response);
                                  },
          422: function (response) {
             $("#msgs").append('<div class="alert alert-danger" role="alert"> معلومات غير صحيحة</div>');
          },
          404: function (response) {
            $("#msgs").append('<div class="alert alert-danger" role="alert">معلومات غير صحيحة</div>');
          }
          },
                             success: function(result){
           console.log(result); 
           alert(result);
                                    }
                          });
                        }
           }
           
    
    </script>
      {{-- end register ajax --}}



  {{-- start login ajax --}}
<script>
function loginModal(){
    $user= $("#exampleInputEmail1").val();
    $password= $("#exampleInputPassword1").val();
    if(!$user || !$password){
   $("#msgs").append('<div class="alert alert-danger" role="alert"> المعرف/كلمة السر مطلوبين</div>');
    }
else{


    $.ajaxSetup({
                         headers: {
                             'Accept':'application/json',
                             'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                         } 
                     });
                      jQuery.ajax({
                          url: "/login", 
                         method: 'post', 
                         data:{
                             "identity":$user,
                             "password":$password,
                             "_token": "{{ csrf_token() }}",
                         },
                         statusCode: {
                            200: function (response) {
                                   location.reload();
                              },
      422: function (response) {
         $("#msgs").append('<div class="alert alert-danger" role="alert"> المعرف أو كلمة السر غير صحيحة</div>');
      },
      404: function (response) {
        $("#msgs").append('<div class="alert alert-danger" role="alert"> المعرف أو كلمة السر غير صحيحة</div>');
      }
      },
                         success: function(result){
       console.log(result); 
       alert(result);
                                }
                      });
                    }
       }
       

</script>
  {{-- end login ajax --}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10" style="background-color: white; border: 1px solid #eee; border-radius: 5px; padding: 40px 0 40px 0; ; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="row justify-content-center">

                    <div class="col-md-4">
                        <div class="profile-img">
                            <img style="cursor: pointer;" data-toggle="modal" data-target="#img-modal" src="{{ $hall->image_url }}" alt=""/>
                        </div>
                        @auth
                            <div class="justify-content-center" style="text-align: center;margin-bottom: 30px;" >
                                <input type="text" class="rating" data-max="5" data-min="1" value="{{ $hall->rating()->where(['user_id' => auth()->id(), 'hall_id' => $hall->id])->first()->rate ?? 0 }}" />
                            </div>
                        @endauth
                    </div>
                    <span style="position: absolute; left: 30px; font-size: 150%; z-index: 100000"><a href="#" onclick="window.history.back();"><i class="fas fa-arrow-left"></i></a></span>
                    <div class="col-md-8">
                        <div class="profile-head">
                            <h5>
                                {{ $hall->name }}
                            </h5>
                            
                            <h6>
                                <i style="color: #4F534C" class="fa fa-money-bill-wave"></i> السعر: {{ $hall->total_price }} ريال سعودي @if($hall->price_type == 'per_person') على الشخص @endif
                            </h6>
                            <h6>
                                <i style="color: #4F534C" class="fa fa-user"></i> &nbsp; الحد الأدنى للحجز: {{ $hall->min_chair_number }} شخص
                            </h6>
                            <h6>
                                <i style="color: #4F534C" class="fa fa-users"></i> الحد الأعلى للسعة:  {{ $hall->max_capacity }} شخص
                            </h6>
                            {{-- @if(Auth::user()->role=="admin")
                            <h6>
                                <i style="color: #4F534C" class="fa fa-users"></i> صاحب القاعة:  {{ $hall->user->first_name }} {{ $hall->user->last_name }} ({{ $hall->user->identity }}) 
                            </h6>
                            @endif --}}
                            <p class="proile-rating" style="color: #FDD835; direction: ltr">
                                @for($i = 1; $i <= $hall->avg_rating; $i++)
                                <i class="fas fa-fw fa-star"></i>
                                @endfor
                                @if($hall->avg_rating - --$i > 0)
                                    <i class="fas fa-fw fa-star-half-alt"></i>
                                @endif
                            </p>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true">@lang('halls.information')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                       aria-controls="profile" aria-selected="false">@lang('halls.description')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#gallery" role="tab"
                                       aria-controls="gallery" aria-selected="false">الصور</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="ratings-tab" data-toggle="tab" href="#ratings" role="tab"
                                       aria-controls="ratings" aria-selected="false">@lang('halls.ratings')</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-work">
                            <a href="#"><i style="color: #4F534C" class="fas fa-fw fa-city"></i> &nbsp; {{ $hall->city->name }}</a><br/>
                            <a href="#"><i style="color: #4F534C" class="fas fa-fw fa-location-arrow"></i> &nbsp; {{ $hall->neighborhood }}</a><br/>
                            <a href="#"><i style="color: #4F534C" class="fas fa-address-card"></i> &nbsp; {{ $hall->address }}</a><br/>
                        </div>
                        {{-- <div class="row"><a><img src="/uploads/gmi.png" style="height: 30px"></a> </div> --}}
                        <input id="myloc" name="map_location" value="{{ $hall->map_location }}" type="hidden"/>
                        <div id="map" style="width: 100%; height: 200px; margin-right: 15px"></div>
                        {{-- <div id="right-panel"></div> --}}
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                @foreach($hall->extras->chunk(2) as $extras)
                               
                                    <div class="row">

                                        @foreach($extras as $extra)
                                        {{-- {{dd($extra)}}  --}}
                                            <div class="col-sm-3">
                                                <label id="{{$extra->id}}">{{ $extra->categoryExtra->field_name }}:</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <p>{{ $extra->value }}</p>
                                            </div>
                                        @endforeach
                                        
                                    </div>
                                @endforeach
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                {{ $hall->description }}
                            </div>
                            <div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="gallery-tab">
                                <section id="gallery">
                                    <div class="container">
                                        <div id="image-gallery">
                                            <div class="row">
                                                @foreach($hall->media as $media)
                                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                                                        <div class="img-wrapper">
                                                            <a href="{{ $media->media_url }}"><img src="{{ $media->media_url }}" class="img-responsive"></a>
                                                            <div class="img-overlay">
                                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div><!-- End row -->
                                        </div><!-- End image gallery -->
                                    </div><!-- End container -->
                                </section>
                            </div>
                            <div class="tab-pane fade" id="ratings" role="tabpanel" aria-labelledby="ratings-tab">

                                <div class="row">
                                    @foreach($hall->rating as $rating)
                                        <div class="col-md-11">
                                            <h5 style="font-weight: 700;">{{ $rating->user->first_name ?? ''}}</h5>
                                        </div>

                                        <div class="col-md-11" style="color: #FDD835">
                                            @for($i = 1; $i <= $rating->rate; $i++)
                                                <i class="fas fa-fw fa-star"></i>
                                            @endfor
                                        </div>


                                        <div class="col-md-11" style="padding: 10px">
                                            <p>{{ $rating->note }}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @if(request()->has('date') && request()->date)
                        <div class="row">
                            @if($chk_status=="Available")
                            <div class="col-md-6">
                                <br><br>
                                {{-- <a @auth data-toggle="modal" data-target="#reserve_modal" href="#" @else href="{{ route('login') }}" @endauth class="btn btn-danger w-100" style=" font-weight: bolder"><i class="fas fa-fw fa-calendar-day"></i> &nbsp; @lang('halls.reserve')</a> --}}
                                <a @auth data-toggle="modal" data-target="#reserve_modal" href="#" @else onclick="$('#loginMoadlButton').click();" @endauth class="btn btn-danger w-100" style=" font-weight: bolder"><i class="fas fa-fw fa-calendar-day"></i> &nbsp; @lang('halls.reserve')</a>
  {{--  --}}
                            </div>
                            @endif
                            <div class="col-md-5">
                                <br><br>
                                <a class="btn btn-success w-100" style="font-weight: bolder" @auth href="{{ route('admin.hall.add.favorite', $hall->id) }}" @else onclick="$('#loginMoadlButton').click();" @endauth><i class="fas fa-fw fa-heart"></i> &nbsp; @lang('halls.add_favorite')</a>
                            </div>
                        </div>
                        @else
                            <div class="row">
                                
                                @if($chk_status=="Available")
                                <div class="col-md-6">
                                    <br><br>   
                                    <a @auth data-toggle="modal" data-target="#reserve_modal_no_dates" href="#" @else onclick="$('#loginMoadlButton').click();" @endauth class="btn btn-danger w-100" style=" font-weight: bolder"><i class="fas fa-fw fa-calendar-day"></i> &nbsp; @lang('halls.reserve')</a>
                                </div>
@endif
                                @if($is_favorite)
                                    <div class="col-md-5">
                                        <br><br>
                                        <a class="btn btn-success w-100" style="font-weight: bolder" href="{{ route('admin.hall.remove.favorite', $hall->id) }}"><i class="fas fa-fw fa-heart-broken"></i> &nbsp; حذف من المفضلة</a>
                                    </div>
                                @else
                                    <div class="col-md-5">
                                        <br><br>
                                        <a class="btn btn-success w-100" style="font-weight: bolder" href="{{ route('admin.hall.add.favorite', $hall->id) }}"><i class="fas fa-fw fa-heart"></i> &nbsp; @lang('halls.add_favorite')</a>
                                    </div>
                                @endif
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">أضف تقييمك للقاعة</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">نص التقييم</label>
                                <input type="text" class="form-control" name="rate_note" id="rate_note">
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="button" class="btn btn-success" onclick="addRateNote()" value="@lang('admin.create')">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                    </div>
                </form>


            </div>
        </div>
    </div><!-- The Modal -->
    <div class="modal" id="img-modal">
        <div class="modal-dialog">
            <div class="modal-content">

                    <!-- Modal body -->
                    <div class="modal-body" style="padding: 0">
                        <img src="{{ $hall->image_url }}">
                    </div>

            </div>
        </div>
    </div><!-- The Modal -->
    @auth
    <div class="modal" id="reserve_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('admin.reservations.store') }}">
                @csrf
                <!-- Modal Header -->
                    <div class="modal-header justify-content-center">
                        <h4 class="modal-title">@lang('halls.send_reserve_data')</h4>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body  justify-content-center">
                        <p class="text-center">@lang('halls.sending_reservations')</p>
                        <div class="row justify-content-center">
                            <div class="form-group col-md-11 text-center" style="border: 1px solid #ccc; border-radius: 10px; padding: 5px;">
 
                                @lang('halls.date'): {{ $gregorian_date }} / {{ $hijri_date }}
                            </div> 
                        </div>
                        <input type="hidden" name="start" value="{{ $gregorian_date }}">
                        <input type="hidden" name="hall_id" value="{{ $hall->id }}">
                        <p class="text-center">@lang('halls.reservations_alert')</p>
                        <p class="text-center text-danger">@lang('halls.reservations_note')</p>
                        <p class="text-center">@lang('halls.has_coupon')</p>
                        <div class="row justify-content-center">
                            <div class="form-group col-md-12 text-center">

                                <input type="text" class="form-control text-center" style="border-radius: 10px" name="coupon_id" value="" placeholder="أدخل رقم الكوبون">
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                            <div class="col-sm">
                                <input type="submit" style="width: 100%" class="btn btn-primary" value="إرسال">
                            </div>
                            <div class="col-sm">
                                <button type="button" style="width: 100%" class="btn btn-outline-primary" data-dismiss="modal">@lang('admin.close')</button>
                            </div>
                    </div>
                </form>


            </div>
        </div>
    </div>

    <div class="modal" id="reserve_modal_no_dates">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('admin.reservations.store') }}">
                @csrf
                <!-- Modal Header -->
                    <div class="modal-header justify-content-center">
                        <h4 class="modal-title">@lang('halls.send_reserve_data')</h4>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body  justify-content-center">
                        <p class="text-center">@lang('halls.sending_reservations')</p>
                        <div class="row justify-content-center">
                            <div class="form-group col-md-11 text-center" style="border: 1px solid #ccc; border-radius: 10px; padding: 5px;">

                                @lang('halls.date'): <input style="border: none; text-align: left" onchange="fillHijri()" name="start" id="from-date" type="text" placeholder="التاريخ الميلادي .." autocomplete="off"> / <input  id="hijri-from" onchange="fillGregorian()" name="hijri_from" type="text" placeholder="التاريخ الهجري .." autocomplete="off" style="border: none">
                            </div>
                        </div>
                        <input type="hidden" name="hall_id" value="{{ $hall->id }}">
                        <input type="hidden" name="must_start" value="1">
                        <p class="text-center">@lang('halls.reservations_alert')</p>
                        <p class="text-center text-danger">@lang('halls.reservations_note')</p>
                        <p class="text-center">@lang('halls.has_coupon')</p>
                        <div class="row justify-content-center">
                            <div class="form-group col-md-12 text-center">

                                <input type="text" class="form-control text-center" style="border-radius: 10px" name="coupon_id" value="" placeholder="أدخل رقم الكوبون">
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                            <div class="col-sm">
                                <input type="submit" style="width: 100%" class="btn btn-primary" id="send-button" value="إرسال">
                                <button type="button" style="width: 100%; display: none" class="btn btn-primary" id="send-a" disabled>إرسال
                                    <div class="spinner-border text-light" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </button>
                            </div>
                            <div class="col-sm">
                                <button type="button" style="width: 100%" class="btn btn-outline-primary" data-dismiss="modal">@lang('admin.close')</button>
                            </div>
                    </div>
                </form>


            </div>
        </div>
    </div>
    @endauth
@endsection
@section('styles')
    <style type="text/css">
        .pulsate {
            -webkit-animation: pulsate 3s ease-out;
            -webkit-animation-iteration-count: infinite;
            opacity: 0.5;
        }
        @-webkit-keyframes pulsate {
            0% {
                opacity: 0.5;
            }
            50% {
                opacity: 1.0;
            }
            100% {
                opacity: 0.5;
            }
        }
        /* ===================== */
        /* ==   FontAwesome   == */
        /* ===================== */
        @font-face {
            font-family:'FontAwesome';
            src:url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.eot?#iefix) format('eot'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.woff) format('woff'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.ttf) format('truetype'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.svg#FontAwesome) format('svg');
            font-weight: normal;
            font-style: normal;
        }

        div.rfWrapper {
            display:inline-block;
            vertical-align:top;
            height:36px;
        }
        /* ======================= */
        /* == CSS RATINGS FIELD == */
        /* ======================= */
        div.rfWrapper div.rfArea {
            display:block;
            direction:rtl;
            height:100%;
            font-size:0px;
            text-align:center;
            border-style:none;
            cursor:default;
            padding:0px;

            /* BOX SIZING */
            box-sizing:border-box;
            -moz-box-sizing:border-box;
            -webkit-box-sizing:border-box;
        }

        div.rfWrapper div.rfArea > input[type="radio"] {
            display:inline-block;
            width:36px;
            height:36px;
            position:absolute;
            top:-32px;
            clip:rect(0,0,0,0);
        }

        div.rfWrapper div.rfArea > label {
            display:inline-block;
            width:36px;
            height:36px;
            font-size:24px;
            line-height:40px;
            text-align:center;
            cursor:pointer;
            position:relative;
            overflow:hidden;
            text-indent:100%;
        }

        div.rfWrapper div.rfArea > label:before {
            font-family:'FontAwesome';
            content:'\f006';

            position:absolute;
            top: 0px;
            left: 0px;

            width:100%;
            height:100%;

            line-height:inherit;
            text-indent:0;
            color:#555555;

            cursor:inherit;

            padding-top:0px;

            /* CSS3 TEXT-SHADOW */
            text-shadow:0px 3px 0px rgba(0, 0, 0, 0.2);

            /* BOX-SIZING */
            box-sizing;border-box;
            -moz-box-sizing:border-box;
            -webkit-box-sizing:border-box;
        }

        div.rfWrapper div.rfArea > input[type="radio"]:checked ~ label:before {
            content:'\f005';
        }

        div.rfWrapper div.rfArea > label:hover:before,
        div.rfWrapper div.rfArea > label:hover ~ label:before {
            color:#FFFFFF;
        }

        div.rfWrapper div.rfArea > input[type="radio"]:checked ~ label:before {
            color:#F9A825;
        }

        div.rfWrapper div.rfArea > label:active {
            position:relative;
            top:2px;
        }

        div.rfWrapper div.rfArea > label:active:before {
            content:'\f005';

            /* CSS3 TEXT-SHADOW */
            text-shadow:0px 1px 0px rgba(0, 0, 0, 0.2);
        }

        .crop {
            width: 100%;
            height: 400px;
            overflow: hidden;
        }

        .crop img {
            width: 100%;
        / / height: 500 px;
            margin: -150px 0 0 0;
        }

        .emp-profile {
            padding: 3%;
            margin-top: 3%;
            margin-bottom: 3%;
            border-radius: 0.5rem;
            background: #fff;
        }

        .profile-img {
            text-align: center;
        }

        .profile-img img {
            width: 70%;
            height: 100%;
        }

        .profile-img .file {
            position: relative;
            overflow: hidden;
            margin-top: -20%;
            width: 70%;
            border: none;
            border-radius: 0;
            font-size: 15px;
            background: #212529b8;
        }

        .profile-img .file input {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
        }

        .profile-head h5 {
            color: #333;
        }

        .profile-head h6 {
            color: #4F534C;
        }

        .profile-edit-btn {
            border: none;
            border-radius: 1.5rem;
            width: 70%;
            padding: 2%;
            font-weight: 600;
            color: #6c757d;
            cursor: pointer;
        }

        .proile-rating {
            font-size: 12px;
            color: #818182;
            margin-top: 5%;
        }

        .proile-rating span {
            color: #495057;
            font-size: 15px;
            font-weight: 600;
        }

        .profile-head .nav-tabs {
            margin-bottom: 5%;
        }

        .profile-head .nav-tabs .nav-link {
            font-weight: 600;
            border: none;
        }

        .profile-head .nav-tabs .nav-link.active {
            border: none;
            border-bottom: 2px solid #4F534C;
        }

        .profile-work {
            padding: 0 14% 14% 14%;
            margin-top: -30px;
        }

        .profile-work p {
            font-size: 12px;
            color: #818182;
            font-weight: 600;
            margin-top: 10%;
        }

        .profile-work a {
            text-decoration: none;
            color: #495057;
            font-weight: 600;
            font-size: 14px;
        }

        .profile-work ul {
            list-style: none;
        }

        .profile-tab label {
            font-weight: 600;
        }

        .profile-tab p {
            font-weight: 600;
            color: #4F534C;
        }


        .img-wrapper {
            position: relative;
            margin-top: 15px;
        }
        .img-wrapper img {
            width: 100%;
        }
        .img-overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0;
        }
        .img-overlay i {
            color: #fff;
            font-size: 3em;
        }


        #overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 999;Removes blue highlight -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        #overlay img {
            margin: 0;
            width: 80%;
            height: auto;
            object-fit: contain;
            padding: 5%;
        }

        #nextButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #nextButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #prevButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #prevButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #exitButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
            position: absolute;
            top: 15px;
            right: 15px;
        }
        #exitButton:hover { 
            opacity: 0.7;
            font-size: 3em;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
            async defer></script>
<script type="text/javascript">

    $('#from-date').datepicker({
        dateFormat: 'yy-mm-dd',
        autoclose: true,
    });

    $('#hijri-from').calendarsPicker({
        calendar: $.calendars.instance('islamic'),
        monthsToShow: [1, 1],
        showOtherMonths: true
    });

    function fillGregorian() {
        if ($('#hijri-from').val()) {
            geo_converted_date = moment($('#hijri-from').val(), 'iYYYY/iMM/iDD').format('YYYY/MM/DD');
            if(geo_converted_date === "Invalid date")
            {

                swal("{!! trans('admin.fail') !!}", "يرجى إدخال تاريخ هجري صحيح", "error");
                return;
            }
            else {
                $('#from-date').val(geo_converted_date);
                getHallReserved(geo_converted_date);
            }
        }

    }

    function fillHijri() {
        if ($('#from-date').val()) {
            $('#hijri-from').val(moment($('#from-date').val()).format('iYYYY/iMM/iDD'));

            getHallReserved($('#from-date').val());
        }

    }

    function getHallReserved(date) {
        var url = '{{ route('admin.hall.reserved') }}';
        $.ajax({
            url: url,
            type: 'POST',
            data: {hall_id: '{{ $hall->id }}', date: date, _token: '{!! csrf_token() !!}'},
            beforeSend: () => {
                $('#send-button').hide();
                $('#send-a').show(); 
            }
        }).done(function(data) {
            if (data != 0) {
                swal("خطأ", "القاعة محجوزة في التاريخ المختار يرجى اختيار تاريخ آخر", "error");
                $('#from-date').val('');
                $('#hijri-from').val('');
            }
            $('#send-button').show();
            $('#send-a').hide();

        }).fail(function(e) {
            swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
        })
    }

    function addRateNote() {
        var note = $('#rate_note').val();
        var url = '{{ route('admin.rate.note') }}';
        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {hall_id: '{{ $hall->id }}', note: note, _token: '{!! csrf_token() !!}'}
        }).done(function(data) {
            $('#myModal').modal('hide');
            swal("{!! trans('admin.success') !!}", "@lang('admin.created_successfully')", "success");
        }).fail(function(e) {
            swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
        })
    }
    /*!
 * jQuery Star-Ratings Plugin
 * http://roguerage.co.uk/
 *
 * Copyright 2014 Project: Rogue and other contributors
 * Released under the MIT license.
 * http://roguerage.co.uk/license.php
 *
 * Requires: ''
 */

    (function ($) {
        "use strict";
        $.fn.ratingsField = function(options) {
            // create elements;
            var selector = this;

            // generate unique ID's for elements;
            var id = [];
            for( var i = 0; i < 10; i++ ) {
                id[i] = Math.floor(Math.random() * 10000000000000001);
            }

            // Define default settings;
            var settings = $.extend(true, {
                classes: {
                    wrapper: 'rfWrapper',
                    area: 'rfArea',
                },
                elements: {
                    field: null,
                    parent: null,
                    area: null,
                },
                ids: {
                    wrapper: 'ratings_'+id[0],
                    score: 'ratingScore_'+id[0]+'_',
                    group: 'ratingGroup_'+id[0],
                },
                field: {
                    ratings: {
                        min: 1,
                        max: 5,
                        sel: 3,
                    },
                    onChange: null,
                },
            }, options);

            // Configure each element;
            selector.each(function(index, element) {
                // Assign element to variable;
                var el = $(this);
                settings.elements.field = el;

                // Wrap element and define parent;
                el.wrap('<div id="'+settings.ids.wrapper+'" class="'+settings.classes.wrapper+'" />');
                settings.elements.parent = el.parent('div.'+settings.classes.wrapper);

                // Hide element;
                settings.elements.field.css({
                    'display':'none',
                });

                settings.elements.field.attr('readonly','readonly');

                // Append ratings 'field';
                settings.elements.parent.append('<div class="'+settings.classes.area+'" />');
                settings.elements.area = settings.elements.parent.children('div.'+settings.classes.area);

                // Create ratings;
                var rmin,rmax,rsel;
                rmin = settings.field.ratings.min;
                rmax = settings.field.ratings.max;
                rsel = settings.field.ratings.sel;

                settings.elements.field.val( rsel );

                for( var r = rmin; r <= rmax; r++ ) {
                    var rchk = ( r == rsel ) ? ' checked="checked"' : '';
                    settings.elements.area.prepend('<label title="Rate '+r+'" for="'+settings.ids.score+r+'"></label>');
                    settings.elements.area.prepend('<input type="radio" '+rchk+' id="'+settings.ids.score+r+'" name="'+settings.ids.group+'" value="'+r+'" />');
                }

                settings.elements.area.children('input[type="radio"]').each(function(index, element) {
                    var s = $(this);

                    s.on('change', function(ev) {
                        settings.elements.field.val( s.val() );

                        if( settings.field.onChange != null && settings.field.onChange !== undefined ) {
                            settings.elements.field.change(settings.field.onChange(event));
                        }
                    });
                });
            });

            // Return element;
            return selector;
        }
    })(jQuery);

    $(document).ready(function(e) {
        $('input[type="text"].rating').each(function(index, element) {
            var selector = $(this);

            var q = {
                min: null,
                max: null,
                sel: null,
            };

            q.min = parseInt( selector.attr('data-min') );
            q.max = parseInt( selector.attr('data-max') );
            q.sel = parseInt( selector.val() );

            selector.ratingsField({
                field: {
                    ratings: {
                        min: ( q.min != null ) ? q.min : 1,
                        max: ( q.max != null ) ? q.max : 5,
                        sel: ( q.sel != null ) ? q.sel : 3,
                    },
                    onChange: function(e) {
                        console.log(e.target.value);
                        var url = '{{ route('admin.halls.rate') }}'
                        $.ajax({
                            url: url,
                            type: 'POST',
                            datatype: 'JSON',
                            data: {hall_id: '{{ $hall->id }}', value: e.target.value, _token: '{!! csrf_token() !!}'}
                        }).done(function(data) {
                            $('#myModal').modal();
                        }).fail(function(e) {
                            swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
                        })
                    },
                },
            });
        });
    });


    $( ".img-wrapper" ).hover(
        function() {
            $(this).find(".img-overlay").animate({opacity: 1}, 600);
        }, function() {
            $(this).find(".img-overlay").animate({opacity: 0}, 600);
        }
    );

    // Lightbox
    var $overlay = $('<div id="overlay"></div>');
    var $image = $("<img>");
    var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-right"></i></div>');
    var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-left"></i></div>');
    var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

    // Add overlay
    $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
    $("#gallery").append($overlay);

    // Hide overlay on default
    $overlay.hide();

    // When an image is clicked
    $(".img-overlay").click(function(event) {
        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        var imageLocation = $(this).prev().attr("href");
        // Add the image src to $image
        $image.attr("src", imageLocation);
        // Fade in the overlay
        $overlay.fadeIn("slow");
    });

    // When the overlay is clicked
    $overlay.click(function() {
        // Fade out the overlay
        $(this).fadeOut("slow");
    });

    // When next button is clicked
    $nextButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").next().find("img"));
        // All of the images in the gallery
        var $images = $("#image-gallery img");
        // If there is a next image
        if ($nextImg.length > 0) {
            // Fade in the next image
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        } else {
            // Otherwise fade in the first image
            $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
        }
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When previous button is clicked
    $prevButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").prev().find("img"));
        // Fade in the next image
        $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When the exit button is clicked
    $exitButton.click(function() {
        // Fade out the overlay
        $("#overlay").fadeOut("slow");
    });




    function initAutocomplete() {
        let map, infoWindow,positionCords;

//init map and source/destination
        var input11 = document.getElementById("myloc").value
        var coords = input11.split(",");
        var input23 = new google.maps.LatLng(coords[0], coords[1]);
        var dest=input23;
        map = new google.maps.Map(document.getElementById('map'), {
            center: input23,
            zoom: 13,
            mapTypeControl: false,
            fullscreenControl:true,
            streetViewControl:false,
            mapTypeId: 'roadmap',

        });
        directionsService = new google.maps.DirectionsService,
        directionsDisplay = new google.maps.DirectionsRenderer({
        map: map
         });
         navigator.geolocation.getCurrentPosition(
            function(position) {
                // Get current cordinates.
                positionCords = position.coords.latitude+','+position.coords.longitude;
                console.log("currentLocation",positionCords);
            },
            function(error) {
                console.log("coudn't get location")
            },
            {timeout: 30000, enableHighAccuracy: false, maximumAge: 75000}
    );

         var gotoMapButton = document.createElement("div");
  gotoMapButton.setAttribute("id", "dirButton");
//   gotoMapButton.setAttribute("hidden", "hidden");
gotoMapButton.setAttribute("style", "margin: 5px;border: 1px solid;padding: 1px 12px;cursor: pointer;background-image: url('/uploads/gmi.png');background-repeat: no-repeat, repeat;background-size: 10px;z-index: 0;position: absolute;left: 0px;top: 0px;");
gotoMapButton.innerHTML = "View Directions";
map.controls[google.maps.ControlPosition.TOP_LEFT].push(gotoMapButton);
google.maps.event.addDomListener(gotoMapButton, "click", function () {
    if(positionCords != 'undefined'){
        var url = 'https://www.google.com/maps/dir/'+positionCords +'/'+ encodeURIComponent(input11);
    }else{
    var url = 'https://www.google.com/maps/place/' + encodeURIComponent(input11);
    }
    window.open(url);
});

        infoWindow = new google.maps.InfoWindow();
                 // directionsDisplay.setMap(map);

                //  var currentLocation=new google.maps.LatLng(pos['lat'], pos['lng'])




    function calculateAndDisplayRoute(directionsService, directionsDisplay, currentLocation, dest) {
  directionsService.route({
    origin:currentLocation ,
    destination: dest,
    travelMode: google.maps.TravelMode.DRIVING
  }, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
      console.log("response",response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

//END Display Route line
// start init Markers on map
var marker = new google.maps.Marker({
            map: map,
            position: map.getCenter()
        })
        // Create the search box and link it to the UI element.
        var input = document.getElementById('myloc');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }
    
</script>
<script>
function checkExtras($v){
    
    if($v =="7"){
        var str =document.getElementById("extra[7]").value;
        var women_seats = document.getElementById("div[8]");
        var women_seats_input = document.getElementById("extra[8]");
        var men_seats_input = document.getElementById("extra[9]");
    if(str==1){
        women_seats.setAttribute("hidden", "hidden");
        women_seats_input.removeAttribute("required");
        women_seats_input.value=10;
        document.getElementById("lab[9]").innerHTML="عدد المقاعد";
        $("#extra[7]").keyup();
        $("#extra[7]").click();
    
    }
    else{
        women_seats.removeAttribute("hidden");
        women_seats_input.setAttribute("required","required");
        document.getElementById("lab[9]").innerHTML="عدد المقاعد الرجال";
        $("#extra[7]").keyup();
        $("#extra[7]").click();
    }
    }
    
    if($v =="1"){
        var str =document.getElementById("extra[1]").value;
        var women_seats = document.getElementById("div[2]");
        var women_seats_input = document.getElementById("extra[2]");
        var men_seats_input = document.getElementById("extra[4]");
    
    
    if(str==1){
        women_seats.setAttribute("hidden", "hidden");
        women_seats_input.removeAttribute("required");
        women_seats_input.value=10;
        document.getElementById("lab[4]").innerHTML="عدد المقاعد";
        $("#extra[1]").keyup();
        $("#extra[1]").click();
    
    }
    else{
        women_seats.removeAttribute("hidden");
        women_seats_input.setAttribute("required","required");
        document.getElementById("lab[4]").innerHTML="عدد المقاعد الرجال";
        $("#extra[1]").keyup();
        $("#extra[1]").click();
    
       }
    }
  
    }

        </script>
@endsection