<?php //dd(auth()->user()->hasRole('administrator')) ?>
@extends('layouts.app')

@section('title', 'الرئيسية')

@section('content')
{{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css"> --}}
{{-- <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}"> --}}

{{-- <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center" style="padding-bottom: 30px!important;">
            <div class="col-md-10">
                @ifCanDo('create_halls')
                <a href="{{ route('admin.halls.create') }}" class="btn btn-primary"
                   style="width: 200px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"><i class="fa fa-plus"></i> @lang('admin.add_new')</a> &nbsp; &nbsp; &nbsp; &nbsp;
                @endif
                @ifCanDo('show_pending_halls')
                <a href="{{ route('admin.get.pending.halls') }}" class="btn btn-primary" style="width: 200px; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                    @lang('sidebar.pending_halls')
                </a>
                @endif
            </div>

        </div>
        <div class="row justify-content-center search-bar">
            <div class="col-md-10 justify-content-center" style="margin-top: 7%">
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>@lang('halls.city')</label>
                            <select name="city_id" id="city-id" class="form-control" style="padding: 3px;">
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>@lang('halls.hijri')</label>
                            <input class="form-control" id="hijri-from" onchange="fillGregorian()" name="hijri_from" type="text" placeholder="تاريخ .." autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>@lang('halls.gregorian')</label>
                            <input class="form-control" id="from-date" onchange="fillHijri()" name="from_date" type="text" placeholder="تاريخ .." autocomplete="off">
                        </div>
                    </div>

                </div>
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="button" name="gregorian" class="btn btn-primary w-100" onclick="setSearchString()" value="@lang('halls.search')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center search-bar">

        </div>

        <div class="row justify-content-center d-none badge-bar" style="margin-bottom: 10px;">
            <div class="col-md-10">
                <div class="row">
                    <label for="name" class="w-30 p-2" style="cursor: pointer" onclick="toggleFilters()"><i class="fa fa-filter"></i></label>
                    <div class="col-sm">
                        <div class="input-group">
                            <div class="input-group-append">
                                <button class="btn btn-primary" onclick="setSearchString()" style="border-radius: 5px;" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control border-5 rounded small" id="search-string" placeholder="@lang('halls.search')..." aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append" style="margin-right: 30px">
                                <button class="btn btn-primary" onclick="goToMap()" style="border-radius: 5px; padding-left: 70px; padding-right: 70px" type="button">
                                    <i class="fas fa-map fa-sm"></i> بحث في الخريطة
                                </button>
                            </div>
                        </div>
                        <div class="row" id="filters" style="margin-top: 20px">
                            <div class="col-md-4">
                                <select class="form-control" id="price-sort" onchange="setSort('total_price', $(this).val())" >
                                    <option>ترتيب حسب السعر</option>
                                    <option value="asc">@lang('halls.price_asc')</option>
                                    <option value="desc">@lang('halls.price_desc')</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id="capacity-sort" onchange="setSort('max_capacity', $(this).val())">
                                    <option>ترتيب حسب السعة</option>
                                    <option value="asc">@lang('halls.capacity_asc')</option>
                                    <option value="desc">@lang('halls.capacity_desc')</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id="rate-sort" onchange="setSort('avg_rating', $(this).val())">
                                    <option >ترتيب حسب التقييم</option>
                                    <option value="asc">@lang('halls.rating_asc')</option>
                                    <option value="desc">@lang('halls.rating_desc')</option>
                                </select>
                            </div>
                            <input type="hidden" id="order_by" value="priority">
                            <input type="hidden" id="order" value="asc">
                            <input type="hidden" id="tabValue">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-10">

            </div>
        </div>

        <div class="row justify-content-center d-none badge-bar">
            <div class="col-md-10">
                <div class="row">
                    @foreach($categories as $category)
                        <div class="col-sm">
                            <button class="btn categoty-part w-100 btn-outline-primary @if($loop->first && !request()->has('category_id')) active @endif" id="category-{{ $category->id }}" onclick="setCategory({{ $category->id }})">{{ $category->name }}</button>
                        </div>
                    @endforeach
                        <div class="col-sm">
                            <button class="btn categoty-part w-100 btn-outline-primary" onclick="setCategory(3);showPartners();" id="category-3">شركاء الاحتفال</button>
                        </div>
                </div>
            </div>
 
            <input type="hidden" id="category-id">
        </div>
        <div class="row justify-content-center" style="margin-top: 30px">
            <div class="col-md-10">
                <div class="row" id="halls-container">
                    @foreach($partners as $partner)
                        <div class="col-md-4 partners" style="padding-bottom: 30px!important;
                       display: none"
                         >
<div class="card" onclick=" window.location = '/admin/partners/branches/{{$partner->user_id}}';">
                                <div class="crop">
                                    <img class="card-img-top" style="cursor: pointer" src="{{ url('/uploads/' . $partner->avatar_path) }}" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <h6 style="font-weight: bolder;font-size: 90%;" class="card-text">{{ $partner->name }}</h6>
                                        </div>
                                    </div>
                                    <p title="{{ $partner->partner_word }}" style="font-size: 90%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: auto;">{{ $partner->partner_word }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="loader" style="display: none"></div>
        </div>
    </div>
    <input type="hidden" id="partner-shown" value="0">
@endsection

@section('styles')
    <style type="text/css">

        .card {
            border-radius: .7rem;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
           // flex-wrap: wrap;
        }

        .card-img-top {
            border-top-left-radius: .7rem;
            border-top-right-radius: .7rem;
        }

        .crop {
            width: 100%;
            //height: 150px;
            overflow: hidden;
        }

        .crop img {
            width: 100%;
            height: 150px;//margin: -75px 0 0 0;
        }


        .btn {
            border-radius: .7rem;
        }

        .btn-light {
            border: 1px solid #ccc;
        }

        .col-sm {
            padding: 5px;
        }

        /* .loader {
            border: 16px solid #f3f3f3; 
            border-top: 16px solid #3498db; 
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        } */

/* start loader style */
.loader {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255);
    z-index:9999;
    display:none;
    
}

@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

.loader::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border: 16px solid #f3f3f3; 
    border-top: 16px solid #3498db; 
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
/* end */
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
    <script>

        var last_page = 0;
        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });
            page = 0;
            //getHalls();
            @if(request()->has('city_id'))
                $('#city-id').val({{ request()->city_id }});
                $('#category-id').val('{{ request()->category_id }}');
                $('#order_by').val('{{ request()->order_by }}');
                $('#order').val('{{ request()->order }}');
                $('#search-string').val('{{ request()->q }}');
                $('#hijri-from').val('{{ request()->hijri }}');
                $('#from-date').val('{{ request()->gregorian }}');
                $('.btn-outline-primary').removeClass('active');
                $('#category-{{ request()->category_id }}').addClass('active');
                max_page = '{{ request()->page }}';
                for (let ss=1; ss<=max_page; ss++) {
                    getHalls();
                }
            @endif
        });

        function toggleFilters() {
            $('#filters').fadeToggle();
        }

        function showPartners() {
            $('#category-id').val(3);
            // setGetParameter('category_id',3);
            $('.old-hall').remove();
            $('.partners').fadeIn();
            $('#partner-shown').val(1);
            $('.btn-outline-primary').removeClass('active')


        }

        function setCategory(id) {
            $('#category-id').val(id);
            $('.old-hall').remove();
            page = 0;
            $('.btn-outline-primary').removeClass('active')
            getHalls();
        }
        function setTabCategory(id) {
            $('#category-id').val(id);
            $('.old-hall').remove();
            // page = 0;
            $('.btn-outline-primary').removeClass('active')
            $("#category-"+id).addClass( "active" );
        }
        function setSearchString() {
            $('.old-hall').remove();
            page = 0;
            getHalls();
        }

        function setSort(order_by, order) {

if(order_by=="total_price"){
$("#rate-sort").val("ترتيب حسب التقييم");
$("#capacity-sort").val("ترتيب حسب السعة");
}

if(order_by=="max_capacity"){
$("#rate-sort").val("ترتيب حسب التقييم");
$("#price-sort").val("ترتيب حسب السعر");
}

if(order_by=="avg_rating"){
$("#capacity-sort").val("ترتيب حسب السعة");
$("#price-sort").val("ترتيب حسب السعر");
}

            $('#order_by').val(order_by);
            $('#order').val(order);
            if(order=="asc" || order =="desc"){
            $('.old-hall').remove();
            page = 0;
            $('.btn-outline-primary').removeClass('active');
            getHalls();
            }
            else{
                $('#order_by').val('priority');
                $('#order').val('asc');
                $('.old-hall').remove();
            page = 0;
            $('.btn-outline-primary').removeClass('active');
            getHalls();
            }
        }

        $('#from-date').datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
            minDate:'d',
            
        });
        // $('#hijri-from').calendarsPicker({calendar: $.calendars.instance('UmmAlQura')});
        $('#hijri-from').calendarsPicker({
            calendar: $.calendars.instance('islamic'),
            monthsToShow: [1, 1],
            showOtherMonths: true,
            selectDefaultDate:true,
            defaultDate: 'd',
            defaultDate: 'd',
            Date: 'd',
            minDate:'d' 
        });
    
    //     $('#hijri-from').calendarsPicker(
    //         { 
    //         defaultDate: '+1d'
    //         }
    // );
    // $(".hijri-from").datepicker({dateFormat: 'dd/mm/dd'});

        function changeStatus(id) {
            var url = '';
            $.ajax({
                url: url,
                type: 'POST',
                data: {id: id, _token: '{!! csrf_token() !!}'}
            }).done(function (data) {
            }).fail(function (e) {
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })
        }

        function fillGregorian() {
            if ($('#hijri-from').val()) { 
                geo_converted_date = moment($('#hijri-from').val(), 'iYYYY/iMM/iDD').format('YYYY/MM/DD');
                if(geo_converted_date === "Invalid date")
                {

                    swal("{!! trans('admin.fail') !!}", "يرجى إدخال تاريخ هجري صحيح", "error");
                    return;
                }
                else
                    $('#from-date').val(geo_converted_date);

            }

        } 
 
        function fillHijri() {
            if ($('#from-date').val()) {
                $('#hijri-from').val(moment($('#from-date').val()).format('iYYYY/iMM/iDD'));
                // alert(moment($('#from-date').val()).format('iYYYY/iMM/iDD'));
            }

        }

        function deleteItem(id, url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method: 'delete'}
                }).done(function (data) {
                    swal({
                            title: data.status,
                            text: data.message,
                            type: data.type
                        },
                        function () {
                            location.reload();
                        }
                    );
                }).fail(function (e) {
                    console.log(e);
                    swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                })
            });
        }

        function inArray(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i] == needle) return true;
            }
            return false;
        }
        serialize = function(obj) {
            var str = [];
            for (var p in obj)
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            return str.join("&");
        }

        function goToMap() {
            var hijri = $('#hijri-from').val();
            var gregorian = $('#from-date').val();

            var m = moment(gregorian, "YYYY-MM-DD");
            if (!m.isAfter(moment()) && !m.isSame(moment().format('YYYY/MM/DD'))&&(cd!=3)) {
                swal("{!! trans('admin.fail') !!}", 'يرجى إدخال تاريخ بعد تاريخ اليوم', "error");
                return;
            }

            var city = $('#city-id').val();
            var category_id = $('#category-id').val();
            var order_by = $('#order_by').val();
            var order = $('#order').val();
            var q = $('#search-string').val();

            var query = {page: page, hijri: hijri, gregorian: gregorian, city_id: city, category_id: category_id, order_by: order_by, order: order, q: q}
            query = serialize(query);

            window.location.href = '{{ route('admin.get.halls.map') }}?' + query; 
        }

        function getHalls() {
            $('.partners').fadeOut();
            $('#partner-shown').val(0);
            var hijri = $('#hijri-from').val();
            var gregorian = $('#from-date').val();

            var current = gregorian;
            var type = 'gregorian';
            var cd = $('#category-id').val(); 
            var m = moment(gregorian, "YYYY-MM-DD");
            if (!m.isAfter(moment()) && !m.isSame(moment().format('YYYY/MM/DD'))&&(cd!=3)) {
                swal("{!! trans('admin.fail') !!}", 'يرجى إدخال تاريخ بعد تاريخ اليوم', "error");
                return;
            }
            // if(cd=3){
            //     $('#category-3').click();
            // }
            var city = $('#city-id').val();
            var category_id = $('#category-id').val(); 
            var order_by = $('#order_by').val();
            var order = $('#order').val();
            var q = $('#search-string').val();

            page++;
            var url = '{{ route('admin.get.halls') }}';
            if (page > last_page && last_page > 0) {
                $(".loader").hide();
                return;
            }

            var query = {page: page, hijri: hijri, gregorian: gregorian, city_id: city, category_id: category_id, order_by: order_by, order: order, q: q}
            window.history.replaceState(null, null, "{{ route('admin.halls.index') }}?" + serialize(query));
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'JSON',
                data: query,
                beforeSend:() => {
                    $(".loader").show();
                }
            }).done(function (all_data) {
                $('#tabValue').val(all_data.tab);
                $t=$('#tabValue').val();
        setTabCategory($t);
           
                $(".loader").hide();
                // if (!all_data.halls.data.length) {

                //     swal("{!! trans('admin.fail') !!}", 'عليك ادخال تاريخ في فلاتر البحث او لا يوجد قاعات في القائمة المطلوبة', "error");
                //     return;
                    
                // }
                var html = ``;
                var data = all_data.halls;
                var reserved_ids = all_data.reserved_ids;
                last_page = data.last_page;
                //if (data.data.length > 0) {
                    $('.search-bar').hide();
                    $('.badge-bar').removeClass('d-none');
                if (q) {
                    $('.categoty-part').hide();
                } else {
                    $('.categoty-part').show();
                }
                //}
                $.each(data.data, function(key, value) {
                    // console.log(value.user_id);
    // console.log("S");
                    var operations = ``;//vendor
 if ({{ auth()->check() && auth()->user()->hasRole('administrator') ? 'true' : 'false' }}) {

                        operations = `<div class="row">
                         
                            <div class="col-sm">
                                <a href="${value.edit_url}" class="btn btn-light" style="width: 100%">
                                    <i class="fa fa-edit"></i> @lang('admin.edit')
                                </a>
                            </div>
                            <div class="col-sm">
                                <a onclick="deleteItem(${value.id}, '${value.delete_url}')" class="btn btn-danger"style="width: 100%; cursor: pointer; color: #fff">
                                    <i class="fa fa-trash"></i> @lang('admin.delete')
                                </a>
                            </div>
                        </div>`;


                        /*<div class="row">
                            <div class="col-sm">
                                <a href="${value.reservations_url}" class="btn btn-primary"style="width: 100%">
                                    <i class="fa fa-calendar"></i> @lang('halls.reservations')
                                </a>
                            </div>
                        </div>`;*/
                    }
                    // console.log(value.user_id);
    // console.log({{ auth()->id()}});
                    if ( {{auth()->check() && auth()->user()->role=="vendor" ? 'true' : 'false'}} &&(value.user_id=="{{ auth()->id()}}")) {
                        
                        operations = `<div class="row">
    
    <div class="col-sm">
        <a href="${value.edit_url}" class="btn btn-light" style="width: 100%">
            <i class="fa fa-edit"></i> @lang('admin.edit')
        </a>
    </div>
    <div class="col-sm">
        <a onclick="deleteItem(${value.id}, '${value.delete_url}')" class="btn btn-danger"style="width: 100%; cursor: pointer; color: #fff">
            <i class="fa fa-trash"></i> @lang('admin.delete')
        </a>
    </div>
</div>`;

                    }

    var is_reserved_str = ``;
    if (inArray(value.id, reserved_ids)) { 
        is_reserved_str = `<p style="color: red; font-weight: bolder;font-size: 90%;" onclick="window.location='${value.show_url}?date=${current}&type=${type}&city=${city}';">@lang('halls.is_reserved')</p>`;
    } else {
        is_reserved_str = `<p style="color: green; font-weight: bolder;font-size: 90%;" onclick="window.location='${value.show_url}?date=${current}&type=${type}&city=${city}';">@lang('halls.is_free')</p>`;
    }

                    var additinal_data = `<p style="font-size: 90%;" onclick="window.location='${value.show_url}?date=${current}&type=${type}&city=${city}';">@lang('halls.capacity'): ${value.max_capacity} شخص</p>
                                    <p style="font-size: 90%;" onclick="window.location='${value.show_url}?date=${current}&type=${type}&city=${city}';">@lang('halls.price'): ${value.total_price} ريال سعودي</p>`;
                    if (value.category_id == 5) {
                        additinal_data = '';
                    }

                    html += `
                        <div class="col-md-4 old-hall" style="padding-bottom: 30px!important;">
                            <div class="card" >
                                <div class="crop">
                                    <img class="card-img-top" style="cursor: pointer" src="${value.image_url}" alt="Card image cap" onclick="window.location='${value.show_url}?date=${current}&type=${type}&city=${city}';">
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h6 style="font-weight: bolder;font-size: 90%;" class="card-text" onclick="window.location='${value.show_url}?date=${current}&type=${type}&city=${city}';">${value.name}</h5>
                                        </div>
                                        <div class="col-sm" style="font-size: 90%;">
                                        <span style="float: left">
                                            <i class="fas fa-fw fa-star" style="color: #FDD835!important;"></i> ${parseFloat(value.avg_rating).toFixed(1)*2}
                                        </span>
                                        </div>
                                    </div>
                                    <p data-toggle="tooltip" data-placement="top" title="${value.address}" style="font-size: 90%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: auto;" onclick="window.location='${value.show_url}?date=${current}&type=${type}&city=${city}';">${value.address}</p>


                                    ${additinal_data}
                                    ${is_reserved_str}
                                
                                    ${operations}

                                </div>
                                <div class="card-footer bg-transparent p-0">
                                    <ul class="list-group list-group-flush p-0">
                                        <li class="list-group-item p-0" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                                            <a class="btn btn-primary w-100"  href="${value.show_url}?date=${current}&type=${type}&city=${city}" style="border-top-left-radius: 0;border-top-right-radius: 0;">@lang('halls.show_more')</a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>`;
                });
                $("#halls-container").append(html);
            }).fail(function (e) {
                console.log(e);
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })


        }
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 50) {

                if ($('#partner-shown').val() == 0 && page > 0) {
                    getHalls();
                }

            }
        });


        function setGetParameter(paramName, paramValue)
{
    var url = window.location.href;
    var hash = location.hash;
    url = url.replace(hash, '');
    if (url.indexOf(paramName + "=") >= 0)
    {
        var prefix = url.substring(0, url.indexOf(paramName + "=")); 
        var suffix = url.substring(url.indexOf(paramName + "="));
        suffix = suffix.substring(suffix.indexOf("=") + 1);
        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
        url = prefix + paramName + "=" + paramValue + suffix;
    }
    else
    {
    if (url.indexOf("?") < 0)
        url += "?" + paramName + "=" + paramValue;
    else
        url += "&" + paramName + "=" + paramValue;
    }
    // window.location.href = url;
}
    </script>
 {{-- if(isset($_GET["category_id"])&&($_GET["category_id"]==3))?>
<script>
$('#category-4').click();
    // alert("Ss");
</script>
 ?> --}}
 <script>
       window.onload = function() {
    var ch=$("#category-id").val();
    if(ch==3){
        $('#category-3').click();
        // alert(ch);
    }
       };
    </script>
@endsection

