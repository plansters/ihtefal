@extends('layouts.app')

@section('title',trans('sidebar.users'))

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            @include('flash-message')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">القاعات</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>اسم القاعة</th>
                        <th>التصنيف</th>
                        <th>صاحب القاعة</th>
                        <th>الهاتف/الايميل</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($halls as $hall)
                    <tr class="odd gradeX">
                        <td>{{ $hall->id }}</td>
                    <td><a href="/admin/halls/{{$hall->id}}">{{ $hall->name}}</a></td>
                        <td>{{ $hall->category->name ?? "" }}</td>
                        <td>{{ $hall->user->first_name  . ' ' . $hall->user->last_name }} @if($hall->user->role !="vendor")({{ $hall->user->role ?? "" }})@endif</td>
                        <td>{{ $hall->user->phone ?? "" }}<br>{{ $hall->user->email ?? "" }}</td>
                    </tr> 
                    @endforeach
 

                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
            <br>

        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

@endsection


@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable({
            "order":[],
            // "scrollY":        300,
            // "scrollX":        true,
            // "scrollCollapse": true,
            // "paging":         false,
            // "fixedColumns":   true,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
            }
        });

    });

    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
function fillName() {
    var userQP = getUrlParameter('user');
    if(userQP){
        $('#cv').val(userQP).keyup()

    }
        }
        window.onload = fillName;


function transN(){
       var x= $('#cv').val();
       x = fixNumbers(x);
       $('#cv').val(x);
} 

    var
    persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
    arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
    fixNumbers = function (str)
    {
          if(typeof str === 'string')
      {
        for(var i=0; i<10; i++)
        {
          str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
        }
      }
    //   alert(str);
      return str;
    };
    
                </script>
@endsection

