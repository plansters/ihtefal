@extends('layouts.app')

@section('title',trans('sidebar.halls'))

@section('content')
    <div class="container-fluid" style="margin-top: 30px">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center" id="halls-container">

        </div>
        <div class="row justify-content-center">
            <div class="loader"></div>
        </div>
    </div>
@endsection

@section('styles')
    <style type="text/css">
        .card {
            border-radius: .7rem;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        .card-img-top {
            border-top-left-radius: .7rem;
            border-top-right-radius: .7rem;
        }

        .btn {
            border-radius: .7rem;
        }

        .btn-light {
            border: 1px solid #ccc;
        }

        .col-sm {
            padding: 5px;
        }

        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });
            page = 0;
            getHalls();

        });

        function changeStatus(id) {
            var url = '';
            $.ajax({
                url: url,
                type: 'POST',
                data: {id: id, _token: '{!! csrf_token() !!}'}
            }).done(function (data) {
            }).fail(function (e) {
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })
        }

        function deleteItem(id, url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method: 'delete'}
                }).done(function (data) {
                    swal({
                            title: data.status,
                            text: data.message,
                            type: data.type
                        },
                        function () {
                            location.reload();
                        }
                    );
                }).fail(function (e) {
                    console.log(e);
                    swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                })
            });
        }
        $(document).ajaxSend(function() {
            $(".loader").show();
        });
        var last_page = 0;
        function getHalls() {
            page++;
            var url = '{{ route('admin.get.pending.halls.ajax') }}';
            if (page > last_page && last_page > 0) {
                return;
            }
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'JSON',
                data: {page: page}
            }).done(function (data) {
                $(".loader").hide();
                var html = ``;
                last_page = data.last_page;
                if (data.data.length) {
                    $.each(data.data, function(key, value) {
                        var operations = ``;
                        if (value.user_id === {{ auth()->id() ?? 0 }} || {{ auth()->check() && auth()->user()->hasRole('administrator') ? 'true' : 'false' }}) {
                            operations = `<div class="row">
                            <div class="col-sm">
                                <a href="${value.edit_url}" class="btn btn-light" style="width: 100%">
                                    <i class="fa fa-edit"></i> @lang('admin.edit')
                                </a>
                            </div>
                            <div class="col-sm">
                                <a onclick="deleteItem(${value.id}, '${value.delete_url}')" class="btn btn-danger"style="width: 100%; cursor: pointer; color: #fff">
                                    <i class="fa fa-trash"></i> @lang('admin.delete')
                                </a>
                            </div>
                        </div>
                        `;
                        }
                        var rate_str = ``;
                        for(var s = 1; s <= value.avg_rating; s++) {
                            rate_str += `<i class="fas fa-fw fa-star"></i>`;
                        }
                        if (value.avg_rating - --s > 0) {
                            rate_str += `<i class="fas fa-fw fa-star-half-alt"></i>`;
                        }
                        html += `
                        <div class="col-md-5" style="padding-bottom: 30px!important;">
                            <div class="card">
                                <img class="card-img-top" style="cursor: pointer" onclick="window.location.href='${value.show_url}'" src="${value.image_url}" alt="Card image cap">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm">
                                            <h5 style="font-weight: bolder" class="card-text">${value.name}</h5>
                                        </div>
                                        <div class="col-sm">
                                        <span style="float: left; color: #FDD835!important; direction: ltr">
                                            ${rate_str}
                                        </span>
                                        </div>
                                    </div>
                                    ${operations}
                                    <div class="row">
                                        <div class="col-sm">
                                            <a onclick="activeHall('${value.active_url}')" href="#" class="btn btn-primary"style="width: 100%">
                                                <i class="fa fa-check"></i> @lang('halls.active')
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
                    });
                } else {
                    html = `<h2 style="margin-top: 15%">لا يوجد قاعات قيد الانتظار</h2>`;
                }

                $("#halls-container").append(html);
            }).fail(function (e) {
                console.log(e);
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })


        }
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 50) {
                getHalls();
            }
        });

        function activeHall(url) {
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}'}
            }).done(function (data) {
                swal({
                        title: '@lang('admin.done')',
                        text: '@lang('admin.update_created_successfully')',
                        type: 'success'
                    },
                    function () {
                        location.reload();
                    }
                );
            }).fail(function (e) {
                console.log(e);
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })
        }
    </script>

@endsection
