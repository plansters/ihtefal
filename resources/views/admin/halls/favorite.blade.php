@extends('layouts.app')

@section('title', 'الرئيسية')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>

        <div class="row justify-content-center" style="margin-top: 30px">
            <div class="col-md-10">
                <div class="row" id="halls-container">

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="loader" style="display: none"></div>
        </div>
    </div>
    <input type="hidden" id="partner-shown" value="0">
@endsection

@section('styles')
    <style type="text/css">

        .card {
            border-radius: .7rem;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
           // flex-wrap: wrap;
        }

        .card-img-top {
            border-top-left-radius: .7rem;
            border-top-right-radius: .7rem;
        }

        .crop {
            width: 100%;
            //height: 150px;
            overflow: hidden;
        }

        .crop img {
            width: 100%;
            height: 150px;//margin: -75px 0 0 0;
        }


        .btn {
            border-radius: .7rem;
        }

        .btn-light {
            border: 1px solid #ccc;
        }

        .col-sm {
            padding: 5px;
        }

        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
    <script>

        var last_page = 0;
        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });
            page = 0;
            getHalls();
        });

        $(document).ajaxSend(function() {
            $(".loader").show();
        });

        serialize = function(obj) {
            var str = [];
            for (var p in obj)
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            return str.join("&");
        }

        function getHalls() {
            page++;
            var url = '{{ route('admin.halls.get.favorite') }}';
            if (page > last_page && last_page > 0) {

                return;
            }
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'JSON',
            }).done(function (all_data) {
                $(".loader").hide();
                if (!all_data.halls.data.length) {
                    swal("{!! trans('admin.fail') !!}", 'لا يوجد لديك قاعات في المفضلة', "error");
                    return;
                }
                let html = ``;
                let data = all_data.halls;
                last_page = data.last_page;
                //console.log(data)
                $.each(data.data, function(key, value) {
                    let operations = ``;

                    let is_reserved_str = ``;


                    let additinal_data = `<p style="font-size: 90%;">@lang('halls.capacity'): ${value.max_capacity} شخص</p>
                                    <p style="font-size: 90%;">@lang('halls.price'): ${value.total_price} ريال سعودي</p>`;
                    if (value.category_id == 5) {
                        additinal_data = '';
                    }

                    html += `
                        <div class="col-md-4 old-hall" style="padding-bottom: 30px!important;">
                            <div class="card" >
                                <div class="crop">
                                    <img class="card-img-top" style="cursor: pointer" src="${value.image_url}" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <h6 style="font-weight: bolder;font-size: 90%;" class="card-text">${value.name}</h5>
                                        </div>
                                        <div class="col-sm" style="font-size: 90%;">
                                        <span style="float: left">
                                            <i class="fas fa-fw fa-star" style="color: #FDD835!important;"></i> ${parseFloat(value.avg_rating).toFixed(1)*2}
                                        </span>
                                        </div>
                                    </div>
                                    <p data-toggle="tooltip" data-placement="top" title="${value.address}" style="font-size: 90%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: auto;">${value.address}</p>


                                    ${additinal_data}
                                    ${is_reserved_str}
                                    ${operations}

                                </div>
                                <div class="card-footer bg-transparent p-0">
                                    <ul class="list-group list-group-flush p-0">
                                        <li class="list-group-item p-0" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                                            <a class="btn btn-primary w-100" href="${value.show_url}" style="border-top-left-radius: 0;border-top-right-radius: 0;">@lang('halls.show_more')</a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </div>`;
                });
                $("#halls-container").append(html);
            }).fail(function (e) {
                console.log(e);
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })


        }
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 50) {
                if ($('#partner-shown').val() == 0) {
                    getHalls();
                }

            }
        });
    </script>

@endsection
