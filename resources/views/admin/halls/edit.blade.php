<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('halls.edit'))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('halls.edit')</div>
                    {{-- @if(session()->has('test')) @endif --}}
                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.halls.update', $hall->id) }}" enctype="multipart/form-data" id="form">
                            @csrf
                            @method('PUT')
                            <input type="hidden" value="1" id="tabChecker" name="tabChecker">

                            <div class="row">
                                <div class="col-12">
                                    <section id="tabs">
                                        <div class="row">
                                            <div class="col-12 ">
                                                <nav>
                                                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                        <a class="nav-item nav-link @if(session()->has('nav-home-tab') || !session()->has('success')) active @endif
                                                         " id="nav-home-tab" onclick="$('#tabChecker').val('1');"
                                                           data-toggle="tab" href="#nav-home" role="tab"
                                                           aria-controls="nav-home"
                                                           aria-selected="true">@lang('halls.data')</a>
                                                        <a class="nav-item nav-link @if(session()->has('nav-profile-tab')) active @endif" id="nav-profile-tab"
                                                           data-toggle="tab" href="#nav-profile" role="tab" onclick="$('#tabChecker').val('2');"
                                                           aria-controls="nav-profile"
                                                           aria-selected="false">@lang('halls.category')</a>
                                                        <a class="nav-item nav-link @if(session()->has('nav-images-tab')) active @endif" id="nav-images-tab"
                                                           data-toggle="tab" href="#nav-images" role="tab" onclick="$('#tabChecker').val('3');"
                                                           aria-controls="nav-images"
                                                           aria-selected="false">الصور</a>
                                                        <a class="nav-item nav-link @if(session()->has('nav-prices-tab')) active @endif" id="nav-prices-tab"
                                                           data-toggle="tab" href="#nav-prices" role="tab" onclick="$('#tabChecker').val('4');"
                                                           aria-controls="nav-prices"
                                                           aria-selected="false">@lang('halls.prices')</a>

                                                    </div>
                                                </nav>
                                                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                                                    <div class="tab-pane fade show @if(session()->has('nav-home-tab') || !session()->has('success')))show active @endif" id="nav-home"
                                                         role="tabpanel" aria-labelledby="nav-home-tab">
                                                        <div class="row">
                                                            <div class="col-6">

                                                                <div class="form-group">
                                                                    <label for="hall-name">@lang('halls.name') *</label>
                                                                    <input type="text" value="{{ $hall->name }}" class="form-control" name="name" id="hall-name" placeholder="">
                                                                </div>
                                                                <!--div class="form-group">
                                                                    <label for="hall-name">@lang('halls.color') *</label>
                                                                    <input type="button" class="btn btn-outline-primary btn-sm" style="width: 100%; background-color: {{ $hall->color }}" id="color-picker-2" value="Open Picker">
                                                                    <input type="hidden" id="hall-color" name="color" value="{{ $hall->color }}">
                                                                </div-->
                                                                <div class="form-row">
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <label for="city-name">@lang('halls.city') *</label>
                                                                            <select id="city-name" name="city_id" class="form-control">
                                                                                @foreach($cities as $city)
                                                                                    <option value="{{ $city->id }}" @if($city->id == $hall->city_id) selected @endif>{{ $city->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <label for="neighborhood">@lang('halls.neighborhood')</label>
                                                                            <input type="text" value="{{ $hall->neighborhood }}" class="form-control" name="neighborhood" id="neighborhood" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="descritipn">@lang('halls.description') *</label>
                                                                    <textarea class="form-control" name="description" id="description" rows="3"> {{ $hall->description }}</textarea>
                                                                </div>
@if(auth::user()->role=='admin')
                                                                <div class="form-group">
                                                                    <label for="priority">الأولوية* <small style=" font-size: 69%; font-family: unset; ">(أولوية الظهور في التطبيق ونتائج البحث)</small></label>
                                                                    {{-- <i class="fa fa-star"></i> --}}
                                                                    <div class="form-row">
                                                                        <div class="col-sm-12">
            <select id="priority" name="priority" class="form-control" style=" text-align-last: center; ">
<option value="1" @if($hall->priority =="1") selected @endif style="background:#BFB2F3">A+</option>
<option value="2" @if($hall->priority =="2") selected @endif style="background:#96CAF7">A</option>
<option value="3" @if($hall->priority =="3") selected @endif style="background:#9CDCAA">B+</option>
<option value="4" @if($hall->priority =="4") selected @endif style="background:#E5E1AB">B</option>
<option value="5" @if($hall->priority =="5") selected @endif style="background:#F3C6A5">C+</option>
<option value="6" @if($hall->priority =="6") selected @endif style="background:#F8A3A8">C</option>
                                                                            </select>

                                                                        </div>
                                                                    </div>
                                                                </div>
@endif
                                                                <div class="form-group">
                                                                    <label for="price">@lang('halls.price') *</label>
                                                                    <div class="form-row">
                                                                        <div class="col-6">
                                                                            <select id="select-preson" name="price_type" class="form-control">
                                                                                <option value="per_person" @if($hall->price_type =="per_person") selected @endif> على الشخص </option>
                                                                                <option value="per_hall" @if($hall->price_type =="per_hall") selected @endif> على كل القاعة </option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <input type="text" class="form-control" value="{{ $hall->total_price }}" name="total_price" id="price" placeholder="">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="form-row">
                                                                        <div class="col-6">
                                                                            <label for="capacity-size">@lang('halls.minimum_count') *</label>
                                                                            <input type="text" class="form-control" value="{{ $hall->min_chair_number }}" name="min_chair_number" id="capacity-size" placeholder="">
                                                                        </div>

                                                                        <div class="col-6">
                                                                            <label for="capacity">@lang('halls.max_capacity')
                                                                                *</label>
                                                                            <input type="number" name="max_capacity" value="{{ $hall->max_capacity }}" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="hall-title">@lang('halls.address') *</label>
                                                                    <input type="text" class="form-control" value="{{ $hall->address }}" name="address" id="hall-title" placeholder="">
                                                                </div>

                                                                <div class="form-group">
                                                                    <input id="myloc" name="map_location" value="{{ $hall->map_location }}" type="text" hidden/>
                                                                    <div id="map" style="width: 100%; height: 400px"></div>
                                                                </div>


                                                            </div>
                                                            <div class="col-6">

                                                                <div class="form-group" id="addForm">
                                                                    <label>@lang('halls.main_image') *</label>
                                                                    <div class="drop">
                                                                        <div class="cont">
                                                                            <i class="fa fa-upload"></i>
                                                                            <div class="tit">
                                                                                Drag & Drop
                                                                            </div>
                                                                            <div class="desc">
                                                                                your files to Assets, or
                                                                            </div>
                                                                            <div class="browse">
                                                                                click here to browse
                                                                            </div>
                                                                        </div>
                                                                        <input id="file" name="image" type="file"/>
                                                                        <output id="list">
                                                                            <span>
                                                                                <img class="thumb" src="{{ $hall->image_url }}" title="11_10.jpg">
                                                                            </span>
                                                                        </output>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group" id="addForm">
                                                                    <label>@lang('halls.album') *</label>
                                                                    <div class="drop">
                                                                        <div class="cont">
                                                                            <i class="fa fa-upload"></i>
                                                                            <div class="tit">
                                                                                Drag & Drop
                                                                            </div>
                                                                            <div class="desc">
                                                                                your files to Assets, or
                                                                            </div>
                                                                            <div class="browse">
                                                                                click here to browse
                                                                            </div>
                                                                        </div>
                                                                        <input id="album" name="album[]" multiple="true" type="file"/>
                                                                        <output id="album-list"></output>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- ----------------- -->


                                                        </div>

                                                    </div>
                                                    <div class="tab-pane fade @if(session()->has('nav-profile-tab'))show active @endif " id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="category_id">@lang('halls.category') *</label>
                                                                    <select id="category_id" name="category_id" onchange="getCategoriesExtra()" class="form-control">
                                                                        <option value="">@lang('halls.category')</option>
                                                                        @foreach($categories as $category)
                                                                            <option value="{{ $category->id }}" @if($category->id == $hall->category_id) selected @endif>{{ $category->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <span id="category-info"></span>
                                                        <div id="extra-inputs">
                                                            <div class="row">

                                                            @foreach($hall_extras as $hall_extra)
                                                                    @if($hall_extra->categoryExtra->field_type == 'text')
                                                                    <div class="col-md-4" id="div[{{ $hall_extra->categoryExtra->id }}]"> 
                                                                        <div class="form-group">
                                                                            <label id="lab[{{ $hall_extra->categoryExtra->id }}]">{{ $hall_extra->categoryExtra->field_name }} *</label>
                                                                            <input class="form-control" value="{{ $hall_extra->value }}" type="text" name="extra[{{ $hall_extra->categoryExtra->id }}]" id="extra[{{ $hall_extra->categoryExtra->id }}]" onchange="checkExtras({{ $hall_extra->categoryExtra->id }})" required>
                                                                        </div>
                                                                    </div>
                                                                    @elseif($hall_extra->categoryExtra->field_type == 'number')
                                                                            
                                                                        <div class="col-md-4" id="div[{{ $hall_extra->categoryExtra->id }}]">
                                                                            <div class="form-group">
                                                                                <label id="lab[{{ $hall_extra->categoryExtra->id }}]">{{ $hall_extra->categoryExtra->field_name }} *</label>
                                                                                <input class="form-control" value="{{ $hall_extra->value }}" type="number" @if(($hall_extra->categoryExtra->id !='5')&&($hall_extra->categoryExtra->id !='6')&&($hall_extra->categoryExtra->id !='22')&&($hall_extra->categoryExtra->id !='10')&&($hall_extra->categoryExtra->id !='11')) min="1" @endif  name="extra[{{ $hall_extra->categoryExtra->id }}]" id="extra[{{ $hall_extra->categoryExtra->id }}]" onchange="checkExtras({{ $hall_extra->categoryExtra->id }})" required>
                                                                            </div>
                                                                        </div>

                                                                    @elseif($hall_extra->categoryExtra->field_type == 'select')
                                                                        <div class="col-md-4"  id="div[{{ $hall_extra->categoryExtra->id }}]">
                                                                            <div class="form-group">
                                                                                <label id="lab[{{ $hall_extra->categoryExtra->id }}]">{{ $hall_extra->categoryExtra->field_name }} *</label>
                                                                                <select name="extra[{{ $hall_extra->categoryExtra->id }}]" class="form-control">
                                                                                    @foreach(explode(',', $hall_extra->categoryExtra->field_values) as $value)
                                                                                        <option value="{{ $value }}" @if($hall_extra->value == $value) selected @endif>{{ $value }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>

                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane fade @if(session()->has('nav-images-tab'))show active @endif" id="nav-images" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                        <section id="gallery">
                                                            <div class="container">
                                                                <div id="image-gallery">
                                                                    <div class="row">
                                                                        @foreach($hall->media as $media)
                                                                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image" id="item-{{ $media->id }}">
                                                                                <div class="img-wrapper">
                                                                                    <a href="{{ $media->media_url }}"><img src="{{ $media->media_url }}" class="img-responsive"></a>
                                                                                    <div class="img-overlay">
                                                                                        <i class="fa fa-plus" aria-hidden="true"></i> &nbsp; &nbsp;
                                                                                        <i style="cursor: pointer" onclick="deleteItem('{{ $media->id }}','{{ route('admin.media.destroy',$media->id) }}')" class="fa fa-trash" aria-hidden="true"></i>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div><!-- End row -->
                                                                </div><!-- End image gallery -->
                                                            </div><!-- End container -->
                                                        </section>
                                                    </div>

                                                    <div class="tab-pane fade @if(session()->has('nav-prices-tab'))show active @endif" id="nav-prices" role="tabpanel" aria-labelledby="nav-prices-tab">
                                                        <div class="list-group" id="sortable">
                                                            @foreach($hall->prices as $price)
                                                                <a href="#" data-id="{{ $price->id }}" id="item-{{ $price->id }}" class="list-group-item list-group-item-action flex-column align-items-start">
                                                                    <div class="d-flex w-100 justify-content-between">
                                                                        <h5 class="mb-1">{{ $price->title }} - {{ $price->price }} ريال سعودي</h5>
                                                                        <small>@lang('halls.'.$price->price_type)</small>
                                                                    </div>
                                                                    <p class="mb-1">
                                                                        @if($price->days)
                                                                            @foreach(explode(',', $price->days) as $day)
                                                                                @lang('halls.'.$day)@if(!$loop->last),@endif
                                                                            @endforeach
                                                                            @if($price->from_date)
                                                                                -  @lang('halls.from')
                                                                     {{-- {{ $price->price_type == 'by_gregorian' ? date('Y-m-d', strtotime($price->from_date)) : convertToHijri($price->from_date) }} --}}
                                                                     <span style="font-weight: 700;">{{convertToHijri($price->from_date) }}</span> @lang('halls.to') <span style="font-weight: 700;">{{convertToHijri($price->to_date) }}</span>
                                                                              
                                                                     
                                                                  <br>  <small style="font-family: Cai;"> {{ date('Y-m-d', strtotime($price->from_date))}} @lang('halls.to') {{ date('Y-m-d', strtotime($price->to_date))}}</small>
                                                                     {{-- {{ $price->price_type == 'by_gregorian' ? date('Y-m-d', strtotime($price->to_date)) : convertToHijri($price->to_date) }} --}}
                                                                                @endif
                                                                        @else
                                                                            @lang('halls.from')
                                                                                {{ $price->price_type == 'by_gregorian' ? date('Y-m-d', strtotime($price->from_date)) : $price->hijri_from }}
                                                                            @lang('halls.to') {{ $price->price_type == 'by_gregorian' ? date('Y-m-d', strtotime($price->to_date)) : $price->hijri_to }}
                                                                        @endif 
                                                                    </p>
                                                                    <small>
                                                                        <button href="#" class="btn btn-danger btn-sm" onclick="event.preventDefault();deleteItem('{{ $price->id }}','{{ route('admin.prices.destroy',$price->id) }}')"><i class="fa fa-trash"></i> @lang('admin.delete')</button>
                                                                    </small>
                                                                </a>
                                                            @endforeach
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                                <a class="btn btn-sm btn-success" href="#" data-toggle="modal" data-target="#add-price">@lang('halls.add_price')</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                            {{-- <a onclick="sbmt()" class="btn btn-primary">@lang('admin.edit')</a> --}}
                            <button type="submit" class="btn btn-primary">@lang('admin.edit')</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function sbmt() {
            if($("input[name='extra[7]']").val()=='0'){
            alert("قيمة خصائص التصنيف لا يمكن أن تكون صفراَ");
        }  
        else{
            $("#form").submit();
        }
        } 
            
        </script>


    <!-- The Modal -->
    <div class="modal" id="add-price">
        <div class="modal-dialog">
            <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('halls.add_price')</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form method="post" action="{{ route('admin.prices.store') }}" id="add_price_modal">
                            @csrf
                        <div class="row">
                            <input type="hidden" id="event-id">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('halls.price_title')</label>
                                <input type="text" class="form-control" id="edit-title" name="title" required>
                                <input type="hidden" name="price_type" id="price_type">
                                <input type="hidden" name="hall_id" value="{{ $hall->id }}" id="hall_id">
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" id="event-id">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('halls.price')</label>
                                <input type="text" class="form-control" id="edit-title" name="price" required>
                            </div>
                        </div>

                        <div id="accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            @lang('halls.by_days')
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label class="text-md-right">@lang('halls.days')</label>
                                                <select type="text" style="width: 100%" class="form-control" id="days" multiple name="days[]" required>
                                                    <option value="saturday">@lang('halls.saturday')</option>
                                                    <option value="sunday">@lang('halls.sunday')</option>
                                                    <option value="monday">@lang('halls.monday')</option>
                                                    <option value="tuesday">@lang('halls.tuesday')</option>
                                                    <option value="wednesday">@lang('halls.wednesday')</option>
                                                    <option value="thursday">@lang('halls.thursday')</option>
                                                    <option value="friday">@lang('halls.friday')</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button type="button" class="btn btn-link" onclick="setPriceType('by_gregorian')" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                            @lang('halls.by_gregorian')
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne1" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-md-right">@lang('halls.from_date')</label>
                                                <input class="form-control" id="from-date" name="from_date" type="text" placeholder="من .." autocomplete="off">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text-md-right">@lang('halls.to_date')</label>
                                                <input class="form-control" id="to-date" name="to_date" type="text" placeholder="الى .." autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button type="button" class="btn btn-link" onclick="setPriceType('by_islamic')" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="true" aria-controls="collapseOne">
                                            @lang('halls.by_islamic')
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapseOne2" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label class="text-md-right">@lang('halls.from_date')</label>
                                                <input class="form-control" id="hijri-from"   name="hijri_from" type="text" placeholder="من .." autocomplete="off">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="text-md-right">@lang('halls.to_date')</label>
                                                <input class="form-control" id="hijri-to" name="hijri_to" type="text" placeholder="الى .." autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success" onclick="$('#add_price_modal').submit();//savePrice()">@lang('admin.create')</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                    </div>



            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style type="text/css">
        .select2-selection__rendered {
            line-height: 31px !important;
        }

        .select2-container .select2-selection--single {
            height: 35px !important;
        }

        .select2-selection__arrow {
            height: 34px !important;
        }

        #gallery {
            padding-top: 40px;
            padding: 60px 30px 0 30px;
        }

        .img-wrapper {
            position: relative;
            margin-top: 15px;
        }
        .img-wrapper img {
            width: 100%;
        }
        .img-overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0;
        }
        .img-overlay i {
            color: #fff;
            font-size: 3em;
        }


        #overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 999;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        #overlay img {
            margin: 0;
            width: 80%;
            height: auto;
            object-fit: contain;
            padding: 5%;
        }

        #nextButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #nextButton:hover {
             opacity: 0.7;
            font-size: 3em;
        }

        #prevButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #prevButton:hover {
             opacity: 0.7;
            font-size: 3em;
        }

        #exitButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
            position: absolute;
            top: 15px;
            right: 15px;
        }
        #exitButton:hover {
             opacity: 0.7;
            font-size: 3em;
        }
    </style>
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
            async defer></script>
    <script src="{{ asset('js/pickr.min.js') }}"></script>
    <script type="text/javascript">
        // Gallery image hover
        function setPriceType(type) {
            $('#price_type').val(type);
        }

        $( function() {
            $( "#sortable" ).sortable({
                stop: function(evt, ui) {
                    var init_data = $(this).sortable('serialize');
                    init_data +="&_token={{ csrf_token() }}";
                    console.log(init_data);
                    var url = '{{ route('admin.prices.sort') }}';
                    $.ajax({
                        url: url,
                        type: 'POST',
                        datatype: 'JSON',
                        data: init_data
                    }).done(function(data) {
                        console.log(data);
                    }).fail(function(e) {
                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
                    })
                }
            });
        } );

        $( ".img-wrapper" ).hover(
            function() {
                $(this).find(".img-overlay").animate({opacity: 1}, 600);
            }, function() {
                $(this).find(".img-overlay").animate({opacity: 0}, 600);
            }
        );
        $('#days').select2({
            width: '100%'
        });
        $('#from-date').datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
            minDate:'d'
        });
        // Li
        $('#to-date').datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
            minDate:'+1d'
        });

        $('#hijri-from').calendarsPicker({
            calendar: $.calendars.instance('islamic'),
            monthsToShow: [1, 1],
            showOtherMonths: true,
            selectDefaultDate:true,
            defaultDate: '+1d',
            Date: '+1d',
            minDate:'+1d'
        });

        $('#hijri-to').calendarsPicker({
            calendar: $.calendars.instance('islamic'),
            monthsToShow: [1, 1],
            showOtherMonths: true,
            parent: '#add-price',
            selectDefaultDate:true,
            defaultDate: '+2d',
            Date: '+2d',
            minDate:'+2d' 
        });


        // Lightbox
        var $overlay = $('<div id="overlay"></div>');
        var $image = $("<img>");
        var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-right"></i></div>');
        var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-left"></i></div>');
        var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

        // Add overlay
        $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
        $("#gallery").append($overlay);

        // Hide overlay on default
        $overlay.hide();

        // When an image is clicked
        $(".img-overlay").click(function(event) {
            // Prevents default behavior
            event.preventDefault();
            // Adds href attribute to variable
            var imageLocation = $(this).prev().attr("href");
            // Add the image src to $image
            $image.attr("src", imageLocation);
            // Fade in the overlay
            $overlay.fadeIn("slow");
        });

        // When the overlay is clicked
        $overlay.click(function() {
            // Fade out the overlay
            $(this).fadeOut("slow");
        });

        // When next button is clicked
        $nextButton.click(function(event) {
            // Hide the current image
            $("#overlay img").hide();
            // Overlay image location
            var $currentImgSrc = $("#overlay img").attr("src");
            // Image with matching location of the overlay image
            var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            var $nextImg = $($currentImg.closest(".image").next().find("img"));
            // All of the images in the gallery
            var $images = $("#image-gallery img");
            // If there is a next image
            if ($nextImg.length > 0) {
                // Fade in the next image
                $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
            } else {
                // Otherwise fade in the first image
                $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
            }
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

        // When previous button is clicked
        $prevButton.click(function(event) {
            // Hide the current image
            $("#overlay img").hide();
            // Overlay image location
            var $currentImgSrc = $("#overlay img").attr("src");
            // Image with matching location of the overlay image
            var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
            // Finds the next image
            var $nextImg = $($currentImg.closest(".image").prev().find("img"));
            // Fade in the next image
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
            // Prevents overlay from being hidden
            event.stopPropagation();
        });

        // When the exit button is clicked
        $exitButton.click(function() {
            // Fade out the overlay
            $("#overlay").fadeOut("slow");
        });


        $(".select2-single").select2({
            'height': '30px'
        });

        function getCategoriesExtra() {
            var category_id = $('#category_id').val();
            var url = '{{ route('admin.categories.extra') }}';
            $.ajax({
                url: url,
                type: 'GET',
                datatype: 'JSON',
                data: {id: category_id }
            }).done(function(data) {
                var html = `<div id="extra-inputs"><div class="row">`;
                for (var i=0; i<data.length; i++) {
                    if (data[i].field_type == 'text') {
                        html += `

                                <div class="col-md-4" id="div[${data[i].id}]">
                                    <div class="form-group">
                                        <label id="lab[${data[i].id}]">${data[i].field_name} *</label>
                                        <input class="form-control" type="text" name="extra[${data[i].id}]" id="extra[${data[i].id}]" onchange="checkExtras(${data[i].id})" required>
                                    </div>
                                </div>`;
                    } else if (data[i].field_type == 'number') {
                        if((data[i].id != '5')&&(data[i].id != '6')&&(data[i].id != '22')&&(data[i].id != '10')&&(data[i].id != '11')){
                        html += `

                                <div class="col-md-4" id="div[${data[i].id}]">
                                    <div class="form-group">
                                        <label id="lab[${data[i].id}]">${data[i].field_name} *</label>
                                        <input class="form-control"  min="1" type="number" name="extra[${data[i].id}]" id="extra[${data[i].id}]" onchange="checkExtras(${data[i].id})" required>
                                    </div>
                                </div>
                            `;
                        }
                        else{
                            html += `

<div class="col-md-4" id="div[${data[i].id}]">
    <div class="form-group">
        <label id="lab[${data[i].id}]">${data[i].field_name} *</label>
        <input class="form-control"  min="0" type="number" name="extra[${data[i].id}]" id="extra[${data[i].id}]" onchange="checkExtras(${data[i].id})" required>
    </div>
</div>
`;
                        }
                    } else if (data[i].field_type == 'select') {
                        var options = data[i].field_values.split(',');
                        var options_text = ``;
                        $.each(options, function (key, value) {
                            options_text += `<option value="${value}">${value}</option>`
                        })
                        html += `
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>${data[i].field_name} *</label>
                                        <select name="extra[${data[i].id}]" class="form-control">
                                            ${options_text}
                                        </select>
                                    </div>

                            </div>`;
                    }

                }
                html += `</div></div>`;
                $('#extra-inputs').remove();
                $('#category-info').after(html);
            }).fail(function(e) {
                swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
            })
        }

        function initAutocomplete() {
            var markers = [];
            var input11 = document.getElementById("myloc").value
            var coords = input11.split(",");
            var input23 = new google.maps.LatLng(coords[0], coords[1]);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: input23,
                zoom: 13,
                mapTypeId: 'roadmap'
            });

            placeMarker(map, map.getCenter());

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(map, event.latLng);
            });
 
            function placeMarker(map, location) {
                deleteMarkers();
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                $("#myloc").val(location.lat() + ',' + location.lng());
                markers.push(marker);
            }

            function setMapOnAll() {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
                setMapOnAll();
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('myloc');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }


                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }

        var drop = $("input");
        drop.on('dragenter', function (e) {
            $(".drop").css({
                "border": "4px dashed #09f",
                "background": "rgba(0, 153, 255, .05)"
            });

            $(".cont").css({
                "color": "#09f"
            });

        }).on('dragleave dragend mouseout drop', function (e) {
            $(".drop").css({
                "border": "3px dashed #DADFE3",
                "background": "transparent"
            });

            $(".cont").css({
                "color": "#8E99A5"
            });

        });


        function handleFileSelect(evt) {
            $('#list').html('');
            var f = evt.target.files[0]; // FileList object
            // Loop through the FileList and render image files as thumbnails.

            // Only process image files.
            if (!f.type.match('image.*')) {
                //continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            }(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
            window.CP.exitedLoop(0);
        }

        $('#file').change(handleFileSelect);


        function handleAlbumSelect(evt) {
            var files = evt.target.files; // FileList object
            console.log(files);
            // Loop through the FileList and render image files as thumbnails.
            for (var i = 0, f; f = files[i]; i++) {
                // Only process image files.
                if (!f.type.match('image.*')) {
                    continue;
                }

                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = function (theFile) {
                    return function (e) {
                        // Render thumbnail.
                        var span = document.createElement('span');
                        span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
                        document.getElementById('album-list').insertBefore(span, null);
                    };
                }(f);

                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
            }

            //window.CP.exitedLoop(0);
        }

        $('#album').change(handleAlbumSelect);



        function deleteItem(id, url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method: 'delete'}
                }).done(function () {
                    swal({
                            title: "{!! trans('admin.done') !!}",
                            text: "{!! trans('admin.deleted_successfully') !!}",
                            type: "success"
                        },
                        function () {
                                                    //    alert(id);
                            // var element = document.getElementById(id);
                            // element.parentNode.removeChild(element);
                            document.getElementById("item-"+id).remove();
                            // location.reload();
                            //here on delete select the requested tab (prices)
                        }
                    );
                }).fail(function (e) {
                    console.log(e);
                    swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                })
            });
        }

        function savePrice() {
            url = '{{ route('admin.prices.store') }}';
            var data = {
                _token: '{!! csrf_token() !!}',
                title: $('#title').val(),
                price_type: $('#price_type').val(),
                days: $('#days').val(),
                from_date: $('#from_date').val(),
                to_date: $('#to_date').val(),
                hijri_from: $('#hijri_from').val(),
                hijri_to: $('#hijri_to').val(),
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: data
            }).done(function () {
                swal({
                        title: "{!! trans('admin.done') !!}",
                        text: "{!! trans('admin.deleted_successfully') !!}",
                        type: "success"
                    },
                    function () {
                        //location.reload();
                    }
                );
            }).fail(function (e) {
                console.log(e);
                swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
            })
        }

        function fillGregorian() {
            if ($('#hijri-from').val()) { 
                geo_converted_date = moment($('#hijri-from').val(), 'iYYYY/iMM/iDD').format('YYYY/MM/DD');
                if(geo_converted_date === "Invalid date")
                {

                    swal("{!! trans('admin.fail') !!}", "يرجى إدخال تاريخ هجري صحيح", "error");
                    return;
                }
                else
                    $('#from-date').val(geo_converted_date);
            }

        }
        // 
        function fillGregorianToDate() {
            if ($('#hijri-to').val()) { 
                geo_converted_date = moment($('#hijri-to').val(), 'iYYYY/iMM/iDD').format('YYYY/MM/DD');
                if(geo_converted_date === "Invalid date")
                {

                    swal("{!! trans('admin.fail') !!}", "يرجى إدخال تاريخ هجري صحيح", "error");
                    return;
                }
                else
                    $('#to-date').val(geo_converted_date);
            }

        }
// 
        function fillHijri() {
            if ($('#from-date').val()) {
                $('#hijri-from').val(moment($('#from-date').val()).format('iYYYY/iMM/iDD'));
            }

        }
    </script>
 <script>
    function checkExtras($v){
    
    if($v =="7"){
        var str =document.getElementById("extra[7]").value;
        var women_seats = document.getElementById("div[8]");
        var women_seats_input = document.getElementById("extra[8]");
        var men_seats_input = document.getElementById("extra[9]");
    if(str==1){
        women_seats.setAttribute("hidden", "hidden");
        women_seats_input.removeAttribute("required");
        women_seats_input.value=10;
        document.getElementById("lab[9]").innerHTML="عدد المقاعد";
        $("#extra[7]").keyup();
        $("#extra[7]").click();
    
    }
    else{
        women_seats.removeAttribute("hidden");
        women_seats_input.setAttribute("required","required");
        document.getElementById("lab[9]").innerHTML="عدد المقاعد الرجال";
        $("#extra[7]").keyup();
        $("#extra[7]").click();
    }
    }
    
    if($v =="1"){
        var str =document.getElementById("extra[1]").value;
        var women_seats = document.getElementById("div[2]");
        var women_seats_input = document.getElementById("extra[2]");
        var men_seats_input = document.getElementById("extra[4]");
    
    
    if(str==1){
        women_seats.setAttribute("hidden", "hidden");
        women_seats_input.removeAttribute("required");
        women_seats_input.value=10;
        document.getElementById("lab[4]").innerHTML="عدد المقاعد";
        $("#extra[1]").keyup();
        $("#extra[1]").click();
    
    }
    else{
        women_seats.removeAttribute("hidden");
        women_seats_input.setAttribute("required","required");
        document.getElementById("lab[4]").innerHTML="عدد المقاعد الرجال";
        $("#extra[1]").keyup();
        $("#extra[1]").click();
    
       }
    }
 
    }
        </script>
                        <script>
                         @if($hall_extras->first())checkExtras('{{$hall_extras->first()->categoryExtra->id}}');@endif
                                            
                                 </script>
                            {{-- {{dd("ddd",$hall_extras)}} --}}
                            {{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script> --}}
                            {{-- <script>$('#input-id').rating({'update': 2,
                                'showCaption':false,
                                'showClear':false
                              });
                              </script> --}}

@endsection
