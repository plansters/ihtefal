@extends('layouts.app')

@section('title',trans('sidebar.reservations'))

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.reservations')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <hr>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                    <div class="col-md-2" id="external-events">
                        <h4>@lang('calendar.waiting_events')</h4>
                        <div style="background-color: #000000!important;" class="fc-event ui-draggable ui-draggable-handle" ondblclick="editEvent({id: {{ $event->id }},title: '{{ $event->title }}', description: '{{ $event->description }}' })" onmousedown="setEventId({{ $event->id }});" id="event-{{ $event->id }}">{{ $event->title }}</div>

                    </div>
                    <input type="hidden" id="selected-event">
                    {!! $calendar->calendar() !!}

            </div>
        </div>
    </div>


    <!-- The Modal -->
    <div class="modal" id="my-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">@lang('calendar.update_event')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="text-md-right">@lang('calendar.hall')</label>
                            <p class="w-100">
                                <a href="" id="hall-link" target="_blank"></a>
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" id="event-id">
                        <div class="form-group col-md-12">
                            <label class="text-md-right">@lang('calendar.title')</label>
                            <input type="text" class="form-control" id="edit-title" name="edit_name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="text-md-right">@lang('calendar.description')</label>
                            <textarea class="form-control" name="edit_description" id="edit-description" required></textarea>
                        </div>
                    </div>

                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        @lang('calendar.user_info')
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item"><i style="color: #007bff" class="fas fa-address-card"></i> &nbsp; &nbsp;<span id="user-name"></span></li>
                                        <li class="list-group-item"><i style="color: #007bff" class="fas fa-envelope"></i> &nbsp; &nbsp;<span id="user-email"></span></li>
                                        <li class="list-group-item"><i style="color: #007bff" class="fas fa-phone"></i> &nbsp; &nbsp;<span id="user-phone"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="button" class="btn btn-success" data-dismiss="modal" onclick="updateReservation()" value="@lang('admin.edit')">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteReservation()">@lang('admin.delete')</button>
                </div>

            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="add-event-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('admin.reservations.store') }}" method="post">
                    @csrf
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('calendar.add_reservation')</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('calendar.title')</label>
                                <input type="text" class="form-control" id="title" name="title" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('calendar.user')</label>
                                <select type="text" style="width: 100%" class="form-control" id="user_id" name="user_id" required></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('calendar.hall')</label>
                                <select type="text" style="width: 100%" class="form-control" id="hall_id" name="hall_id" required></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">description</label>
                                <textarea class="form-control" name="description" id="description" required></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="@lang('admin.create')">
                        <input type="button" class="btn btn-danger" data-dismiss="modal" value="@lang('admin.close')">
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection


@section('styles')
    <style>

        #external-events {
            float: left;
            padding: 0 10px;
            border: 1px solid #ccc;
            background: #eee;
            text-align: left;
        }

        #external-events h4 {
            font-size: 16px;
            margin-top: 0;
            padding-top: 1em;
        }

        #external-events .fc-event {
            margin: 10px 0;
            cursor: pointer;
        }

        #external-events p {
            margin: 1.5em 0;
            font-size: 11px;
            color: #666;
        }

        #external-events p input {
            margin: 0;
            vertical-align: middle;
        }


        .fc-title {
            color: white;
        }

        .fc-time {
            color: white;
        }

        .fc-widget-content {
            color: #858796!important;
        ;
        }
        .fc-toolbar {
            height: 35px;
        }
    </style>
@endsection


@section('scripts')
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
{!! $calendar->script() !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#calendar-{{ $calendar->getId()}}').addClass('col-md-10');
        $('#external-events .fc-event').each(function() {
            // store data so the calendar knows to render an event upon drop

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
        });

    });

    function dropEnd(date, allDay, hall_id) {
        addEvent(new Date(date), hall_id);
    }

    function addEvent(date, hall_id) {
        var url = '{{ route('admin.event.add') }}';
        var start = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {_token: '{!! csrf_token() !!}', date: start, id: $('#selected-event').val(), all_day: 1, hall_id: hall_id}
        }).done(function(event) {
            console.log(event);
            addCalendarEvent(event.id, event.start.date, event.end.date, event.title,  event.isAllDay, event.options);
            $('#event-' + $('#selected-event').val()).remove();
            //$('.fc-draggable').remove();
        }).fail(function(e) {
            console.log(e);
        })
    }

    function editEvent(info, id) {
        console.log(info);
        if (info.user.id == '{{ \Illuminate\Support\Facades\Auth::id() }}') {
            $('#my-modal').modal();
            $('#event-id').val(info.id);
            $('#user-name').html(info.user.first_name + ' ' + info.user.last_name);
            $('#user-email').html(info.user.email);
            $('#user-phone').html(info.user.phone);
            $('#hall-link').html(info.hall_name);
            $('#edit-title').val(info.title);
            $('#edit-description').val(info.description);
        }

        //console.log(info.id + " start is " + info.start.toISOString() + " end is " + info.end.toISOString());
    }

    function updateReservation() {
        var id = $('#event-id').val();
        var url = '{{ url('admin/reservations') }}/' + id;
        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {
                _token: '{!! csrf_token() !!}',
                title: $('#edit-title').val(),
                description: $('#edit-description').val(),
                _method: 'PUT'
            }
        }).done(function(event) {
            if (event.start) {

                var my_event = $("#calendar-{{ $calendar->getId()}}").fullCalendar( 'clientEvents', id );
                console.log(my_event[0]);
                //$('#calendar-{{ $calendar->getId()}}').fullCalendar( 'removeEvents', [id] );
                my_event[0].title = $('#edit-title').val();
                $('#calendar-{{ $calendar->getId()}}').fullCalendar( 'updateEvent', my_event[0]);
            } else {
                $('#event-' + id).html($('#edit-title').val());
            }

            /*$('#calendar-{{ $calendar->getId()}}').fullCalendar( 'updateEvent', {
                id: id,
                start: event.start,
                end: event.end,
                allDay: event.all_day,
                title: $('#edit-title').val(),
            }, true );*/
        }).fail(function(e) {
            console.log(e);
        })
    }

    function deleteReservation() {
        var id = $('#event-id').val();
        var url = '{{ url('admin/reservations') }}/' + id;
        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {
                _token: '{!! csrf_token() !!}',
                title: $('#edit-title').val(),
                description: $('#edit-description').val(),
                _method: 'DELETE'
            }
        }).done(function(event) {
            if (event.start) {
                $('#calendar-{{ $calendar->getId()}}').fullCalendar( 'removeEvents', [id] );
            } else {
                $('#event-' + id).remove();
            }


        }).fail(function(e) {
            console.log(e);
        })
    }

    function selectItem(start, end, allDay){
        alert(date);
        console.log('start', start.format("iYYYY-MM-DD"));
        console.log(end);
        console.log(element);
    }

    function setEventId(value) {
        $('#selected-event').val(value)
    }

    function resizeEvent(info) {
        console.log(info);
        console.log('hijrit', moment(info.start).format('iYYYY/iM/iD'));
        var url = '{{ route('admin.event.resize') }}';

        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {
                _token: '{!! csrf_token() !!}',
                title: info.title,
                id: info.id,
                start: info.start.toISOString(),
                end: info.end.toISOString(),
                all_day: info.allDay ? 1 : 0
            }
        }).done(function(e) {
            console.log(e);
        }).fail(function(e) {
            console.log(e);
        })
    }

    function set_hijri_content(date, cell) {
        cell.append(`<div style="padding: 3px">${moment(date).format('iYY/iM/iD')}</div>`);
    }

    function getEvents(view, element, id) {
        //console.log(view, element)
        $.ajax({
            type: "POST",
            url: '{{ route('admin.get.events') }}',
            data: {
                start: view.intervalStart.toISOString(),
                end: view.intervalEnd.toISOString(),
                id: id,
                _token: '{{ csrf_token() }}'
            },
            success: function(events) {
                $('#calendar-{{ $calendar->getId() }}').fullCalendar('removeEvents');
                //var event = events[0];
                for (var i = 0; i < events.length; i++) {
                    if (events[i].options.user.id == '{{ \Illuminate\Support\Facades\Auth::id() }}') {
                        addCalendarEvent(events[i].id, events[i].start.date, events[i].end.date, events[i].title,  events[i].isAllDay, events[i].options);
                    } else {
                        events[i].user = '';
                        addCalendarEvent('', events[i].start.date, events[i].end.date, '@lang("calendar.reserved")',  events[i].isAllDay, events[i].options);
                    }
                }

            }
        });
    }

    function addCalendarEvent(id, start, end, title, all_day, options) {
        var eventObject = {
            id: id,
            start: start,
            end: end,
            title: title,
            allDay: all_day,
            color: options.color,
            user: options.user,
            hall_name: options.hall_name,
            hall_route: options.hall_route,
        };

        $('#calendar-{{ $calendar->getId() }}').fullCalendar('renderEvent', eventObject, true);
    }

    function removeEvents() {
        var allEvents = $('#calendar-{{ $calendar->getId() }}').fullCalendar('clientEvents');

        var userEventIds= [];

//Find ever non usercreated event and push the id to an array
        $.each(allEvents,function(index, value){
            if(value.isUserCreated !== true){
                userEventIds.push(value._id);
            }
        });
    }

    function openDay(date, jsEvent, view) {
        $('#calendar-{{ $calendar->getId() }}').fullCalendar('changeView', 'agendaDay');
        $('#calendar-{{ $calendar->getId() }}').fullCalendar('gotoDate',date);
    }
</script>
@endsection
