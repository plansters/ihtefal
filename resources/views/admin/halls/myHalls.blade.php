<?php //dd(auth()->user()->hasRole('administrator')) ?>
@extends('layouts.app')

@section('title', 'الرئيسية')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>

        <div class="row justify-content-center badge-bar">
            
            <div class="col-md-10">
                <div class="row">
                    
                    @foreach($categories as $category)
                    
                        <div class="col-sm">
                            <button class="btn categoty-part w-100 btn-outline-primary @if($loop->first && !request()->has('category_id')) active @endif" id="category-{{ $category->id }}" onclick="setTabCategory2({{ $category->id }})">{{ $category->name }}</button>
                        </div>
                    @endforeach
                        {{-- <div class="col-sm">
                            <button class="btn categoty-part w-100 btn-outline-primary" onclick="showPartners()">شركاء الاحتفال</button>
                        </div> --}}
                </div>
            </div>

            <input type="hidden" id="category-id" value="1">
        </div>
        <div class="row justify-content-center" style="margin-top: 30px">
            <div class="col-md-10">
                <div class="row" id="halls-container">
@foreach($myHalls as $hall)
{{-- start halls loop --}}

                <div class="col-md-4 old-hall" style="padding-bottom: 30px!important;@if($hall->category_id != '1' ) display:none; @endif" id="{{$hall->category_id}}">
                <input type="hidden" value="{{$hall->category_id}}" >
    <div class="card">
        <div class="crop">
        <img class="card-img-top" style="cursor: pointer" src="{{$hall->image_url}}" alt="Card image cap">
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-9">
                    <h6 style="font-weight: bolder;font-size: 90%;" class="card-text">{{$hall->name}}</h6>
                </div>
                <div class="col-sm" style="font-size: 90%;">
                <span style="float: left;display: inline-flex;">
                    @for ($i = 0; $i < (int)$hall->avg_rating; $i++)
                            <i class="fas fa-fw fa-star" style="color: #FDD835!important;"></i>
                    @endfor
                </span>
                </div>
            </div>
            <p data-toggle="tooltip" data-placement="top" title="{{$hall->address}}" style="font-size: 90%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: auto;">{{$hall->address}}</p>


            <p style="font-size: 90%;">@lang('halls.capacity'):{{$hall->max_capacity}} شخص</p>
            <p style="font-size: 90%;">@lang('halls.price'):{{$hall->total_price}} ريال سعودي</p>
            {{-- <p style="color: green; font-weight: bolder;font-size: 90%;">متاحة</p> --}}
            <div class="row">

<div class="col-sm">
<a href="{{$hall->edit_url}}" class="btn btn-light" style="width: 100%">
<i class="fa fa-edit"></i> تعديل        </a>
</div>
<div class="col-sm">
<a onclick="deleteItem({{$hall->id}}, '{{$hall->delete_url}}')" class="btn btn-danger" style="width: 100%; cursor: pointer; color: #fff">
<i class="fa fa-trash"></i> حذف        </a>
</div>
</div>

        </div>
        <div class="card-footer bg-transparent p-0">
            <ul class="list-group list-group-flush p-0">
                <li class="list-group-item p-0" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;">
                <a class="btn btn-primary w-100" href="{{$hall->show_url}}" style="border-top-left-radius: 0;border-top-right-radius: 0;">عرض المزيد</a>
                </li>
            </ul>

        </div>
    </div>
</div>
{{-- end halls loop --}}
@endforeach
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="loader" style="display: none"></div>
        </div>
    </div>
    <input type="hidden" id="partner-shown" value="0">
@endsection

@section('styles')
    <style type="text/css">

        .card {
            border-radius: .7rem;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
           // flex-wrap: wrap;
        }

        .card-img-top {
            border-top-left-radius: .7rem;
            border-top-right-radius: .7rem;
        }

        .crop {
            width: 100%;
            //height: 150px;
            overflow: hidden;
        }

        .crop img {
            width: 100%;
            height: 150px;//margin: -75px 0 0 0;
        }


        .btn {
            border-radius: .7rem;
        }

        .btn-light {
            border: 1px solid #ccc;
        }

        .col-sm {
            padding: 5px;
        }

        /* .loader {
            border: 16px solid #f3f3f3; 
            border-top: 16px solid #3498db; 
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        } */

/* start loader style */
.loader {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255);
    z-index:9999;
    display:none;
    
}

@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

.loader::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border: 16px solid #f3f3f3; 
    border-top: 16px solid #3498db; 
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}
/* end */
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
    <script>

        var last_page = 0;
        $(document).ready(function () {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });
        });

        function showPartners() {
            $('.old-hall').remove();
            $('.partners').fadeIn();
            $('#partner-shown').val(1);
        }

        function setCategory(id) {
            $('#category-id').val(id);
            $('.old-hall').remove();
            page = 0;
            $('.btn-outline-primary').removeClass('active')
            getHalls();
        }
        function setTabCategory(id) {
            $('#category-id').val(id);
            $('.old-hall').remove();
            // page = 0;
            $('.btn-outline-primary').removeClass('active')
            $("#category-"+id).addClass( "active" );
        }

        function setTabCategory2(id) {
            $('#category-id').val(id);
            var slides=document.getElementsByClassName("old-hall");
            // document.getElementsByClassName("old-hall")[0].style.display="block";
            //loop here and add/remove hidden prop from each hall not where hall->category_id != $id
            for (var i = 0; i < slides.length; i++) {
if(slides.item(i).id == id){
    slides.item(i).style.display="block";
}
else {
    slides.item(i).style.display="none";
}
                    // slides.item(i).style.display="none";
                }
            $('.btn-outline-primary').removeClass('active')
            $("#category-"+id).addClass( "active" );
        }

        function deleteItem(id, url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method: 'delete'}
                }).done(function (data) {
                    swal({
                            title: data.status,
                            text: data.message,
                            type: data.type
                        },
                        function () {
                            location.reload();
                        }
                    );
                }).fail(function (e) {
                    console.log(e);
                    swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                })
            });
        }





    </script>

@endsection
