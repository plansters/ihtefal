@extends('layouts.app')

@section('title', 'الرئيسية')

@section('content')
    <div class="container-fluid h-100">
        <input id="myloc" name="map_location" type="text" value="{{ $city->location ? $city->location : '24.7104948,46.6797144' }}"/>
        <div id="map" style="width: 100%; height: 100%;"></div>

    </div>
@endsection

@section('styles')
<style>

</style>
@endsection


@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
            async defer></script>
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
    <script>

        var last_page = 0;
        $(document).ready(function () {

        });

        function initAutocomplete() {
            var markers = [];
            var input11 = document.getElementById("myloc").value
            var coords = input11.split(",");
            var input23 = new google.maps.LatLng(coords[0], coords[1]);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: input23,
                zoom: {{$city->zoom}},
                mapTypeId: 'roadmap'
            });



            @foreach($halls as $hall)
            
                latlong =  "{{ $hall->map_location }}".split(",");
                latitude = latlong[0];
                longitude = latlong[1];
                map_location = new google.maps.LatLng(latitude, longitude);
                placeMarker(map, map_location, '{{ $hall->name }}', '{{ $hall->show_url }}', '{{ $hall->total_price }}', '{{ (int)$hall->avg_rating * 2 }}', '{{ $hall->tags[1] }}');
            @endforeach

            function placeMarker(map, location, text, url, price, rating, is_reserved) {
                let is_reserved_str = '';
                if (is_reserved) {
                    is_reserved_str = `<p style="color: red; font-weight: bolder">@lang('halls.is_reserved')</p>`;
                } else {
                    is_reserved_str = `<p style="color: green; font-weight: 800">@lang('halls.is_free')</p>`;
                }
                var contentString = `<div id="content" style="min-width: 140px; font-family: 'Cairo'!important;">
                    <div id="siteNotice" style="padding: 7px">
                    <h6 id="firstHeading" style="font-weight: 700" class="firstHeading">${text}</h6>
                    <div id="bodyContent">
                    ${is_reserved_str}
                    <p><i class="fa fa-star" style="color: #FDD835!important;"></i> ${rating}  <span style="float: left; font-weight: 700" class="text-success"><i class="fa fa-money-bill-wave"></i> ${price} ر.س</span></p>
                    </div>
                    </div>
                    </div>`;

                var infowindow = new google.maps.InfoWindow({
                    content: contentString,
 
                });

                var marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: {url: '{{ asset('img/marker.png') }}', textAlign: 'left',origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(40,65),
                        labelOrigin: new google.maps.Point(17,15)},
                    label: {color: '#fff', text: price + ' ر.س '},
                    url: url
                });
                //$("#myloc").val(location.lat() + ',' + location.lng());
                //infowindow.open(map,marker);
                markers.push(marker);
                google.maps.event.addListener(marker, 'click', function() {window.location.href = marker.url;});
                marker.addListener('mouseover', function() {
                    infowindow.open(map, marker);
                });
                marker.addListener('mouseout', function() {
                    infowindow.close();
                });
               
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
                setMapOnAll(null);
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('myloc');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
    </script>

@endsection
