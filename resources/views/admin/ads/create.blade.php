@extends('layouts.app')

@section('title',trans('ads.create'))


@section('styles')

    <style>
        .wrapper-pic {
            position: relative;
            display: inline-block;
            opacity: 1;
        }

        .wrapper-pic:hover {
            transition: all .2s ease-in-out;
        }

        .wrapper-pic:hover .edit {
            transition: all .2s ease-in-out;
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            left: 20px;
            top: 10px;
            display: none;
        }
    </style>

@endsection

@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('ads.create')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.ads.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-8">


                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('ads.title')</label>

                                <div class="col-md-10">
                                    <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" required autofocus>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('ads.type')</label>

                                <div class="col-md-10">
                                    <select name="type" class="form-control" onchange="get_item_data(event)">
                                        <option value="ads" selected>@lang('ads.ads')</option>
                                        <option value="hall" >اعلان عن قاعة</option>
                                         
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="adForId" style="display: none">
                                <label class="col-md-2 col-form-label text-md-right">@lang('ads.item')</label>

                                <div class="col-md-10">
                                    <select class="form-control" id="ads_item" name="item_id">

                                    </select>
                                    <span style="color: red;font-family: monospace;font-size: inherit;">*Required</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">@lang('ads.image')</label>

                                <div class="col-md-10">
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>

                                    <input type="hidden" id="selected_item" name="hall_id">


                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                       @lang('admin.create')
                                    </button>

                                </div>
                            </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        function enableHallsSearching() {
            var searchUrl = '{{ route('admin.halls.search') }}';
            searchingFor(searchUrl);
            attachHall();


        }

        function attachHall(){
            $('#ads_item').off('select2:select').on('select2:select', function (event) {
                var id = $(event.currentTarget).find("option:selected").val();
                if(confirm('@lang('admin.sure_to_add_category')'))
                {
                    $('#selected_item').attr('value',id);
                }
            });
        }

        function searchingFor(url)
        {
            //console.log(url);
            if(url === '')
            {
                return;
            }
            $('#ads_item').empty();
            $('#ads_item').select2({
                placeholder: "@lang('admin.type_search_word')",
                minimumInputLength: 2,
                ajax: {
                    url: url,
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                      //  console.log(data);
                        return {
                            results: data
                        };
                    },
                    error: function (err) {
                        console.log(err);
                    },
                    cache: true
                }
            });
        }

       function get_item_data(event)
        {
            var selected_value  = $(event.target).val();
            if(selected_value === "hall"){
                enableHallsSearching();
                // $('#adForId').removeAttr('hidden');
                $("#adForId").css("display", "flex");
                
                }
            else if(selected_value === "ads")
            {
                $("#adForId").css("display", "none");
                $('#ads_item').empty();
                var ele = `<option value="">اعلان مجرد</option>`;
                $('#ads_item').append(ele);
            }
        }

        enableHallsSearching()
    </script>
@endsection
