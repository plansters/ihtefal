@extends('layouts.app')

@section('title',trans('ads.ads'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('ads.ads')</h1>
                <hr>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('ads.id')</th>
                            <th>@lang('ads.title')</th>
                            <th>@lang('ads.type')</th>
                            <th>@lang('ads.item')</th>
                            <th style="width:200px">@lang('ads.image')</th>

                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ads as $ad)
                            <tr class="odd gradeX">
                                <td>{{ $ad->id }}</td>
                                <td>{{ $ad->title ?? '' }}</td>
                                <td>{{ trans('ads.'.$ad->type) }}</td>
                                <td>{{ $ad->get_hall()->name ?? '' }}</td>
                                <td>
                                    <img src="{{ getMediaUrl($ad->image_path) }}" class="img-fluid img-thumbnail" style="max-width: 200px;max-height: 200px">
                                </td>
                                <td>
                                    <a href="{{ route('admin.ads.edit',$ad->id) }}"><i class="fa fa-edit"></i> </a> &nbsp;
                                    <a href="#" style="color: #dc3545" onclick="event.preventDefault();deleteItem('{{ $ad->id }}','{{ route('admin.ads.destroy',$ad->id) }}')"><i class="fa fa-trash"></i></a>&nbsp;
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                <div class="panel-footer">
                    <a href="{{ route('admin.ads.create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>



@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }



    </script>

@endsection
