@extends('layouts.app')

@section('title',trans('sidebar.roles'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.roles')</h1>
                <hr>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('roles.id')</th>
                            <th>@lang('roles.name')</th>
                            <th>@lang('roles.description')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr class="odd gradeX">
                                <td>{{ $role->id }}</td>
                                <td>{{ $role->name ?? '' }}</td>
                                <td>{{ $role->description ?? '' }}</td>

                                <td>
                                    @ifCanDo('update_roles')
                                    <a href="{{ route('admin.roles.edit',$role->id) }}"><i class="fa fa-edit"></i> </a> &nbsp;
                                    @endif
                                    @ifCanDo('delete_roles')
                                    <a href="#" style="color: #dc3545" onclick="event.preventDefault();deleteItem('{{ $role->id }}','{{ route('admin.roles.destroy',$role->id) }}')"><i class="fa fa-trash"></i></a>&nbsp;
                                    @endif
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                @ifCanDo('create_roles')
                <div class="panel-footer">
                    <a href="{{ route('admin.roles.create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
                </div>
                @endif
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>


@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }



    </script>

@endsection
