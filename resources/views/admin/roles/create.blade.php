@extends('layouts.app')

@section('title',trans('roles.create'))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('roles.create')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.roles.store') }}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('roles.name')</label>

                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                </div>
                                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-form-label text-md-right">@lang('roles.description')</label>

                                        <textarea name="description" class="form-control @error('description') is-invalid @enderror" cols="20" rows="4">{{ old('description') }}</textarea>

                                        @error('description')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3><i class="fa fa-lock"></i> @lang('sidebar.roles')</h3>
                                </div>

                                @foreach($permissions as $permission)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="permissions[]" class="custom-control-input" value="{{ $permission->id }}" id="customCheck{{ $permission->id }}">
                                                <label class="custom-control-label" for="customCheck{{ $permission->id }}">{{ $permission->name }}</label>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                                <div class="form-group col-md-12 mb-0">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('admin.create')
                                    </button>

                                </div>
                            </div>




                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
