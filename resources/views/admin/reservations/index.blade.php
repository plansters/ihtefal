@extends('layouts.app')

@section('title',trans('sidebar.reservations'))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center search-bar">
            <div class="col-md-10 justify-content-center" style="margin-top: 7%">
                <form action="{{ route('admin.reservations.index') }}">
                    <div class="row justify-content-center">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>@lang('halls.city')</label>
                                <select name="city_id" id="city-id" class="form-control" style="padding: 3px;">
                                    <option value="">جميع المدن</option>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}"
                                                @if($city->id == request()->city_id) selected @endif>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>@lang('halls.category_info')</label>
                                <select name="category_id" id="city-id" class="form-control" style="padding: 3px;">
                                    <option value="">جميع الصنيفات</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"
                                                @if($category->id == request()->category_id) selected @endif>{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>من (هجري)</label>
                                <input class="form-control" id="hijri-from" value="{{ request()->hijri_from }}"
                                       name="hijri_from" type="text" placeholder="تاريخ .." autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>الى (هجري)</label>
                                <input class="form-control" id="hijri-to" value="{{ request()->hijri_to }}"
                                       name="hijri_to" type="text" placeholder="تاريخ .." autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>من (ميلادي)</label>
                                <input class="form-control" id="from-date" value="{{ request()->from_date }}"
                                       name="from_date" type="text" placeholder="تاريخ .." autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>الى (ميلادي)</label>
                                <input class="form-control" id="to-date" value="{{ request()->to_date }}"
                                       name="to_date" type="text" placeholder="تاريخ .." autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>@lang('halls.status')</label>
                                <select name="status" id="status" class="form-control" style="padding: 3px;">
                                    <option value="">اختر حالة</option>
                                    <option value="pending" @if(request()->status == 'pending') selected @endif>غير
                                        مؤكد
                                    </option>
                                    <option value="approved" @if(request()->status == 'approved') selected @endif>مؤكد
                                    </option>
                                    <option value="completed" @if(request()->status == 'completed') selected @endif>
                                        مكتمل
                                    </option>
                                    <option value="cancelled" @if(request()->status == 'cancelled') selected @endif>
                                        ملغى
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label> اسم القاعة</label>
                                <input class="form-control" id="from-date" value="{{ request()->hall }}"
                                       name="hall" type="text" placeholder="اسم القاعة" autocomplete="off">
                            </div>
                        </div>
                         <div class="col-md-3">
                             <div class="form-group">
                                <label>نوع الحجز</label>
                                <select name="statusres" id="statusres" class="form-control" style="padding: 3px;">

                                    <option value="">
                                        الكل
                                    </option>
                                    <option value="ehtfal" @if(request()->statusres == 'ehtfal') selected @endif>
                                        محتفل
                                    </option>
                                    <option value="vendor" @if(request()->statusres == 'vendor') selected @endif>
                                        صاحب قاعة
                                    </option>

                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label> اسم الزبون</label>
                                <input class="form-control" id="from-date" value="{{ request()->user }}"
                                       name="user" type="text" placeholder="اسم الزبون" autocomplete="off">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label style="visibility: hidden">@lang('halls.gregorian')</label>
                                <input type="submit" class="btn btn-primary w-100" value="@lang('halls.search')">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        <div class="row justify-content-center">
            <h5>مجموع اسعار الحجوزات هو <span id="total_price_h"></span> ريال سعودي</h5>
        </div>
        <div class="row justify-content-center">
        <h5>مجموع عدد الحجوزات هو <span >{{$count}}</span> حجز</h5>
        </div>
        <div class="row justify-content-center" style="margin-top: 30px">
            <div class="col-md-10">
                <div class="row" id="halls-container">
                    @php $total_price = 0; @endphp
                    @forelse($reservations as $reservation)
                        <div class="col-md-6" style="padding-bottom: 30px!important;">
                            <div class="card" style="border-radius: 10px">
                                <div class="card-body">
                                    <p>@lang('reservations.created_at')
                                        : {{ date('Y-m-d', strtotime($reservation->created_at)) }} @if($reservation->status == 'pending')
                                       @if(Auth::user()->role=="admin" ||Auth::user()->role=="vendor")
                                        <button type="button"
                                                    onclick="doAction('{{ route('admin.reservations.cancel', $reservation->id) }}')"
                                                    class="close">×
                                            </button>
                                         @endif
                                            @endif</p>
                                    <p>@lang('reservations.hall'): {{ $reservation->hall->name ?? '' }}</p>
                                    <p>@lang('halls.price'): {{ $reservation->hall->get_current_price($reservation->hall->total_price, $reservation->start) ?? '' }}</p>
                                    @php $total_price += $reservation->hall->get_current_price($reservation->hall->total_price, $reservation->start) ?? 0 @endphp
                                    @if($reservation->user)
                                        <p>@lang('users.name') : {{ ($reservation->user->first_name ?? '') . ' ' . ($reservation->user->last_name ?? '') }}</p>
                                    @else
                                        <p>@lang('users.name') : {{ ($reservation->title ?? '') }}</p>
                                    @endif
                                    @if($reservation->coupon)
                                        <p>رقم كوبون الخصم : {{ ($reservation->coupon->coupon_id ?? '') }}</p>

                                    @endif
                                    <p>@lang('users.phone'): {{ $reservation->user->phone ?? '' }}</p>
                                    <p>قناةالحجز: {{ $reservation->source ?? '' }}</p>
                                    <p>@lang('users.email'): {{ $reservation->user->email ?? '' }}</p>
                                    <p>@lang('reservations.date'): {{ date('Y-m-d', strtotime($reservation->start)) }}
                                        /

                                        {{ convertToHijri($reservation->start) }}

                                    </p>
                                    <p>@lang('reservations.status')
                                        : {{ trans('reservations.' . $reservation->status) }}</p>
                                        @if(Auth::user()->role=="admin" || Auth::user()->role=="vendor")
                                        @if(in_array($reservation->hall_id, $my_halls))
                                        <select style="border: none"
                                                onchange="doAction('{{ route('admin.reservations.status', $reservation->id) }}', $(this).val())">
                                            <option value="">تغيير حالة الحجز</option>
                                            <option value="approved">@lang('reservations.approved')</option>
                                            <option value="pending">@lang('reservations.pending')</option>
                                            <option value="cancelled">@lang('reservations.cancelled')</option>
                                        </select>
                                        <!--a href="#" class="card-link">تثبيت الحجز</a-->
                                        @endif
                                        @endif
                                </div>
                            </div>
                        </div>
                    @empty
                        <h2 class="text-center w-100">لا يوجد نتائج مطابقة</h2>
                    @endforelse
                    <input type="hidden" id="totsl-price" value="{{ number_format($total_price) }}">

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="loader" style="display: none"></div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">

        $('#total_price_h').text($('#totsl-price').val());

        $('#from-date').datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
        });
        $('#hijri-from').calendarsPicker({
            calendar: $.calendars.instance('islamic'),
            monthsToShow: [1, 1],
            showOtherMonths: true
        });

        $('#to-date').datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
        });
        $('#hijri-to').calendarsPicker({
            calendar: $.calendars.instance('islamic'),
            monthsToShow: [1, 1],
            showOtherMonths: true
        });

        function doAction(url, status) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', status: status}
                }).done(function (data) {
                    //alert(data);
                    swal({
                            title: "{!! trans('admin.done') !!}",
                            text: "تم التعديل بنجاح",
                            type: 'success'
                        },
                        function () {
                            location.reload();
                        }
                    );
                }).fail(function (e) {
                    console.log(e);
                    swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                })
            });
        }
    </script>
@endsection
