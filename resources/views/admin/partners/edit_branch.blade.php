@extends('layouts.app')

@section('title','')

@section('content')
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div class="row justify-content-center">
    <div class="col-md-10" style="background-color: white; border: 1px solid #eee; border-radius: 5px; padding: 40px 0 40px 0; ; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
        <form method="POST" action="/admin/partners/updateBranch" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">الاسم</label>

            <input type="text" class="form-control" id="exampleInputEmail1" required placeholder="اسم الفرع" name="name" value="{{$branch->name}}">
              </div>
        
              <input type="hidden"  name="partner_id" value="{{$partner->id}}">
              <input type="hidden"  name="branch_id" value="{{$branch->id}}">
              <div class="row" id="partnerMapContainer">

                <input id="myloc" type="hidden" value="{{$branch->lat}},{{$branch->lon}}" />
                <input id="lat" type="hidden" value="{{$branch->lat}}" name="lat"/>
                <input id="lon" type="hidden" value="{{$branch->lon}}" name="lon"/>
                

        </div>

            <button type="submit" class="btn btn-primary">تعديل</button>
          
        </form>
        <div id="map" style="width: 100%; height: 400px"></div> 
                </div>
                   
            </div> 
            


<script type="text/javascript"> 

function initAutocomplete() {
var markers = [];
var input11 = document.getElementById("myloc").value;
var coords = input11.split(",");
var input23 = new google.maps.LatLng(coords[0], coords[1]);
var map = new google.maps.Map(document.getElementById('map'), {
    center: input23,
    zoom: 15,
    mapTypeId: 'roadmap'
});

google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
});

function placeMarker(map, location) {
    deleteMarkers();
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    $("#myloc").val(location.lat() + ',' + location.lng());
    $("#lat").val(location.lat());
    $("#lon").val(location.lng());
    //infowindow.open(map,marker);
    markers.push(marker);
    //console.log(markers);
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

// Create the search box and link it to the UI element.
var input = document.getElementById('myloc');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function () {
    searchBox.setBounds(map.getBounds());
});

var markers = [];
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function () {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
        return;
    }

    // Clear out the old markers.
    markers.forEach(function (marker) {
        marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function (place) {
        if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
        }
        var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
        }));

        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }
    });
    map.fitBounds(bounds);
});
}
 


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
async defer></script>
@endsection
  