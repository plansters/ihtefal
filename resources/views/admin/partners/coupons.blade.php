@extends('layouts.app')

{{-- @section('title','') --}}

@section('content')

    <div class="container-fluid">
    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" style=" float: left; ">
        إضافة مسؤول مبيعات
</button>
<!-- Modal -->
{{-- {{dd(Auth::User()->role == "partner")}} --}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">إضافة مسؤول مبيعات</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form method="get" action="/admin/partners/salesman/store/">
                @csrf

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="text-md-right">@lang('users.email')</label>

                        <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="mailInput" required>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                    </div>
                    <div class="form-group col-md-12">
                        <label class="text-md-right">@lang('users.password')</label>

                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required id="passwordInput">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                        @enderror

                    </div> 

                </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
        </div>
      </div>
    </div>
  </div>

                   <!-- end modal -->
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">خصومات</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>رمز الكوبون</th>
                            <th>اسم المستخدم</th>
                            <th>الحالة</th>
                            <th style="width: 124px;">تفاصيل الفاتورة</th>
                            <th>انتهاء الخصم
                            <small>(تاريخ اليوم: {{convertToHijri(Carbon\Carbon::now()->toDateString())}})</small>

                            </th>
                            {{-- <th>الإعدادات</th> --}}
                        </tr> 
                        </thead> 
                        <tbody>
                        @foreach($coupons as $coupons)
                            <tr class="odd gradeX">
                                <td>{{ $coupons->id }}</td>
                                <td>{{ $coupons->name ?? '' }}</td>
                                <td>{{ $coupons->Cuser ?? '' }}</td>
                                {{-- <td>{{ $coupons->status == 0 ? "غير مفعل" : "مفعل" }}</td> --}}

                                <td>                            
                                    <div class="form-group">
                                    <div class="custom-control custom-checkbox small">
                                        <input type="checkbox" class="custom-control-input" @if($coupons->status != 0) checked @else disabled @endif id="customCheck{{ $coupons->id }}" onchange="changeStatus({{ $coupons->id }},this.checked)">
                                        <label class="custom-control-label" for="customCheck{{ $coupons->id }}"></label>
                                    </div>
                                </div>
                            </td>
                            <td>
                                {{-- <div class="form-group row"> --}}
                                    <div class="col-xs-2 ">
                                      <input class="form-control form-control-sm" id="price_before{{$coupons->id}}" type="text" style="max-width: 161px" 
                                    value="{{$coupons->price_before}}"
                                    placeholder="السعر قبل الخصم"
                                      >
                                    </div>
                                    <br>
                                    <div class="col-xs-2 "style="margin-top: -19px;">
                                        <input class="form-control form-control-sm" id="price_after{{$coupons->id}}" type="text" style="max-width: 161px"
                                        value="{{$coupons->price_after}}"
                                        placeholder="السعر بعد الخصم"
                                        >
                                      </div>
                                      <br>
                                      @if($coupons->price_before == null && $coupons->price_after == null)
                                      <button type="button" class="btn btn-success btn-sm" style="margin-top: -19px;width: 140px;"
                                      onclick="updateInvoice({{$coupons->id}});"
                                      >تحديث</button>
                                @endif
                                      {{-- </div> --}}
                            </td>
                            <td>{{ $coupons->reservation_date ?? '' }}</td>
<script>
        function changeStatus(id,status) {
            var url = '/api/partner/activateCoupone'; 
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    id: id,
                    status:status,
                    _token: '<?php echo csrf_token(); ?>' }
            }).done(function(data) {}).fail(function(e) {
                swal("<?php echo trans('admin.fail'); ?>",e.responseJSON.message, "error")
            })
        }

</script>
                                {{-- <td>
                                   
                                    <a href="{{ route('admin.categories.edit',$coupons->id) }}" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                       
                                    <a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $coupons->id }}','{{ route('admin.categories.destroy',$coupons->id) }}')"><i class="fa fa-trash"></i></a>
                                   
                                </td> --}}
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->

                   <!-- Modal -->

            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                categorie: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: url,
                    type: 'POST',
                    datacategorie: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {
                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", categorie: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    })
                    .fail(function(e) {
                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
                    })
            });
        }



    </script>
<script>
function updateInvoice(id){
    // alert(id);
    var pb=$("#price_before"+id).val();
    var pa=$("#price_after"+id).val();
    // alert(pb);
    // alert(pa);
    $.ajaxSetup({
    headers: {
        'Accept':'application/json',
        'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    } 
});
 jQuery.ajax({
     url: "/api/partner/couponInvoice", 
    method: 'post', 
    data:{
        "id":id,
        "price_before":pb,
        "price_after":pa,
        "_token": "{{ csrf_token() }}",
    },
    success: function(result){
console.log(result);
if(result["status"]=="success"){
location.reload();
}
           }

 });
}
    
</script>
@endsection