@extends('layouts.app')

@section('content')

{{-- branches --}}
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            @include('flash-message')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">فروعنا</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>الاسم</th>
                        <th>عرض على الخريطة</th>
                        <th>الإعدادات</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($branches as $branch)
                        <tr class="odd gradeX">
                            <td>{{ $branch->id }}</td>
                            <td>{{ $branch->name ?? '' }}</td>
                            <td>{{ $branch->lat ?? '' }}{{ $branch->lon ?? '' }}</td>
                            <td>
                               
<a href="/admin/partners/branch/{{ $branch->id }}/details" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

<a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $branch->id }}','{{ route('admin.categories.destroy',$branch->id) }}')"><i class="fa fa-trash"></i></a>
                               
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
            <br>
            @ifCanDo('create_categories')
            <div class="panel-footer">
                <a href="{{ route('admin.categories.create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
            </div>
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

@endsection


@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
            }
        });

    });



    function deleteItem(id,url) {
        swal({
            title: '{!! trans('admin.are_you_sure') !!}',
            categorie: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "{!! trans('admin.yes'); !!}",
            cancelButtonText: "{!! trans('admin.no'); !!}",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            $.ajax({
                url: url,
                type: 'POST',
                datacategorie: 'JSON',
                data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
            })
                .done(function() {
                    swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", categorie: "success"},
                        function(){
                            location.reload();
                        }
                    );
                })
                .fail(function(e) {
                    swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
                })
        });
    }



</script>
@endsection