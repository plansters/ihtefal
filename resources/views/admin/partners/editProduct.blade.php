@extends('layouts.app')

@section('title','')

@section('content')
@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
@endif
<div class="row justify-content-center">
    
    <div class="col-md-10" style="background-color: white; border: 1px solid #eee; border-radius: 5px; padding: 40px 0 40px 0; ; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);padding: 32px;">
    
        <form method="POST" action="/admin/partners/product/update/existProduct" enctype="multipart/form-data">
            @csrf
            <span style="position: absolute;left: 30px;font-size: 150%;top: 18px;"><a href="#" onclick="window.history.back();"><i class="fas fa-arrow-left"></i></a></span>

            <div class="form-group">
                <label for="exampleInputEmail1">الاسم</label>
            <input type="text" class="form-control" id="exampleInputEmail1" value="{{$PartnerProduct->name}}" required placeholder="اسم الخدمة" name="name" >
              </div>
        
              <div class="form-group">
                <label for="exampleInputEmail2">السعر</label>
                <input type="number" class="form-control" min="0" id="exampleInputEmail2"  required placeholder="سعر الخدمة" name="price" value="{{trim($PartnerProduct->price)}}">
              </div>
              <input type="hidden"  name="partner_id" value="{{$partner->id}}">
              <input type="hidden"  name="product_id" value="{{$PartnerProduct->id}}">

              <div class="form-group row">
                <label class="col-md-2 col-form-label text-md-right">صورة الخدمة</label>
        
                <div class="col-md-4">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="customFile" accept="image/*">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                      </div>
                </div>
                <div class="col-md-6">
                    <div class="text-center">
                        <img src="/uploads/partners/{{$PartnerProduct->image}}" class="rounded" alt="..." style="
                        max-width: 426px;
                        max-height: 198px;
                    ">
                      </div>
                </div>
            </div>
            <div class="col text-center">
            <button type="submit" class="btn btn-primary">حفظ التغييرات</button>
            </div>
        </form>
                </div>
                <div class="modal-footer">

    </div>
</div>

@endsection
  