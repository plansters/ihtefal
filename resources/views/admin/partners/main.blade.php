@extends('layouts.app')

@section('title','')

@section('content')
@if (\Session::has('success'))
<div class="alert alert-success">
    <ul>
        <li>{!! \Session::get('success') !!}</li>
    </ul>
</div>

@endif 
<div class="row justify-content-center" style="margin-top: 30px">
    <div class="col-md-3">
<select class="form-control" id="discount_typeSelector" name="discount_typeSelector" style="width: 100%;margin-bottom: 20px;" onchange="discountTypeChang(this.value)">
    <option value="0" {{ $partner->discount_type == "0" ? "selected" : "" }}>بدون خصم</option>
    <option value="1" {{ $partner->discount_type == "1" ? "selected" : "" }}>خصم عام لكل المنتجات</option>
    <option value="2" {{ $partner->discount_type == "2" ? "selected" : "" }}>خصم حسب المنتج</option>
  </select>   
</div>

<div class="col-md-2" >
    <form method="get" action="/admin/partners/discount/edit" class="form-inline" id="gdForm" style="display: none">
        <div class="">
            @if(session()->has('message')) 
    <div class="alert alert-success">
        {{ session()->get('message') }}
        <script>location.reload();</script>
    </div>
@endif
<script>

function cityChang (disVal){
    // alert(disVal); 
    $.ajaxSetup({
                         headers: {
                             'Accept':'application/json',
                             'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                         } 
                     });
                      jQuery.ajax({
                          url: "/api/partner/city/change", 
                         method: 'post', 
                         data:{
                             "city_id":disVal,
                             "partner_id":{{$partner->user_id}},
                             "_token": "{{ csrf_token() }}",
                         },
                         success: function(result){
console.log(result);
if(result["status"]=="success"){
    location.reload();
} 
                                }

                      });
}


function discountTypeChang(disVal){
if(disVal == "1"){
$("#gdForm").show();
changeDiscType(disVal);
// location.reload();
}
else if(disVal == "2"){

$(".specId").show();
changeDiscType(disVal);
}
else{
    $("#gdForm").hide();
    changeDiscType(disVal);
    // location.reload();
}

}

function changeDiscType(typ){
    $.ajaxSetup({
                         headers: {
                             'Accept':'application/json',
                             'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                         } 
                     });
                      jQuery.ajax({
                          url: "/api/partner/discounType/change", 
                         method: 'post', 
                         data:{
                             "new_value":typ,
                             "partner_id":{{$partner->user_id}},
                             "_token": "{{ csrf_token() }}",
                         },
                         success: function(result){
console.log(result);
if(result["status"]=="success"){
    location.reload();
}
                                }

                      });

}
</script>

            <input type="number" value="{{$partner->discount}}" min="0" max="100" class="form-control mb-2 mr-sm-2" name="general_discount_value" placeholder="%" style="width: 49%;">
        <input type="hidden" name="partner" value="{{$partner->user_id}}">        
            @csrf
            <button type="submit" class="btn btn-primary mb-2">تحديث</button>
        </div>
    </form>
</div>
<div class="col-md-3">
    <div class="col-md-3">
        <select class="form-control" id="city_Selector" name="city_Selector" style="width: 300%;margin-bottom: 20px;" onchange="cityChang(this.value)">
          @foreach($cities as $city)
            <option value="{{$city->id}}" {{ $partner->city_id == $city->id ? "selected" : "" }}> {{$city->name}}</option>
          @endforeach
          </select>   
        </div>
</div>
    <div class="col-md-10">
        <div class="row" id="halls-container" >
            @foreach($partner->products as $product)
            <div class="col-md-3 old-hall" style="padding-bottom: 30px!important;">
                <div class="card">
                    <div class="crop">
                    <img class="card-img-top" style="cursor: pointer;max-height: 138px;" src="{{$product->image}}" alt="Card image cap">
                    </div>
                    <div class="card-body" >
                        <div class="row">
                            <div class="col-md-9">
                                <h6 style="font-weight: bolder;font-size: 90%;" class="card-text">{{$product->name}}</h6>
                            </div>
 
                        </div>
                        <p data-toggle="tooltip" data-placement="top" title="adadad" style="font-size: 90%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: auto;">السعر:{{ $partner->discount_type == "0" ? $product->price : $product->discount_price }}</p>
<p  style="font-size: 90%;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;width: auto;display:none"
 class="specId" id="specId{{$product->id}}">
 قيمة الخصم: {{$product->discount}} %
 <i class="fa fa-align-justify iconToHide" id="iconToHide" aria-hidden="true" onclick="changeInputs({{$product->id}});"></i>
 <form method="POST" action="/admin/partners/product/updatediscountPerProduct" class="form-inline hideOthers" id="{{$product->id}}" style="display: none">
    @csrf
    <input type="number" min="0" class="input-sm" name="newValue" value="{{$product->discount}}" style="width:90px !important"> 
    <input type="hidden" name="id" value="{{$product->id}}">
    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></button>
    <a class="btn btn-danger btn-sm" onclick="hideThis({{$product->id}})";>X</a>
 </form>
</p>

    
                        <div class="row">
             
                <div class="col-sm">
                    <a href="/admin/partners/product/update/{{$product->id}}/{{$partner->id}}" class="btn btn-light" style="width: 100%">
                        <i class="fa fa-edit"></i> تعديل                                </a>
                </div>
                <div class="col-sm">
                <a href="/admin/partners/product/remove/{{$product->id}}/{{$partner->id}}" class="btn btn-danger" style="width: 100%; cursor: pointer; color: #fff">
                        <i class="fa fa-trash"></i> حذف                                </a>
                </div>
            </div>

                    </div>
                </div>
            </div>
            @endforeach
            <script>
                function hideThis(id){
                    document.getElementById('specId'+id).style.display='block';
                    var prdcts = document.getElementsByClassName("hideOthers");
    for (var i = 0; i < prdcts.length; i++) {
        prdcts.item(i).style.display="none";
    }
    
    var icns = document.getElementsByClassName("iconToHide");
    for (var i = 0; i < icns.length; i++) {
        icns.item(i).style.display="inline-block";
    }
                } 
                        function changeInputs(id){
                            document.getElementById('specId'+id).style.display='none';
    
                            var prdcts = document.getElementsByClassName("hideOthers");
    for (var i = 0; i < prdcts.length; i++) {
        prdcts.item(i).style.display="none";
    }
    
    var icns = document.getElementsByClassName("iconToHide");
    for (var i = 0; i < icns.length; i++) {
        icns.item(i).style.display="none";
    }
    
    document.getElementById(''+id+'').style.display='flex';
                        }
                </script>
        </div>
        <!-- Button trigger modal -->
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalLong">
    اضافة خدمة
  </button>
  {{-- <br> <br> --}}

{{-- srart showing branches --}}
<hr style=" margin-right: -97px;
border-top: 2px solid #4f534c;
width: 122%;">  
<div class="col-md-10">
    <div class="row" style="   width: 712px;">
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">الاسم</th>
                <th scope="col" style="width: 1px;">إعدادات</th>
              </tr>
            </thead>
            <tbody>
                @foreach($branches as $branch)
              <tr>
              <th scope="row">{{$branch->id}}</th>
                <td>{{$branch->name}}</td>
                <td style="display: inline-flex">
                    <button type="button" class="btn btn-outline-danger" onclick="removeBranch({{$branch->id}});" style=" margin-left: 2px;">حذف</button>
                <a type="button" class="btn btn-outline-primary" href="/admin/partners/Branch/edit/{{$partner->id}}/{{$branch->id}}">تعديل</a>
                </td>
              </tr> 
        @endforeach
            </tbody>
          </table>
    </div>
</div>
<script>
    function removeBranch($id){
        var check = confirm("تأكيد حذف  الفرع؟");
  if (check == true) {
    $.ajaxSetup({
                         headers: {
                             'Accept':'application/json',
                             'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                         } 
                     });
                      jQuery.ajax({
                          url: "/api/partner/branch/delete", 
                         method: 'post', 
                         data:{
                             "id":$id,
                             "_token": "{{ csrf_token() }}",
                         },
                         success: function(result){
console.log(result);
if(result["status"]=="success"){
    location.reload();
}
                                }

                      });
  } 

    }
</script>
<br>
  <a  class="btn btn-primary" href="/admin/partners/Branch/addNew/{{$partner->id}}">
    اضافة فرع
  </a> 
{{-- end  showing branches --}}
  <!-- Modal -->
  <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
 <form method="POST" action="/admin/partners/product/add" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">الاسم</label>
        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="اسم الخدمة" name="name" required>
      </div>

      <div class="form-group">
        <label for="exampleInputEmail2">السعر</label>
        <input type="number" class="form-control" min="0" id="exampleInputEmail2" required  placeholder="سعر الخدمة" name="price" value="0">
      </div>
      <input type="hidden"  name="partner_id" value="{{$partner->id}}">

      <div class="form-group row">
        <label class="col-md-2 col-form-label text-md-right">صورة الخدمة</label>

        <div class="col-md-10">
            <input type="file" name="image" class="form-control" required accept="image/*">
        </div>
    </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">إنهاء</button>
          <button type="submit" class="btn btn-primary">حفظ التغييرات</button>
        </form>
        </div>
      </div>
    </div>
  </div>
  {{-- end modal --}}

  {{-- add branch modal --}}

  {{-- end --}}
    </div>
</div>


        @if($partner->discount_type == "1")
        <script>
            document.getElementById("gdForm").style.display="flex";//
        </script>
            @endif
    
            @if($partner->discount_type == "2")
            <script>
                var slides = document.getElementsByClassName("specId");
for (var i = 0; i < slides.length; i++) {
   slides.item(i).style.display="block";
}
        </script>
            @endif
    
    
            
@endsection