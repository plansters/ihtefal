@extends('layouts.app')

@section('title',trans('sidebar.halls'))
 
@section('content')
<style> 
    *{font-family: Cairo;}
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10" style="background-color: white; border: 1px solid #eee; border-radius: 5px; padding: 40px 0 40px 0; ; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="profile-img">
                            <img style="cursor: pointer;max-height: 271px;" data-toggle="modal" data-target="#img-modal" src="/uploads/{{ $partner->avatar_path }}" alt=""/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="profile-head">

                            <h4 style="font-weight: bolder;
                            font-family: Cairo;
                            font-size: large;">
                                {{ $partner->name }}
                            </h4>
 


                            <h5>
                                <div class="row" style="  margin-top: 17px;">
                                <img src="/img/icons/Path 44.svg" alt="" height="18" width="20" class="col-sm-1" /><div class="col-sm-11" style="text-align: justify;padding: 0px 0px 0px 52px;"><span>{{ $partner->city->name }}</span> </div> 
                            </div> 
                            </h5>

                            <h5>
                                <div class="row"style="  margin-top: 17px;">
                                <img src="/img/icons/Group 347.svg" alt="" height="18" width="20" class="col-sm-1" /><div class="col-sm-11" style="text-align: justify;padding: 0px 0px 0px 52px;"><span>{{ $partner->address }} </span> </div>
                            </div> 
                            </h5>
                            <h5>
                                <div class="row"style="  margin-top: 17px;">
                                <img src="/img/icons/Group 348.svg" alt="" height="18" width="20" class="col-sm-1"  /><div class="col-sm-11" style="text-align: justify;padding: 0px 0px 0px 52px;"><span>{{ $partner->neighborhood }} </span> </div>
                            </div> 
                            </h5>

                            <h5>
                                <div class="row" style="margin-top: 3%;">
<img src="/img/icons/Path 70.svg" alt="" height="18" width="20" class="col-sm-1" style="margin-top: 3%;"/><div class="col-sm-11" style="text-align: justify;padding: 20px  0px 0px 52px;"><span>{{ $partner->partner_word }}</span> </div>
                                </div>                          
</h5>
                        </div>
                    </div>
                </div>
            
                <div class="row">
                    
                    <div class="col-md-4">
                        <div class="profile-work">
                              <br>
                            <span style="text-decoration: none; font-weight: bolder;
                            font-family: Cairo;
                            font-size: large;"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;فروع أخرى: </span>
                        <br><br>
                        @foreach($branches as $branch)
                        <a href="#">&nbsp;&nbsp;&nbsp;&nbsp;{{ $branch->name }}</a><br/>
                        @endforeach
                        </div>
                        <input id="myloc" name="map_location" value="{{$partner->lat}},{{$partner->lon}}" type="hidden"/>
                        <div class="row">
                        <span class="col-sm-3" style="
                        color: #191097;
                        font-weight: 700;
                        font-family: Cairo;
                        margin-left: 0px;
                        padding-right: 33px;
                        top: 6px;
                        ">فروعنا: </span>
                            <div class="form-group col-sm-9">
                             
        <select class="form-control" id="exampleFormControlSelect1" style="width: 109%;padding-right: 0px;" onchange="initMovingMarker();">
<option value="{{$partner->lat}}" placeholder="{{$partner->lon}}" name="{{$partner->name}}" id="{{$partner->id}}">{{ $partner->name }}</option>
    @foreach($branches as $branch)
<option  value="{{$branch->lat}}" placeholder="{{$branch->lon}}" name="{{$branch->name}}" id="{{$branch->id}}">{{ $branch->name }}</option>
    @endforeach
                                </select>

                                
                                <script>
                                    function initMovingMarker(){
                                        var sel=document.getElementById('exampleFormControlSelect1');  
                                        // console.log(sel.options[ sel.selectedIndex ].getAttribute('id'));
reloadMap(sel.options[ sel.selectedIndex ].getAttribute('name'),sel.options[ sel.selectedIndex ].value,sel.options[ sel.selectedIndex ].getAttribute('placeholder'));
                                    }
                                </script>
@if(isset($branch))
<input type="hidden" value="{{$partner->lat}}" placeholder="{{$partner->lon}}" name="{{$partner->name}}" id="{{$branch->name}}" class="marker">
         @endif
@foreach($branches as $branch)
 <input type="hidden" value="{{$branch->lat}}" placeholder="{{$branch->lon}}" name="{{$branch->name}}" id="{{$branch->name}}" class="marker">

                @endforeach
                              </div>
                    </div>
                        <div id="map" style="width: 100%; height: 200px; margin-right: 15px"></div>
                        


                    </div><div class="col-md-8">
                        <h4 style="font-weight: bolder;
                        font-family: Cairo;
                        font-size: large;
                        float:right;
                        ">
                            الخدمات
                        </h4>
                       
                        @if($partner->discount_type =="1")
<h6 style="font-weight: bolder;
    color: green;
font-family: Cairo;
font-size: 0.9rem;
float:left;
margin-left: 65px;">
    حسم {{$partner->discount}}% على جميع الخدمات لمستخدمي احتفال
</h6>
@endif

@if($partner->discount_type =="2")
<h6 style="font-weight: bolder;
    color: green;
                        font-family: Cairo;
                        font-size: large;
float:left;
margin-left: 65px;">
حسم حسب الخدمة</h6>
@endif
<hr class="style-one" style="clear:both;margin: 0px;margin-top: -2px;border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 10));">
<div id="productsContainer">
    @foreach($partner->products as $product)
    <div class="row">
    <img src="{{$product->image}}" class="rounded float-right" alt="..." style="height: 100px;width: 100px;margin-right: 15px;    margin-top: 19px;margin-bottom: 19px;">
    <ul style="list-style: none;margin-top: 23px;">
        <li>
            <span style="
font-weight: bolder;
                        font-family: Cairo;
                        font-size: large;
        ">{{$product->name}}</span>
        </li>
        @if($partner->discount_type !="1" && $partner->discount_type !="2")
        <li>
            <span ><span style="color: #191097;font-family: Cairo;">السعر: </span>{{$product->price}} ر.س</span>
        </li>  
        @endif
        
        @if($partner->discount_type =="1")
        <li>
            <span style="color: red;font-family: Cairo;"><span style="color: #191097;">السعر قبل الخصم:</span> {{$product->price}} ر.س</span>
        </li>  
        <li>
            <span style="color: green;font-family: Cairo;"><span style="color: #191097;">السعر بعد الخصم: </span>{{$product->discount_price}} ر.س</span>
        </li>
        @endif

        @if($partner->discount_type =="2")
        <li>
            <span style="color: red;font-family: Cairo;"><span style="color: #191097;">السعر قبل الخصم:</span> {{$product->price}} ر.س</span>
        </li>  
        <li>
            <span style="color: green;font-family: Cairo;"><span style="color: #191097;">السعر بعد الخصم:</span> {{$product->discount_price}} ر.س</span> <span style="font-weight: 700;">{{$product->discount}}%</span>
        </li>
        @endif
    </ul>
    </div>
    <hr class="style-one" style="clear:both;margin: 0px;margin-top: -2px;border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 10));">
    
    @endforeach
    </div>
                    </div>
                </div>
            </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



@endsection
@section('styles')
    <style type="text/css">
        .pulsate {
            -webkit-animation: pulsate 3s ease-out;
            -webkit-animation-iteration-count: infinite;
            opacity: 0.5;
        }
        @-webkit-keyframes pulsate {
            0% {
                opacity: 0.5;
            }
            50% {
                opacity: 1.0;
            }
            100% {
                opacity: 0.5;
            }
        }
        /* ===================== */
        /* ==   FontAwesome   == */
        /* ===================== */
        @font-face {
            font-family:'FontAwesome';
            src:url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.eot?#iefix) format('eot'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.woff) format('woff'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.ttf) format('truetype'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.svg#FontAwesome) format('svg');
            font-weight: normal;
            font-style: normal;
        }

        div.rfWrapper {
            display:inline-block;
            vertical-align:top;
            height:36px;
        }
        /* ======================= */
        /* == CSS RATINGS FIELD == */
        /* ======================= */
        div.rfWrapper div.rfArea {
            display:block;
            direction:rtl;
            height:100%;
            font-size:0px;
            text-align:center;
            border-style:none;
            cursor:default;
            padding:0px;

            /* BOX SIZING */
            box-sizing:border-box;
            -moz-box-sizing:border-box;
            -webkit-box-sizing:border-box;
        }

        div.rfWrapper div.rfArea > input[type="radio"] {
            display:inline-block;
            width:36px;
            height:36px;
            position:absolute;
            top:-32px;
            clip:rect(0,0,0,0);
        }

        div.rfWrapper div.rfArea > label {
            display:inline-block;
            width:36px;
            height:36px;
            font-size:24px;
            line-height:40px;
            text-align:center;
            cursor:pointer;
            position:relative;
            overflow:hidden;
            text-indent:100%;
        }

        div.rfWrapper div.rfArea > label:before {
            font-family:'FontAwesome';
            content:'\f006';

            position:absolute;
            top: 0px;
            left: 0px;

            width:100%;
            height:100%;

            line-height:inherit;
            text-indent:0;
            color:#555555;

            cursor:inherit;

            padding-top:0px;

            /* CSS3 TEXT-SHADOW */
            text-shadow:0px 3px 0px rgba(0, 0, 0, 0.2);

            /* BOX-SIZING */
            box-sizing;border-box;
            -moz-box-sizing:border-box;
            -webkit-box-sizing:border-box;
        }

        div.rfWrapper div.rfArea > input[type="radio"]:checked ~ label:before {
            content:'\f005';
        }

        div.rfWrapper div.rfArea > label:hover:before,
        div.rfWrapper div.rfArea > label:hover ~ label:before {
            color:#FFFFFF;
        }

        div.rfWrapper div.rfArea > input[type="radio"]:checked ~ label:before {
            color:#F9A825;
        }

        div.rfWrapper div.rfArea > label:active {
            position:relative;
            top:2px;
        }

        div.rfWrapper div.rfArea > label:active:before {
            content:'\f005';

            /* CSS3 TEXT-SHADOW */
            text-shadow:0px 1px 0px rgba(0, 0, 0, 0.2);
        }

        .crop {
            width: 100%;
            height: 400px;
            overflow: hidden;
        }

        .crop img {
            width: 100%;
        / / height: 500 px;
            margin: -150px 0 0 0;
        }

        .emp-profile {
            padding: 3%;
            margin-top: 3%;
            margin-bottom: 3%;
            border-radius: 0.5rem;
            background: #fff;
        }

        /* .profile-img {
            text-align: center;
        }

        .profile-img img {
            width: 70%;
            height: 100%;
        } */
        .profile-img {
            text-align: center;
    max-width: 291px;
        }

        .profile-img img {
            width: 105%;
            height: 100%;
        }
        .profile-img .file {
            position: relative;
            overflow: hidden;
            margin-top: -20%;
            width: 70%;
            border: none;
            border-radius: 0;
            font-size: 15px;
            background: #212529b8;
        }

        .profile-img .file input {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
        }

        .profile-head h5 {
            /* color: #333; */
            text-decoration: none;
    color: #255c93;
    font-weight: 600;
    font-size: 15px;
    letter-spacing: 0.5px;
        }

        .profile-head h6 {
            color: #4F534C;
        }

        .profile-edit-btn {
            border: none;
            border-radius: 1.5rem;
            width: 70%;
            padding: 2%;
            font-weight: 600;
            color: #6c757d;
            cursor: pointer;
        }

        .proile-rating {
            font-size: 12px;
            color: #818182;
            margin-top: 5%;
        }

        .proile-rating span {
            color: #495057;
            font-size: 15px;
            font-weight: 600;
        }

        .profile-head .nav-tabs {
            margin-bottom: 5%;
        }

        .profile-head .nav-tabs .nav-link {
            font-weight: 600;
            border: none;
        }

        .profile-head .nav-tabs .nav-link.active {
            border: none;
            border-bottom: 2px solid #4F534C;
        }

        .profile-work {
            padding: 0 14% 14% 14%;
            margin-top: 0px;
        }

        .profile-work p {
            font-size: 12px;
            color: #818182;
            font-weight: 600;
            margin-top: 10%;
        }

        .profile-work a {
            text-decoration: none;
    color: #255c93;
    font-weight: 600;
    font-size: 15px;
    letter-spacing: 0.5px;

        }

        .profile-work ul {
            list-style: none;
        }

        .profile-tab label {
            font-weight: 600;
        }

        .profile-tab p {
            font-weight: 600;
            color: #4F534C;
        }


        .img-wrapper {
            position: relative;
            margin-top: 15px;
        }
        .img-wrapper img {
            width: 100%;
        }
        .img-overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0;
        }
        .img-overlay i {
            color: #fff;
            font-size: 3em;
        }


        #overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 999;Removes blue highlight -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        #overlay img {
            margin: 0;
            width: 80%;
            height: auto;
            object-fit: contain;
            padding: 5%;
        }

        #nextButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #nextButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #prevButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #prevButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #exitButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
            position: absolute;
            top: 15px;
            right: 15px;
        }
        #exitButton:hover { 
            opacity: 0.7;
            font-size: 3em;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
            async defer></script>
<script type="text/javascript">
var map="";
    $( ".img-wrapper" ).hover(
        function() {
            $(this).find(".img-overlay").animate({opacity: 1}, 600);
        }, function() {
            $(this).find(".img-overlay").animate({opacity: 0}, 600);
        }
    );

    // Lightbox
    var $overlay = $('<div id="overlay"></div>');
    var $image = $("<img>");
    var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-right"></i></div>');
    var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-left"></i></div>');
    var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

    // Add overlay
    $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
    $("#gallery").append($overlay);

    // Hide overlay on default
    $overlay.hide();

    // When an image is clicked
    $(".img-overlay").click(function(event) {
        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        var imageLocation = $(this).prev().attr("href");
        // Add the image src to $image
        $image.attr("src", imageLocation);
        // Fade in the overlay
        $overlay.fadeIn("slow");
    });

    // When the overlay is clicked
    $overlay.click(function() {
        // Fade out the overlay
        $(this).fadeOut("slow");
    });

    // When next button is clicked
    $nextButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").next().find("img"));
        // All of the images in the gallery
        var $images = $("#image-gallery img");
        // If there is a next image
        if ($nextImg.length > 0) {
            // Fade in the next image
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        } else {
            // Otherwise fade in the first image
            $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
        }
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When previous button is clicked
    $prevButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").prev().find("img"));
        // Fade in the next image
        $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When the exit button is clicked
    $exitButton.click(function() {
        // Fade out the overlay
        $("#overlay").fadeOut("slow");
    });


var map;

      function initAutocomplete() {
        let  infoWindow,positionCords;

     map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng({{$partner->location}}),
      zoom: 17,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: true
    });

    directionsService = new google.maps.DirectionsService,
        directionsDisplay = new google.maps.DirectionsRenderer({
        map: map,
        suppressMarkers : true
         });




    infoWindow = new google.maps.InfoWindow;
        var markers = document.getElementsByClassName('marker');
        Array.prototype.forEach.call(markers, function(markerElem) {
          var id = markerElem.getAttribute('id');
          var name = markerElem.getAttribute('name');
          var point = new google.maps.LatLng(
              parseFloat(markerElem.getAttribute('value')),
              parseFloat(markerElem.getAttribute('placeholder')));

          var infowincontent = document.createElement('div');
          var strong = document.createElement('strong');
          strong.textContent = name
          infowincontent.appendChild(strong);
          var marker = new google.maps.Marker({
            map: map,
            
            position: point,
            title:name,
            icon: {  
                textalign:'right',
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(60,65),
                labelOrigin: new google.maps.Point(17,15)
                },
                label: {
                    color: '#06023a',
                     text: name,
                     fontWeight: "bold"
                      }
          }
          );
          marker.addListener('click', function() {
            infoWindow.setContent(infowincontent);
            infoWindow.open(map, marker);
          });
        });

//         function calculateAndDisplayRoute(directionsService, directionsDisplay, currentLocation, dest) {
//   directionsService.route({
//     origin:currentLocation ,
//     destination: dest,
//     travelMode: google.maps.TravelMode.DRIVING
//   }, function(response, status) {
//     if (status == google.maps.DirectionsStatus.OK) {
//       directionsDisplay.setDirections(response);
//     } else {
//       window.alert('Directions request failed due to ' + status);
//     }
//   });
// } 
navigator.geolocation.getCurrentPosition(
            function(position) {
                // Get current cordinates.
                positionCords = position.coords.latitude+','+position.coords.longitude;
                console.log("currentLocation",positionCords);
            },
            function(error) {
                console.log("coudn't get location")
            },
            {timeout: 30000, enableHighAccuracy: false, maximumAge: 75000}
    );

var gotoMapButton = document.createElement("div");
  gotoMapButton.setAttribute("id", "dirButton");
gotoMapButton.setAttribute("style", "margin: 5px;border: 1px solid;padding: 1px 12px;cursor: pointer;background-image: url('/uploads/gmi.png');background-repeat: no-repeat, repeat;background-size: 10px;z-index: 0;position: absolute;left: 0px;top: 0px;");
gotoMapButton.innerHTML = "View Directions";
map.controls[google.maps.ControlPosition.TOP_LEFT].push(gotoMapButton);
google.maps.event.addDomListener(gotoMapButton, "click", function () {
    if(positionCords != 'undefined'){
        var url = 'https://www.google.com/maps/dir/'+positionCords +'/'+ encodeURIComponent($("#myloc").val());
    }else{
    var url = 'https://www.google.com/maps/place/' + encodeURIComponent($("#myloc").val());
    }
    window.open(url);
});


    }
    function reloadMap(name,lat,lan){
    var myLatlng = new google.maps.LatLng(lat,lan);
    var marker = new google.maps.Marker({
    position: myLatlng,
    title:name,
});
$po= new google.maps.LatLng( lat, lan );
    marker.setPosition($po);
    $("#myloc").val(lat + ',' + lan);
    // initAutocomplete();
map.panTo( new google.maps.LatLng( lat, lan ) );
}

</script>
@endsection