@extends('layouts.app')

@section('title',trans('sidebar.halls'))

@section('content')
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="loginMoadlButton" hidden>
    login
  </button>
  
  <!-- start login modal  -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">البريد او الرقم *</label>
                  <input required type="text" class="form-control"  id="exampleInputEmail1"  placeholder="البريد أو الرقم">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">كلمة السر *</label>
                  <input required type="password" class="form-control" id="exampleInputPassword1" placeholder="كلمة السر" >
                </div>
            </form>

            <a href="#" data-toggle="modal" data-target="#exampleModalLong" data-dismiss="modal">
                إنشاءحساب؟
            </a>
        </div>
        <div id="msgs">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">إنهاء</button>
          <button type="submit" class="btn btn-primary" onclick="loginModal()">تسجيل الدخول</button>
        </div>
      </div>
    </div> 
  </div>
  {{-- end login modal --}}



  {{-- start register modal --}}
  <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-body">
            <form >
                @csrf
                <div class="row">
              
            <div class="col-sm-6"> 
                        <div class="form-group">
                            <label for="first_name">الاسم الاول</label>
                            <input type="text" class="form-control"  name="first_name" id="first_name"  >
                          </div>
             </div>
             <div class="col-sm-6">             
                          <div class="form-group">
                            <label for="last_name">الاسم الاخير</label>
                            <input type="text" class="form-control" name="last_name" id="last_name"  >
                          </div>
            </div> 
            
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="phone"> الهاتف </label>
                    <input type="text" name="phone" class="form-control" id="phone"  >
                  </div>
            
                  <div class="form-group">
                    <label for="email"> البريد الالكتروني </label>
                    <input type="email" name="email" class="form-control" id="email"  >
                  </div>
            
                </div> 
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="register_password_input"> كلمة السر </label>
                        <input type="password" class="form-control" name="password" id="register_password_input">
                      </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password_confirmation"> تاكيد كلمة السر  </label>
                        <input type="password" class="form-control " name="password_confirmation" id="password_confirmation">                      </div>
                </div>

                
                    <div class="form-group col-sm-6">
                        <label class="text-md-right">التسجيل عن طريق</label>
                        <select style="padding-top: 0;" class="form-control " name="register_type" id="register_type">
                            <option value="phone">الهاتف</option>
                            <option value="email">البريد الالكتروني</option>
                        </select>
                        
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="text-md-right">نوع الحساب </label>
                        <select style="padding-top: 0;" class="form-control " name="role" id="role">
                            <option value="client">المحتفل</option>
                            <option value="vendor">صاحب قاعة</option>
                        </select>
                        
                    </div>
              
                    {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                </div>
            
        </div>
        <div id="msgs2">
        </div>
        <div class="modal-footer">
          {{-- <button type="submit" class="btn btn-primary">إنشاء حساب</button> --}}
          <a  class="btn btn-primary" onclick="registerModal()">إنشاء حساب</a>

        </form>
        </div>
      </div>
    </div>
  </div>
    {{-- end register modal --}}

{{-- start register ajax --}}
<script>
    function registerModal(){
        $first_name= $("#first_name").val();
        $last_name= $("#last_name").val();
        $phone= $("#phone").val();
        $email= $("#email").val();
        $password= $("#register_password_input").val();
        $confirm_password= $("#password_confirmation").val();
        $role= $("#role").val();
        $register_type= $("#register_type").val();

        if(!$first_name || !$last_name || !$password || !$confirm_password){
       $("#msgs2").append('<div class="alert alert-danger" role="alert"> الرجاء ملئ جميع الحقول</div>');
        }
    else{

    
        $.ajaxSetup({
                             headers: {
                                 'Accept':'application/json',
                                 'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                             } 
                         });
                          jQuery.ajax({
                              url: "{{ route('register') }} ", 
                             method: 'post', 
                             data:{
                                 "first_name": $first_name,
                                 "last_name": $last_name,
                                 "phone": $phone,
                                 "email": $email,
                                 "password": $password,
                                 "password_confirmation": $confirm_password,
                                 "role": $role,
                                 "register_type": $register_type,
                                 "_token": "{{ csrf_token() }}"
                                //  "method_field":"{{ route('register') }} "
                             },
                             statusCode: {
                                200: function (response) {
                                       location.reload();
                                    console.log(response);
                                  },
          422: function (response) {
             $("#msgs").append('<div class="alert alert-danger" role="alert"> معلومات غير صحيحة</div>');
          },
          404: function (response) {
            $("#msgs").append('<div class="alert alert-danger" role="alert">معلومات غير صحيحة</div>');
          }
          },
                             success: function(result){
           console.log(result); 
           alert(result);
                                    }
                          });
                        }
           }
           
    
    </script>
      {{-- end register ajax --}}



  {{-- start login ajax --}}
<script>
function loginModal(){
    $user= $("#exampleInputEmail1").val();
    $password= $("#exampleInputPassword1").val();
    if(!$user || !$password){
   $("#msgs").append('<div class="alert alert-danger" role="alert"> المعرف/كلمة السر مطلوبين</div>');
    }
else{


    $.ajaxSetup({
                         headers: {
                             'Accept':'application/json',
                             'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                         } 
                     });
                      jQuery.ajax({
                          url: "/login", 
                         method: 'post', 
                         data:{
                             "identity":$user,
                             "password":$password,
                             "_token": "{{ csrf_token() }}",
                         },
                         statusCode: {
                            200: function (response) {
                                   location.reload();
                              },
      422: function (response) {
         $("#msgs").append('<div class="alert alert-danger" role="alert"> المعرف أو كلمة السر غير صحيحة</div>');
      },
      404: function (response) {
        $("#msgs").append('<div class="alert alert-danger" role="alert"> المعرف أو كلمة السر غير صحيحة</div>');
      }
      },
                         success: function(result){
       console.log(result); 
       alert(result);
                                }
                      });
                    }
       }
       

</script>
  {{-- end login ajax --}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10" style="background-color: white; border: 1px solid #eee; border-radius: 5px; padding: 40px 0 40px 0; ; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                <div class="row justify-content-center">

                    <div class="col-md-4">
                        <div class="profile-img">
                            <img style="cursor: pointer;" data-toggle="modal" data-target="#img-modal" src="{{ $partner->avatar_path }}" alt=""/>
                        </div>
                    </div>
                    <span style="position: absolute; left: 30px; font-size: 150%; z-index: 100000"><a href="#" onclick="window.history.back();"><i class="fas fa-arrow-left"></i></a></span>
                    <div class="col-md-8">
                        <div class="profile-head">
                            <h5>
                                {{ $partner->name }}
                            </h5>
                            
                            <h6>
                                <i style="color: #4F534C" class="fa fa-money-bill-wave"></i> السعر: {{ $partner->price }} ريال سعودي
                            </h6>
 
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                       aria-controls="home" aria-selected="true">@lang('halls.information')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                       aria-controls="profile" aria-selected="false">@lang('halls.description')</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#gallery" role="tab"
                                       aria-controls="gallery" aria-selected="false">الصور</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="ratings-tab" data-toggle="tab" href="#ratings" role="tab"
                                       aria-controls="ratings" aria-selected="false">@lang('halls.ratings')</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-work">
                            <a href="#"><i style="color: #4F534C" class="fas fa-fw fa-city"></i> &nbsp; {{ $partner->name }}</a><br/>
                            <a href="#"><i style="color: #4F534C" class="fas fa-fw fa-location-arrow"></i> &nbsp; {{ $partner->neighborhood }}</a><br/>
                            <a href="#"><i style="color: #4F534C" class="fas fa-address-card"></i> &nbsp; {{ $partner->address }}</a><br/>
                        </div>
                        <input id="myloc" name="map_location" value="{{ $branch->lat }},{{ $branch->lon }}" type="hidden"/>
                        <div id="map" style="width: 100%; height: 200px; margin-right: 15px"></div>
                    </div>
                    <div class="col-md-8">
                        <div class="tab-content profile-tab" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                {{-- @foreach($hall->extras->chunk(2) as $extras)
                                
                                    <div class="row">
                                        @foreach($extras as $extra)
                                            <div class="col-md-3">
                                                <label>{{ $extra->categoryExtra->field_name }}:</label>
                                            </div>
                                            <div class="col-md-3">
                                                <p>{{ $extra->value }}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach --}}
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                {{ $partner->description }}
                            </div>
                            <div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="gallery-tab">
                                <section id="gallery">
                                    <div class="container">
                                        <div id="image-gallery">
                                            <div class="row">
                                                {{-- @foreach($hall->media as $media)
                                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
                                                        <div class="img-wrapper">
                                                            <a href="{{ $media->media_url }}"><img src="{{ $media->media_url }}" class="img-responsive"></a>
                                                            <div class="img-overlay">
                                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach --}}
                                            </div><!-- End row -->
                                        </div><!-- End image gallery -->
                                    </div><!-- End container -->
                                </section>
                            </div>
                            <div class="tab-pane fade" id="ratings" role="tabpanel" aria-labelledby="ratings-tab">

                                <div class="row">
                                    {{-- @foreach($hall->rating as $rating)
                                        <div class="col-md-11">
                                            <h5 style="font-weight: 700;">{{ $rating->user->first_name }}</h5>
                                        </div>

                                        <div class="col-md-11" style="color: #FDD835">
                                            @for($i = 1; $i <= $rating->rate; $i++)
                                                <i class="fas fa-fw fa-star"></i>
                                            @endfor
                                        </div>


                                        <div class="col-md-11" style="padding: 10px">
                                            <p>{{ $rating->note }}</p>
                                        </div>
                                    @endforeach --}}
                                </div>
                            </div>
                        </div>
                        @if(request()->has('date') && request()->date)
                        <div class="row">
                            @if($chk_status=="Available")
                            <div class="col-md-6">
                                <br><br>
                                {{-- <a @auth data-toggle="modal" data-target="#reserve_modal" href="#" @else href="{{ route('login') }}" @endauth class="btn btn-danger w-100" style=" font-weight: bolder"><i class="fas fa-fw fa-calendar-day"></i> &nbsp; @lang('halls.reserve')</a> --}}
                                <a @auth data-toggle="modal" data-target="#reserve_modal" href="#" @else onclick="$('#loginMoadlButton').click();" @endauth class="btn btn-danger w-100" style=" font-weight: bolder"><i class="fas fa-fw fa-calendar-day"></i> &nbsp; @lang('halls.reserve')</a>
  {{--  --}}
                            </div>
                            @endif
                            <div class="col-md-5">
                                <br><br>
                                <a class="btn btn-success w-100" style="font-weight: bolder" @auth href="#" @else onclick="$('#loginMoadlButton').click();" @endauth><i class="fas fa-fw fa-heart"></i> &nbsp; @lang('halls.add_favorite')</a>
                            </div>
                        </div>
                        @else
                            <div class="row">
                                
                                {{-- @if($chk_status=="Available")
                                <div class="col-md-6">
                                    <br><br>   
                                    <a @auth data-toggle="modal" data-target="#reserve_modal_no_dates" href="#" @else onclick="$('#loginMoadlButton').click();" @endauth class="btn btn-danger w-100" style=" font-weight: bolder"><i class="fas fa-fw fa-calendar-day"></i> &nbsp; @lang('halls.reserve')</a>
                                </div>
@endif --}}
                                {{-- @if($is_favorite)
                                    <div class="col-md-5">
                                        <br><br>
                                        <a class="btn btn-success w-100" style="font-weight: bolder" href="{{ route('admin.hall.remove.favorite', $hall->id) }}"><i class="fas fa-fw fa-heart-broken"></i> &nbsp; حذف من المفضلة</a>
                                    </div>
                                @else
                                    <div class="col-md-5">
                                        <br><br>
                                        <a class="btn btn-success w-100" style="font-weight: bolder" href="{{ route('admin.hall.add.favorite', $hall->id) }}"><i class="fas fa-fw fa-heart"></i> &nbsp; @lang('halls.add_favorite')</a>
                                    </div>
                                @endif --}}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>

    </div>



@endsection
@section('styles')
    <style type="text/css">
        .pulsate {
            -webkit-animation: pulsate 3s ease-out;
            -webkit-animation-iteration-count: infinite;
            opacity: 0.5;
        }
        @-webkit-keyframes pulsate {
            0% {
                opacity: 0.5;
            }
            50% {
                opacity: 1.0;
            }
            100% {
                opacity: 0.5;
            }
        }
        /* ===================== */
        /* ==   FontAwesome   == */
        /* ===================== */
        @font-face {
            font-family:'FontAwesome';
            src:url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.eot?#iefix) format('eot'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.woff) format('woff'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.ttf) format('truetype'),
            url(https://netdna.bootstrapcdn.com/font-awesome/2.0/font//fontawesome-webfont.svg#FontAwesome) format('svg');
            font-weight: normal;
            font-style: normal;
        }

        div.rfWrapper {
            display:inline-block;
            vertical-align:top;
            height:36px;
        }
        /* ======================= */
        /* == CSS RATINGS FIELD == */
        /* ======================= */
        div.rfWrapper div.rfArea {
            display:block;
            direction:rtl;
            height:100%;
            font-size:0px;
            text-align:center;
            border-style:none;
            cursor:default;
            padding:0px;

            /* BOX SIZING */
            box-sizing:border-box;
            -moz-box-sizing:border-box;
            -webkit-box-sizing:border-box;
        }

        div.rfWrapper div.rfArea > input[type="radio"] {
            display:inline-block;
            width:36px;
            height:36px;
            position:absolute;
            top:-32px;
            clip:rect(0,0,0,0);
        }

        div.rfWrapper div.rfArea > label {
            display:inline-block;
            width:36px;
            height:36px;
            font-size:24px;
            line-height:40px;
            text-align:center;
            cursor:pointer;
            position:relative;
            overflow:hidden;
            text-indent:100%;
        }

        div.rfWrapper div.rfArea > label:before {
            font-family:'FontAwesome';
            content:'\f006';

            position:absolute;
            top: 0px;
            left: 0px;

            width:100%;
            height:100%;

            line-height:inherit;
            text-indent:0;
            color:#555555;

            cursor:inherit;

            padding-top:0px;

            /* CSS3 TEXT-SHADOW */
            text-shadow:0px 3px 0px rgba(0, 0, 0, 0.2);

            /* BOX-SIZING */
            box-sizing;border-box;
            -moz-box-sizing:border-box;
            -webkit-box-sizing:border-box;
        }

        div.rfWrapper div.rfArea > input[type="radio"]:checked ~ label:before {
            content:'\f005';
        }

        div.rfWrapper div.rfArea > label:hover:before,
        div.rfWrapper div.rfArea > label:hover ~ label:before {
            color:#FFFFFF;
        }

        div.rfWrapper div.rfArea > input[type="radio"]:checked ~ label:before {
            color:#F9A825;
        }

        div.rfWrapper div.rfArea > label:active {
            position:relative;
            top:2px;
        }

        div.rfWrapper div.rfArea > label:active:before {
            content:'\f005';

            /* CSS3 TEXT-SHADOW */
            text-shadow:0px 1px 0px rgba(0, 0, 0, 0.2);
        }

        .crop {
            width: 100%;
            height: 400px;
            overflow: hidden;
        }

        .crop img {
            width: 100%;
        / / height: 500 px;
            margin: -150px 0 0 0;
        }

        .emp-profile {
            padding: 3%;
            margin-top: 3%;
            margin-bottom: 3%;
            border-radius: 0.5rem;
            background: #fff;
        }

        .profile-img {
            text-align: center;
        }

        .profile-img img {
            width: 70%;
            height: 100%;
        }

        .profile-img .file {
            position: relative;
            overflow: hidden;
            margin-top: -20%;
            width: 70%;
            border: none;
            border-radius: 0;
            font-size: 15px;
            background: #212529b8;
        }

        .profile-img .file input {
            position: absolute;
            opacity: 0;
            right: 0;
            top: 0;
        }

        .profile-head h5 {
            color: #333;
        }

        .profile-head h6 {
            color: #4F534C;
        }

        .profile-edit-btn {
            border: none;
            border-radius: 1.5rem;
            width: 70%;
            padding: 2%;
            font-weight: 600;
            color: #6c757d;
            cursor: pointer;
        }

        .proile-rating {
            font-size: 12px;
            color: #818182;
            margin-top: 5%;
        }

        .proile-rating span {
            color: #495057;
            font-size: 15px;
            font-weight: 600;
        }

        .profile-head .nav-tabs {
            margin-bottom: 5%;
        }

        .profile-head .nav-tabs .nav-link {
            font-weight: 600;
            border: none;
        }

        .profile-head .nav-tabs .nav-link.active {
            border: none;
            border-bottom: 2px solid #4F534C;
        }

        .profile-work {
            padding: 0 14% 14% 14%;
            margin-top: -30px;
        }

        .profile-work p {
            font-size: 12px;
            color: #818182;
            font-weight: 600;
            margin-top: 10%;
        }

        .profile-work a {
            text-decoration: none;
            color: #495057;
            font-weight: 600;
            font-size: 14px;
        }

        .profile-work ul {
            list-style: none;
        }

        .profile-tab label {
            font-weight: 600;
        }

        .profile-tab p {
            font-weight: 600;
            color: #4F534C;
        }


        .img-wrapper {
            position: relative;
            margin-top: 15px;
        }
        .img-wrapper img {
            width: 100%;
        }
        .img-overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0;
        }
        .img-overlay i {
            color: #fff;
            font-size: 3em;
        }


        #overlay {
            background: rgba(0, 0, 0, 0.7);
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 999;Removes blue highlight -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        #overlay img {
            margin: 0;
            width: 80%;
            height: auto;
            object-fit: contain;
            padding: 5%;
        }

        #nextButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #nextButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #prevButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
        }
        #prevButton:hover {
            opacity: 0.7;
            font-size: 3em;
        }

        #exitButton {
            color: #fff;
            font-size: 2em;
            transition: opacity 0.8s;
            position: absolute;
            top: 15px;
            right: 15px;
        }
        #exitButton:hover { 
            opacity: 0.7;
            font-size: 3em;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
            async defer></script>
<script type="text/javascript">

    // function getHallReserved(date) {
    //     var url = '/';
    //     $.ajax({
    //         url: url,
    //         type: 'POST',
    //         data: {partner_id: '{{ $partner->id }}', date: date, _token: '{!! csrf_token() !!}'},
    //         beforeSend: () => {
    //             $('#send-button').hide();
    //             $('#send-a').show(); 
    //         }
    //     }).done(function(data) {
    //         if (data != 0) {
    //             swal("خطأ", "القاعة محجوزة في التاريخ المختار يرجى اختيار تاريخ آخر", "error");
    //             $('#from-date').val('');
    //             $('#hijri-from').val('');
    //         }
    //         $('#send-button').show();
    //         $('#send-a').hide();

    //     }).fail(function(e) {
    //         swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
    //     })
    // }





    $( ".img-wrapper" ).hover(
        function() {
            $(this).find(".img-overlay").animate({opacity: 1}, 600);
        }, function() {
            $(this).find(".img-overlay").animate({opacity: 0}, 600);
        }
    );

    // Lightbox
    var $overlay = $('<div id="overlay"></div>');
    var $image = $("<img>");
    var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-right"></i></div>');
    var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-left"></i></div>');
    var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

    // Add overlay
    $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
    $("#gallery").append($overlay);

    // Hide overlay on default
    $overlay.hide();

    // When an image is clicked
    $(".img-overlay").click(function(event) {
        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        var imageLocation = $(this).prev().attr("href");
        // Add the image src to $image
        $image.attr("src", imageLocation);
        // Fade in the overlay
        $overlay.fadeIn("slow");
    });

    // When the overlay is clicked
    $overlay.click(function() {
        // Fade out the overlay
        $(this).fadeOut("slow");
    });

    // When next button is clicked
    $nextButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").next().find("img"));
        // All of the images in the gallery
        var $images = $("#image-gallery img");
        // If there is a next image
        if ($nextImg.length > 0) {
            // Fade in the next image
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        } else {
            // Otherwise fade in the first image
            $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
        }
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When previous button is clicked
    $prevButton.click(function(event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").prev().find("img"));
        // Fade in the next image
        $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When the exit button is clicked
    $exitButton.click(function() {
        // Fade out the overlay
        $("#overlay").fadeOut("slow");
    });


    function initAutocomplete() {
        var input11 = document.getElementById("myloc").value
        var coords = input11.split(",");
        var input23 = new google.maps.LatLng(coords[0], coords[1]);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: input23,
            zoom: 13,
            mapTypeId: 'roadmap'
        });
        var marker = new google.maps.Marker({
            map: map,
            position: map.getCenter()
        })
        // Create the search box and link it to the UI element.
        var input = document.getElementById('myloc');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }
</script>
@endsection