@extends('layouts.app')

@section('title',trans('sidebar.partners'))

@section('content')
<div class="col-md-12" id="Msgs"></div>
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.partners')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('partners.id')</th>
                            <th>@lang('partners.name')</th>
                            <th>المدينة</th>
                            <th>@lang('partners.avatar')</th>
                            <th>الأولوية</th>
                            <th>@lang('partners.word')</th>
                            <th>كوبونات</th>
                            <th>الحالة</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody> 
                        {{-- {{$partners}} --}} 
    @foreach($partners as $partner)
        <tr class="odd gradeX">
            <td>{{ $partner->id }}</td> 
            <td><a href="/admin/partners/main/{{ $partner->user_id}}">{{ $partner->name ?? '' }}
            </a><br>
            {{ $partner->phone ?? '' }}</td>
            <td>{{ $partner->city->name ?? '' }}</td>
            <td><img src="{{ $partner->avatar_url ?? '' }}" style="width: 100px" class="img-thumbnail"></td>
            <td>
            <select class="form-control" onchange="changePriority(this.value,{{$partner->id}});" id="{{$partner->id}}">
                <option selected value="{{ $partner->priority }}"> {{ $partner->priority }}</option>
                @for ($i = 1; $i <= sizeof($partners); $i++)
        <option  value="{{ $i }}"> {{ $i }}</option>
                @endfor
           </select>
            </td>


            <td>{{ $partner->partner_word ?? '' }}</td>
            <td><a href="/admin/partners/coupons/{{ $partner->user_id}}">حسومات</a></td>
            <input type="hidden" id="partner-name-{{ $partner->id }}" value="{{ $partner->name ?? '' }}">
            <input type="hidden" id="partner-word-{{ $partner->id }}" value="{{ $partner->partner_word ?? '' }}">
          
            <td>                            
                <div class="form-group">
                <div class="custom-control custom-checkbox small">
                    <input type="checkbox" class="custom-control-input" @if($partner->status != 0) checked @endif id="customCheck{{ $partner->id }}" onchange="changeStatus({{ $partner->id }},this.checked)">
                    <label class="custom-control-label" for="customCheck{{ $partner->id }}"></label>
                </div>
            </div></td>
            <td>
                @ifCanDo('update_partners')
                <a data-toggle="modal" style="cursor: pointer; color: #4e73df" data-target="#EditModal" onclick="updateItem({{ $partner->id }},'{{ route('admin.partners.update',$partner->id) }}')" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                @endif
                {{-- @ifCanDo('delete_partners')
                <a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $partner->id }}','{{ route('admin.partners.destroy',$partner->id) }}')"><i class="fa fa-trash"></i></a>
                @endif --}}
            </td>
        </tr>
    @endforeach
    <script>
        function changeStatus(id,status) {
            console.log(status);
            var url = '/api/partner/activatePartner'; 
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    id: id,
                    status:status,
                    _token: '<?php echo csrf_token(); ?>' }
            }).done(function(data) {}).fail(function(e) {
                swal("<?php echo trans('admin.fail'); ?>",e.responseJSON.message, "error")
            })
        }
    
    </script>

                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                @ifCanDo('create_partners')
                <div class="panel-footer">
                    <button  data-toggle="modal" data-target="#myModal" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</button>
                </div>
                @endif
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <script>
        function changePriority(op,id){
           $.ajaxSetup({
                         headers: {
                             'Accept':'application/json',
                             'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8"
                         } 
                     });
                      jQuery.ajax({
                          url: "/api/partner/priority/update/", 
                         method: 'get', 
                         data:{
                             "id":id,
                             "value":op
                         },
       
                         success: function(result){
       console.log(result); 
       if(result["status"]=="success"){
       $("#Msgs").append('<div class="alert alert-warning alert-dismissible fade show" role="alert" style="text-align: end;">Priority Updated Successfully<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
       location.reload();
       }
       else{
       $("#Msgs").append('<div class="alert alert-warning alert-dismissible fade show" role="alert" style="text-align: end;">Error<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
       
       }
                                }
                      });
       }
       
                   </script>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('admin.partners.store') }}" enctype="multipart/form-data">
                    @csrf
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('partners.create')</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            {{-- start login info--}}
                            <div class="form-group col-md-6">
                                <label class="text-md-right">اسم المستخدم</label>

                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="phone" required>

                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-6">
                                <label class="text-md-right">كلمة السر</label>

                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label for="city-name">@lang('halls.city') *</label>
                                    <select id="city-name" name="city_id" class="form-control">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" >{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{-- end here --}}
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('partners.name')</label>

                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror
 
                            </div>

                            <div class="form-group col-md-12">
                                <label for="phone"> الهاتف </label>
                                <input type="text" name="phone" class="form-control" id="phone"  >
                              </div>

                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('partners.avatar')</label>

                                <input type="file" accept="image/*" class="@error('image') is-invalid @enderror" name="image" style="width: 100%">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('partners.partner_word')</label>

                                <textarea name="partner_word" class="form-control @error('partner_word') is-invalid @enderror" cols="20" rows="4"></textarea>

                                @error('partner_word')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input id="myloc" name="map_location" type="text" value="24.7104948,46.6797144"/>
                                <div id="map" style="width: 457px; height: 400px;margin: 17px"></div>
                                </div>

                        </div>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="@lang('admin.create')">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                    </div>
                </form>


            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="EditModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="edit-form" action="" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('partners.edit')</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">كلمة السر الجديدة</label>

                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('partners.name')</label>

                                <input type="text" class="form-control @error('name') is-invalid @enderror" id="edit-name" name="name" required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('partners.avatar')</label>

                                <input type="file" accept="image/*"  class="@error('image') is-invalid @enderror" style="width: 100%" name="image">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror
 
                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('partners.partner_word')</label>

                                <textarea name="partner_word" id="edit-word" class="form-control @error('partner_word') is-invalid @enderror" cols="20" rows="8" required></textarea>

                                @error('partner_word')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="@lang('admin.edit')">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                    </div>
                </form>


            </div>
        </div>
    </div>

@endsection


@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
async defer></script>
    <script>
        
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "order": [],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                citie: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    citie: 'POST',
                    datacitie: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", citie: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }

        function updateItem(id, url) {
            $('#edit-form').attr("action", url);
            $('#edit-name').val($('#partner-name-'+id).val());
            $('#edit-word').val($('#partner-word-'+id).val());
        }


        function transN(){
       var x= $('#cv').val();
       x = fixNumbers(x);
       $('#cv').val(x);
} 

    var
    persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
    arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
    fixNumbers = function (str)
    {
          if(typeof str === 'string')
      {
        for(var i=0; i<10; i++)
        {
          str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
        }
      }
    //   alert(str);
      return str;
    };

    </script>
<script>
            function initAutocomplete() {
            var markers = [];
            var input11 = document.getElementById("myloc").value;
            var coords = input11.split(",");
            var input23 = new google.maps.LatLng(coords[0], coords[1]);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: input23,
                zoom: 13,
                mapTypeId: 'roadmap'
            });

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(map, event.latLng);
            });

            function placeMarker(map, location) {
                deleteMarkers();
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                $("#myloc").val(location.lat() + ',' + location.lng());
                //infowindow.open(map,marker);
                markers.push(marker);
                //console.log(markers);
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
                setMapOnAll(null);
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('myloc');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
</script>
@endsection
