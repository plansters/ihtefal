@extends('layouts.app')

@section('title',trans('sidebar.cities'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.cities')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <hr>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('cities.id')</th>
                            <th>@lang('cities.name')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)
                            <tr class="odd gradeX">
                                <td>{{ $city->id }}</td>
                                <td>{{ $city->name ?? '' }}</td>
                                <input type="hidden" id="city-{{ $city->id }}" value="{{ $city->name ?? '' }}">
                                <td>
                                    @ifCanDo('update_cities')
                                    <a style="cursor: pointer; color: #4e73df" href="{{ route('admin.cities.edit',$city->id) }}" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    @endif
                                    @ifCanDo('delete_cities')
                                    <a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $city->id }}','{{ route('admin.cities.destroy',$city->id) }}')"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->


                <br>
                @ifCanDo('create_cities')
                <div class="panel-footer">
                    <button onclick="window.location.href='{{ route('admin.cities.create') }}'" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</button>
                </div>
                @endif
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

    <!-- The Modal -->

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                citie: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: url,
                    type: 'POST',
                    datatype: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {
                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    })
                    .fail(function(e) {
                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
                    })


            });
        }

        function updateItem(id, url) {
            $('#edit-form').attr("action", url);
            $('#edit-name').val($('#city-'+id).val());
        }


    </script>

@endsection
