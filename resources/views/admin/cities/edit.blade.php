<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('cities.edit'))

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('cities.edit')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.cities.update', $city->id) }}" id="main-form">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="text-md-right">@lang('cities.name')</label>

                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $city->name }}" required>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>

                                <div class="form-group col-md-12" id="map_div">
                                    <input id="zoomLevel" name="zoom" type="hidden" value="7"/>
                                    <input id="myloc" name="location" type="text" value="{{ $city->location ? $city->location : '24.7104948,46.6797144' }}"/>
                                    <div id="map" style="width: 100%; height: 400px"></div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-primary" value="@lang('admin.edit')">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <style type="text/css">
        .select2-selection__rendered {
            line-height: 31px !important;
        }

        .select2-container .select2-selection--single {
            height: 35px !important;
        }

        .select2-selection__arrow {
            height: 34px !important;
        }
    </style>
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
            async defer></script>
    <script type="text/javascript">


        function initAutocomplete() {
            var markers = [];
            var input11 = document.getElementById("myloc").value
            var coords = input11.split(",");
            var input23 = new google.maps.LatLng(coords[0], coords[1]);
            var map = new google.maps.Map(document.getElementById('map'), {
                center: input23,
                zoom: {{$city->zoom}},
                mapTypeId: 'roadmap'
            });

            google.maps.event.addListener(map, 'click', function(event) {
                placeMarker(map, event.latLng);
            });

            function placeMarker(map, location) {
                deleteMarkers();
                var zoomL=map.getZoom();
                document.getElementById('zoomLevel').value=zoomL;
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                $("#myloc").val(location.lat() + ',' + location.lng());
                //infowindow.open(map,marker);
                markers.push(marker);
                //console.log(markers);
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            // Removes the markers from the map, but keeps them in the array.
            function clearMarkers() {
                setMapOnAll(null);
            }

            // Deletes all markers in the array by removing references to them.
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('myloc');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }
    </script>
@endsection
