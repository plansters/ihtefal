@extends('layouts.app')

@section('title',trans('alerts.alerts'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('alerts.alerts')</h1>
                <hr>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('alerts.id')</th>
                            <th>@lang('alerts.title')</th>
                            <th>@lang('alerts.message')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($app_alerts as $alert)
                            <tr class="odd gradeX">
                                <td>{{ $alert->id }}</td>
                                <td>{{ $alert->title ?? '' }}</td>
                                <td>{{ $alert->message ?? '' }}</td>
                                <td>
                                    <a href="#" style="color: #dc3545" onclick="event.preventDefault();deleteItem('{{ $alert->id }}','{{ route('admin.alerts.destroy',$alert->id) }}')"><i class="fa fa-trash"></i></a>&nbsp;
                                    <a href="#" style="color: #0e0e0e" onclick="event.preventDefault();sendAlert('{{ $alert->id }}','{{ route('admin.alerts.send') }}')"><i class="fas fa-paper-plane"></i></a>&nbsp;
                                </td>
                            </tr> 
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                <div class="panel-footer">
                    <a href="#" data-toggle="modal" data-target="#add_alert_modal" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>


    <div class="modal" id="add_alert_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">@lang('alerts.create')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{ route('admin.alerts.store') }}" method="POST" id="add_alert_form">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">@lang('alerts.title')</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" required autofocus>

                                        @error('title')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">@lang('alerts.message')</label>

                                    <div class="col-md-8">
                                        <textarea name="message" class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">

                    <button type="button" onclick="$('#add_alert_form').submit()" class="btn btn-primary">@lang('admin.save')</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                </div>

            </div>
        </div>
    </div>



@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }



        function sendAlert(id,url) {

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', alert_id : id }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.sent_successfully') !!}", type: "success"},
                            function(){
                                location.reload();
                            }
                        );


                    })
                    .fail(function(e) {

                        console.log(e)



                    })
        }



    </script>

@endsection
