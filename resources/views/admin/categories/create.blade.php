<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('categories.create'))

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('categories.create')</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.categories.store') }}" id="main-form">
                        @csrf

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('categories.name')</label>

                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                       required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                        </div>
                        <span id="extra"></span>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="button" class="btn btn-success" value="@lang('categories.add_extra')"
                                       onclick="addExtraData()">


                            </div>
                        </div>
                        <hr>
                        <input type="hidden" name="max_extra" id="max_extra">
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-primary" value="@lang('admin.create')">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')
<style type="text/css">
    .select2-selection__rendered {
        line-height: 31px !important;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .select2-selection__arrow {
        height: 34px !important;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    $(".select2-single").select2({
        'height': '30px'
    });
    var i = 0;

    function addExtraData() {
        i++;
        var html = `
        <div class="row" id="row${i}">

        <div class="form-group col-md-4">
        <label class="text-md-right">@lang("categories.title")</label>
        <input type="text" class="form-control" name="extras[${i}][title]" required>
        </div>

        <div class="form-group col-md-3">
        <label class="text-md-right">@lang("categories.type")</label>
        <select class="form-control" name="extras[${i}][type]" id="type${i}" onchange="showValues(${i})" required>
        <option value="text">@lang("categories.text")</option>
        <option value="number">@lang("categories.number")</option>
        <option value="select">@lang("categories.select")</option>

        </select>
        </div>

        <div class="form-group col-md-4" style="display: none" id="values-div${i}">
        <label class="text-md-right">@lang("categories.values")</label>
        <select class="form-control selected_values" name="extras[${i}][values][]" multiple></select>
        </div>

        <div class="form-group col-md-1" id="values-div${i}">
        <label class="text-md-right" style="color: #fff">delete</label>
            <input type="button" class="btn btn-danger" onclick="removeRow(${i})" value="@lang('admin.delete')">
        </div>

        </div>`;
        $('#extra').before(html);
        $('#max_extra').val(i);
        $('[data-toggle="tooltip"]').tooltip();
        $(".selected_values").select2({
            tags: true,
            width: '100%'
        });
    }

    function removeRow(i) {
        $('#row'+i).fadeOut(500).remove();
    }

    function showValues(s) {
        if ($('#type' + s).val() == 'select')
            $('#values-div' + s).fadeIn(500).show();
        else
            $('#values-div' + s).fadeOut(500).hide();
    }
</script>
@endsection
