<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('categories.edit'))

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">@lang('categories.edit')</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.categories.update', $category->id) }}" id="main-form">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('categories.name')</label>

                                <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{ $category->name }}" name="name"
                                       required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                        </div>
                        @forelse($category->extras as $extra)
                        <div class="row" id="row{{ $i }}">
                            <input type="hidden" name="extras[{{ $i }}][id]" value="{{ $extra->id }}">
                            <div class="form-group col-md-4">
                                <label class="text-md-right">@lang("categories.title")</label>
                                <input type="text" class="form-control" name="extras[{{ $i }}][title]" value="{{ $extra->field_name }}" required>
                            </div>

                            <div class="form-group col-md-3">
                                <label class="text-md-right">@lang("categories.type")</label>
                                <select class="form-control" name="extras[{{ $i }}][type]" id="type{{ $i }}" onchange="showValues({{ $i }})" required>
                                    <option value="text" @if($extra->field_type == 'text') selected @endif>@lang("categories.text")</option>
                                    <option value="number" @if($extra->field_type == 'number') selected @endif>@lang("categories.number")</option>
                                    <option value="select" @if($extra->field_type == 'select') selected @endif>@lang("categories.select")</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4" @if($extra->field_type != 'select') style="display: none" @endif id="values-div{{ $i }}">
                                <label class="text-md-right">@lang("categories.values")</label>
                                <select class="form-control selected_values" value="{{ $extra->field_values }}" name="extras[{{ $i }}][values][]" multiple>
                                    @if($extra->field_values)
                                        @forelse(explode(',', $extra->field_values) as $value)
                                            <option value="{{ $value }}" selected>{{ $value }}</option>
                                        @empty
                                        @endforelse
                                    @endif
                                </select>
                            </div>

                            <!-- <div class="form-group col-md-1" id="values-div{{ $i }}">
                                <label class="text-md-right" style="color: #fff">delete</label>
                                <input type="button" class="btn btn-danger" onclick="removeRow({{ $i++ }},'{{ route('admin.category_extras.destroy',$extra->id) }}');" value="@lang('admin.delete')">
                            </div> -->

                        </div>
                        @empty
                        @endforelse
                        <span id="extra"></span>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <input type="button" class="btn btn-success" value="@lang('categories.add_extra')"
                                       onclick="addExtraData()">


                            </div>
                        </div>
                        <hr>
                        <input type="hidden" name="max_extra" id="max_extra">
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-primary" value="@lang('admin.edit')">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')
<style type="text/css">
    .select2-selection__rendered {
        line-height: 31px !important;
    }

    .select2-container .select2-selection--single {
        height: 35px !important;
    }

    .select2-selection__arrow {
        height: 34px !important;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    $(".selected_values").select2({
        tags: true,
        width: '100%'
    });
    var i = '{!! $i !!}';
    i = parseInt(i);

    function addExtraData() {
        i++;
        var html = `
        <div class="row" id="row${i}">
        <input type="hidden" name="extras[${i}][id]" value="0">
        <div class="form-group col-md-4">
        <label class="text-md-right">@lang("categories.title")</label>
        <input type="text" class="form-control" name="extras[${i}][title]" required>
        </div>

        <div class="form-group col-md-3">
        <label class="text-md-right">@lang("categories.type")</label>
        <select class="form-control" name="extras[${i}][type]" id="type${i}" onchange="showValues(${i})" required>
        <option value="text">@lang("categories.text")</option>
        <option value="number">@lang("categories.number")</option>
        <option value="select">@lang("categories.select")</option>

        </select>
        </div>

        <div class="form-group col-md-4" style="display: none" id="values-div${i}">
        <label class="text-md-right">@lang("categories.values")</label>
        <select class="form-control selected_values" name="extras[${i}][values][]" multiple></select>
        </div>

        <div class="form-group col-md-1" id="values-div${i}">
        <label class="text-md-right" style="color: #fff">delete</label>
            <input type="button" class="btn btn-danger" onclick="removeRow(${i})" value="@lang('admin.delete')">
        </div>

        </div>`;

        $('#extra').before(html);
        $('#max_extra').val(i);
        $(".selected_values").select2({
            tags: true,
            width: '100%'
        });
        $('[data-toggle="tooltip"]').tooltip();
    }

    function removeRow(i, url) {
        swal({
            title: '{!! trans('admin.are_you_sure') !!}',
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "{!! trans('admin.yes'); !!}",
            cancelButtonText: "{!! trans('admin.no'); !!}",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
    }, function () {


            $('#row'+i).fadeOut(500).remove();

            if (typeof url != 'undefined') {
                //alert(url); return;
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                }).done(function() {
                    swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"});
                }).fail(function(e) {
                    console.log(e);
                    swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
                })
            } else {
                swal.close()
            }
            });




    }

    function showValues(s) {
        if ($('#type' + s).val() == 'select')
            $('#values-div' + s).fadeIn(500).show();
        else
            $('#values-div' + s).fadeOut(500).hide();
    }
</script>
@endsection
