@extends('layouts.app')

@section('title',trans('sidebar.categories'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.categories')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('categories.id')</th>
                            <th>@lang('categories.name')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr class="odd gradeX">
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->name ?? '' }}</td>
                                <td>
                                    @ifCanDo('update_categories')
                                    <a href="{{ route('admin.categories.edit',$category->id) }}" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    @endif
                                    @ifCanDo('delete_categories')
                                    <a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $category->id }}','{{ route('admin.categories.destroy',$category->id) }}')"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                @ifCanDo('create_categories')
                <div class="panel-footer">
                    <a href="{{ route('admin.categories.create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
                </div>
                @endif
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                categorie: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {
                $.ajax({
                    url: url,
                    type: 'POST',
                    datacategorie: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {
                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", categorie: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    })
                    .fail(function(e) {
                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")
                    })
            });
        }



    </script>

@endsection
