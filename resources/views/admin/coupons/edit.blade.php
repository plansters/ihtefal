@extends('layouts.app')

@section('title',trans('coupons.edit'))

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('coupons.edit')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.coupons.update',$coupon->id) }}">
                            @csrf
                            @method('put')

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('coupons.title')</label>
                                    <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{ $coupon->title }}" name="title" required>
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('calendar.hall')</label>
                                    <select type="text" style="width: 100%" class="form-control" id="hall_id" name="hall_id" ></select>
                                    @error('hall_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('coupons.coupon_id')</label>
                                    <input type="text" class="form-control @error('coupon_id') is-invalid @enderror" value="{{ $coupon->coupon_id }}" name="coupon_id" required>
                                    @error('coupon_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('coupons.coupon_type')</label>
                                    <select class="form-control @error('coupon_type') is-invalid @enderror" name="coupon_type" required>
                                        <option value="percentage">@lang('coupons.percentage')</option>
                                        <option value="value">@lang('coupons.value')</option>
                                    </select>
                                    @error('coupon_type')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('coupons.coupon_value')</label>
                                    <input type="number" value="{{ $coupon->coupon_value }}" class="form-control @error('coupon_value') is-invalid @enderror" name="coupon_value" required>
                                    @error('coupon_value')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('coupons.start_date')</label>
                                    <input type="text" value="{{ $coupon->start_date }}" class="form-control datepicker @error('start_date') is-invalid @enderror" name="start_date" required>
                                    @error('start_date')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('coupons.expired_date')</label>
                                    <input type="text" value="{{ $coupon->expired_date }}" class="form-control datepicker @error('expired_date') is-invalid @enderror" name="expired_date" required>
                                    @error('expired_date')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="text-md-right">@lang('halls.description')</label>

                                    <textarea name="description" class="form-control @error('description') is-invalid @enderror" cols="20" rows="10">{{ $coupon->description }}</textarea>

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <hr>



                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('admin.edit')
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('styles')
    <style type="text/css">
        .select2-selection__rendered {
            line-height: 31px !important;
        }

        .select2-container .select2-selection--single {
            height: 35px !important;
        }

        .select2-selection__arrow {
            height: 34px !important;
        }
    </style>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(".select2-single").select2({
            'height': '30px'
        });

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });

        searchingForHall('{{ route('admin.search.hall') }}')

        function searchingForHall(url){
            //console.log(url);
            if(url === '')
            {
                return;
            }
            $('#hall_id').empty();
            $('#hall_id').select2({
                placeholder: "@lang('admin.type_search_word')",
                minimumInputLength: 2,
                ajax: {
                    url: url,
                    dataType: 'json',
                    data: function (params) {
                        return {
                            q: $.trim(params.term)
                        };
                    },
                    processResults: function (data) {
                        //  console.log(data);
                        return {
                            results: data
                        };
                    },
                    error: function (err) {
                        console.log(err);
                    },
                    cache: true
                }
            });
        }
    </script>
@endsection
