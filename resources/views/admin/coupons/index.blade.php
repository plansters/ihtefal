@extends('layouts.app')

@section('title',trans('sidebar.coupons'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.coupons')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('coupons.id')</th>
                            <th>@lang('coupons.title')</th>
                            <th>اسم القاعة</th>
                            <th>@lang('coupons.description')</th>
                            <th>@lang('coupons.coupon_id')</th>
                            <th>@lang('coupons.type')</th>
                            <th>@lang('coupons.start')</th>
                            <th>@lang('coupons.expired')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($coupons as $coupon)
                            <tr class="odd gradeX">
                                <td>{{ $coupon->id }}</td>
                                <td>{{ $coupon->title ?? '' }}</td>
                                <td>{{ $coupon->hall->name ?? '---' }}</td>
                                <td>{{ $coupon->description ?? '' }}</td>
                                <td>{{ $coupon->coupon_id ?? '' }}</td>
                                <td>{{ trans('coupons.'.$coupon->coupon_type ?? '') }}</td>
                                <td>{{ date('Y-m-d', strtotime($coupon->start_date)) ?? '' }}</td>
                                <td>{{ date('Y-m-d', strtotime($coupon->expired_date)) ?? '' }}</td>
                                <td>
                                    @ifCanDo('update_coupons')
                                    <a href="{{ route('admin.coupons.edit',$coupon->id) }}" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    @endif
                                    @ifCanDo('delete_coupons')
                                    <a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $coupon->id }}','{{ route('admin.coupons.destroy',$coupon->id) }}')"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                @ifCanDo('create_coupons')
                <div class="panel-footer">
                    <a href="{{ route('admin.coupons.create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
                </div>
                @endif
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                categorie: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    type: 'POST',
                    datatype: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", categorie: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }



    </script>

@endsection
