<style>
    .fc-day-grid-event{
        margin-top: 20px !important;
    }
</style>
@extends('layouts.app')

@section('title',trans('sidebar.reservations'))

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.reservations')</h1>
            </div>
            
            <!-- /.col-lg-12 -->
        </div>
        <div class="alert alert-danger" role="alert" style="display: none" id="calError">
            
          </div>
        <hr>
        <!-- /.row -->
        
        <div class="row">
            <div class="col-lg-12">
                    <div class="col-md-2" id="external-events">
                        <h4 style="text-align: right">@lang('calendar.waiting_events')</h4>
                        @forelse($freeEvents as $event)
                            <div style="background-color: #43A047!important;" class="fc-event ui-draggable ui-draggable-handle" ondblclick="editEvent({id: {{ $event->id }},title: '{{ $event->title }}', description: '{{ $event->description }}' })" onmousedown="setEventId({{ $event->id }});" id="event-{{ $event->id }}">{{ $event->title }}</div>
                        @empty 
                            <p style="text-align: center"><label>@lang('calendar.no_events')</label></p>
                        @endforelse
                        <p>
                            <!--input type="text" class="form-control" id="txtHijriDate"-->
                            <input type="button" data-toggle="modal" data-target="#add-event-modal" class="btn btn-outline-primary btn-sm" style="width: 100%" value="@lang('calendar.new_event')">
                        </p>
                    </div>
                    <input type="hidden" id="selected-event">
                    {!! $calendar->calendar() !!}
 
            </div>
        </div>
    </div>


    <!-- The Modal -->
    <div class="modal" id="my-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">@lang('calendar.update_event')</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="text-md-right">@lang('calendar.hall')</label>
                            <p class="w-100">
                                <a href="" id="hall-link" target="_blank"></a>
                            </p>

                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" id="event-id">
                        <div class="form-group col-md-12">
                            <label class="text-md-right">@lang('calendar.title')</label>
                            <input type="text" class="form-control" id="edit-title" name="edit_name" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="text-md-right">@lang('calendar.description')</label>
                            <textarea class="form-control" name="edit_description" id="edit-description" ></textarea>
                        </div>
                    </div>

                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        @lang('calendar.user_info')
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item"><i style="color: #007bff" class="fas fa-address-card"></i> &nbsp; &nbsp;<span id="user-name"></span></li>
                                        <li class="list-group-item"><i style="color: #007bff" class="fas fa-envelope"></i> &nbsp; &nbsp;<span id="user-email"></span></li>
                                        <li class="list-group-item"><i style="color: #007bff" class="fas fa-phone"></i> &nbsp; &nbsp;<span id="user-phone"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <input type="button" class="btn btn-success" data-dismiss="modal" onclick="updateReservation()" value="@lang('admin.edit')">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deleteReservation()">@lang('admin.delete')</button>
                </div>

            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="add-event-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('admin.reservations.store') }}" method="post">
                    @csrf
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('calendar.add_reservation')</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12"> 
                                <label class="text-md-right">اسم صاحب الحجز</label>
                                <input type="text" class="form-control" id="title" name="title" >
                                <input type="hidden" class="form-control" id="title" name="coupon_id" >
                                <input type="hidden" class="form-control" name="isAdmin" value="1" >
                                <input type="hidden" class="form-control" id="title" name="redirect" value="{{ url()->current() }}" >
                            </div>
                        </div>
                        <!--div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('calendar.user')</label>
                                <select type="text" style="width: 100%" class="form-control" id="user_id" name="user_id" ></select>
                            </div>
                        </div-->
                        <input type="hidden" name="admin" value="1">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('calendar.hall')</label>
                                <select type="text" style="width: 100%" class="form-control" id="hall_id" name="hall_id" ></select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('calendar.description')</label>
                                <textarea class="form-control" name="description" id="description" ></textarea>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="@lang('admin.create')">
                        <input type="button" class="btn btn-danger" data-dismiss="modal" value="@lang('admin.close')">
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection


@section('styles')
    <style>

        #external-events {
            float: left;
            padding: 0 10px;
            border: 1px solid #ccc;
            background: #eee;
            text-align: left;
        }

        #external-events h4 {
            font-size: 16px;
            margin-top: 0;
            padding-top: 1em;
        }

        #external-events .fc-event {
            margin: 10px 0;
            cursor: pointer;
        }

        #external-events p {
            margin: 1.5em 0;
            font-size: 11px;
            color: #666;
        }

        #external-events p input {
            margin: 0;
            vertical-align: middle;
        }


        .fc-title {
            color: white;
        }

        .fc-time {
            color: white;
        }

        .fc-widget-content {
            color: #858796!important;
        ;
        }
        .fc-toolbar {
            height: 35px;
        }
    </style>
@endsection


@section('scripts')
 
    <script src="{{ asset('js/moment-hijri.js') }}"></script>
{!! $calendar->script() !!}
<script type="text/javascript">
    $(document).ready(function() {
        $('#calendar-{{ $calendar->getId()}}').addClass('col-md-10');
        $('#external-events .fc-event').each(function() {
            // store data so the calendar knows to render an event upon drop

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });
        });

        //$('#user_id').select2();
        searchingForUser('{{ route('admin.search.user') }}');
        searchingForHall('{{ route('admin.search.hall') }}')
    });

    function set_hijri_content(date, cell) {
        cell.append(`<div style="padding: 3px">${moment(date).format('iD/iM/iYYYY')}</div>`);
        // cell.append(`<div style="padding: 3px;line-height: 0px;">${moment(date).format('DD/MM/YY')}</div>`);

    }

    function dropEnd(date, allDay, ui) {
        addEvent(new Date(date)); // Add the event to the db

    }

    function addEvent(date) {
        var url = '{{ route('admin.event.add') }}';
        var start = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {_token: '{!! csrf_token() !!}', date: start, id: $('#selected-event').val(), all_day: 1}
        }).done(function(event) {

            if(event['status']=="FAILURE"){
            // console.log(e['errors']);
                $("#calError").text(event['errors']);
                $("#calError").show();
                setTimeout(function() {
    location.reload();
}, 2000);
            }
            addCalendarEvent(event.id, event.start.date, event.end.date, event.title,  event.isAllDay, event.options);
            $('#event-' + $('#selected-event').val()).remove();
        }).fail(function(e) {
            // console.log(e);
        })
    } 

    function editEvent(info) {
        //console.log(info);
        $('#my-modal').modal();
        $('#event-id').val(info.id);
        // $('#user-name').html(info.user.first_name + ' ' + info.user.last_name);
        $('#user-name').html(info.title);
        $('#user-email').html(info.user.email);
        $('#user-phone').html(info.user.phone);
        $('#hall-link').html(info.hall_name);
        $('#hall-link').attr("href", info.hall_route);
        $('#edit-title').val(info.title);
        $('#edit-description').val(info.description);
        console.log("info is:",info); 
        // console.log(info);
        //console.log(info.id + " start is " + info.start.toISOString() + " end is " + info.end.toISOString());
    }

    function updateReservation() {
        var id = $('#event-id').val();
        var url = '{{ url('admin/reservations') }}/' + id;
        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {
                _token: '{!! csrf_token() !!}',
                title: $('#edit-title').val(),
                description: $('#edit-description').val(),
                _method: 'PUT'
            }
        }).done(function(event) {
            if (event.start) {
                var my_event = $("#calendar-{{ $calendar->getId()}}").fullCalendar( 'clientEvents', id );
                // console.log(my_event[0]);
                //$('#calendar-{{ $calendar->getId()}}').fullCalendar( 'removeEvents', [id] );
                my_event[0].title = $('#edit-title').val();
                $('#calendar-{{ $calendar->getId()}}').fullCalendar( 'updateEvent', my_event[0]);
            } else {
                $('#event-' + id).html($('#edit-title').val());
            }

            /*$('#calendar-{{ $calendar->getId()}}').fullCalendar( 'updateEvent', {
                id: id,
                start: event.start,
                end: event.end,
                allDay: event.all_day,
                title: $('#edit-title').val(),
            }, true );*/
        }).fail(function(e) {
            // console.log(e);
        })
    }

    function deleteReservation() {
        var id = $('#event-id').val();
        var url = '{{ url('admin/reservations') }}/' + id;
        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {
                _token: '{!! csrf_token() !!}',
                title: $('#edit-title').val(),
                description: $('#edit-description').val(),
                _method: 'DELETE'
            }
        }).done(function(event) {
            if (event.start) {
                $('#calendar-{{ $calendar->getId()}}').fullCalendar( 'removeEvents', [id] );
            } else {
                $('#event-' + id).remove();
            }


        }).fail(function(e) {
            // console.log(e);
        })
    }

    function selectItem(start, end, allDay){
        alert(date);
        // console.log('start', start.format("YYYY-MM-DD"));
        // console.log(end);
        // console.log(element);
    }

    function setEventId(value) {
        $('#selected-event').val(value)
    }

    function resizeEvent(info) {
        // console.log(info);
        var url = '{{ route('admin.event.resize') }}';

        $.ajax({
            url: url,
            type: 'POST',
            datatype: 'JSON',
            data: {
                _token: '{!! csrf_token() !!}',
                title: info.title,
                id: info.id,
                start: info.start.toISOString(),
                end: info.end.toISOString(),
                all_day: info.allDay ? 1 : 0
            }
        }).done(function(e) {
            // console.log(e['status']=="");
            if(e['status']=="FAILURE"){
            // console.log(e['errors']);
                $("#calError").text(e['errors']);
                $("#calError").show();
                setTimeout(function() {
    location.reload();
}, 2000);
            }
        }).fail(function(e) {
            console.log(e);
        })
    }

    function getEvents(view, element) {
        //console.log(view.intervalStart.toISOString(), view.intervalEnd.toISOString())
        $.ajax({
            type: "POST",
            url: '{{ route('admin.get.events') }}',
            data: {
                start: view.intervalStart.toISOString(),
                end: view.intervalEnd.toISOString(),
                _token: '{{ csrf_token() }}'
            },
            success: function(events) {
                $('#calendar-{{ $calendar->getId() }}').fullCalendar('removeEvents');
                //var event = events[0];
                for (var i = 0; i < events.length; i++) {
                    addCalendarEvent(events[i].id, events[i].start.date, events[i].end.date, events[i].title,  events[i].isAllDay, events[i].options);
                }

            }
        });
    }
    function addCalendarEvent(id, start, end, title, all_day, options) {
        //console.log(options);
        var eventObject = {
            id: id,
            start: start,
            end: end,
            title: title,
            allDay: all_day,
            color: options.color,
            user: options.user,
            hall_name: options.hall_name,
            hall_route: options.hall_route,
        };

        $('#calendar-{{ $calendar->getId() }}').fullCalendar('renderEvent', eventObject, true);
    }

    function removeEvents() {
        var allEvents = $('#calendar-{{ $calendar->getId() }}').fullCalendar('clientEvents');

        var userEventIds= [];

//Find ever non usercreated event and push the id to an array
        $.each(allEvents,function(index, value){
            if(value.isUserCreated !== true){
                userEventIds.push(value._id);
            }
        });
    }

    function openDay(date, jsEvent, view) {
        $('#calendar-{{ $calendar->getId() }}').fullCalendar('changeView', 'agendaDay');
        $('#calendar-{{ $calendar->getId() }}').fullCalendar('gotoDate',date);
    }

    function searchingForUser(url) {
        //console.log(url);
        if(url === '')
        {
            return;
        }
        $('#user_id').empty();
        $('#user_id').select2({
            placeholder: "@lang('admin.type_search_word')",
            minimumInputLength: 2,
            ajax: {
                url: url,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    //  console.log(data);
                    return {
                        results: data
                    };
                },
                error: function (err) {
                    console.log(err);
                },
                cache: true
            }
        });
    }

    function searchingForHall(url){
        //console.log(url);
        if(url === '')
        {
            return;
        }
        $('#hall_id').empty();
        $('#hall_id').select2({
            placeholder: "@lang('admin.type_search_word')",
            minimumInputLength: 2,
            ajax: {
                url: url,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: $.trim(params.term)
                    };
                },
                processResults: function (data) {
                    //  console.log(data);
                    return {
                        results: data
                    };
                },
                error: function (err) {
                    console.log(err);
                },
                cache: true
            }
        });
    }
</script>
@endsection
