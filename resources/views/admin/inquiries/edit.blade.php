@extends('layouts.app')

@section('title',trans('companytypes.edit'))

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">@lang('companytypes.edit')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.companytypes.update',$type->id) }}">
                            @csrf
                            @method('put')

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">@lang('companytypes.name')</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $type->name }}" required autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>



                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('admin.edit')
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
