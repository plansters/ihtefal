@extends('layouts.app')

@section('title',trans('sidebar.inquiries'))

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@lang('sidebar.inquiries')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>@lang('inquiries.id')</th>
                            <th>@lang('inquiries.user')</th>
                            <th>@lang('inquiries.question')</th>
                            <th>@lang('inquiries.admin')</th>
                            <th>@lang('inquiries.answer')</th>
                            <th>@lang('admin.settings')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($inquiries as $inquiry)
                            <tr class="odd gradeX">
                                <td>{{ $inquiry->id }}</td>
                                <td>{{ $inquiry->user->identity ?? '' }}</td>
                                <td>{{ $inquiry->question ?? '' }}</td>
                                <td>{{ $inquiry->admin->email ?? '' }}</td>
                                <td>{{ $inquiry->answer ?? '' }}</td>
                                <input type="hidden" id="inquiry-question-{{ $inquiry->id }}" value="{{ $inquiry->question ?? '' }}">
                                <input type="hidden" id="inquiry-answer-{{ $inquiry->id }}" value="{{ $inquiry->answer ?? '' }}">
                                <td>
                                    @ifCanDo('update_inquiries')
                                    <a data-toggle="modal" style="cursor: pointer; color: #4e73df" data-target="#EditModal" onclick="updateItem({{ $inquiry->id }},'{{ route('admin.inquiries.update',$inquiry->id) }}')" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i> </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    @endif
                                    {{-- @ifCanDo('delete_inquiries')
                                    <a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $inquiry->id }}','{{ route('admin.inquiries.destroy',$inquiry->id) }}')"><i class="fa fa-trash"></i></a>
                                    @endif --}}
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <br>
                @ifCanDo('create_inquiries')
                <div class="panel-footer">
                    <button  data-toggle="modal" data-target="#myModal" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</button>
                </div>
                @endif
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" action="{{ route('admin.inquiries.store') }}" enctype="multipart/form-data">
                    @csrf
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('inquiries.create')</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('inquiries.name')</label>

                                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('inquiries.avatar')</label>

                                <input type="file" class="@error('image') is-invalid @enderror" name="image" style="width: 100%">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                @enderror

                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('inquiries.inquiry_word')</label>

                                <textarea name="inquiry_word" class="form-control @error('inquiry_word') is-invalid @enderror" cols="20" rows="10"></textarea>

                                @error('inquiry_word')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="@lang('admin.create')">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                    </div>
                </form>


            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="EditModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="POST" id="edit-form" action="" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">@lang('inquiries.edit')</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-md-12">

                                <p id="edit-question"></p>

                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-md-right">@lang('inquiries.answer')</label>

                                <textarea name="answer" id="edit-answer" class="form-control @error('answer') is-invalid @enderror" cols="20" rows="7"></textarea>

                                @error('answer')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="@lang('admin.edit')">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('admin.close')</button>
                    </div>
                </form>


            </div>
        </div>
    </div>

@endsection


@section('scripts')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').dataTable({
                "aaSorting": [],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
                }
            });

        });



        function deleteItem(id,url) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                citie: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {



                $.ajax({
                    url: url,
                    citie: 'POST',
                    datacitie: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
                })
                    .done(function() {


                        swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", citie: "success"},
                            function(){
                                location.reload();
                            }
                        );
                    })
                    .fail(function(e) {

                        swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                    })


            });
        }

        function updateItem(id, url) {
            //alert();
            $('#edit-form').attr("action", url);
            $('#edit-question').html($('#inquiry-question-'+id).val());
            $('#edit-answer').val($('#inquiry-answer-'+id).val());
        }



    </script>

@endsection
