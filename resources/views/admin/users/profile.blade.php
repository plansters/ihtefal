<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('users.edit'))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('users.edit')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.profile.update') }}">
                            @csrf
                            @method('PUT')
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.first_name')</label>

                                    <input type="text" value="{{ $user->first_name }}" class="form-control @error('first_name') is-invalid @enderror" name="first_name" required>

                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.last_name')</label>

                                    <input type="text" value="{{ $user->last_name }}" class="form-control @error('last_name') is-invalid @enderror" name="last_name" required>

                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                            </div>

                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.password')</label>

                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.password_confirmation')</label>

                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation">

                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-12 mb-0">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('admin.edit')
                                    </button>

                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
<style type="text/css">
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    $( ".select2-single" ).select2({
        'height' : '30px'
    });
</script>
@endsection
