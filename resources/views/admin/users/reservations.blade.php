@extends('layouts.app')

@section('title',trans('sidebar.reservations'))

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center" style="margin-top: 30px">
            <div class="col-md-10">
                <div class="row" id="halls-container">
                    @forelse($reservations as $reservation)
                        <div class="col-md-6" style="padding-bottom: 30px!important;">
                            <div class="card" style="border-radius: 10px">
                                <div class="card-body">
                                    <p>@lang('reservations.created_at')
                                        : {{ date('Y-m-d', strtotime($reservation->created_at)) }} @if($reservation->status == 'pending')
                                            <button type="button"
                                                    onclick="doAction('{{ route('admin.reservations.cancel', $reservation->id) }}')"
                                                    class="close">×
                                            </button> @endif</p>
                                    <p>@lang('reservations.hall'): {{ $reservation->hall->name ?? '' }}</p>

                                    @if($reservation->coupon)
                                        <p>رقم كوبون الخصم : {{ ($reservation->coupon->coupon_id ?? '') }}</p>

                                    @endif
                                    <p>@lang('reservations.date'): {{ date('Y-m-d', strtotime($reservation->start)) }}
                                        / {{ convertToHijri($reservation->start) }}</p>
                                    <p>@lang('reservations.status')
                                        : {{ trans('reservations.' . $reservation->status) }}</p>

                                </div>
                            </div>
                        </div>
                    @empty
                        <h2 class="text-center w-100">لا يوجد نتائج مطابقة</h2>
                    @endforelse
                    <div class="col-md-10">
                        {{ $reservations->links() }}
                    </div>

                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="loader" style="display: none"></div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">

        $('#from-date').datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
        });
        $('#hijri-from').calendarsPicker({
            calendar: $.calendars.instance('islamic'),
            monthsToShow: [1, 1],
            showOtherMonths: true
        });

        function doAction(url, status) {
            swal({
                title: '{!! trans('admin.are_you_sure') !!}',
                type: "warning",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "{!! trans('admin.yes'); !!}",
                cancelButtonText: "{!! trans('admin.no'); !!}",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function () {


                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {_token: '{!! csrf_token() !!}', status: status}
                }).done(function (data) {
                    //alert(data);
                    swal({
                            title: "{!! trans('admin.done') !!}",
                            text: "تم التعديل بنجاح",
                            type: 'success'
                        },
                        function () {
                            location.reload();
                        }
                    );
                }).fail(function (e) {
                    console.log(e);
                    swal("{!! trans('admin.fail') !!}", e.responseJSON.message, "error")
                })
            });
        }
    </script>
@endsection
