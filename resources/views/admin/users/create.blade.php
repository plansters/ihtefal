<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('users.create'))

@section('content')

    <div class="container-fluid" onload="sampleFunction()">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('users.create')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.users.store') }}" id="createUserForm">
                            @csrf
                            <div class="row">

                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('users.role')</label>
                                    <select class="form-control @error('role') is-invalid @enderror" name="role" required autofocus>
                                        <option value="client">@lang('users.client')</option>
                                        <option value="vendor">@lang('users.vendor')</option>
                                        <option value="admin">@lang('users.admin')</option>
                                    </select>
                                    @error('role')
                                    <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('users.first_name')</label>

                                    <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" required>

                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('users.last_name')</label>

                                    <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" required>

                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label class="text-md-right">@lang('users.register_type')</label>

                                    <select class="form-control @error('register_type') is-invalid @enderror" name="register_type" id="register_type" required>
                                        <option value="phone">@lang('users.phone')</option>
                                        <option value="email">@lang('users.email')</option>
                                    </select>

                                    @error('register_type')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-3">
                                    <label class="text-md-right">@lang('users.email')</label>

                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="mailInput">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-3">
                                    <label class="text-md-right">@lang('users.password')</label>

                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required id="passwordInput">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div> 
                                <div class="form-group col-md-3">
                                    <label class="text-md-right">@lang('users.password_confirmation')</label>

                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" id="conPasswordInput" required>

                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.address')</label>

                                    <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" required>

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.phone')</label>

                                    <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phoner" required>

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror
                                    <span class="invalid-feedback" role="alert" id="phonerError" style="display: none">
                                        <strong>يجب أن يبدأ الرقم ب 05 متبوعاً ب 8 أرقام</strong>
                                    </span>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <h3><i class="fa fa-lock"></i> @lang('sidebar.roles')</h3>
                                </div>

                                @foreach($roles as $role)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="roles[]" class="custom-control-input" value="{{ $role->id }}" id="customCheck{{ $role->id }}">
                                                <label class="custom-control-label" for="customCheck{{ $role->id }}">{{ $role->name }}</label>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                                <div class="form-group col-md-12 mb-0">
                                    <a onclick="subForm();" class="btn btn-primary">
                                        @lang('admin.create')
                                    </a>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

function subForm(){ 
    var phoneNumberPattern = /(05)[0-9]{8}/;
    var rt=$("#register_type").val();
   
   var x= $('#phoner').val();
   var passReg= $('#passwordInput').val();
   var conPassReg= $('#conPasswordInput').val();

   passReg = fixNumbers(passReg);
   conPassReg = fixNumbers(conPassReg);
   x = fixNumbers(x);
   
   $('#phoner').val(x);
   $('#passwordInput').val(passReg);
   $('#conPasswordInput').val(conPassReg);
   
   var result= phoneNumberPattern.test(x);
   
            if((result) ||(rt =="email" && result=="")){
    $("#createUserForm").submit();
}
else{
            $('#phoner').addClass('is-invalid');
            $('#phonerError').show();
        }
}
</script>
@endsection

@section('styles')
<style type="text/css">
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    $( ".select2-single" ).select2({
        'height' : '30px'
    });
</script>
<script>
$("#mailInput").val('');
// $("#passwordInput").val('');
// $("#conPasswordInput").val('');
var
persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
fixNumbers = function (str)
{
      if(typeof str === 'string')
  {
    for(var i=0; i<10; i++)
    {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
//   alert(str);
  return str;
};

</script>
@endsection
