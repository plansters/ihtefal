<?php //dd($errors) ?>
@extends('layouts.app')

@section('title',trans('users.edit'))

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">@lang('users.edit')</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.users.update', $user->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="row">

                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('users.role')</label>
                                    <select class="form-control @error('role') is-invalid @enderror" name="role" required autofocus>
                                        <option value="client" @if($user->role == 'client') selected @endif>@lang('users.client')</option>
                                        <option value="vendor" @if($user->role == 'vendor') selected @endif>@lang('users.vendor')</option>
                                        <option value="admin" @if($user->role == 'admin') selected @endif>@lang('users.admin')</option>
                                    </select>
                                    @error('role')
                                    <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                    @enderror
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('users.first_name')</label>

                                    <input type="text" value="{{ $user->first_name }}" class="form-control @error('first_name') is-invalid @enderror" name="first_name" required>

                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-4">
                                    <label class="text-md-right">@lang('users.last_name')</label>

                                    <input type="text" value="{{ $user->last_name }}" class="form-control @error('last_name') is-invalid @enderror" name="last_name" required>

                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                            </div>
 
                            <div class="row">
                                <div class="form-group col-md-3" >
                                    <label class="text-md-right">@lang('users.register_type')</label>

                                    <select class="form-control @error('register_type') is-invalid @enderror" name="register_type" required>
                                        <option value="phone" @if($user->register_type == 'phone') selected @endif>@lang('users.phone')</option>
                                        <option value="email" @if($user->register_type == 'email') selected @endif>@lang('users.email')</option>
                                    </select>

                                    @error('register_type')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-3">
                                    <label class="text-md-right">@lang('users.email')</label>

                                    <input type="email" value="{{ $user->email }}" class="form-control @error('email') is-invalid @enderror" name="email" >

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-3">
                                    <label class="text-md-right">@lang('users.password')</label>

                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-3">
                                    <label class="text-md-right">@lang('users.password_confirmation')</label>

                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation">

                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.address')</label>

                                    <input type="text" value="{{ $user->address }}" class="form-control @error('address') is-invalid @enderror" name="address">

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-md-right">@lang('users.phone')</label>

                                    <input type="text" value="{{ $user->phone }}" class="form-control @error('phone') is-invalid @enderror" name="phone" required>

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                    @enderror

                                </div>
                            </div>
 

                            <div class="row">
                                <div class="col-md-12">
                                    <h3><i class="fa fa-lock"></i>نوع الحساب</h3>
                                </div>

                                @foreach($roles as $role)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="roles[]" class="custom-control-input" value="{{ $role->id }}" @if(in_array($role->id, $user_roles)) checked @endif id="customCheck{{ $role->id }}">
                                                <label class="custom-control-label" for="customCheck{{ $role->id }}">{{ $role->name }}</label>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                                <div class="form-group col-md-12 mb-0">
                                    <button type="submit" class="btn btn-primary">
                                        @lang('admin.edit')
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
<style type="text/css">
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
    }
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    $( ".select2-single" ).select2({
        'height' : '30px'
    });
</script>
@endsection
