@extends('layouts.app')

@section('title',trans('sidebar.users'))

@section('content')

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            @include('flash-message')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">@lang('sidebar.users')</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class=" table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr>
                        <th>@lang('users.id')</th>
                        <th>@lang('users.type')</th>
                        <th>@lang('users.email')</th>
                        <th>@lang('users.name')</th>
                        <th>@lang('users.address')</th>
                        <th>@lang('users.phone')</th>
                        @ifCanDo('update_users')
                        <th>@lang('admin.status')</th>
                        @endif
                        <th>@lang('admin.settings')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr class="odd gradeX">
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->role }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ @$user->first_name . ' ' . $user->last_name ?? '' }}</td>

                        <td>{{ @$user->address }}</td>
                        <td>{{ @$user->phone }}</td>

                        @ifCanDo('update_users')
                        <td>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox small">
                                    <input type="checkbox" class="custom-control-input" @if($user->status == 'active') checked @endif id="customCheck{{ $user->id }}" onchange="changeStatus({{ $user->id }})">
                                    <label class="custom-control-label" for="customCheck{{ $user->id }}"></label>
                                </div>
                            </div>
                        </td>
                        @endif
                        <td>
                            @ifCanDo('update_users')
                            <a href="{{ route('admin.users.edit',$user->id) }}" data-toggle="tooltip" title="@lang('admin.edit')"><i class="fa fa-edit"></i></a> &nbsp; &nbsp;
                            @endif
                            @ifCanDo('delete_users')
                            <a href="#" style="color: #dc3545" data-toggle="tooltip" title="@lang('admin.delete')" onclick="event.preventDefault();deleteItem('{{ $user->id }}','{{ route('admin.users.destroy',$user->id) }}')"><i class="fa fa-trash"></i></a>
                            @endif
                        </td> 
                    </tr>
                    @endforeach
 

                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
            <br>

            @ifCanDo('create_users')
            <div class="panel-footer">
                <a href="{{ route('admin.users.create') }}" class="btn btn-primary" ><i class="fa fa-plus"></i> @lang('admin.add_new')</a>
            </div>
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>

@endsection


@section('scripts')
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable({
            // "pagingType": "full_numbers",
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Arabic.json"
            }
        });

    });



    function deleteItem(id,url) {
        swal({
            title: '{!! trans('admin.are_you_sure') !!}',
            type: "warning",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "{!! trans('admin.yes'); !!}",
            cancelButtonText: "{!! trans('admin.no'); !!}",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
    }, function () {



            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {_token: '{!! csrf_token() !!}', _method : 'delete' }
            })
                .done(function() {


                    swal({title: "{!! trans('admin.done') !!}", text: "{!! trans('admin.deleted_successfully') !!}", type: "success"},
                        function(){
                            location.reload();
                        }
                    );


                })
                .fail(function(e) {

                    swal("{!! trans('admin.fail') !!}",e.responseJSON.message, "error")

                })


        });
    }

    function changeStatus(id) {
        var url = '{{ route("admin.users.status") }}';
        $.ajax({
            url: url,
            type: 'POST',
            data: {id: id, _token: '<?php echo csrf_token(); ?>' }
        }).done(function(data) { }).fail(function(e) {
            swal("<?php echo trans('admin.fail'); ?>",e.responseJSON.message, "error")
        })
    }


    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
function fillName() {
    var userQP = getUrlParameter('user');
    if(userQP){
        $('#cv').val(userQP).keyup()

    }
        }
        window.onload = fillName;


function transN(){
       var x= $('#cv').val();
       x = fixNumbers(x);
       $('#cv').val(x);
} 

    var
    persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
    arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
    fixNumbers = function (str)
    {
          if(typeof str === 'string')
      {
        for(var i=0; i<10; i++)
        {
          str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
        }
      }
    //   alert(str);
      return str;
    };
    
                </script>
@endsection

