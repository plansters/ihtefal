<div style="width: 17rem!important;" class="navbar-nav sidebar">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" style="position: fixed; overflow-y: scroll; max-height: 100%;"
        id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" 
        @if(auth::check()&&Auth::user()->role== "admin")
        {{-- href="{{ route('admin.dashboard') }}" --}}
        href='/admin/halls/'
        @endif
        >
            <div class="sidebar-brand-icon  ">
                <img src="{{ asset('img/logo-white.svg') }}" class="img-fluid" style="width: 100px" alt="Ihtefal">
            </div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="fas fa-fw fa-home"></i>
                <span>@lang('sidebar.home')</span></a>
        </li>
        -->
        <!-- Divider
        <hr class="sidebar-divider">
        -->
    <!-- Heading
    <div class="sidebar-heading">
        @lang('sidebar.users_management')
            </div>-->
        <li class="nav-item {{ checkCurrentItem('halls') ? "active" : "" }}">
            <a class="nav-link" href="{{ route('admin.halls.index') }}">
                <i class="fas fa-fw fa-hotel"></i>
                <span>@lang('sidebar.primary')</span>
            </a>
        </li>

        @if(auth::check()&&Auth::user()->role == "sales")
        <li class="active">
            <a class="nav-link" href="/admin/partners/coupons/{{auth::user()->partner_id}}">
                <i class="fas fa-fw fa-hotel"></i>
                <span>الكوبونات</span>
            </a>
        </li>
        @endif 


        @if(auth::check()&&Auth::user()->role!= "partner")
        @if(auth::check()&&Auth::user()->role!= "sales")
        <li class="nav-item {{ checkCurrentItem('myHalls') ? "active" : "" }}">
            <a class="nav-link" href="/admin/myHalls"> 
                <i class="fas fa-home"></i> 
                <span>قاعاتي</span>
            </a>
        </li>
@endif

        @ifCanDo('Show_Hall_Owner_Info')
        
        <li class="nav-item {{ checkCurrentItem('Show_Hall_Owner_Info') ? "active" : "" }}">
            <a class="nav-link" href="/admin/halls/list">
                <i class="fas fa-home"></i> 
                <span>القاعات</span>
            </a>
        </li>
        @endif
        @if(auth::check()&&Auth::user()->role!= "sales")
        <li class="nav-item {{ checkCurrentItem('favorite') ? "active" : "" }}">
            <a class="nav-link" href="{{ route('admin.halls.favorite') }}">
                <i class="fas fa-fw fa-heart"></i>
                <span>@lang('sidebar.favorite')</span>
            </a>
        </li>
@endif
        @ifCanDo('reservations')

            <li class="nav-item {{ checkCurrentItem('reservations') ? "active" : "" }}">
                <a class="nav-link" href="{{ route('admin.reservations.index') }}">
                    <i class="fas fa-fw fa-calendar-day"></i>
                    <span>@lang('sidebar.reservations')</span>
                </a>
            </li>
        @else
        @if(auth::check()&&Auth::user()->role!= "sales")
            <li class="nav-item {{ checkCurrentItem('reservations') ? "active" : "" }}">
                <a class="nav-link" href="{{ route('admin.user.reservations') }}">
                    <i class="fas fa-fw fa-calendar-day"></i>
                    <span>@lang('sidebar.my_reservations')</span>
                </a>
            </li>
        @endif   
        @endif
@endif
        @ifCanDo('calendar')
            <li class="nav-item {{ checkCurrentItem('calendar') ? "active" : "" }}">
                <a class="nav-link" href="{{ route('admin.calendar.index') }}">
                    <i class="fas fa-fw fa-calendar-day"></i>
                    <span>@lang('sidebar.calendar')</span>
                </a>
            </li>
        @endif
        <!-- Nav Item - Pages Collapse Menu -->
        <!-- Nav Item - Charts -->
        @ifCanDo('show_categories')
        <li class="nav-item  {{ checkCurrentItem('categories') ? "active" : "" }}">
            <a class="nav-link" href="{{ route('admin.categories.index') }}">
                <i class="fas fa-fw fa-tags"></i>
                <span>@lang('sidebar.categories')</span></a>
        </li>
    @endif

    <!-- Nav Item - Pages Collapse Menu -->
        <!-- Nav Item - Charts -->

        @ifCanDo('show_cities')
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.cities.index') }}">
                <i class="fas fa-fw fa-city"></i>
                <span>@lang('sidebar.cities')</span></a>
        </li>
    @endif

    <!-- Nav Item - Pages Collapse Menu -->
        <!-- Nav Item - Charts -->
        @ifCanDo('show_coupons')
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.coupons.index') }}">
                <i class="fas fa-fw fa-percent"></i>
                <span>@lang('sidebar.coupons')</span></a>
        </li>
        @endif



        @ifCanDo('notifications')
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.alerts.index') }}">
                <i class="fas fa-bell"></i>
                <span>@lang('sidebar.notifications')</span></a>
        </li>
        @endif


        @ifCanDo('show_users|show_roles')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#users_collabse"
               aria-expanded="true" aria-controls="users_collabse">
                <i class="fas fa-users"></i>
                <span>@lang('sidebar.users')</span>
            </a>
            <div id="users_collabse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @ifCanDo('show_users')
                    <a class="collapse-item" href="{{ route('admin.users.index') }}">@lang('sidebar.users')</a>
                    @endif
                    @ifCanDo('show_roles')
                    <a class="collapse-item" href="{{ route('admin.roles.index') }}">الأدوار</a>
                    @endif
                </div>
            </div>
        </li>
        @endif
        @ifCanDo('show_ads')
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.ads.index') }}">
                <i class="fas fa-bullhorn"></i>
                <span>@lang('sidebar.ads')</span>
            </a>
        </li>
        @endif
        @ifCanDo('show_partners')
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.partners.index') }}">
                    <i class="fas fa-fw fa-handshake"></i>
                    <span>@lang('sidebar.partners')</span>
                </a> 
            </li>
        @endif
        @ifCanDo('show_inquiries')
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.inquiries.index') }}">
                <i class="fas fa-fw fa-question-circle"></i>
                <span>@lang('sidebar.inquiries')</span>
            </a>
        </li>
    @endif{{-- end of is partner check --}}

    <!-- Divider
        <hr class="sidebar-divider">

        <-- Heading --
        <div class="sidebar-heading">
        @lang('sidebar.services')
            </div>-->

        <!-- Nav Item - Pages Collapse Menu -->
        <!-- Nav Item - Charts -->
        <!--li class="nav-item">
            <a class="nav-link" href="{{ route('admin.categories.index') }}">
                <i class="fas fa-fw fa-tags"></i>
                <span>@lang('sidebar.categories')</span></a>
        </li-->
        @guest
        <li class="nav-item  {{ checkCurrentItem('login') ? "active" : "" }}">
            <a class="nav-link" href="{{ route('login') }}">
                <i class="fas fa-sign-in-alt"></i>
                <span>@lang('sidebar.login')</span>
            </a>
        </li>
@endguest
{{-- start partner options --}}
@if(auth::check()&&Auth::user()->role== "partner")
<li class="nav-item {{ checkCurrentItem('partners') ? "" : "" }}">
    <a class="nav-link" href="/admin/partners/main/{{Auth::user()->id}}"> 
        <i class="fas fa-gem"></i>
        <span>منتجاتي</span>
    </a> 
</li> 


<li class="nav-item {{ checkCurrentItem('partners') ? "" : "" }}">
    <a class="nav-link" href="/admin/partners/getSalesMen/{{Auth::user()->id}}"> 
        <i class="fas fa-user"></i>
        <span>مسؤوولي المبيعات</span>
    </a> 
</li> 


<li class="nav-item {{ checkCurrentItem('branches') ? "" : "" }}">
    <a class="nav-link" href="/admin/partners/branches/{{Auth::user()->id}}"> 
        <i class="fas fa-fw fa-hotel"></i>
        <span>متجري</span>
        
    </a> 
</li>

<li class="nav-item {{ checkCurrentItem('branches') ? "" : "" }}">
    <a class="nav-link" href="/admin/partners/coupons/{{Auth::user()->id}}"> 
        <i class="fas fa-fw fa-percent"></i>
        <span>كوبونات الخصم</span>
        
    </a> 
</li>
@endif  {{-- end of is partner check --}}
{{-- end options --}}
        <li class="nav-item">
            <a class="nav-link" href="/user-guide">
                <i class="fas fa-sign-in-alt"></i>
                <span>دليل المستخدم</span>
            </a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>
</div>

<style type="text/css">
    .sidebar-brand {
        margin-top: 30px!important;
        margin-bottom: 80px!important;
    }

    .sidebar::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    .sidebar::-webkit-scrollbar
    {
        width: 6px;
        background-color: #F5F5F5;
        display: none;
    }

    .sidebar::-webkit-scrollbar-thumb
    {
        background-color: #4F534C;
        display: none;
    }
</style>
