@auth
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" style="visibility: hidden">
        <div class="input-group">
            <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">

        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
        <li class="nav-item dropdown no-arrow d-sm-none">
            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
            </a>
            <!-- Dropdown - Messages -->
            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>
        @ifCanDo('notifications')
        <!-- Nav Item - Alerts -->
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw alert_bell" style="font-size: 25px;"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter" id="notifications_count">{{ $alerts == 0 ? '' : $alerts}}</span>
            </a>
            <!-- Dropdown - Alerts -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                    @lang('admin.notifications')
                </h6>
                <div id="notification_bar">
                    @forelse($notifications as $notification)
                        <a class="dropdown-item d-flex align-items-center" style="display: none" href="{{ $notification->notification_link }}">
                            <div class="mr-3">
                                <div class="{{ $notification->type == "order" ? "icon-circle bg-warning" : "icon-circle bg-primary" }}">
                                    <i class="fas {{ $notification->type == "order" ? "fa-shopping-basket" : "fa-file-alt" }} text-white"></i>
                                </div>
                            </div>
                            <div>
                                <div class="small text-gray-500">{{ formatDateAsName($notification->created_at) }}</div>
                                <span class="font-weight-bold">{{ $notification->title }}</span>
                                @if($notification->is_new == "yes")
                                    <span class="badge badge-pill badge-success">@lang('admin.new')</span>
                                @endif
                            </div>
                        </a>
                    @empty
                    @endforelse
                </div>
            </div>
        </li>
        @endif
        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               @if(auth()->user()->role != "partner")
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->first_name . ' '. auth()->user()->last_name }}</span>
               @else
               <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->first_name}}</span>

               @endif
                <img class="img-profile rounded-circle" src="https://cdn3.iconfinder.com/data/icons/character/512/41-512.png">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                <a class="dropdown-item" href="{{ route('admin.profile.edit') }}" >
                    <i class="fa fa-user-cog fa-sm fa-fw mr-2 text-gray-400"></i>

                    تعديل بياناتي
                </a>
                <form action="{{ route('logout') }}" method="POST" id="logoutForm">
                    @csrf
                    <a class="dropdown-item" href="#" onclick="submitForm('logoutForm')" >
                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>

                        تسجيل الخروج
                    </a>
                </form>
            </div>
        </li>

    </ul>

</nav>

@else

@endauth
