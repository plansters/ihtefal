@extends('layouts.app')
 
@section('title','إنشاء حساب')

@section('content')
{{-- @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif --}}
    <div class="row">
        <img src="{{ asset('img/logo.svg') }}" style="margin-top: 45px; margin-right: auto; margin-left: auto; width: 175px;">
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('flash-message')
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card-header">@lang('login.signup')</div>
                <form method="POST" action="{{ route('register') }}" id="register-form" >
                    @csrf
                    <div class="card shadow p-3 mb-5 bg-white rounded">

                        <div class="card-body">

<div class="row">

    <div class="form-group col-md-6" style="">
        <label class="text-md-right" style=" float: right;">@lang('login.account_type')</label>
        <select style="padding-top: 0;" class="form-control @error('role') is-invalid @enderror" name="role" onchange="checkRoleType(this.value)" id="roler">
            <option value="client" @if(old('account_type') == 'client') selected  @endif >@lang('login.client')</option>
            <option value="vendor" @if(old('account_type') == 'vendor') selected  @endif >@lang('login.vendor')</option>
            <option value="partner" @if(old('account_type') == 'partner') selected  @endif >شريك احتفال</option>

        </select> 
        @error('password_confirmation')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
<script>
function checkRoleType(type){
if(type == "partner"){
$("#register_typer").hide();
// $("#emailr").hide();  
$("#last_namer").hide();
$("#register_typer").hide();
$("#password_confirmationr").hide(); 
$('#register-form').attr('action', '{{ route("admin.partners.store") }}');
$('#register-form').attr('enctype', 'multipart/form-data');
$('#firsNameLabel').text('الاسم');
$('#first_namer').attr('name', 'name');
$('#first_namer_class').attr('class', 'form-group col-md-12');
$('#passwordrClass').attr('class', 'form-group col-md-12');
$("#partnerAvatarContainer").append('<div class="row col-md-12"><div class="form-group col-md-6"><div class="form-group"><label for="city-name" data-required="yes">@lang("halls.city")</label><select id="city-name" name="city_id" class="form-control"></select></div></div> <div class="form-group col-md-6"><div class="form-group "><label for="neighborhood">@lang("halls.neighborhood")</label><input type="text" class="form-control" name="neighborhood" value="{{ old("neighborhood") }}" id="neighborhood" placeholder=""></div></div></div><div class="form-group form-group col-md-12"><label for="descritipn" data-required="yes">@lang("halls.description")</label><textarea class="form-control" name="description" id="description" rows="3">{{ old("description") }}</textarea></div><div class="form-group col-md-12"><label class="text-md-right">@lang("partners.avatar")</label><input type="file" accept="image/*" class="@error("image") is-invalid @enderror" name="image" style="width: 100%">@error("name")<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror</div><div class="form-group col-md-12"><label class="text-md-right">@lang("partners.partner_word")</label><textarea name="partner_word" class="form-control @error("partner_word") is-invalid @enderror" cols="10" rows="5"></textarea>@error("partner_word")<span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>@enderror</div>');
$("#partnerMapContainer").show();
$("#map").show();
// $("#myloc").attr("name","map_location");//loccy1
$("#loccy1").attr("name","map_location");
}
if(type == "vendor" || type == "client"){
// $("#register_typer").show();
// $("#emailr").show();  
$("#last_namer").show();
// $("#register_typer").show();
$("#password_confirmationr").show();
$('#firsNameLabel').text('الاسم الاول');
$('#register-form').attr('action', '{{ route("register") }}');
$('#first_namer').attr('name', 'first_name');
$('#first_namer_class').attr('class', 'form-group col-md-6');
$('#passwordrClass').attr('class', 'form-group col-md-6');
$("#partnerAvatarContainer").empty();
$("#partnerMapContainer").hide();
$("#map").hide();
$("#loccy1").removeAttr("name");

}

// get cities here
if(type){
$.ajaxSetup({
headers: {
'Accept':'application/json',
'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8"
}
});
jQuery.ajax({
url: "{{ url('/api/cities') }}",
method: 'get',
success: function(result){ 
console.log(result['data']);
$("#city-name").empty();

for (var i = 0; i < result['data'].length; i++) {
var obj = result['data'][i];
$("#city-name").append('<option value='+obj.id+'>'+obj.name+'</option>');
}
}
});
}
}
</script>
    </div>
</div>
                            <div class="row" style=" margin-top: 13px;">
                                <div class="form-group col-md-6" id="first_namer_class">
                        <label class="text-md-right" data-required="yes" id="firsNameLabel">@lang('login.first_name')</label>
                                    <input type="text" class="form-control @error('first_name') is-invalid @enderror"
                                           name="first_name" value="{{ old('first_name') }}" id="first_namer">
                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-6" id="last_namer">
                                    <label class="text-md-right" data-required="yes">@lang('login.last_name')</label>
                                    <input type="text" value="{{ old('last_name') }}"
                                           class="form-control @error('last_name') is-invalid @enderror"
                                           name="last_name" >
                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="text-md-right" data-required="yes">@lang('login.phone')</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                        value="{{ old('phone') }}" name="phone" id="phoner" placeholder="ex: 0509338888" required>
                                        <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1" style="direction: initial;">+966</span>
                                      </div>

                                    </div>
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback" role="alert" id="phonerError" style="display: none">
                                        <strong>يجب أن يبدأ الرقم ب 05 متبوعاً ب 8 أرقام</strong>
                                    </span>
                                </div>
                            </div>
                            <div class="row" id="emailr" hidden>
                                <div class="form-group col-md-12">
                                    <label class="text-md-right">@lang('login.email')</label>
                                    <input  type="email" class="form-control @error('email') is-invalid @enderror"
                                           name="email" value="{{ old('email') }}" id="emailr" placeholder="ihtefaalUser@gmail.com">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6" id="passwordrClass">
                                    <label class="text-md-right" data-required="yes">@lang('login.password')</label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                           name="password" id="register_password_input" id="passwordr">
            <input type="hidden"  class="form-control" id="loccy1" value="24.7104948,46.6797144">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert" id="register_password_alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-6" id="password_confirmationr">
                                    <label class="text-md-right" data-required="yes">@lang('login.password_confirmation')</label>
         <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                                           name="password_confirmation" id="password_confirmation" >
                                    @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="row" id="partnerAvatarContainer"></div>
                            <div class="row" id="partnerMapContainer">

                                    <input id="myloc" type="hidden" value="24.7104948,46.6797144" />
             
                            </div>
                            <div class="row" hidden>
                                <div class="form-group col-md-6" id="register_typer">
                                    <label class="text-md-right">@lang('login.register_type')</label>
                                    <select style="padding-top: 0;" class="form-control @error('register_type') is-invalid @enderror" name="register_type"  >
                                        <option value="phone" @if(old('register_type') == 'phone') selected  @endif >@lang('login.phone')</option>
                                        {{-- <option value="email" @if(old('register_type') == 'email') selected  @endif >@lang('login.email')</option> --}}
                                    </select>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                
                            </div>

                            <hr>
                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <button type="button" onclick="checkReg();" style="width: 100%" class="btn btn-primary btn-sm">@lang('admin.create')</button>
                                    

                                </div>
                            </div>
                        </div>
                    </div> 
                </form>
            </div>

            <script>
var
persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g],
fixNumbers = function (str)
{
      if(typeof str === 'string')
  {
    for(var i=0; i<10; i++)
    {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
//   alert(str);
  return str;
};

            </script>
<script>
    function checkReg(){
        var phoneNumberPattern = /(05)[0-9]{8}/;
       var x= $('#phoner').val();
       var passReg= $('#register_password_input').val();
       var conPassReg= $('#password_confirmation').val();
       passReg = fixNumbers(passReg);
       conPassReg = fixNumbers(conPassReg);

       x = fixNumbers(x);
       $('#phoner').val(x);
       $('#register_password_input').val(passReg);
       $('#password_confirmation').val(conPassReg);
    //    alert(x);
// 
// ٠٥٨٧١١٤٦١٠
 
// 
            var result= phoneNumberPattern.test(x);
                if(result){
                setAction('register');
                $('#register-form').submit();
            }
            else{
                $('#phoner').addClass('is-invalid');
                $('#phonerError').show();
            }
    }
</script>

            <div class="col-md-6">
                <div class="card-header">@lang('login.login')</div>
                <form method="POST" action="{{ route('login') }}" id="login-form">
                    @csrf
                    <div class="card shadow p-3 mb-5 bg-white rounded">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="text-md-right" data-required="yes">@lang('login.email_or_phone')</label>
                                    <input type="text" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" 
                                    name="identity" id="loginphoneinput">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror

                                    <span class="invalid-feedback" role="alert" id="loginphoneinputError" style="display: none">
                                        <strong> يجب أن يبدأ المعرف ب 05 متبوعاً ب 8 أرقام أو بصيغة إيميل صحيح</strong>
                                    </span>

                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="text-md-right" data-required="yes">@lang('login.password')</label>
                                    <input type="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           name="password" id="login_password_input">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert" id="login_password_alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <hr>
                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <button type="button" onclick="checkLogReg();" style="width: 100%" class="btn btn-primary btn-sm" id="loginsubmitButton">@lang('login.login')</button>
                                    <a href="#" onclick="$('#exampleModal').show();" data-toggle="modal" data-target="#exampleModal">نسيت كلمة المرور؟</a>
                                    <br>
                                    {{-- <a href="#" data-toggle="modal" data-target="#exampleModal1">تسجيل الدخول ك شريك</a>  --}}
   
                                </div>
                            </div>
<script>
function checkLogReg(){
    // 
    var phoneNumberPattern = /(05)[0-9]{8}/;
       var x= $('#loginphoneinput').val();
       var y= $('#login_password_input').val();
       if(!isEmail(x)){
       x = fixNumbers(x);
       y = fixNumbers(y);
       $('#loginphoneinput').val(x);
       $('#login_password_input').val(y);
            var result= phoneNumberPattern.test(x);
                if(result){
 setAction('login');
$('#login-form').submit();
            }
            else{
                $('#loginphoneinputError').addClass('is-invalid');
                $('#loginphoneinputError').show();
            }
       }
       else{
        setAction('login');
        $('#login-form').submit(); 
       }

}
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
</script>


                            <script>
                            //     $("#login_password_alert").keydown(function(e){
                            //     if(e.which === 13){
                            //         $("#loginsubmitButton").click();
                            //     }
                            // });
                        
                            var input = document.getElementById("loginphoneinput");
                            var input2 = document.getElementById("login_password_input");

// Execute a function when the user releases a key on the keyboard
input.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("loginsubmitButton").click();
  }
});

// Execute a function when the user releases a key on the keyboard
input2.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    document.getElementById("loginsubmitButton").click();
  }
});
                            </script>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="map" style="width: 100%; height: 400px;display: none"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog" role="document" >
      <div class="modal-content">

        <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">البريد او الرقم *</label>
                  <input required type="text" class="form-control" name="identity" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="البريد أو الرقم">
                </div>
            </form>
        </div>
        <div id="msgs">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">إنهاء</button>
          <button type="submit" class="btn btn-primary" onclick="fbModal()">كلمة سر جديدة </button>
        </div>
      </div>
    </div> 
  </div>

  {{-- start partner modal --}}
  <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true" style="display: none">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-body">
            <form>
                <div class="form-group">
                  <label for="exampleInputEmail2">البريد او الرقم *</label>
                  <input required type="text" class="form-control"  id="exampleInputEmail2"  placeholder="اسم المستخدم">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">كلمة السر *</label>
                  <input required type="password" class="form-control" id="exampleInputPassword2" placeholder="كلمة السر" >
                </div>
            </form>

        </div>
        <div id="msgs3">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">إنهاء</button>
          <button type="submit" class="btn btn-primary" onclick="loginPartnerModal()">تسجيل الدخول</button>
        </div>
      </div>
    </div> 
  </div>
  {{-- end here --}}
@section('styles')
    <style type="text/css">
        .card-header {
            background-color: #f8f9fc;
            border: none;
        }


        label {
            margin-bottom: 2px;
        }
    </style>
@endsection

@section('scripts')
    <script>
        function setAction(action_name) {
            window.localStorage.clear();
            window.localStorage.setItem('action_name', action_name);
        }

        $(document).ready(() => {

            let action_name = window.localStorage.getItem('action_name');
            if(action_name !== "login")
            {
                $('#login_password_alert').remove();
                $('#login_password_input').removeClass('is-invalid');
            }
            else if(action_name !== "register")
            {
                $('#register_password_alert').remove();
                $('#register_password_input').removeClass('is-invalid');
            }

        });

    </script>

      {{-- start login ajax --}}
<script>
function fbModal(){
    $user= $("#exampleInputEmail1").val();
    if(!$user){
   $("#msgs").append('<div class="alert alert-danger" role="alert"> المعرف مطلوب</div>');
    }
else{


    $.ajaxSetup({
                         headers: {
                             'Accept':'application/json',
                             'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                             'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                         } 
                     });
                      jQuery.ajax({
                          url: "api/auth/forgetPassword", 
                         method: 'post', 
                         data:{
                             "identity":$user,
                             "_token": "{{ csrf_token() }}",
                         },
                         success: function(result){
                            console.log(result); 
                            if(result['Code']=="100"){
                                alert("تم إرسال كلمة السر الجديدة في رسالة");
                            }
                                },
    error: function(XMLHttpRequest, textStatus, errorThrown) { 
        // alert("Status: " + textStatus); 
        // alert("Error: " + errorThrown); 
        if(errorThrown=="Unprocessable Entity"){
            alert("المعرف غير موجود");
        }
    } 

                      });
                    }
       }
       

</script>
  {{-- end login ajax --}}

  {{-- start partner login script --}}

  <script>
    function loginPartnerModal(){
        var partner_username= $("#exampleInputEmail2").val();
        var partner_password= $("#exampleInputPassword2").val();
        if(!partner_username || !partner_password){
       $("#msgs3").append('<div class="alert alert-danger" role="alert"> المعرف/كلمة السر مطلوبين</div>');
        }
    else{
    
    
        $.ajaxSetup({
                             headers: {
                                 'Accept':'application/json',
                                 'Content-Type':"application/x-www-form-urlencoded; charset=UTF-8",
                                 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                             } 
                         });
                          jQuery.ajax({
                              url: "api/auth/forgetPassword", 
                             method: 'post', 
                             data:{
                                 "identity":$user,
                                 "_token": "{{ csrf_token() }}",
                             },
                             success: function(result){
                                console.log(result); 
                                if(result['Code']=="100"){
                                    alert("تم إرسال كلمة السر الجديدة في رسالة");
                                }
                                    },
        error: function(XMLHttpRequest, textStatus, errorThrown) { 
            // alert("Status: " + textStatus); 
            // alert("Error: " + errorThrown); 
            if(errorThrown=="Unprocessable Entity"){
                alert("المعرف غير موجود");
            }
        } 
    
                          });
                        }
           }
           
    
    </script>


      {{-- end login script here --}}

      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoNx0lZSVmTUatcvCJsQB8_DOwmhjRup4&libraries=places&callback=initAutocomplete"
        async defer></script>
<script type="text/javascript">

    function initAutocomplete() {
        var markers = [];
        var input11 = document.getElementById("myloc").value;
        var coords = input11.split(",");
        var input23 = new google.maps.LatLng(coords[0], coords[1]);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: input23,
            zoom: 9,
            mapTypeId: 'roadmap'
        });

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(map, event.latLng);
        });

        function placeMarker(map, location) {
            deleteMarkers();
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            $("#myloc").val(location.lat() + ',' + location.lng());
            $("#loccy1").val(location.lat() + ',' + location.lng());
            //infowindow.open(map,marker);
            markers.push(marker);
            //console.log(markers);
        }

        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll(null);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        // Create the search box and link it to the UI element.
        var input = document.getElementById('myloc');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }


</script>


@endsection
