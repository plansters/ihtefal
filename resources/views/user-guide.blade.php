@extends('layouts.app')

@section('title','')

@section('content')

<!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width'>
      <title>Privacy Policy</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

      <style> 
:root {
  --primary-color: #333;
  --secondary-color: #444;
  --overlay-color: rgba(0, 0, 0, 0.7);
}

* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-family: cairo;
}

body {
  font-family: 'Catamaran', sans-serif;
  line-height: 1.6;
  color: #333;
  font-size: 1.1rem;
}

h1,
h2,
h3,
h4 {
  line-height: 1.3;
}

a {
  color: #444;
  text-decoration: none;
}

ul {
  list-style: none;
}

img {
  width: 100%;
}

.container {
  max-width: 1100px;
  margin: auto;
  overflow: hidden;
  padding: 0 2rem;
}

.navbar {
  font-size: 1.2rem;
  padding-top: 0.3rem;
  padding-bottom: 0.3rem;
}

.navbar .container {
  /* display: grid; */
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 2rem;
}

.navbar .logo {
  font-size: 2rem;
}

.navbar ul {
  justify-self: flex-end;
  display: flex;
  align-items: center;
  justify-content: center;
}

.navbar a {
  padding: 0 1rem;
}

.navbar a:hover {
  color: #555;
}

.section-a {
  margin: 2rem 0;
}

.section-a .container {
  /* display: grid; */
  grid-template-columns: 1fr 1fr;
  grid-gap: 3rem;
  align-items: center;
  justify-content: center;
}

.section-a h1 {
  font-size: 4rem;
  color: var(--primary-color);
}

.section-a p {
  margin: 1rem 0;
}

.section-b {
  position: relative;
  background: url('https://i.ibb.co/1RS1dqC/section-b.jpg') no-repeat bottom center/cover;
  height: 600px;
}

.section-b-inner {
  color: #fff;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
  margin: auto;
  max-width: 860px;
  padding: 5rem 0;
}

.section-b-inner h3 {
  font-size: 2rem;
}

.section-b-inner h2 {
  font-size: 5rem;
  margin-top: 1rem;
}

.section-b-inner p {
  margin-top: 1rem;
  font-size: 1.5rem;
}

.section-c .gallery {
  display: grid;
  grid-template-columns: repeat(5, 1fr);
}
.section-c .gallery a:first-child {
  /* grid-row-start: 1;
  grid-row-end: 3; */
  grid-row: 1/3;
  grid-column: 1/3;
}

.section-c .gallery a:nth-child(2) {
  grid-column-start: 3;
  grid-column-end: 5;
}

.section-c .gallery img,
.section-c .gallery a {
  width: 100%;
  height: 100%;
}

.section-footer {
  background: var(--primary-color);
  color: #fff;
  padding: 4rem 0;
}

.section-footer .container {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 1rem;
}

.section-footer h2 {
  font-size: 2rem;
  margin-bottom: 1rem;
}

.section-footer h3 {
  margin-bottom: 0.7rem;
}

.section-footer a {
  line-height: 1.9;
  color: #ccc;
}

.section-footer a > i {
  color: #555;
  margin-right: 0.5rem;
}

.email-form {
  width: 100%;
  display: inline-block;
  background-color: #555;
  position: relative;
  border-radius: 20px;
  line-height: 0;
  margin-top: 1rem;
}

/* // .form-control-wrap {
// 	position: relative;
// 	display: inline-block;
// 	width: 100%;
// } */

.email-form .form-control {
  display: inline-block;
  border: 0;
  outline: 0;
  font-size: 1rem;
  color: #ddd;
  background-color: transparent;
  font-family: inherit;
  margin: 0;
  padding: 0 3rem 0 1.5rem;
  width: 100%;
  height: 45px;
  border-radius: 20px;
}

.email-form .submit {
  display: inline-block;
  position: absolute;
  top: 0;
  right: 0;
  width: 45px;
  height: 45px;
  background-color: #eee;
  font-size: 1rem;
  text-align: center;
  margin: 0;
  padding: 0;
  outline: 0;
  border: 0;
  color: #333;
  cursor: pointer;
  border-radius: 0 20px 20px 0;
}

.btn {
  display: inline-block;
  background: var(--primary-color);
  color: #fff;
  padding: 0.8rem 1.5rem;
  border: none;
  cursor: pointer;
  font-size: 1.1rem;
  border-radius: 30px;
}

.btn:hover {
  background: var(--secondary-color);
}

.overlay {
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: var(--overlay-color);
}

@media (max-width: 700px) {
  .section-a .container {
    grid-template-columns: 1fr;
    text-align: center;
  }

  .section-a .container div:first-child {
    order: 2;
  }

  .section-a .container div:nth-child(2) {
    order: -1;
  }

  .section-a img {
    width: 80%;
    margin: auto;
  }

  .section-c .gallery {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
  }
  .section-c .gallery a:first-child {
    grid-row: 1/1;
    grid-column: 1/1;
  }

  .section-c .gallery a:nth-child(2) {
    grid-column: 2/4;
    grid-row: 2/2;
  }

  .section-c .gallery a:last-child {
    display: none;
  }

  .section-footer {
    padding: 2rem 0;
  }
  .section-footer .container {
    grid-template-columns: 1fr;
    text-align: center;
  }

  .section-footer div:nth-child(2),
  .section-footer div:nth-child(3) {
    display: none;
  }
}
.logoHeader{
    background-color: #50534c !important;
    height: 200px;
    background: url('/img/Logo-Splash.svg') 50% 50% no-repeat;
    background-size: 295px;
    }

       </style>
    </head>
<body>


  <!-- Showcase -->
  <section class="section-a" style="margin-top: 0px;">
    <div class="container" style="text-align: end;    padding: 0px;">
      <div>
        {{-- <img src="/img/logo.svg" alt="" style="height: 33%;width:8%;" /> --}}
      <div class="logoHeader"></div>
        <h1 style="text-align:center;margin-bottom: -23px;line-height: 2.3;">دليل الاستخدام</h1>
        
        <p style="text-align: justify;direction: rtl;padding:0px 50px 50px 50px;">
            تطبيق احتفال تطبيق مخصص لحجز قاعات الفنادق وقصور الافراح والاستراحات ،
            ويتميز بسهولة البحث عن القاعات المقصوده وإمكانية حجزها بسهوله .
            لحجز القاعه او قصر الافراح او الاستراحه المناسبه 
            أولا : الدخول من الصفحه الرئيسيه واختيار المدينه والتاريخ إما الهجري أو الميلادي
            ثم بحث وتظهر صفحة القاعات وفيها تختار نوع القاعه إما قاعات فنادق أو قصور أفراح أو إستراحات وبعد التحديد تظهر القاعات مرتبه تحت بعض ، ولتسهيل عملية البحث أضفنا عدة فلاتر 
            فتستطيع أن تجعل ترتيب القاعات حسب السعر أو حسب السعه أو حسب التقييم.
            وأيضاً يوجد محرك بحث يساعدك للوصول سريعاً للقاعه المطلوبه . 
            ويوجد أيضاًبحث في الخريطه ليسهل عليك معرفة مواقع كل القاعات واختيار ما يناسبك منها.
            إذا وجدت مايناسبك من القاعات تقوم بالضغط على عرض المزيد فتفتح صفحه فيها كل مواصفات ومعلومات القاعه وصورها وموقعها .
            فإذا كانت القاعه مناسبه لاحتياجك فيوجد اسفل الصفحه إضافه للمفضله (بحيث تستطيع مقارنتها مع غيرها من القاعات) ويوجد ايضاً ايقونه لحجز القاعه .
            فإذا أردت أن تحجز القاعه عليك بالضغط على حجز القاعه فإذا سبق لك إنشاء حساب فسيتم الحجز مباشره وإذا لم يكن لديك حساب فسيضهر لك نافذه من خلالها تستطيع إنشاء حساب .
            وبعد استكمال معلومات الحساب ثم الضفط على إنشاء يتم الحجز .
            وسيصلك رساله تفيدك بأنه تم الحجز ويكون حجزك غير مؤكد لمدة 24  ساعه لإعطائك فرصه لمعاينة القاعه ودفع العربون ، وبعد دفع العربون 
            يتم تأكيد الحجز عن طريق صاحب القاعه .
            وبعدها تصلك رسالك بتأكيد الحجز 
            وتتضمن الرساله كود الخصومات .
            أما إذا لم يتم دفع العربون خلال 24 ساعه فيلغى الحجز آلياً .
            شركاء الاحتفال
            حرصاً منّا في تطبيق احتفال في تقديم أفضل الخدمات للمحتفلين تعاقدنا مع مجموعه من الشركات لتقدم تخفيضات للمحتفلين .
            وتستطيع الاستفاده من التخفيضات إذا تم تأكيد حجزك عبر تطبيق احتفال فقط .
            طريقة الاستفاده من كود الخصم :
            بعد تأكيد الحجز من قِبل صاحب القاعه يصلك رساله تأكيد الحجز وفيها كود الخصومات 
            وهذا الكود يستخدم  عند جميع الشركاء بحيث تستطيع الاستفاده منه مره واحده فقط مع كل شريك .
            وصلاحية الكود من تاريخ تأكيد الحجز حتى يوم الاحتفال .
            ملاحظه : تستطيع الاستفاده من الكود مع الشركاء المسجلين في التطبيق وقت تأكيد الحجز . اما الشركاء الذين تم الاتفاق معهم وتسجيهلم في التطبيق بعد تأكيدك الحجز فلن تستطيع الاستفاده منهم ولن يكون كود الخصم الخاص بك مسجل لديهم
        </p>
      </div>
    </div>
  </section>

  <script>
$(function() {
        const $gallery = $('.gallery a').simpleLightbox();
      });

  </script>
  <div class="col-sm-12  text-center">
  <a href="{{ asset('IUG.pdf') }}" target="_blank"><button type="button" class="btn btn-outline-success " >تحميل</button></a>
  </div>
</body>
    </html>
    @endsection