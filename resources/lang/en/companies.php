<?php
return [
    'companies' => 'Companies',
    'id' => 'id',
    'email' => 'Email',
    'type' => 'Type',
    'name' => 'Name',
    'contact_name' => 'Contact Name',
    'country' => 'Country',
    'phone' => 'Phone',
    'create' => 'Create Company',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'avatar' => 'Avatar',
    'card' => 'Card',
    'platforms' => 'Platforms',
    'services' => 'Services',
    'edit' => 'Edit Company',
];