<?php
return [
    'normal_writer' => 'Normal Writer Roles',
    'active_writer' => 'Active Writer Roles',
    'committee_writer' => 'Committee Writer Roles',
    'producer' => 'Producer Role',
    'id' => 'ID',
    'name' => 'Name',
    'description' => 'Description',
    'type' => 'Type',
];
