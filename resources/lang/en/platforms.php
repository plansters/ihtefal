<?php
return [
    'create' => 'Create Platform',
    'id' => 'ID',
    'name' => 'Name',
    'price' => 'Price',
    'description' => 'Description',
    'edit' => 'Edit Platform',
    'active_icon' => 'Active Icon',
    'inactive_icon' => 'Inactive Icon',
];
