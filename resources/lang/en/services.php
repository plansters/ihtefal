<?php
return [
    'create' => 'Create Service',
    'id' => 'ID',
    'name' => 'Name',
    'price' => 'Price',
    'description' => 'Description',
    'edit' => 'Edit Service',
];
