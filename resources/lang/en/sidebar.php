<?php
return [
    'dashboard' => 'Dashboard',
    'subscriptions' => 'Subscriptions',
    'users_management' => 'Users Management',
    'users' => 'Users',
    'roles' => 'Roles',
    'services' => 'Services',
    'platforms' => 'Platforms',
    'writers' => 'Writers',
    'companies' => 'Companies',
    'companytypes' => 'Company Types',
    'EServices' => 'E-Services'
];
