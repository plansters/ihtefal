<?php
return [
    'companies' => 'Companies',
    'id' => 'ID',
    'email' => 'Email',
    'title' => 'Title',
    'name' => 'Name',
    'birth_date' => 'Birth Date',
    'country' => 'Country',
    'phone' => 'Phone',
    'create' => 'Create',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'avatar' => 'Avatar',
    'card' => 'Card',
    'platforms' => 'Platforms',
    'services' => 'Services',
    'edit' => 'Edit',
    'about_me' => 'About Me',
    'Mr' => 'Mr',
    'Mrs' => 'Mrs',
    'Ms' => 'Ms',
];