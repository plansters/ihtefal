<?php
return [
    'new_event' => 'New Event',
    'no_events' => 'No More Events',
    'waiting_events' => 'Waiting Events',
];