<?php
return [
    'id' => 'المعرف',
    'title' => 'العنوان',
    'description' => 'الوصف',
    'type' => 'النوع',
    'coupon_id' => 'رقم الكوبون',
    'start' => 'تاريخ البداية',
    'expired' => 'تاريخ الانتهاء',
    'create' => 'إنشاء حسم',
    'percentage' => 'نسبة مئوية',
    'value' => 'قيمة ثابتة',
    'edit' => 'تعديل حسم',
    'coupon_type' => 'النوع',
    'coupon_value' => 'قيمة الحسم',
    'start_date' => 'تاريخ البداية',
    'expired_date' => 'تاريخ الانتهاء',
];
