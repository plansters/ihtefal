<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'معلومات الدخول غير صحيحة أو الحساب غير مفعل',
    'inactive' => 'حسابك غير مفعل',

    'throttle' => 'قمت بمحاولة تسجيل الدخول مرات عديدة. الرجاء المحاولة خلال :ثانية seconds.',

];
