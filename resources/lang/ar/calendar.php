<?php
return [
    'update_event' => 'تعديل الموعد',
    'title' => 'العنوان',
    'description' => 'الوصف',
    'user_info' => 'معلومات المشترك',
    'hall' => 'اسم القاعة',
    'new_event' => 'حجز جديد',
    'reserved' => 'محجوزة',
    'user' => 'المستخدم',
    'waiting_events' => 'طلبات الانتظار',
    'no_events' => 'لا يوجد طلبات',
    'add_reservation' => 'إضافة حجز جديد',
];
