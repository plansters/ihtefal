<?php
return [
    'created_at' => 'تاريخ الطلب',
    'hall' => 'القاعة المحجوزة',
    'date' => 'تاريخ الحجز',
    'status' => 'حالة الحجز',
    'pending' => 'غير مؤكد',
    'approved' => 'مؤكد',
    'completed' => 'مكتمل',
    'cancelled' => 'ملغى'
];