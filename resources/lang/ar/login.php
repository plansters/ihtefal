<?php
return [
    'login' => 'تسجيل الدخول',
    'email' => 'البريد الالكتروني',
    'email_or_phone' => 'البريد او الرقم',
    'password' => 'كلمة السر',
    'rememberme' => 'تذكرني',
    'signup' => 'انشاء حساب',
    'first_name' => 'الاسم الاول',
    'last_name' => 'الاسم الاخير',
    'phone' => 'الهاتف',
    'client' => 'محتفل',
    'vendor' => 'صاحب قاعة',
    'password_confirmation' => 'تاكيد كلمة السر',
    'register_type' => 'التسجيل عن طريق',
    'account_type' => 'نوع الحساب ',
    ''
    /*'signup' => '',
    'signup' => '',
    'signup' => '',
    'signup' => '',*/
];
