<?php
return [
    'normal_writer' => 'صلاحيات كاتب عادي',
    'active_writer' => 'صلاحيات كاتب نشط',
    'committee_writer' => 'صلاحيات كاتب لجنة',
    'producer' => 'صلاحية شركة',
    'id' => 'المعرف',
    'create' => 'إنشاء صلاحية',
    'edit' => 'تعديل صلاحية',
    'name' => 'الاسم',
    'description' => 'الوصف',
    'type' => 'النوع',
];
