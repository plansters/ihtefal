-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2020 at 09:08 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ihtefal`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(11) NOT NULL,
  `image_path` varchar(250) NOT NULL DEFAULT '',
  `image_url` varchar(250) NOT NULL DEFAULT '',
  `title` text DEFAULT NULL,
  `type` enum('hall','ads') DEFAULT 'ads',
  `hall_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `image_path`, `image_url`, `title`, `type`, `hall_id`, `created_at`, `updated_at`) VALUES
(1, 'ads/M4IxzQbFXUf7VH40T5BtAZ5oz5flAKt6idiQLLm7.jpeg', 'http://127.0.0.1/ihte/public/uploads/ads/M4IxzQbFXUf7VH40T5BtAZ5oz5flAKt6idiQLLm7.jpeg', 'Ullam deserunt volup', 'hall', 2, '2020-03-15 19:47:11', '2020-03-15 20:04:09'),
(2, 'ads/vcyTrCYLy3PBviBnhJhP2uJxlRtzlLJpm2hRb6wg.jpeg', 'http://127.0.0.1/ihte/public/uploads/ads/vcyTrCYLy3PBviBnhJhP2uJxlRtzlLJpm2hRb6wg.jpeg', 'Itaque dicta ab erro', 'hall', 3, '2020-03-15 19:54:44', '2020-03-15 19:54:44');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'قاعة فنادق أو استراحة', '2020-02-23 12:30:47', '2020-02-23 12:34:45'),
(2, 'استراحة', '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(4, 'صالة أفراح', NULL, NULL),
(5, 'شركاء النجاح', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_extras`
--

CREATE TABLE `category_extras` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `field_name` varchar(100) NOT NULL,
  `field_type` enum('text','number','select') NOT NULL,
  `field_values` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_extras`
--

INSERT INTO `category_extras` (`id`, `category_id`, `field_name`, `field_type`, `field_values`, `created_at`, `updated_at`) VALUES
(1, 1, 'عدد مقاعد الرجال', 'number', NULL, '2020-02-23 12:30:47', '2020-02-23 12:30:47'),
(2, 1, 'عدد مقاعد النساء', 'number', NULL, '2020-02-23 12:30:47', '2020-02-23 12:31:05'),
(3, 1, 'مزايا اضافية', 'text', NULL, '2020-02-23 12:30:47', '2020-02-23 12:30:47'),
(4, 1, 'عدد الأقسام', 'number', NULL, '2020-02-23 12:30:47', '2020-02-23 12:31:05'),
(5, 1, 'عدد العمال', 'number', NULL, '2020-02-23 12:30:47', '2020-02-23 12:30:47'),
(6, 1, 'عدد العاملات', 'number', NULL, '2020-02-23 12:30:47', '2020-02-23 12:30:47'),
(7, 2, 'المساحة الاجمالية', 'number', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(8, 2, 'عدد المجالس', 'number', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(9, 2, 'عدد صالات الطعام', 'number', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(10, 2, 'عدد المطابخ', 'number', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(11, 2, 'عدد غرف النوم', 'number', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(12, 2, 'عدد المسابح', 'number', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(13, 2, 'وجود ألعاب أطفال', 'select', 'نعم,لا', '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(14, 2, 'موعد الدخول', 'text', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11'),
(15, 2, 'موعد الخروج', 'text', NULL, '2020-02-23 12:34:11', '2020-02-23 12:34:11');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'دمشق', '2020-02-23 12:35:40', '2020-02-23 12:35:40'),
(2, 'حلب', '2020-02-23 12:35:47', '2020-02-23 12:35:47'),
(3, 'حمص', '2020-02-23 12:35:53', '2020-02-23 12:35:53'),
(4, 'حماة', '2020-02-23 12:36:01', '2020-02-23 12:36:12');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `coupon_id` varchar(200) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `coupon_type` enum('percentage','value') NOT NULL DEFAULT 'value',
  `coupon_value` varchar(50) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `expired_date` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `halls`
--

CREATE TABLE `halls` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `color` varchar(20) DEFAULT NULL,
  `image_path` varchar(100) DEFAULT NULL,
  `city_id` varchar(100) NOT NULL,
  `neighborhood` varchar(100) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `min_capacity` int(100) DEFAULT NULL,
  `max_capacity` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `price_type` enum('per_person','per_hall') DEFAULT NULL,
  `min_chair_number` varchar(100) DEFAULT NULL,
  `map_location` varchar(100) DEFAULT NULL,
  `category_id` varchar(100) NOT NULL,
  `avg_rating` decimal(10,2) DEFAULT 0.00,
  `status` enum('published','pending') DEFAULT 'pending',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `halls`
--

INSERT INTO `halls` (`id`, `user_id`, `name`, `color`, `image_path`, `city_id`, `neighborhood`, `description`, `address`, `min_capacity`, `max_capacity`, `total_price`, `price_type`, `min_chair_number`, `map_location`, `category_id`, `avg_rating`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'صالة الوداد', '#13309F', 'hall/1h5X1Re4AWLK1VNCXsRwDkw98KnIqlRglQpsth61.jpeg', '1', 'شارع بغداد', 'في منتصف شارع بغداد مع سيارة ليموزين للأفراح', 'شارع بغداد دخلة القزازين', 100, 500, 10000, 'per_person', '1000', '-7.3344454, 112.7898796', '1', '5.00', 'published', '2020-02-23 12:42:03', '2020-03-03 09:32:00'),
(2, 1, 'صالة الآمال', '#6C930D', 'hall/vrQzkPsMIRTKPLn9txnxiTYYXIrqXmfbNYRZXEqb.jpeg', '1', 'مزة الشيخ سعد', 'صالة كبيرة جدا', 'الشيخ سعد جوار الفرن الآلي', 1500, 2000, 1000000, 'per_hall', '1700', '-7.3344454, 112.7898796', '2', '4.00', 'published', '2020-02-23 12:55:35', '2020-02-23 12:58:19'),
(3, 1, 'صالة الوداع', '#13309F', 'hall/Kb1A3B5WGS7x70GijbuVC1idcZqdUGsO4uweN9FM.jpeg', '1', 'شارع بغداد', 'في منتصف شارع بغداد مع سيارة ليموزين للأفراح', 'شارع بغداد دخلة القزازين', 1000, 5001, 50000, 'per_person', '1000', '-7.3344454, 112.7898796', '1', '3.00', 'published', '2020-02-23 12:42:03', '2020-03-08 12:45:20'),
(4, 1, 'صالة الوداد', '#13309F', 'hall/cwxxsfxUktICnA75g6WChiE5zwixicIAwTsn0lep.jpeg', '1', 'شارع بغداد', 'في منتصف شارع بغداد مع سيارة ليموزين للأفراح', 'شارع بغداد دخلة القزازين ', 100, 500, 500000, 'per_person', '1000', '-7.3344454, 112.7898796', '1', '5.00', 'published', '2020-02-23 12:42:03', '2020-03-03 09:51:22'),
(5, 1, 'Dorian Frederick', '#FF9E9D', 'hall/zI5UTMZbezuXxeoUMhXjov7RyajoQlz8xoHEDmla.png', '2', 'Anim autem quia temp', 'Sit voluptas veniam', 'Eum Nam debitis volu', 500, 1000, 783, 'per_hall', '9', '-7.3344454, 112.7898796', '2', '0.00', 'published', '2020-02-27 19:42:19', '2020-02-27 19:42:21'),
(6, 1, 'Vaughan Jimenez', NULL, 'hall/hEjEs8jhkqBs7gIEFxyLt4ScIB66bfH041psjWBr.png', '4', 'Aute laudantium cup', 'Ut saepe earum optio', 'Incidunt esse dolo', 500, 2000, 577, 'per_hall', '661', 'Inventore nulla vita', '5', '0.00', 'pending', '2020-03-01 09:25:15', '2020-03-01 09:25:15'),
(7, 1, 'Keaton Russo', NULL, 'hall/i40mxfl6Dq3NL81zHSmJVJZ1G4CneFhKfZuNaUe3.jpeg', '4', 'Nostrud minima ipsam', 'Fugiat voluptatem al', 'Aut voluptas omnis a', 346, 2044, 483, 'per_hall', '346', '-7.3344454, 112.7898796', '4', '0.00', 'pending', '2020-03-08 10:29:47', '2020-03-08 10:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `hall_extras`
--

CREATE TABLE `hall_extras` (
  `id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `category_extra_id` int(11) NOT NULL,
  `value` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hall_extras`
--

INSERT INTO `hall_extras` (`id`, `hall_id`, `category_extra_id`, `value`, `created_at`, `updated_at`) VALUES
(7, 2, 7, '1000', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(8, 2, 8, '2000', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(9, 2, 9, '5', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(10, 2, 10, '3', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(11, 2, 11, '2', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(12, 2, 12, '20', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(13, 2, 13, 'نعم', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(14, 2, 14, 'التاسعة صباحا', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(15, 2, 15, 'الواحدة ليلا', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(22, 5, 7, '1000', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(23, 5, 8, '200', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(24, 5, 9, '6', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(25, 5, 10, '4', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(26, 5, 11, '8', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(27, 5, 12, '3', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(28, 5, 13, 'نعم', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(29, 5, 14, 'التاسعة صباحا', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(30, 5, 15, 'الواحدة ليلا', '2020-02-27 19:42:19', '2020-02-27 19:42:19'),
(31, 1, 1, '700', '2020-03-03 09:32:00', '2020-03-03 09:32:00'),
(32, 1, 2, '800', '2020-03-03 09:32:00', '2020-03-03 09:32:00'),
(33, 1, 3, 'اضاءة حسب الطلب', '2020-03-03 09:32:00', '2020-03-03 09:32:00'),
(34, 1, 4, '17', '2020-03-03 09:32:00', '2020-03-03 09:32:00'),
(35, 1, 5, '22', '2020-03-03 09:32:00', '2020-03-03 09:32:00'),
(36, 1, 6, '27', '2020-03-03 09:32:00', '2020-03-03 09:32:00');

-- --------------------------------------------------------

--
-- Table structure for table `hall_media`
--

CREATE TABLE `hall_media` (
  `id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `media_path` varchar(200) NOT NULL,
  `media_url` varchar(200) NOT NULL,
  `media_type` enum('image','video') NOT NULL DEFAULT 'image',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hall_media`
--

INSERT INTO `hall_media` (`id`, `hall_id`, `media_path`, `media_url`, `media_type`, `created_at`, `updated_at`) VALUES
(1, 1, 'hall_album/2TkUdN6YtiNJrLZMJGFy650kYYXHk35Fq8Pb8FEA.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/2TkUdN6YtiNJrLZMJGFy650kYYXHk35Fq8Pb8FEA.jpeg', 'image', '2020-02-23 12:42:03', '2020-02-23 12:42:03'),
(2, 1, 'hall_album/2bH8jCGD3zqgHdRN7TOqTdpn5jrkrSl5OmHCPWpP.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/2bH8jCGD3zqgHdRN7TOqTdpn5jrkrSl5OmHCPWpP.jpeg', 'image', '2020-02-23 12:42:03', '2020-02-23 12:42:03'),
(3, 1, 'hall_album/MMtXI3xNFvabbeOqeuBnXBDlohpcv2GIHxhuFg7j.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/MMtXI3xNFvabbeOqeuBnXBDlohpcv2GIHxhuFg7j.jpeg', 'image', '2020-02-23 12:42:03', '2020-02-23 12:42:03'),
(4, 1, 'hall_album/IJv8QtVufJObY4Zf1ViJ1XrDDR9dSgpJUx2VUZZh.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/IJv8QtVufJObY4Zf1ViJ1XrDDR9dSgpJUx2VUZZh.jpeg', 'image', '2020-02-23 12:42:03', '2020-02-23 12:42:03'),
(5, 1, 'hall_album/jDjOuPhaoCOCYY8dNMlt0A8xf3TL9CkzexrTVac5.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/jDjOuPhaoCOCYY8dNMlt0A8xf3TL9CkzexrTVac5.jpeg', 'image', '2020-02-23 12:42:03', '2020-02-23 12:42:03'),
(6, 1, 'hall_album/dR7iQ0virwv02C7euCkAE6HUDGI5AbuO5ltbck9N.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/dR7iQ0virwv02C7euCkAE6HUDGI5AbuO5ltbck9N.jpeg', 'image', '2020-02-23 12:42:03', '2020-02-23 12:42:03'),
(7, 2, 'hall_album/aBsFmB1IAqCeDwG6Kcan7oi7FCMV2UcihQwxbFDw.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/aBsFmB1IAqCeDwG6Kcan7oi7FCMV2UcihQwxbFDw.jpeg', 'image', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(8, 2, 'hall_album/TkZVkghqnwrRaNwhVnBQI3mNBTmqHDaFX53zmPNZ.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/TkZVkghqnwrRaNwhVnBQI3mNBTmqHDaFX53zmPNZ.jpeg', 'image', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(9, 2, 'hall_album/C7bjLzoVHBXWeb248eDOoVtnRKp7jV1eeEfDR6Qd.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/C7bjLzoVHBXWeb248eDOoVtnRKp7jV1eeEfDR6Qd.jpeg', 'image', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(10, 2, 'hall_album/SPMCd68sXJBZ8MgMOiRTPU23T5tMNr5hIJnGa2re.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/SPMCd68sXJBZ8MgMOiRTPU23T5tMNr5hIJnGa2re.jpeg', 'image', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(11, 2, 'hall_album/MFiwDx8dxGFcMbN2tDmWbuH4dkgUhsXIAQgducRp.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/MFiwDx8dxGFcMbN2tDmWbuH4dkgUhsXIAQgducRp.jpeg', 'image', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(12, 2, 'hall_album/nvE7Ng0ldbOgIHpri1IHHAlNXTp9z93NhwfrmWxY.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/nvE7Ng0ldbOgIHpri1IHHAlNXTp9z93NhwfrmWxY.jpeg', 'image', '2020-02-23 12:55:35', '2020-02-23 12:55:35'),
(13, 5, 'hall_album/Srp1wmCvJkyMw0c0bq1gTVmqkGzh6KiBk5dZHyjY.png', 'http://127.0.0.1/ihtefal/public/uploads/hall_album/Srp1wmCvJkyMw0c0bq1gTVmqkGzh6KiBk5dZHyjY.png', 'image', '2020-02-27 19:42:21', '2020-02-27 19:42:21'),
(14, 5, 'hall_album/HHeWfr8lmjMGvIio0BJIZalvR4kHCG2OTuli8n8O.png', 'http://127.0.0.1/ihtefal/public/uploads/hall_album/HHeWfr8lmjMGvIio0BJIZalvR4kHCG2OTuli8n8O.png', 'image', '2020-02-27 19:42:21', '2020-02-27 19:42:21'),
(15, 5, 'hall_album/XaTkYX7mD7aTNMvYQNJkRfy5sSWUyNoTfkqdCW1N.png', 'http://127.0.0.1/ihtefal/public/uploads/hall_album/XaTkYX7mD7aTNMvYQNJkRfy5sSWUyNoTfkqdCW1N.png', 'image', '2020-02-27 19:42:21', '2020-02-27 19:42:21'),
(16, 7, 'hall_album/1jRh1KJaj2hcOe143yTFiHnhQexoQTeYsXVki4TY.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/1jRh1KJaj2hcOe143yTFiHnhQexoQTeYsXVki4TY.jpeg', 'image', '2020-03-08 10:29:47', '2020-03-08 10:29:47'),
(17, 7, 'hall_album/AAaBFmMdx0ognhJKGsIKe3F53i2sjw6GGF7TrIRi.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/AAaBFmMdx0ognhJKGsIKe3F53i2sjw6GGF7TrIRi.jpeg', 'image', '2020-03-08 10:29:47', '2020-03-08 10:29:47'),
(18, 7, 'hall_album/1IG0EkSrquoKDGvTZJnd0x3hz1VVf0Uqv25kpt3k.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/1IG0EkSrquoKDGvTZJnd0x3hz1VVf0Uqv25kpt3k.jpeg', 'image', '2020-03-08 10:29:47', '2020-03-08 10:29:47'),
(19, 7, 'hall_album/7VH7MpMXysuZmQTyEBKEDG1OAuFz6PBk6GSj4a5g.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/7VH7MpMXysuZmQTyEBKEDG1OAuFz6PBk6GSj4a5g.jpeg', 'image', '2020-03-08 10:29:47', '2020-03-08 10:29:47'),
(20, 7, 'hall_album/luA3Ghf763f2JjlgdoYXGWiFSxaFGT0M9n7MNQDI.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/luA3Ghf763f2JjlgdoYXGWiFSxaFGT0M9n7MNQDI.jpeg', 'image', '2020-03-08 10:29:47', '2020-03-08 10:29:47'),
(21, 7, 'hall_album/sn0w2wmwiexWHkL4iOK4cKHpV3dovI9id4sqpUsE.jpeg', 'http://127.0.0.1/ihte/public/uploads/hall_album/sn0w2wmwiexWHkL4iOK4cKHpV3dovI9id4sqpUsE.jpeg', 'image', '2020-03-08 10:29:47', '2020-03-08 10:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `hall_prices`
--

CREATE TABLE `hall_prices` (
  `id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `price_type` enum('by_days','by_islamic','by_gregorian') NOT NULL DEFAULT 'by_gregorian',
  `price` int(11) NOT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `days` text DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hall_prices`
--

INSERT INTO `hall_prices` (`id`, `hall_id`, `title`, `price_type`, `price`, `from_date`, `to_date`, `days`, `priority`, `created_at`, `updated_at`) VALUES
(1, 1, 'سعر رمضان المبارك', 'by_islamic', 1800000, '2020-03-23 00:00:00', '2020-05-22 00:00:00', NULL, 1, '2020-02-23 13:19:09', '2020-02-23 13:29:45'),
(2, 1, 'سعر عطل نهاية الاسبوع', 'by_days', 1200000, NULL, NULL, 'saturday,friday', 3, '2020-02-23 13:19:35', '2020-02-23 13:29:46'),
(3, 1, 'عطلة رأس السنة', 'by_gregorian', 2000000, '2020-12-20 00:00:00', '2021-01-05 00:00:00', NULL, 2, '2020-02-23 13:24:57', '2020-02-23 13:29:46');

-- --------------------------------------------------------

--
-- Table structure for table `hall_ratings`
--

CREATE TABLE `hall_ratings` (
  `id` int(11) NOT NULL,
  `hall_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rate` enum('1','2','3','4','5') DEFAULT NULL,
  `note` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hall_ratings`
--

INSERT INTO `hall_ratings` (`id`, `hall_id`, `user_id`, `rate`, `note`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '5', 'ألوان الصالة جميلة جدا وأجوائها ولا أحلى', '2020-02-23 12:43:56', '2020-02-23 12:44:37'),
(2, 2, 1, '4', 'أعجبتني جدا هذه القاعة بألوانها الزاهية وألعاب الأطفال الجميلة وقاعاتها ومسابحها الواسعة والجميلة بالإضافة إلى الأثاث الفاخر وكثافة المطابخ والعمال والعاملات وسرعة تلبية الطلبات والمفاجآت التي تقدمها للضيوف', '2020-02-23 12:58:19', '2020-02-23 12:59:27'),
(3, 3, 1, '3', NULL, '2020-03-08 12:44:27', '2020-03-08 12:45:20');

-- --------------------------------------------------------

--
-- Table structure for table `inquiries`
--

CREATE TABLE `inquiries` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `question` text NOT NULL,
  `answer` text DEFAULT NULL,
  `notified` enum('yes','no') NOT NULL DEFAULT 'no',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inquiries`
--

INSERT INTO `inquiries` (`id`, `user_id`, `admin_id`, `question`, `answer`, `notified`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'سؤال تجريبي', 'جواب تجريبي ايضا', 'no', '2020-03-15 17:27:24', '2020-03-15 19:00:05');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `avatar_path` varchar(100) DEFAULT NULL,
  `avatar_url` varchar(100) DEFAULT NULL,
  `partner_word` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `avatar_path`, `avatar_url`, `partner_word`, `created_at`, `updated_at`) VALUES
(1, 'Sierra Holloway', 'C:\\xampp\\htdocs\\ihte\\storage\\app/public/partners/image-1582466565.jpg', 'http://127.0.0.1/ihte/public/storage/partners/image-1582466565.jpg', 'Magni irure sed alia', '2020-02-23 14:02:45', '2020-02-23 14:02:45');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `created_at`, `updated_at`) VALUES
(1, 'login', 'login', NULL, NULL, NULL),
(2, 'عرض الجحوزات', 'reservations', 'امكانية فتح وعرض صفحة الحجوزات (للأدمن وصاحب القاعة)', NULL, NULL),
(3, 'عرض التصنيفات', 'show_categories', 'عرض معلومات تصانيف القاعات', NULL, NULL),
(4, 'إضافة تصنيف', 'create_categories', 'إضافة تصنيف جديد للقاعات', NULL, NULL),
(5, 'تعديل تصنيف', 'update_categories', 'تعديل بيانات تصنيف موجود مسبقا', NULL, NULL),
(6, 'حذف تصنيف', 'delete_categories', 'حذف التصنيف مع جميع البيانات المرتبطة به', NULL, NULL),
(7, 'عرض المدن', 'show_cities', 'عرض المدن الحالية', NULL, NULL),
(8, 'إضافة مدن', 'create_cities', 'إضافة مدن جديدة', NULL, NULL),
(9, 'تعديل مدن', 'update_cities', 'تعديل المدن الموجودة مسبقا', NULL, NULL),
(10, 'حذف مدن', 'delete_cities', 'حذف بيانات المدن الموجودة مسبقا', NULL, NULL),
(11, 'عرض الحسومات', 'show_coupons', 'عرض الحسومات الحالية', NULL, NULL),
(12, 'اضافة حسومات', 'create_coupons', 'اضافة حسمية جديدة', NULL, NULL),
(13, 'تعديل الحسومات', 'update_coupons', 'تعديل بيانات الحسومات الموجودة', NULL, NULL),
(14, 'حذف الحسومات', 'delete_coupons', 'حذف الحسومات الموجودة مسبقا', NULL, NULL),
(15, 'عرض القاعات', 'show_halls', 'عرض القاعات الموجودة', NULL, NULL),
(16, 'اضافة قاعات', 'create_halls', 'اضافة قاعة جديدة', NULL, NULL),
(17, 'تعديل قاعات', 'update_halls', 'تعديل القاعات الموجودة مسبقا', NULL, NULL),
(18, 'حذف قاعات', 'delete_halls', 'حذف قاعة موجودة مسبقا', NULL, NULL),
(19, 'تقييم قاعة', 'rate_halls', 'تقييم قاعة موجودة مسبقا واضافة تعليق عليها', NULL, NULL),
(20, 'عرض الشركاء', 'show_partners', 'عرض الشركاء الحاليين', NULL, NULL),
(21, 'اضافة شركاء', 'create_partners', 'اضافة شركاء جدد', NULL, NULL),
(22, 'تعديل الشركاء', 'update_partners', 'تعديل الشركاء الموجودين مسبقا', NULL, NULL),
(23, 'حذف الشركاء', 'delete_partners', 'حذف الشركاء الموجودين مسبقا', NULL, NULL),
(24, 'عرض الأدوار', 'show_roles', 'عرض الأدوار الحالية', NULL, NULL),
(25, 'اضافة دور', 'create_roles', 'اضافة دور جديد', NULL, NULL),
(26, 'تعديل الادوار', 'update_roles', 'تعديل الادوار الموجودة مسبقا', NULL, NULL),
(27, 'حذف دور', 'delete_roles', 'حذف ادوار موجودة مسبقا', NULL, NULL),
(28, 'عرض المستخدمين', 'show_users', 'عرض المستخدمين الحاليين', NULL, NULL),
(29, 'انشاء مستخدم', 'create_users', 'انشاء مستخدم جديد', NULL, NULL),
(30, 'تعديل مستخدم', 'update_users', 'تعديل مستخدمين موجودين مسبقا', NULL, NULL),
(31, 'حذف مستخدم', 'delete_users', 'حذف مستخدمين موجودين مسبقا', NULL, NULL),
(32, 'قاعات الانتظار', 'show_pending_halls', NULL, NULL, NULL),
(33, 'عرض الاستفسارات', 'show_inquiries', NULL, NULL, NULL),
(34, 'تعديل الاستفسارات', 'update_inquiries', NULL, NULL, NULL),
(35, 'حذف الاستفسارات', 'delete_inquiries', NULL, NULL, NULL),
(36, 'التقويم', 'calendar', NULL, NULL, NULL),
(37, 'إضافة إعلان', 'ads_create', NULL, NULL, NULL),
(38, 'تعديل اعلان', 'ads_update', NULL, NULL, NULL),
(39, 'حذف اعلان', 'ads_remover', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 6, NULL, NULL),
(3, 1, 7, NULL, NULL),
(4, 2, 1, NULL, NULL),
(5, 24, 1, NULL, NULL),
(6, 26, 1, NULL, NULL),
(7, 3, 1, NULL, NULL),
(10, 6, 1, NULL, NULL),
(11, 7, 1, NULL, NULL),
(13, 9, 1, NULL, NULL),
(15, 11, 1, NULL, NULL),
(18, 14, 1, NULL, NULL),
(19, 15, 1, NULL, NULL),
(20, 16, 1, NULL, NULL),
(21, 17, 1, NULL, NULL),
(22, 18, 1, NULL, NULL),
(23, 19, 1, NULL, NULL),
(24, 20, 1, NULL, NULL),
(26, 22, 1, NULL, NULL),
(28, 25, 1, NULL, NULL),
(29, 27, 1, NULL, NULL),
(30, 28, 1, NULL, NULL),
(33, 31, 1, NULL, NULL),
(34, 4, 1, NULL, NULL),
(35, 5, 1, NULL, NULL),
(36, 8, 1, NULL, NULL),
(37, 10, 1, NULL, NULL),
(38, 12, 1, NULL, NULL),
(39, 13, 1, NULL, NULL),
(40, 21, 1, NULL, NULL),
(41, 23, 1, NULL, NULL),
(42, 29, 1, NULL, NULL),
(43, 30, 1, NULL, NULL),
(44, 32, 1, NULL, NULL),
(45, 33, 1, NULL, NULL),
(46, 34, 1, NULL, NULL),
(47, 35, 1, NULL, NULL),
(48, 36, 1, NULL, NULL),
(49, 37, 1, NULL, NULL),
(50, 38, 1, NULL, NULL),
(51, 39, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `hall_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` enum('pending','approved','completed','cancelled') NOT NULL DEFAULT 'pending',
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `all_day` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `hall_id`, `user_id`, `coupon_id`, `title`, `description`, `status`, `start`, `end`, `all_day`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 'حجز زفاف السيد أنس إبراهيم', NULL, 'cancelled', '2020-02-14 02:00:00', '2020-03-14 02:00:00', 1, '2020-02-23 13:33:13', '2020-03-10 10:13:11'),
(2, 1, 2, NULL, 'حجز زفاف السيد أنس إبراهيم', NULL, 'completed', '2020-03-14 02:00:00', '2020-03-14 02:00:00', 1, '2020-02-23 13:33:13', '2020-03-15 12:42:52'),
(3, 1, 1, NULL, 'tittttle', 'gdg ds fsgg sgfd sfsg sfa', 'cancelled', '2020-12-20 00:00:00', NULL, 1, '2020-02-25 12:44:21', '2020-02-25 12:44:21'),
(4, 1, 2, NULL, 'tittttle', 'gdg ds fsgg sgfd sfsg sfa', 'pending', '2020-12-20 00:00:00', NULL, 1, '2020-02-25 12:44:53', '2020-02-25 12:44:53'),
(5, 1, 1, 1, NULL, NULL, 'approved', NULL, NULL, 1, '2020-02-29 10:19:17', '2020-02-29 10:48:09'),
(9, 1, 1, NULL, NULL, NULL, 'completed', '2020-03-10 00:00:00', NULL, 1, '2020-02-29 10:22:02', '2020-03-10 10:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `slug` varchar(191) NOT NULL,
  `description` text DEFAULT NULL,
  `type` enum('writer','company','admin') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `type`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'administrator', 'super admin', 'admin', '2019-12-04 10:24:15', '2020-02-12 11:19:05'),
(6, 'Vendor', 'test', 'يستطيع اضافة قاعات', NULL, '2020-02-12 09:55:08', '2020-02-12 11:03:01'),
(7, 'Client', 'client', 'يستطيع التصفح وحجز القاعات', NULL, '2020-02-12 11:03:41', '2020-02-12 11:03:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `role` enum('client','vendor','admin') NOT NULL DEFAULT 'client',
  `register_type` enum('phone','email') NOT NULL DEFAULT 'email',
  `identity` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `phone`, `email`, `address`, `role`, `register_type`, `identity`, `password`, `status`, `created_at`, `updated_at`) VALUES
(1, 'علي', 'Hajjow', '0938431726', 'admin@test.com', 'somewhere here or there', 'admin', 'email', 'admin@test.com', '$2y$10$J45abFB5X22vmWuiD3sx3uhPYehLM4wzzLxZGVJkbaKBByxipoV0S', 'active', NULL, NULL),
(2, 'محمود', 'Hajjow', '0938431726', 'alihajjow@gmail.com', 'Some where', 'client', 'phone', '0938431726', '$2y$10$.CdYpS9HpKU10x.FbOKPs.u8LTZ8k6beCNrMgQplvk3WiCVKVk3im', 'active', '2020-01-14 08:50:20', '2020-01-19 13:29:29'),
(3, 'Ali', 'Hajjow', '0938431728', 'alihajjow@gmail.co', 'Some where', 'client', 'email', 'alihajjow@gmail.co', '$2y$10$xSieZsHZFGnuxLTQsc94muuc6splc1pSHomxSe/YI1BXeNr9T5ArC', 'inactive', '2020-01-14 08:50:59', '2020-01-14 08:50:59'),
(4, 'xuj', 'dkk', '0946321328', 'admin@test.cm', 'skdj, sjdssfdsf', 'vendor', 'email', 'admin@test.cm', '$2y$10$K36D6X2fcADtTwVZhoGcPOWwoLxlib5l4CF9arYJrPMzaJszhhhbW', 'inactive', '2020-01-19 12:29:01', '2020-01-19 13:22:17'),
(5, 'Gavin', 'Snider', '16495642067', 'ryqiriw@mailinator.com', 'Placeat deserunt no', 'admin', 'email', 'ryqiriw@mailinator.com', '$2y$10$tf/dyO5ofukZLkFpt/dhYuLpuMnxjzA53YLmts72G4974Dz3jEjka', 'inactive', '2020-02-12 11:51:31', '2020-02-12 11:51:31'),
(12, 'Ali', 'Hajjow', '0938431726', 'alhajo@gmai.codm', 'Test ttesstttt', 'vendor', 'phone', '0938431726', '$2y$10$Txnxl.zpwSRH8t2Piqd/yuW315b0roZq8g8yPGi1kYncWOk.h/kTa', 'inactive', '2020-02-25 09:19:48', '2020-02-25 09:19:48'),
(13, 'Teagan', 'Dennis', '0999888777', 'popanuguna@mailinator.com', NULL, 'vendor', 'email', 'popanuguna@mailinator.com', '$2y$10$4K0HK5fPn1NFQBlnZkfaZ.PprqTCm4vSKUrZspP8YOIyXh/NZjBCK', 'inactive', '2020-02-27 20:28:39', '2020-02-27 20:28:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_extras`
--
ALTER TABLE `category_extras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupon_id` (`coupon_id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `halls`
--
ALTER TABLE `halls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hall_extras`
--
ALTER TABLE `hall_extras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hall_media`
--
ALTER TABLE `hall_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hall_prices`
--
ALTER TABLE `hall_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hall_ratings`
--
ALTER TABLE `hall_ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `category_extras`
--
ALTER TABLE `category_extras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `halls`
--
ALTER TABLE `halls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `hall_extras`
--
ALTER TABLE `hall_extras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `hall_media`
--
ALTER TABLE `hall_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `hall_prices`
--
ALTER TABLE `hall_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hall_ratings`
--
ALTER TABLE `hall_ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
